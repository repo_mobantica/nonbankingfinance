package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.GoldLoanScheme;

public interface GoldLoanSchemeRepository extends MongoRepository<GoldLoanScheme, String> {

	
}
