package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.Branch;

public interface BranchRepository extends MongoRepository<Branch, String> {

}
