package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.Bank;

public interface BankRepository extends MongoRepository<Bank, String> {

}
