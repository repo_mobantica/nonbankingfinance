package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, String>{

}
