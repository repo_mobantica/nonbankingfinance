package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.Department;

public interface DepartmentRepository extends MongoRepository<Department, String>  {

}
