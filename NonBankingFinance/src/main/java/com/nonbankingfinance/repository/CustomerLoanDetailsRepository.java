package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.CustomerLoanDetails;

public interface CustomerLoanDetailsRepository extends MongoRepository<CustomerLoanDetails, String>  {

}
