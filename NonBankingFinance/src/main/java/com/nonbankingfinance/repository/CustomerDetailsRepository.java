package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.CustomerDetails;

public interface CustomerDetailsRepository extends MongoRepository<CustomerDetails, String>{

}
