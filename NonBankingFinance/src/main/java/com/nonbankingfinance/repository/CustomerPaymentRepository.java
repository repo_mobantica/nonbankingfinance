package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.CustomerPayment;

public interface CustomerPaymentRepository extends MongoRepository<CustomerPayment, String> {

}
