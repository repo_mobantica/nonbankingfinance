package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.ChartOfAccount;

public interface ChartOfAccountRepository extends MongoRepository<ChartOfAccount, String>{

}
