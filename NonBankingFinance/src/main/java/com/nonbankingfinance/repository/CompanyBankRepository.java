package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.CompanyBank;

public interface CompanyBankRepository extends MongoRepository<CompanyBank, String> {

}
