package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.BankPaymentDetails;

public interface BankPaymentDetailsRepository extends MongoRepository<BankPaymentDetails, String>{

}
