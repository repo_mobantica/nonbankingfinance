package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.CustomerPaymentStatusHistory;

public interface CustomerPaymentStatusHistoryRepository extends MongoRepository<CustomerPaymentStatusHistory, String>{

}
