package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.nonbankingfinance.bean.Login;

public interface LoginRepository extends MongoRepository<Login, String>
{
		
}
