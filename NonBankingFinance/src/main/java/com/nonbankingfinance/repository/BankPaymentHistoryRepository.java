package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.BankPaymentHistory;

public interface BankPaymentHistoryRepository extends MongoRepository<BankPaymentHistory, String> {

}
