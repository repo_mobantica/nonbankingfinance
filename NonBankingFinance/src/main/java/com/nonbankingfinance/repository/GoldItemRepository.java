package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.GoldItem;

public interface GoldItemRepository extends MongoRepository<GoldItem, String> {

}
