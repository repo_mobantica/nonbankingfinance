package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.CustomerDocument;

public interface CustomerDocumentRepository extends MongoRepository<CustomerDocument, String> {

}
