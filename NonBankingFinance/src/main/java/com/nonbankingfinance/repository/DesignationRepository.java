package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.Designation;

public interface DesignationRepository extends MongoRepository<Designation, String> {

}
