package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.CustomerGoldItemDetails;

public interface CustomerGoldItemDetailsRepository extends MongoRepository<CustomerGoldItemDetails, String>  {

}
