package com.nonbankingfinance.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.BranchPaymentHistory;

public interface BranchPaymentHistoryRepository extends MongoRepository<BranchPaymentHistory, String> {

	List<BranchPaymentHistory> findByPaymentTypeAndDebiteOrcreditAndPacketOrbranchIdAndStatus(int paymentType, int debiteOrcredit, String packetOrbranchId, int status);
}
