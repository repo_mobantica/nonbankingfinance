package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.SubChartOfAccount;

public interface SubChartOfAccountRepository extends MongoRepository<SubChartOfAccount, String> {

}
