package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.EnquirySource;

public interface EnquirySourceRepository extends MongoRepository<EnquirySource, String> {

}
