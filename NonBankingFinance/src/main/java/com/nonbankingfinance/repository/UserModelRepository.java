package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.UserModel;

public interface UserModelRepository extends MongoRepository<UserModel, String>{

}
