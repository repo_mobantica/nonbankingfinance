package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.Occupation;

public interface OccupationRepository extends MongoRepository<Occupation, String> {

}
