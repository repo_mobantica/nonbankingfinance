package com.nonbankingfinance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nonbankingfinance.bean.BranchPayment;

public interface BranchPaymentRepository extends MongoRepository<BranchPayment, String> {

	BranchPayment findByBranchId(String branchId);
}
