package com.nonbankingfinance.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="usermodel")
public class UserModel {

	   @Id
	   private String userModelId;
	   private String employeeId;
	   private String userStatus;

		private Date creationDate;
		private Date updateDate;
		private String userId;
		
	   public UserModel() {}

	public UserModel(String userModelId, String employeeId, String userStatus, Date creationDate, Date updateDate,
			String userId) {
		super();
		this.userModelId = userModelId;
		this.employeeId = employeeId;
		this.userStatus = userStatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getUserModelId() {
		return userModelId;
	}

	public void setUserModelId(String userModelId) {
		this.userModelId = userModelId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	   
}
