package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "companybanks")
public class CompanyBank {

	@Id
	private long companyBankId;
	private String companyId;
	private String bankId;
	private String branchName;
	private String bankIfsc;
	private String bankAccountNumber;
	
	@Transient
	private String bankName;

	@Transient
	private String bankBranchName;

	@Transient
	private long creditedAmount;

	@Transient
	private long debitedAmount;
	
	public CompanyBank() {}

	public CompanyBank(long companyBankId, String companyId, String bankId, String branchName, String bankIfsc,
			String bankAccountNumber) {
		super();
		this.companyBankId = companyBankId;
		this.companyId = companyId;
		this.bankId = bankId;
		this.branchName = branchName;
		this.bankIfsc = bankIfsc;
		this.bankAccountNumber = bankAccountNumber;
	}

	public long getCompanyBankId() {
		return companyBankId;
	}

	public void setCompanyBankId(long companyBankId) {
		this.companyBankId = companyBankId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBankIfsc() {
		return bankIfsc;
	}

	public void setBankIfsc(String bankIfsc) {
		this.bankIfsc = bankIfsc;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranchName() {
		return bankBranchName;
	}

	public void setBankBranchName(String bankBranchName) {
		this.bankBranchName = bankBranchName;
	}

	public long getCreditedAmount() {
		return creditedAmount;
	}

	public void setCreditedAmount(long creditedAmount) {
		this.creditedAmount = creditedAmount;
	}

	public long getDebitedAmount() {
		return debitedAmount;
	}

	public void setDebitedAmount(long debitedAmount) {
		this.debitedAmount = debitedAmount;
	}

}
