package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "departments")
public class Department {


	  @Id
	    private String departmentId;
		private String departmentName;
		
		private String creationDate;
		private String updateDate;
	    private String userId;
	    
	    public Department() {}

		public Department(String departmentId, String departmentName, String creationDate, String updateDate,
				String userId) {
			super();
			this.departmentId = departmentId;
			this.departmentName = departmentName;
			this.creationDate = creationDate;
			this.updateDate = updateDate;
			this.userId = userId;
		}

		public String getDepartmentId() {
			return departmentId;
		}

		public void setDepartmentId(String departmentId) {
			this.departmentId = departmentId;
		}

		public String getDepartmentName() {
			return departmentName;
		}

		public void setDepartmentName(String departmentName) {
			this.departmentName = departmentName;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}
	    
}
