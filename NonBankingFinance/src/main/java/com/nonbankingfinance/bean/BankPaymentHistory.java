package com.nonbankingfinance.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bankpaymenthistories")
public class BankPaymentHistory {

	@Id
	private String bankpaymentHistoryId;
	private long companyBankId;
	private String customerId;
	private String packetNumber;
	private long totalAmount;
	private String paymentpourpose;
	private String paymentStatus;
	
	private Date creationDate;
	private String userId;
	private String branchId; 
	
	@Transient
	private String customerName;
	@Transient
	private String date;
	
	public BankPaymentHistory() {}

	public BankPaymentHistory(String bankpaymentHistoryId, long companyBankId, String customerId, String packetNumber, long totalAmount,
			String paymentpourpose, String paymentStatus, Date creationDate, String userId, String branchId) {
		super();
		this.bankpaymentHistoryId = bankpaymentHistoryId;
		this.companyBankId = companyBankId;
		this.customerId = customerId;
		this.packetNumber=packetNumber;
		this.totalAmount = totalAmount;
		this.paymentpourpose = paymentpourpose;
		this.paymentStatus = paymentStatus;
		this.creationDate = creationDate;
		this.userId = userId;
		this.branchId = branchId;
	}

	public String getBankpaymentHistoryId() {
		return bankpaymentHistoryId;
	}

	public void setBankpaymentHistoryId(String bankpaymentHistoryId) {
		this.bankpaymentHistoryId = bankpaymentHistoryId;
	}

	public long getCompanyBankId() {
		return companyBankId;
	}

	public void setCompanyBankId(long companyBankId) {
		this.companyBankId = companyBankId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getPacketNumber() {
		return packetNumber;
	}

	public void setPacketNumber(String packetNumber) {
		this.packetNumber = packetNumber;
	}

	public long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getPaymentpourpose() {
		return paymentpourpose;
	}

	public void setPaymentpourpose(String paymentpourpose) {
		this.paymentpourpose = paymentpourpose;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
