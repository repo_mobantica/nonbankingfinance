package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "banks")
public class Bank {

	@Id
	private String bankId;
	private String bankName;
	private String bankBranchName;
	private String bankPhoneno;
	private String bankifscCode;
	private String bankAddress;
	private String pinCode;
	
	private String creationDate;
	private String updateDate;
	private String userId;
	
	public Bank() {}

	public Bank(String bankId, String bankName, String bankBranchName, String bankPhoneno, String bankifscCode,
			String bankAddress,
			String pinCode, String creationDate, String updateDate, String userId) {
		super();
		this.bankId = bankId;
		this.bankName = bankName;
		this.bankBranchName = bankBranchName;
		this.bankPhoneno = bankPhoneno;
		this.bankifscCode = bankifscCode;
		this.bankAddress = bankAddress;
		this.pinCode = pinCode;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranchName() {
		return bankBranchName;
	}

	public void setBankBranchName(String bankBranchName) {
		this.bankBranchName = bankBranchName;
	}

	public String getBankPhoneno() {
		return bankPhoneno;
	}

	public void setBankPhoneno(String bankPhoneno) {
		this.bankPhoneno = bankPhoneno;
	}

	public String getBankifscCode() {
		return bankifscCode;
	}

	public void setBankifscCode(String bankifscCode) {
		this.bankifscCode = bankifscCode;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
