package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "branches")
public class Branch {

    @Id
    private String branchId;
	private String branchName;
	private String companyId;
	private String branchAddress;
	
	private String pinCode;
	private String phoneNumber;
	
	private String creationDate;
	private String updateDate;
    private String userId;


    public Branch() {}

    
    
	public Branch(String branchId, String branchName, String companyId, String branchAddress, String pinCode, String phoneNumber,
			String creationDate, String updateDate, String userId) {
		super();
		this.branchId = branchId;
		this.branchName = branchName;
		this.companyId = companyId;
		this.branchAddress = branchAddress;
		this.pinCode = pinCode;
		this.phoneNumber = phoneNumber;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}



	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
