package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customergolditemdetails")
public class CustomerGoldItemDetails {

	@Id
	private long customergolditemdetailsId;
	private String packetNumber;
	private String customerId;
	private String goldItemName;
	private double itemGrossWeight;
	private double itemQty;
	private double itemDWeight;
	private double itemWeight;
	private double purityPer;
	private double purWeight;
	private double itemMarketValue;
	
	public CustomerGoldItemDetails() {}

	public CustomerGoldItemDetails(long customergolditemdetailsId, String packetNumber, String customerId,
			String goldItemName, double itemGrossWeight, double itemQty, double itemDWeight, double itemWeight,
			double purityPer, double purWeight, double itemMarketValue) {
		super();
		this.customergolditemdetailsId = customergolditemdetailsId;
		this.packetNumber = packetNumber;
		this.customerId = customerId;
		this.goldItemName = goldItemName;
		this.itemGrossWeight = itemGrossWeight;
		this.itemQty = itemQty;
		this.itemDWeight = itemDWeight;
		this.itemWeight = itemWeight;
		this.purityPer = purityPer;
		this.purWeight = purWeight;
		this.itemMarketValue = itemMarketValue;
	}

	public long getCustomergolditemdetailsId() {
		return customergolditemdetailsId;
	}

	public void setCustomergolditemdetailsId(long customergolditemdetailsId) {
		this.customergolditemdetailsId = customergolditemdetailsId;
	}

	public String getPacketNumber() {
		return packetNumber;
	}

	public void setPacketNumber(String packetNumber) {
		this.packetNumber = packetNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getGoldItemName() {
		return goldItemName;
	}

	public void setGoldItemName(String goldItemName) {
		this.goldItemName = goldItemName;
	}

	public double getItemGrossWeight() {
		return itemGrossWeight;
	}

	public void setItemGrossWeight(double itemGrossWeight) {
		this.itemGrossWeight = itemGrossWeight;
	}

	public double getItemQty() {
		return itemQty;
	}

	public void setItemQty(double itemQty) {
		this.itemQty = itemQty;
	}

	public double getItemDWeight() {
		return itemDWeight;
	}

	public void setItemDWeight(double itemDWeight) {
		this.itemDWeight = itemDWeight;
	}

	public double getItemWeight() {
		return itemWeight;
	}

	public void setItemWeight(double itemWeight) {
		this.itemWeight = itemWeight;
	}

	public double getPurityPer() {
		return purityPer;
	}

	public void setPurityPer(double purityPer) {
		this.purityPer = purityPer;
	}

	public double getPurWeight() {
		return purWeight;
	}

	public void setPurWeight(double purWeight) {
		this.purWeight = purWeight;
	}

	public double getItemMarketValue() {
		return itemMarketValue;
	}

	public void setItemMarketValue(double itemMarketValue) {
		this.itemMarketValue = itemMarketValue;
	}



}
