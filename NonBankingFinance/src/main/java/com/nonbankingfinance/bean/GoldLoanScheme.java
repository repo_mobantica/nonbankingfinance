package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "goldloanschemes")
public class GoldLoanScheme {

    @Id
    private String goldloanschemeId;
	private String goldloanschemeName;
	private double interestRate;
	private double minimumLoanAmount;
	private double maximumLoanAmount;
	private double lateFine;
	private double maximumTenure;
	
	private double interestRate_3_6_month;
	private double interestRate_6_9_month;
	
	private String creationDate;
	private String updateDate;
    private String userId;
    
    public GoldLoanScheme() {}

	public GoldLoanScheme(String goldloanschemeId, String goldloanschemeName, double interestRate,
			double minimumLoanAmount, double maximumLoanAmount, double lateFine, double maximumTenure,
			double interestRate_3_6_month, double interestRate_6_9_month, String creationDate, String updateDate,
			String userId) {
		super();
		this.goldloanschemeId = goldloanschemeId;
		this.goldloanschemeName = goldloanschemeName;
		this.interestRate = interestRate;
		this.minimumLoanAmount = minimumLoanAmount;
		this.maximumLoanAmount = maximumLoanAmount;
		this.lateFine = lateFine;
		this.maximumTenure = maximumTenure;
		this.interestRate_3_6_month = interestRate_3_6_month;
		this.interestRate_6_9_month = interestRate_6_9_month;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getGoldloanschemeId() {
		return goldloanschemeId;
	}

	public void setGoldloanschemeId(String goldloanschemeId) {
		this.goldloanschemeId = goldloanschemeId;
	}

	public String getGoldloanschemeName() {
		return goldloanschemeName;
	}

	public void setGoldloanschemeName(String goldloanschemeName) {
		this.goldloanschemeName = goldloanschemeName;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getMinimumLoanAmount() {
		return minimumLoanAmount;
	}

	public void setMinimumLoanAmount(double minimumLoanAmount) {
		this.minimumLoanAmount = minimumLoanAmount;
	}

	public double getMaximumLoanAmount() {
		return maximumLoanAmount;
	}

	public void setMaximumLoanAmount(double maximumLoanAmount) {
		this.maximumLoanAmount = maximumLoanAmount;
	}

	public double getLateFine() {
		return lateFine;
	}

	public void setLateFine(double lateFine) {
		this.lateFine = lateFine;
	}

	public double getMaximumTenure() {
		return maximumTenure;
	}

	public void setMaximumTenure(double maximumTenure) {
		this.maximumTenure = maximumTenure;
	}

	public double getInterestRate_3_6_month() {
		return interestRate_3_6_month;
	}

	public void setInterestRate_3_6_month(double interestRate_3_6_month) {
		this.interestRate_3_6_month = interestRate_3_6_month;
	}

	public double getInterestRate_6_9_month() {
		return interestRate_6_9_month;
	}

	public void setInterestRate_6_9_month(double interestRate_6_9_month) {
		this.interestRate_6_9_month = interestRate_6_9_month;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
