package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "subchartofaccount")
public class SubChartOfAccount {

	@Id
	private String subChartOfAccountId;
	private int subChartOfAccountNumber;
	private String subChartOfAccountName;
	private String chartOfAccountId;
	private int chartOfAccountNumber;
    private String creationDate;
    private String updateDate;
    private String userId;
    
    @Transient
    private String status;
    
    public SubChartOfAccount() {}

	public SubChartOfAccount(String subChartOfAccountId, int subChartOfAccountNumber, String subChartOfAccountName,
			String chartOfAccountId, int chartOfAccountNumber, String creationDate, String updateDate, String userId) {
		super();
		this.subChartOfAccountId = subChartOfAccountId;
		this.subChartOfAccountNumber = subChartOfAccountNumber;
		this.subChartOfAccountName = subChartOfAccountName;
		this.chartOfAccountId = chartOfAccountId;
		this.chartOfAccountNumber = chartOfAccountNumber;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getSubChartOfAccountId() {
		return subChartOfAccountId;
	}

	public void setSubChartOfAccountId(String subChartOfAccountId) {
		this.subChartOfAccountId = subChartOfAccountId;
	}

	public int getSubChartOfAccountNumber() {
		return subChartOfAccountNumber;
	}

	public void setSubChartOfAccountNumber(int subChartOfAccountNumber) {
		this.subChartOfAccountNumber = subChartOfAccountNumber;
	}

	public String getSubChartOfAccountName() {
		return subChartOfAccountName;
	}

	public void setSubChartOfAccountName(String subChartOfAccountName) {
		this.subChartOfAccountName = subChartOfAccountName;
	}

	public String getChartOfAccountId() {
		return chartOfAccountId;
	}

	public void setChartOfAccountId(String chartOfAccountId) {
		this.chartOfAccountId = chartOfAccountId;
	}

	public int getChartOfAccountNumber() {
		return chartOfAccountNumber;
	}

	public void setChartOfAccountNumber(int chartOfAccountNumber) {
		this.chartOfAccountNumber = chartOfAccountNumber;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
