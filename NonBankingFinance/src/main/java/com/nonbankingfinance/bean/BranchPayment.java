package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class BranchPayment {

    @Id
    private String branchId;
	private long amount;
	
	public BranchPayment() {}

	public BranchPayment(String branchId, long amount) {
		super();
		this.branchId = branchId;
		this.amount = amount;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	
}
