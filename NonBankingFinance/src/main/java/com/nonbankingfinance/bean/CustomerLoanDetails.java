package com.nonbankingfinance.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customerloandetails")
public class CustomerLoanDetails {

	@Id
	private String packetNumber;
	private String customerId;
	
	private long loanAmount;
	private String goldloanschemeId;
	private double interestRate;
	private int processingFee;
	
	private String status;
	private String paymentStatus;

	private Date creationDate;
	private Date updateDate;
	private String userId;
	private String branchId;
	

	@Transient
	private String customerName;

	@Transient
	private String loanDate;

	@Transient
	private long totalPaidPrincipalAmount;
	
	@Transient
	private long totalPaidInterestAmount;
	
	@Transient
	private String loanClearDate;
	
	public CustomerLoanDetails()
	{}

	public String getPacketNumber() {
		return packetNumber;
	}

	public void setPacketNumber(String packetNumber) {
		this.packetNumber = packetNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public long getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(long loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getGoldloanschemeId() {
		return goldloanschemeId;
	}

	public void setGoldloanschemeId(String goldloanschemeId) {
		this.goldloanschemeId = goldloanschemeId;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public int getProcessingFee() {
		return processingFee;
	}

	public void setProcessingFee(int processingFee) {
		this.processingFee = processingFee;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getLoanDate() {
		return loanDate;
	}

	public void setLoanDate(String loanDate) {
		this.loanDate = loanDate;
	}

	public long getTotalPaidPrincipalAmount() {
		return totalPaidPrincipalAmount;
	}

	public void setTotalPaidPrincipalAmount(long totalPaidPrincipalAmount) {
		this.totalPaidPrincipalAmount = totalPaidPrincipalAmount;
	}

	public long getTotalPaidInterestAmount() {
		return totalPaidInterestAmount;
	}

	public void setTotalPaidInterestAmount(long totalPaidInterestAmount) {
		this.totalPaidInterestAmount = totalPaidInterestAmount;
	}

	public String getLoanClearDate() {
		return loanClearDate;
	}

	public void setLoanClearDate(String loanClearDate) {
		this.loanClearDate = loanClearDate;
	}

}
