package com.nonbankingfinance.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customerpayments")
public class CustomerPayment {

	@Id
	private String customerPaymentId;
	private String packetNumber;
	private String customerId;
	private long principalAmount;
	private double interestAmount;
	private long paymentDue;
	private long noticeCharge;
	private String bankName;
	private String branchName; 
	private String paymentMode;
	private String refNumber;
	private String narration;
	
	private Date creationDate;
	private String userId;
	private String branchId; 
	private String paymentStatus;
	
	@Transient
	private String paidDate;

	@Transient
	private String customerName;
	@Transient
	private long totalAmount;
	
	public CustomerPayment() {}

	public CustomerPayment(String customerPaymentId, String packetNumber, String customerId, long principalAmount,
			double interestAmount, long paymentDue, String bankName, String branchName, String paymentMode,
			String refNumber, String narration, Date creationDate, String userId, String branchId,
			String paymentStatus) {
		super();
		this.customerPaymentId = customerPaymentId;
		this.packetNumber = packetNumber;
		this.customerId = customerId;
		this.principalAmount = principalAmount;
		this.interestAmount = interestAmount;
		this.paymentDue = paymentDue;
		this.bankName = bankName;
		this.branchName = branchName;
		this.paymentMode = paymentMode;
		this.refNumber = refNumber;
		this.narration = narration;
		this.creationDate = creationDate;
		this.userId = userId;
		this.branchId = branchId;
		this.paymentStatus = paymentStatus;
	}

	public String getCustomerPaymentId() {
		return customerPaymentId;
	}

	public void setCustomerPaymentId(String customerPaymentId) {
		this.customerPaymentId = customerPaymentId;
	}

	public String getPacketNumber() {
		return packetNumber;
	}

	public void setPacketNumber(String packetNumber) {
		this.packetNumber = packetNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public long getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(long principalAmount) {
		this.principalAmount = principalAmount;
	}

	public double getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(double interestAmount) {
		this.interestAmount = interestAmount;
	}

	public long getPaymentDue() {
		return paymentDue;
	}

	public void setPaymentDue(long paymentDue) {
		this.paymentDue = paymentDue;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public long getNoticeCharge() {
		return noticeCharge;
	}

	public void setNoticeCharge(long noticeCharge) {
		this.noticeCharge = noticeCharge;
	}

}
