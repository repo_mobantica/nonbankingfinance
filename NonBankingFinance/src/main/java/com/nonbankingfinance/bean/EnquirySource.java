package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "enquiries")
public class EnquirySource {

	  @Id
	    private String enquirySourceId;
		private String enquirySourceName;
		
		private String creationDate;
		private String updateDate;
	    private String userId;
	    
	    @Transient
	    private String status;
	    
	    public EnquirySource() {}

		public EnquirySource(String enquirySourceId, String enquirySourceName, String creationDate, String updateDate, String userId) {
			super();
			this.enquirySourceId = enquirySourceId;
			this.enquirySourceName = enquirySourceName;
			this.creationDate = creationDate;
			this.updateDate = updateDate;
			this.userId = userId;
		}

		public String getEnquirySourceId() {
			return enquirySourceId;
		}

		public void setEnquirySourceId(String enquirySourceId) {
			this.enquirySourceId = enquirySourceId;
		}

		public String getEnquirySourceName() {
			return enquirySourceName;
		}

		public void setEnquirySourceName(String enquirySourceName) {
			this.enquirySourceName = enquirySourceName;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
	    
}
