package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "companies")
public class Company {

    @Id
    private String companyId;
	private String companyName;
	private String companyType;
	private String companyPanno;
	private String companyRegistrationno;
	private String companyTanno;
	private String companyPfno;
	private String companyGstno;
	private String companyAddress;
	
	private String pinCode;
	private String phoneNumber;
	private String creationDate;
	private String updateDate;
    private String userId;

    public Company() {}

	public Company(String companyId, String companyName, String companyType, String companyPanno,
			String companyRegistrationno, String companyTanno, String companyPfno, String companyGstno,
			String companyAddress,
			String pinCode, String phoneNumber, String creationDate, String updateDate, String userId) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyType = companyType;
		this.companyPanno = companyPanno;
		this.companyRegistrationno = companyRegistrationno;
		this.companyTanno = companyTanno;
		this.companyPfno = companyPfno;
		this.companyGstno = companyGstno;
		this.companyAddress = companyAddress;
		this.pinCode = pinCode;
		this.phoneNumber = phoneNumber;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getCompanyPanno() {
		return companyPanno;
	}

	public void setCompanyPanno(String companyPanno) {
		this.companyPanno = companyPanno;
	}

	public String getCompanyRegistrationno() {
		return companyRegistrationno;
	}

	public void setCompanyRegistrationno(String companyRegistrationno) {
		this.companyRegistrationno = companyRegistrationno;
	}

	public String getCompanyTanno() {
		return companyTanno;
	}

	public void setCompanyTanno(String companyTanno) {
		this.companyTanno = companyTanno;
	}

	public String getCompanyPfno() {
		return companyPfno;
	}

	public void setCompanyPfno(String companyPfno) {
		this.companyPfno = companyPfno;
	}

	public String getCompanyGstno() {
		return companyGstno;
	}

	public void setCompanyGstno(String companyGstno) {
		this.companyGstno = companyGstno;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
