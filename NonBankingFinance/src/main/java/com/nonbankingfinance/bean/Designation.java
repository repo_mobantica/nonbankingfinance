package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "designations")
//@CompoundIndexes({@CompoundIndex(name="designationIndex", unique= true, def="{'designationName':1}")})
public class Designation 
{
	@Id
	private String designationId;
	private String designationName;
	private String departmentId;
    private String creationDate;
    private String updateDate;
    private String userId;
    
    @Transient
    private String status;
    
    public Designation() 
    {
    }

	public Designation(String designationId, String designationName, String departmentId, String creationDate, String updateDate,
			String userId) {
		super();
		this.designationId = designationId;
		this.designationName = designationName;
		this.departmentId = departmentId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getDesignationId() {
		return designationId;
	}

	public void setDesignationId(String designationId) {
		this.designationId = designationId;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
  
}
