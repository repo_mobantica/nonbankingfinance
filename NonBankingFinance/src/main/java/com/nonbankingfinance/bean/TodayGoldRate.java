package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "todaygoldrate")
public class TodayGoldRate {

	@Id
	private String goldRateId;
	private int goldRateBy99_1;
	private int goldRate;
	
	private String creationDate;
	private String updateDate;
    private String userId;
    
    public TodayGoldRate() {}

	public TodayGoldRate(String goldRateId, int goldRateBy99_1, int goldRate, String creationDate, String updateDate,
			String userId) {
		super();
		this.goldRateId = goldRateId;
		this.goldRateBy99_1 = goldRateBy99_1;
		this.goldRate = goldRate;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getGoldRateId() {
		return goldRateId;
	}

	public void setGoldRateId(String goldRateId) {
		this.goldRateId = goldRateId;
	}

	public int getGoldRateBy99_1() {
		return goldRateBy99_1;
	}

	public void setGoldRateBy99_1(int goldRateBy99_1) {
		this.goldRateBy99_1 = goldRateBy99_1;
	}

	public int getGoldRate() {
		return goldRate;
	}

	public void setGoldRate(int goldRate) {
		this.goldRate = goldRate;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
