package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bankpaymentdetails")
public class BankPaymentDetails {

	@Id
	private String bankpaymentdetailsId;
	private String companyBankId;
	private long totalAmount;

	public BankPaymentDetails() {}

	public BankPaymentDetails(String bankpaymentdetailsId, String companyBankId, long totalAmount) {
		super();
		this.bankpaymentdetailsId = bankpaymentdetailsId;
		this.companyBankId = companyBankId;
		this.totalAmount = totalAmount;
	}

	public String getBankpaymentdetailsId() {
		return bankpaymentdetailsId;
	}

	public void setBankpaymentdetailsId(String bankpaymentdetailsId) {
		this.bankpaymentdetailsId = bankpaymentdetailsId;
	}

	public String getCompanyBankId() {
		return companyBankId;
	}

	public void setCompanyBankId(String companyBankId) {
		this.companyBankId = companyBankId;
	}

	public long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}

}
