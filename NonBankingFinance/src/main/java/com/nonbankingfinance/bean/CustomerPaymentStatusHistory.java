package com.nonbankingfinance.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "customerpaymentstatushistories")
public class CustomerPaymentStatusHistory {

	@Id
	private String historyId;
	private String customerPaymentId;
	private String packetNumber;
	private String customerId;
	private String companyBankId;
	private String paymentStatus;
	private String clearanceDate;
	private long totalAmount;
	
	private Date creationDate;
	private String userId;
	private String branchId; 
	
	public CustomerPaymentStatusHistory() {}

	public CustomerPaymentStatusHistory(String historyId, String customerPaymentId, String packetNumber,
			String customerId, String companyBankId, String paymentStatus, String clearanceDate, long totalAmount,
			Date creationDate, String userId, String branchId) {
		super();
		this.historyId = historyId;
		this.customerPaymentId = customerPaymentId;
		this.packetNumber = packetNumber;
		this.customerId = customerId;
		this.companyBankId = companyBankId;
		this.paymentStatus = paymentStatus;
		this.clearanceDate = clearanceDate;
		this.totalAmount = totalAmount;
		this.creationDate = creationDate;
		this.userId = userId;
		this.branchId = branchId;
	}

	public String getHistoryId() {
		return historyId;
	}

	public void setHistoryId(String historyId) {
		this.historyId = historyId;
	}

	public String getCustomerPaymentId() {
		return customerPaymentId;
	}

	public void setCustomerPaymentId(String customerPaymentId) {
		this.customerPaymentId = customerPaymentId;
	}

	public String getPacketNumber() {
		return packetNumber;
	}

	public void setPacketNumber(String packetNumber) {
		this.packetNumber = packetNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCompanyBankId() {
		return companyBankId;
	}

	public void setCompanyBankId(String companyBankId) {
		this.companyBankId = companyBankId;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getClearanceDate() {
		return clearanceDate;
	}

	public void setClearanceDate(String clearanceDate) {
		this.clearanceDate = clearanceDate;
	}

	public long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}


}
