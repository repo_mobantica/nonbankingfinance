package com.nonbankingfinance.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customerdetails")
public class CustomerDetails {

	@Id
	private String customerId;
	private String customerName;
	private String mobileNumber;
	private String customerPhoneno;
	private String emailId;
	private String customerPancardno;
	private String customerAadharcardno;
	private String occupationName;

	private String customerAddress;
	private String pinCode;

	private String customerBankName;
	private String bankBranchName;
	private String banckIFSC;
	private String bankAccountNumber;

	private Date creationDate;
	private Date updateDate;
	private String userId;
	private String branchId;


	@Transient
	private String packetNumber;

	//private String loanDate;
	/*
	@Transient
	private long totalPaidPrincipalAmount;

	@Transient
	private long totalPaidInterestAmount;

	@Transient
	private String loanClearDate;
	 */
	public CustomerDetails() {}

	public CustomerDetails(String customerId, String customerName, String mobileNumber, String customerPhoneno,
			String emailId, String customerPancardno, String customerAadharcardno, String occupationName,
			String customerAddress, String pinCode, String customerBankName, String bankBranchName, String banckIFSC,
			String bankAccountNumber, Date creationDate, Date updateDate, String userId, String branchId) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.mobileNumber = mobileNumber;
		this.customerPhoneno = customerPhoneno;
		this.emailId = emailId;
		this.customerPancardno = customerPancardno;
		this.customerAadharcardno = customerAadharcardno;
		this.occupationName = occupationName;
		this.customerAddress = customerAddress;
		this.pinCode = pinCode;
		this.customerBankName = customerBankName;
		this.bankBranchName = bankBranchName;
		this.banckIFSC = banckIFSC;
		this.bankAccountNumber = bankAccountNumber;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
		this.branchId = branchId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCustomerPhoneno() {
		return customerPhoneno;
	}

	public void setCustomerPhoneno(String customerPhoneno) {
		this.customerPhoneno = customerPhoneno;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCustomerPancardno() {
		return customerPancardno;
	}

	public void setCustomerPancardno(String customerPancardno) {
		this.customerPancardno = customerPancardno;
	}

	public String getCustomerAadharcardno() {
		return customerAadharcardno;
	}

	public void setCustomerAadharcardno(String customerAadharcardno) {
		this.customerAadharcardno = customerAadharcardno;
	}

	public String getOccupationName() {
		return occupationName;
	}

	public void setOccupationName(String occupationName) {
		this.occupationName = occupationName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getCustomerBankName() {
		return customerBankName;
	}

	public void setCustomerBankName(String customerBankName) {
		this.customerBankName = customerBankName;
	}

	public String getBankBranchName() {
		return bankBranchName;
	}

	public void setBankBranchName(String bankBranchName) {
		this.bankBranchName = bankBranchName;
	}

	public String getBanckIFSC() {
		return banckIFSC;
	}

	public void setBanckIFSC(String banckIFSC) {
		this.banckIFSC = banckIFSC;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getPacketNumber() {
		return packetNumber;
	}

	public void setPacketNumber(String packetNumber) {
		this.packetNumber = packetNumber;
	}


}
