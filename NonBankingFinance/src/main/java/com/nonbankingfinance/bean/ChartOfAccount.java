package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "chartofaccounts")
public class ChartOfAccount {


    @Id
    private String chartOfAccountId;
    private String chartOfAccountName;
    private int chartOfAccountNumber;
    private String creationDate;
    private String updateDate;
    private String userId;
    
    @Transient
    private String status;
    
    public ChartOfAccount() {}

	public ChartOfAccount(String chartOfAccountId, String chartOfAccountName, int chartOfAccountNumber,
			String creationDate, String updateDate, String userId) {
		super();
		this.chartOfAccountId = chartOfAccountId;
		this.chartOfAccountName = chartOfAccountName;
		this.chartOfAccountNumber = chartOfAccountNumber;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getChartOfAccountId() {
		return chartOfAccountId;
	}

	public void setChartOfAccountId(String chartOfAccountId) {
		this.chartOfAccountId = chartOfAccountId;
	}

	public String getChartOfAccountName() {
		return chartOfAccountName;
	}

	public void setChartOfAccountName(String chartOfAccountName) {
		this.chartOfAccountName = chartOfAccountName;
	}

	public int getChartOfAccountNumber() {
		return chartOfAccountNumber;
	}

	public void setChartOfAccountNumber(int chartOfAccountNumber) {
		this.chartOfAccountNumber = chartOfAccountNumber;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
