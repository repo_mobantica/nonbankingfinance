package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customerdocuments")
public class CustomerDocument {

	@Id
	private String customerDocumentId;
	private String customerId;
	private String documentName;
    private String documentNumber;
    private String extension;

    private String creationDate;
	private String updateDate;
	private String userId;
	
	public CustomerDocument() {}
	
	public CustomerDocument(String customerDocumentId, String customerId, String documentName, String documentNumber,
			String extension, String creationDate, String updateDate, String userId) {
		super();
		this.customerDocumentId = customerDocumentId;
		this.customerId = customerId;
		this.documentName = documentName;
		this.documentNumber = documentNumber;
		this.extension = extension;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getCustomerDocumentId() {
		return customerDocumentId;
	}
	public void setCustomerDocumentId(String customerDocumentId) {
		this.customerDocumentId = customerDocumentId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}
