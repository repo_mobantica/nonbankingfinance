package com.nonbankingfinance.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "golditems")
public class GoldItem {


    @Id
    private String goldItemId;
    private String goldItemName;
    private String creationDate;
    private String updateDate;
    private String userId;
    
    public GoldItem() {}

	public GoldItem(String goldItemId, String goldItemName, String creationDate, String updateDate, String userId) {
		super();
		this.goldItemId = goldItemId;
		this.goldItemName = goldItemName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userId = userId;
	}

	public String getGoldItemId() {
		return goldItemId;
	}

	public void setGoldItemId(String goldItemId) {
		this.goldItemId = goldItemId;
	}

	public String getGoldItemName() {
		return goldItemName;
	}

	public void setGoldItemName(String goldItemName) {
		this.goldItemName = goldItemName;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
    
}
