package com.nonbankingfinance.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class BranchPaymentHistory {


    @Id
    private String id;
    private String branchId;
	private Double amount;
	private int paymentType;  //1. customer, 2. branch to branch

	private int debiteOrcredit;  // 0. wait 1. debited  // 2. Credited
	private String packetOrbranchId;

	private Date createDate;
    private String userId;
	private int status;        //1. clear , 0. cancel
	
	@Transient
	private String userName;
	@Transient
	private String customerOrBranchName;
	@Transient
	private String dateInString;
	
	public BranchPaymentHistory() {}

	public BranchPaymentHistory(String id, String branchId, Double amount, int paymentType, int debiteOrcredit,
			String packetOrbranchId, Date createDate, String userId, int status) {
		super();
		this.id = id;
		this.branchId = branchId;
		this.amount = amount;
		this.paymentType = paymentType;
		this.debiteOrcredit = debiteOrcredit;
		this.packetOrbranchId = packetOrbranchId;
		this.createDate = createDate;
		this.userId = userId;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public int getDebiteOrcredit() {
		return debiteOrcredit;
	}

	public void setDebiteOrcredit(int debiteOrcredit) {
		this.debiteOrcredit = debiteOrcredit;
	}

	public String getPacketOrbranchId() {
		return packetOrbranchId;
	}

	public void setPacketOrbranchId(String packetOrbranchId) {
		this.packetOrbranchId = packetOrbranchId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCustomerOrBranchName() {
		return customerOrBranchName;
	}

	public void setCustomerOrBranchName(String customerOrBranchName) {
		this.customerOrBranchName = customerOrBranchName;
	}

	public String getDateInString() {
		return dateInString;
	}

	public void setDateInString(String dateInString) {
		this.dateInString = dateInString;
	}

}
