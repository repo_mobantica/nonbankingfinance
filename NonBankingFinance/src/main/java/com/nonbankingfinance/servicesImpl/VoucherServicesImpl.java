package com.nonbankingfinance.servicesImpl;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.nonbankingfinance.bean.Branch;
import com.nonbankingfinance.bean.BranchPaymentHistory;
import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.repository.BranchPaymentHistoryRepository;
import com.nonbankingfinance.repository.BranchPaymentRepository;
import com.nonbankingfinance.repository.BranchRepository;
import com.nonbankingfinance.services.VoucherServices;

@Repository
public class VoucherServicesImpl implements VoucherServices{

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	BranchPaymentRepository branchPaymentRepository;
	@Autowired
	BranchPaymentHistoryRepository branchPaymentHistoryRepository;
	@Autowired
	BranchRepository branchRepository;


	public List GetVoucherList(int paymentType, int debiteOrcredit, String branchId, int status) {
		// TODO Auto-generated method stub

		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		List<BranchPaymentHistory> branchPaymentHistoryList=branchPaymentHistoryRepository.findByPaymentTypeAndDebiteOrcreditAndPacketOrbranchIdAndStatus(paymentType, debiteOrcredit, branchId, status);

		List<Branch> branchList = branchRepository.findAll();
		for(int i=0;i<branchPaymentHistoryList.size();i++)
		{
			for(int j=0;j<branchList.size();j++)
			{
				if(branchList.get(j).getBranchId().equalsIgnoreCase(branchPaymentHistoryList.get(i).getPacketOrbranchId()))
				{
					branchPaymentHistoryList.get(i).setCustomerOrBranchName(branchList.get(j).getBranchName());
					branchPaymentHistoryList.get(i).setDateInString(createDateFormat.format(branchPaymentHistoryList.get(i).getCreateDate()));
				}
			}
			
		}
		
		return branchPaymentHistoryList;
	}

}
