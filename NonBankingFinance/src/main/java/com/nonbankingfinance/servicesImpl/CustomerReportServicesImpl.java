package com.nonbankingfinance.servicesImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.dto.CustomerReportDTO;
import com.nonbankingfinance.repository.CustomerLoanDetailsRepository;
import com.nonbankingfinance.services.CustomerReportServices;

@Repository
public class CustomerReportServicesImpl implements CustomerReportServices{

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	CustomerLoanDetailsRepository customerloandetailsRepository;

	public List GetCustomerReportList(String branchId) {
		// TODO Auto-generated method stub

		Query query = new Query();

		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("paymentStatus").is("Remaining")),CustomerLoanDetails.class);

		query = new Query();
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

		query = new Query();
		List<CustomerPayment> customerPaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerPayment.class);

		List<CustomerReportDTO> customerReportDTOList=new ArrayList<CustomerReportDTO>();
		CustomerReportDTO customerReportDTO=new CustomerReportDTO();

		long balanceAmount=0;
		long paidPrincipalAmount=0;
		double paidInterestAmount;
		double totalItemQty=0.0;
		String itemList="";
		double itemMarketValue=0.0;
		long ltv=0;
		for(int i=0;i<customerLoanDetailsList.size();i++)
		{

			paidPrincipalAmount=0;
			paidInterestAmount=0.0;
			balanceAmount=0;
			totalItemQty=0.0;
			itemList="";
			itemMarketValue=0.0;
			ltv=0;
			for(int j=0;j<customerPaymentList.size();j++)
			{
				if( customerLoanDetailsList.get(i).getPacketNumber().equalsIgnoreCase(customerPaymentList.get(j).getPacketNumber()))
				{
					paidPrincipalAmount=paidPrincipalAmount+customerPaymentList.get(j).getPrincipalAmount();
					paidInterestAmount=paidInterestAmount+customerPaymentList.get(j).getInterestAmount();
				}
			}

			if(balanceAmount<customerLoanDetailsList.get(i).getLoanAmount())
			{

				customerReportDTO=new CustomerReportDTO();
				customerReportDTO.setCustomerId(customerLoanDetailsList.get(i).getCustomerId());

				for(int j=0;j<customerDetailsList.size();j++)
				{
					if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
					{
						customerReportDTO.setCustomerName(customerDetailsList.get(j).getCustomerName());
						customerReportDTO.setCustomerEmail(customerDetailsList.get(j).getEmailId());
						customerReportDTO.setCustomerMobileNo(customerDetailsList.get(j).getMobileNumber());
					}
				}

				balanceAmount=customerLoanDetailsList.get(i).getLoanAmount()-paidPrincipalAmount;
				customerReportDTO.setPacketNumber(customerLoanDetailsList.get(i).getPacketNumber());
				customerReportDTO.setLoanDate(createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate()));
				customerReportDTO.setLoanAmount(""+customerLoanDetailsList.get(i).getLoanAmount());
				customerReportDTO.setBalanceAmount(""+balanceAmount);

				query = new Query();
				List<CustomerGoldItemDetails> customerGoldItemDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(customerLoanDetailsList.get(i).getPacketNumber())),CustomerGoldItemDetails.class);

				for(int j=0;j<customerGoldItemDetailsList.size();j++)
				{
					totalItemQty=totalItemQty+customerGoldItemDetailsList.get(j).getItemGrossWeight();
					itemMarketValue=itemMarketValue+customerGoldItemDetailsList.get(j).getItemMarketValue();
					if(j==0)
					{
						itemList=itemList+customerGoldItemDetailsList.get(j).getGoldItemName()+" - "+customerGoldItemDetailsList.get(j).getItemGrossWeight();
					}
					else
					{
						itemList=itemList+", "+customerGoldItemDetailsList.get(j).getGoldItemName()+" - "+customerGoldItemDetailsList.get(j).getItemGrossWeight();
					}
				}
				ltv=(long) ((customerLoanDetailsList.get(i).getLoanAmount()/itemMarketValue)*100);
				customerReportDTO.setTotalWt(""+totalItemQty);
				customerReportDTO.setItem(itemList);
				customerReportDTO.setLtv(""+ltv);


				customerReportDTOList.add(customerReportDTO);
			}
		}

		return customerReportDTOList;
	}

	public List GetPendingCustomerList(String branchId) throws ParseException {
		// TODO Auto-generated method stub

		Query query = new Query();

		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("paymentStatus").is("Remaining")),CustomerLoanDetails.class);

		query = new Query();
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

		query = new Query();
		List<CustomerPayment> customerPaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerPayment.class);

		List<CustomerReportDTO> customerReportDTOList=new ArrayList<CustomerReportDTO>();
		CustomerReportDTO customerReportDTO=new CustomerReportDTO();

		long balanceAmount=0;
		long paidPrincipalAmount=0;
		double paidInterestAmount;
		double totalItemQty=0.0;
		String itemList="";
		double itemMarketValue=0.0;
		long ltv=0;
		Date lastPayemtDate;
		long totalMonths;
		long totalPendingMonths;
		for(int i=customerLoanDetailsList.size()-1;i>=0;i--)
		{

			paidPrincipalAmount=0;
			paidInterestAmount=0.0;
			balanceAmount=0;
			totalItemQty=0.0;
			itemList="";
			itemMarketValue=0.0;
			ltv=0;
			totalMonths=0;
			totalPendingMonths=0;
			lastPayemtDate=customerLoanDetailsList.get(i).getCreationDate();
			
			for(int j=0;j<customerPaymentList.size();j++)
			{
				if( customerLoanDetailsList.get(i).getPacketNumber().equalsIgnoreCase(customerPaymentList.get(j).getPacketNumber()))
				{
					paidPrincipalAmount=paidPrincipalAmount+customerPaymentList.get(j).getPrincipalAmount();
					paidInterestAmount=paidInterestAmount+customerPaymentList.get(j).getInterestAmount();
					lastPayemtDate=customerPaymentList.get(j).getCreationDate();
				}
			}
			long totalDays=0;

			String todayDate1=createDateFormat.format(new Date());
			Date firstDate = null;
			Date secondDate = null;
			Date loanDate = null;

			String lastPayemtDate1=createDateFormat.format(lastPayemtDate);
			firstDate = createDateFormat.parse(lastPayemtDate1);
			secondDate = createDateFormat.parse(todayDate1);

			String loanDate1=createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate());
			loanDate = createDateFormat.parse(loanDate1);

			long diff = secondDate.getTime() - firstDate.getTime();
			long diff1=secondDate.getTime()-loanDate.getTime();
			totalDays = diff / (24 * 60 * 60 * 1000);

			if(balanceAmount<customerLoanDetailsList.get(i).getLoanAmount())
			{
				totalPendingMonths=totalDays/30;
				totalMonths=(diff / (24 * 60 * 60 * 1000))/30;
				customerReportDTO=new CustomerReportDTO();
				customerReportDTO.setCustomerId(customerLoanDetailsList.get(i).getCustomerId());

				for(int j=0;j<customerDetailsList.size();j++)
				{
					if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
					{
						customerReportDTO.setCustomerName(customerDetailsList.get(j).getCustomerName());
						customerReportDTO.setCustomerEmail(customerDetailsList.get(j).getEmailId());
						customerReportDTO.setCustomerMobileNo(customerDetailsList.get(j).getMobileNumber());
					}
				}

				balanceAmount=customerLoanDetailsList.get(i).getLoanAmount()-paidPrincipalAmount;
				customerReportDTO.setPacketNumber(customerLoanDetailsList.get(i).getPacketNumber());
				customerReportDTO.setLoanDate(createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate()));
				customerReportDTO.setLoanAmount(""+customerLoanDetailsList.get(i).getLoanAmount());
				customerReportDTO.setBalanceAmount(""+balanceAmount);
				customerReportDTO.setTotalMonths(""+totalMonths+"."+(diff / (24 * 60 * 60 * 1000))%30);
				customerReportDTO.setPendingMonths(""+totalPendingMonths+"."+totalDays%30);
				query = new Query();
				List<CustomerGoldItemDetails> customerGoldItemDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(customerLoanDetailsList.get(i).getPacketNumber())),CustomerGoldItemDetails.class);

				for(int j=0;j<customerGoldItemDetailsList.size();j++)
				{
					totalItemQty=totalItemQty+customerGoldItemDetailsList.get(j).getItemGrossWeight();
					itemMarketValue=itemMarketValue+customerGoldItemDetailsList.get(j).getItemMarketValue();
					if(j==0)
					{
						itemList=itemList+customerGoldItemDetailsList.get(j).getGoldItemName()+" - "+customerGoldItemDetailsList.get(j).getItemGrossWeight();
					}
					else
					{
						itemList=itemList+", "+customerGoldItemDetailsList.get(j).getGoldItemName()+" - "+customerGoldItemDetailsList.get(j).getItemGrossWeight();
					}
				}
				ltv=(long) ((customerLoanDetailsList.get(i).getLoanAmount()/itemMarketValue)*100);
				customerReportDTO.setTotalWt(""+totalItemQty);
				customerReportDTO.setItem(itemList);
				customerReportDTO.setLtv(""+ltv);


				customerReportDTOList.add(customerReportDTO);
			}
		}

		return customerReportDTOList;
	}

}
