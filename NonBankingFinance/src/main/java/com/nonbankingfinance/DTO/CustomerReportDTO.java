package com.nonbankingfinance.dto;

public class CustomerReportDTO {

	private String srNo;
	private String customerId;
	private String packetNumber;
	private String customerName;
	private String customerEmail;
	private String customerMobileNo;
	private String loanDate;
	private String loanAmount;
	private String balanceAmount;
	private String item;
	private String totalWt;
	private String ltv;
	private String totalMonths;
	private String pendingMonths;
	
	public CustomerReportDTO() {}

	public String getSrNo() {
		return srNo;
	}

	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getPacketNumber() {
		return packetNumber;
	}

	public void setPacketNumber(String packetNumber) {
		this.packetNumber = packetNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getLoanDate() {
		return loanDate;
	}

	public void setLoanDate(String loanDate) {
		this.loanDate = loanDate;
	}

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(String balanceAmount) {
		this.balanceAmount = balanceAmount;
	}


	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getTotalWt() {
		return totalWt;
	}

	public void setTotalWt(String totalWt) {
		this.totalWt = totalWt;
	}

	public String getLtv() {
		return ltv;
	}

	public void setLtv(String ltv) {
		this.ltv = ltv;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerMobileNo() {
		return customerMobileNo;
	}

	public void setCustomerMobileNo(String customerMobileNo) {
		this.customerMobileNo = customerMobileNo;
	}

	public String getTotalMonths() {
		return totalMonths;
	}

	public void setTotalMonths(String totalMonths) {
		this.totalMonths = totalMonths;
	}

	public String getPendingMonths() {
		return pendingMonths;
	}

	public void setPendingMonths(String pendingMonths) {
		this.pendingMonths = pendingMonths;
	}
	
}
