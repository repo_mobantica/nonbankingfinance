package com.nonbankingfinance.dto;

import java.util.Date;

import org.springframework.data.annotation.Transient;

public class NoticeChargeReportDTO {

	private String packetNumber;
	private String customerId;
	private String customerName;
	private String customerMobileNo;
	private String customerEmailId;
	
	private long loanAmount;
	private String goldloanschemeId;
	private double interestRate;
	private String loanDate;

	private String userId;
	private String branchId;
	private String branchName;
	
	private long totalPaidPrincipalAmount;
	
	private long remainingPrincipal;
	private double interestPer;
	private long interestAmount;
	private long noticeCharge;
	private long totalRemainingAmount;
	
	public NoticeChargeReportDTO() {}

	public String getPacketNumber() {
		return packetNumber;
	}

	public void setPacketNumber(String packetNumber) {
		this.packetNumber = packetNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(long loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getGoldloanschemeId() {
		return goldloanschemeId;
	}

	public void setGoldloanschemeId(String goldloanschemeId) {
		this.goldloanschemeId = goldloanschemeId;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public String getLoanDate() {
		return loanDate;
	}

	public void setLoanDate(String loanDate) {
		this.loanDate = loanDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public long getTotalPaidPrincipalAmount() {
		return totalPaidPrincipalAmount;
	}

	public void setTotalPaidPrincipalAmount(long totalPaidPrincipalAmount) {
		this.totalPaidPrincipalAmount = totalPaidPrincipalAmount;
	}

	public long getRemainingPrincipal() {
		return remainingPrincipal;
	}

	public void setRemainingPrincipal(long remainingPrincipal) {
		this.remainingPrincipal = remainingPrincipal;
	}

	public double getInterestPer() {
		return interestPer;
	}

	public void setInterestPer(double interestPer) {
		this.interestPer = interestPer;
	}

	public long getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(long interestAmount) {
		this.interestAmount = interestAmount;
	}

	public long getNoticeCharge() {
		return noticeCharge;
	}

	public void setNoticeCharge(long noticeCharge) {
		this.noticeCharge = noticeCharge;
	}

	public long getTotalRemainingAmount() {
		return totalRemainingAmount;
	}

	public void setTotalRemainingAmount(long totalRemainingAmount) {
		this.totalRemainingAmount = totalRemainingAmount;
	}

	public String getCustomerMobileNo() {
		return customerMobileNo;
	}

	public void setCustomerMobileNo(String customerMobileNo) {
		this.customerMobileNo = customerMobileNo;
	}

	public String getCustomerEmailId() {
		return customerEmailId;
	}

	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}

}
