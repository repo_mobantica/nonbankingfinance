package com.nonbankingfinance.services;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface VoucherServices {

	List GetVoucherList(int paymentType, int debiteOrcredit, String branchId, int status);
}
