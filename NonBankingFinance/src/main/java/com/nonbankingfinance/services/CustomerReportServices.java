package com.nonbankingfinance.services;

import java.text.ParseException;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface CustomerReportServices {

	List GetCustomerReportList(String branchId);
	
	List GetPendingCustomerList(String branchId) throws ParseException;
}
