package com.nonbankingfinance.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nonbankingfinance.bean.Branch;
import com.nonbankingfinance.bean.Employee;
import com.nonbankingfinance.bean.Login;
import com.nonbankingfinance.bean.TodayGoldRate;
import com.nonbankingfinance.repository.BranchRepository;
import com.nonbankingfinance.repository.LoginRepository;
import com.nonbankingfinance.repository.TodayGoldRateRepository;

@Controller
@RequestMapping("/")
public class LoginController
{

	@Autowired
	TodayGoldRateRepository todaygoldrateRepository;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	BranchRepository branchRepository;  

	@Autowired
	MongoTemplate mongoTemplate;
	
	
    //Request Mapping For Login
	@RequestMapping(method = RequestMethod.GET)
	public String loginRedirect(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{

    	List<Branch> branchList = branchRepository.findAll();
	    model.addAttribute("branchList", branchList);
	   return "login";
	}
	    
	@RequestMapping(value="/login",method = RequestMethod.GET)
	public String LoginReturnByLogout(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{

    	List<Branch> branchList = branchRepository.findAll();
	    model.addAttribute("branchList", branchList);
	   return "login";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST) 
	public String checkLogin(@ModelAttribute Login login, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		String userName = req.getParameter("userName");
		String passWord = req.getParameter("passWord");
		String branchId = req.getParameter("branchId");
		String employeeName="";
		int todayGoldRate=0;
		int goldRateBy99_1=0;
		try
		{
			List<Login> loginUser= new ArrayList<Login>();
			List<TodayGoldRate> goldRateList = todaygoldrateRepository.findAll();
			for(int i=0;i<goldRateList.size();i++)
			{
				todayGoldRate=goldRateList.get(i).getGoldRate();
				goldRateBy99_1=goldRateList.get(i).getGoldRateBy99_1();
			}
			
			Query query = new Query();
			
			loginUser = mongoTemplate.find(query.addCriteria(Criteria.where("userName").is(userName).and("passWord").is(passWord).and("status").is("Active")), Login.class);
			
		   if(loginUser.size()==1)
		   {
			   
			 query = new Query();
		     List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginUser.get(0).getEmployeeId())),Employee.class);
		     employeeName=employeeDetails.get(0).getEmployeeId()+" "+employeeDetails.get(0).getEmployeelastName();
		   
			 query = new Query();
		     List<Branch> branchDetails = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),Branch.class);
		     String branchName=branchDetails.get(0).getBranchName();
			 HttpSession session = req.getSession(true);
			 session.setAttribute("employeeId",employeeDetails.get(0).getEmployeeId());
			 session.setAttribute("employeeName",employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeelastName());
			 session.setAttribute("userName",employeeName);
			 session.setAttribute("branchId",branchId);  
			 session.setAttribute("branchName",branchName); 
			 session.setAttribute("goldRateBy99_1",goldRateBy99_1);  
			 session.setAttribute("todayGoldRate",todayGoldRate);  
			 return "redirect:home";
		   }
		   else
		   {
			   model.addAttribute("loginStatus", "Fail");

		    	List<Branch> branchList = branchRepository.findAll();
			    model.addAttribute("branchList", branchList);
			   return "redirect:login";
		   }
		}
		catch(Exception e)
		{

	    	List<Branch> branchList = branchRepository.findAll();
		    model.addAttribute("branchList", branchList);
			return "login";
		}
	}
	
	
	@RequestMapping("/logout")
	public String logout(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		HttpSession session = req.getSession(false);
		
		session.removeAttribute("employeeId");
		session.removeAttribute("employeeName");
		session.removeAttribute("userName");
		session.removeAttribute("branchId");
		session.removeAttribute("branchName");
		session.removeAttribute("todayGoldRate");
		
    	List<Branch> branchList = branchRepository.findAll();
	    model.addAttribute("branchList", branchList);
		return "redirect:login";
	}
	
}