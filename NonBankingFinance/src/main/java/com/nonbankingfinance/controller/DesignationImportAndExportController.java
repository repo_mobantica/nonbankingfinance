package com.nonbankingfinance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nonbankingfinance.bean.Department;
import com.nonbankingfinance.bean.Designation;
import com.nonbankingfinance.bean.SubChartOfAccount;
import com.nonbankingfinance.repository.DepartmentRepository;
import com.nonbankingfinance.repository.DesignationRepository;

@Controller
@RequestMapping("/")
public class DesignationImportAndExportController {


	@Autowired
    DesignationRepository designationRepository;
	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
    ServletContext context;
	
	Designation designation = new Designation();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;
	
	HSSFSheet worksheet;
	XSSFSheet worksheet1;
	
	@Autowired
	MongoTemplate mongoTemplate;
	   
		//Code For Auto generation for designation id
			String designationCode;
		    
		    private String DesignationCode()
		    {
		    	long designationCount  = designationRepository.count();
		    	
		        if(designationCount<10)
		        {
		        	designationCode = "ST000"+(designationCount+1);
		        }
		        else if((designationCount>=10) && (designationCount<100))
		        {
		        	designationCode = "ST00"+(designationCount+1);
		        }
		        else if((designationCount>=100) && (designationCount<1000))
		        {
		        	designationCode = "ST0"+(designationCount+1);
		        }
		        else if(designationCount>1000)
		        {
		        	designationCode = "ST"+(designationCount+1);
		        }
		        
		        return designationCode;
		    }

		    @RequestMapping(value="/ImportNewDesignation")
		    public String importNewDesignation(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
		    {

		    	model.addAttribute("progressBarPer",0);
		    	model.addAttribute("statusFlag",0);
		    	
		        return "ImportNewDesignation";
		    }
			
			//private static final String INTERNAL_FILE="Newdesignation.xlsx";
			@RequestMapping(value = "/DownloadDesignationTemplate")
			public String downloadDesignationTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
			{
				
				String filename = "NewDesignation.xlsx";
		        response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				
				String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
				response.setContentType("APPLICATION/OCTET-STREAM");
				response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");
		 
				
				FileInputStream fileInputStream = new FileInputStream(filepath+filename);
		 
				int i;
				while ((i = fileInputStream.read()) != -1) {
					out.write(i);
				}
				fileInputStream.close();
				out.close();
				
				return "ImportNewDesignation";
			}
			
			@SuppressWarnings({ "deprecation"})
			@RequestMapping(value = "/ImportNewDesignation", method = RequestMethod.POST)
			public String uploadDesignationDetails(@RequestParam("designation_excel") MultipartFile designation_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
			{

		    	List<Department> departmentList= departmentRepository.findAll();
		    	String employeeId = (String)request.getSession().getAttribute("employeeId");
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
				LocalDate localDate = LocalDate.now();
				String todayDate=dtf.format(localDate);
				
				List<Designation> designationList= designationRepository.findAll();
		    	
		    	List<Designation> designationList1= new ArrayList<Designation>();
		    	

				int progressBarPer=0;
				int flag1=0,flag2=0;
				String departmentId="";
				
				if (!designation_excel.isEmpty() || designation_excel != null )
				{
					try 
					{
						if(designation_excel.getOriginalFilename().endsWith("xls") || designation_excel.getOriginalFilename().endsWith("xlsx") || designation_excel.getOriginalFilename().endsWith("csv"))
						{
							if(designation_excel.getOriginalFilename().endsWith("xlsx"))
							{
								 InputStream stream = designation_excel.getInputStream();
								 XSSFWorkbook workbook = new XSSFWorkbook(stream);
								 
								 XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet
								 
								 int firstRow = sheet.getFirstRowNum();
								 
								 XSSFRow firstrow = sheet.getRow(firstRow);
								 
								@SuppressWarnings("unused")
								int lastColumnCount = firstrow.getLastCellNum();
								 
								@SuppressWarnings("unused")
								 Iterator<Row> rowIterator = sheet.iterator();   
								 int last_no=sheet.getLastRowNum();
								 
								 Designation designation = new Designation();
								 for(int i=0;i<last_no;i++)
						         {
									 flag1=0;
									 flag2=0;
									 try
									 {
									 XSSFRow row = sheet.getRow(i+1);
									 
						                 	 // Skip read heading 
						                     if (row.getRowNum() == 0) 
						                     {
						                    	 continue;
						                     }
						                     
						                     for(int k=0;k<departmentList.size();k++)
											 {
						                    	 if(departmentList.get(k).getDepartmentName().equalsIgnoreCase(row.getCell(1).getStringCellValue()))
						                    	 {
						                    		 flag1=1;
						                    		 departmentId=departmentList.get(k).getDepartmentId();
								                     break;
						                    	 }
												 
											 }
						                     for(int j=0;j<designationList.size();j++)
						                     {
						                    	 if(designationList.get(j).getDepartmentId().equalsIgnoreCase(departmentId) && designationList.get(j).getDesignationName().equalsIgnoreCase(row.getCell(0).getStringCellValue()))
						                    	 {
						                    		 flag2=1;
						                    	 }
						                     }
						                     if(flag1==1 && flag2==0)
						                     {
						                    	 designation = new Designation();
							                     designation.setDesignationId(DesignationCode());
							                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
							                     designation.setDesignationName(row.getCell(0).getStringCellValue());
							                     designation.setDepartmentId(departmentId);
												 designation.setCreationDate(todayDate);
												 designation.setUpdateDate(todayDate);
												 designation.setUserId(employeeId);
												 designationRepository.save(designation);
						                     }
						                     else
						                     {

						                    	 designation = new Designation();
							                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
							                     designation.setDesignationName(row.getCell(0).getStringCellValue());
							                     designation.setDepartmentId(row.getCell(1).getStringCellValue());
							                     if(flag1==0)
							                     {
							                    	 designation.setStatus("Department Not present"); 
							                     }
							                     else
							                     {
							                    	 designation.setStatus("This Designation already exit"); 
							                     }
							                     
							                     designationList1.add(designation);
						                     }
						         }
								 catch (Exception e) {
									// TODO: handle exception
								}        
											
						          }

								 progressBarPer= ((last_no-designationList1.size())*100)/last_no;

								 workbook.close();
							}
							
						}//if after inner if
						
					}
					catch(Exception e)
					{
						
					}
				}

				int statusFlag;
				if(designationList1.size()!=0)
				{
					statusFlag=1;	
				}
				else
				{
					statusFlag=2;
				}
				
		    	model.addAttribute("progressBarPer",progressBarPer);
		    	
		    	model.addAttribute("statusFlag",statusFlag);
		    	model.addAttribute("designationList1",designationList1);
				return "ImportNewDesignation";
			}
			
			
}
