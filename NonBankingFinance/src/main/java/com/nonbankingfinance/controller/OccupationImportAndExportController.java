package com.nonbankingfinance.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nonbankingfinance.bean.ChartOfAccount;
import com.nonbankingfinance.bean.Occupation;
import com.nonbankingfinance.repository.OccupationRepository;

@Controller
@RequestMapping("/")
public class OccupationImportAndExportController {

	@Autowired
	OccupationRepository occupationRepository;
	@Autowired
    ServletContext context;
	
	Occupation occupation = new Occupation();
	
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;
	
	HSSFSheet worksheet;
	XSSFSheet worksheet1;
	
	@Autowired
	MongoTemplate mongoTemplate;
	   

	String occupationCode;
	private String OccupationCode()
	{
		long occupationCount = occupationRepository.count();
		
		if(occupationCount<10)
		{
			occupationCode = "OC000"+(occupationCount+1);
		}
		else if((occupationCount>=10) && (occupationCount<100))
		{
			occupationCode = "OC00"+(occupationCount+1);
		}
		else if((occupationCount>=100) && (occupationCount<1000))
		{
			occupationCode = "OC0"+(occupationCount+1);
		}
		else if(occupationCount>1000)
		{
			occupationCode = "OC"+(occupationCount+1);
		}
		
		return occupationCode;
	} 

    @RequestMapping(value="/ImportNewOccupation")
    public String importNewDesignation(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {

    	model.addAttribute("progressBarPer",0);
    	model.addAttribute("statusFlag",0);
        return "ImportNewOccupation";
    }
	
	//private static final String INTERNAL_FILE="Newdesignation.xlsx";
	@RequestMapping(value = "/DownloadOccupationTemplate")
	public String downloadDesignationTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{
		
		String filename = "NewOccupation.xlsx";
        response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");
 
		
		FileInputStream fileInputStream = new FileInputStream(filepath+filename);
 
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
		
		return "ImportNewOccupation";
	}
	

	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewOccupation", method = RequestMethod.POST)
	public String ImportNewOccupation(@RequestParam("occupation_excel") MultipartFile occupation_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

    	
    	String employeeId = (String)request.getSession().getAttribute("employeeId");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		
		int flag1=0;
		int progressBarPer=0;
    	List<Occupation> occupationList= occupationRepository.findAll();
		List<Occupation> occupationList1=new ArrayList<Occupation>();
		
		if (!occupation_excel.isEmpty() || occupation_excel != null )
		{
			try 
			{
				if(occupation_excel.getOriginalFilename().endsWith("xls") || occupation_excel.getOriginalFilename().endsWith("xlsx") || occupation_excel.getOriginalFilename().endsWith("csv"))
				{
					if(occupation_excel.getOriginalFilename().endsWith("xlsx"))
					{
						 InputStream stream = occupation_excel.getInputStream();
						 XSSFWorkbook workbook = new XSSFWorkbook(stream);
						 
						 XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet
						 
						 int firstRow = sheet.getFirstRowNum();
						 
						 XSSFRow firstrow = sheet.getRow(firstRow);
						 
						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();
						 
						@SuppressWarnings("unused")
						 Iterator<Row> rowIterator = sheet.iterator();   
						 int last_no=sheet.getLastRowNum();
						 
						 Occupation occupation = new Occupation();
						 for(int i=0;i<last_no;i++)
				         {
							 try
							 {
							 XSSFRow row = sheet.getRow(i+1);
							 
				                 	 // Skip read heading 
				                     if (row.getRowNum() == 0) 
				                     {
				                    	 continue;
				                     }
				                    
				                     
				                     for(int j=0;j<occupationList.size();j++)
				                     {
				                    	 if(occupationList.get(j).getOccupationName().equalsIgnoreCase(row.getCell(0).getStringCellValue()))
				                    	 {
				                    		 flag1=1;
				                    		 break;
				                    	 }
				                    		 
				                     }
				                     if(flag1==0)
				                     {
				                         occupation = new Occupation();
				                	     occupation.setOccupationId(OccupationCode());
					                     
					                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
					                     occupation.setOccupationName(row.getCell(0).getStringCellValue());
					                     
					                     occupation.setCreationDate(todayDate);
					                     occupation.setUpdateDate(todayDate);
					                     occupation.setUserId(employeeId);
					                     occupationRepository.save(occupation);
				                     }
				                     else
				                     {
					                      occupation = new Occupation();
					                	  occupation.setOccupationId(OccupationCode());
						                     
						                  row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
						                  occupation.setOccupationName(row.getCell(0).getStringCellValue());
						                  occupation.setStatus("This occupation already exit");
						                  occupationList1.add(occupation);
				                     }
					         }
							 catch (Exception e) {
								// TODO: handle exception
							}	
				          }

						 progressBarPer= ((last_no-occupationList1.size())*100)/last_no;
				         workbook.close();
					}
					
				}//if after inner if
				
			}
			catch(Exception e)
			{
			}
		}

		int statusFlag;
		if(occupationList1.size()!=0)
		{
			statusFlag=1;	
		}
		else
		{
			statusFlag=2;
		}
		
    	model.addAttribute("progressBarPer",progressBarPer);
    	
    	model.addAttribute("statusFlag",statusFlag);
    	model.addAttribute("occupationList1",occupationList1);
		return "ImportNewOccupation";
	}
	
	
	

}
