package com.nonbankingfinance.controller;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.repository.CompanyRepository;
import com.nonbankingfinance.repository.CustomerDetailsRepository;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.CustomerPaymentRepository;
import com.nonbankingfinance.repository.GoldItemRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class CustomerMonthlyReportController {


	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
    GoldItemRepository goldItemRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	CustomerDetailsRepository customerDetailsRepository;
	@Autowired
	CustomerPaymentRepository customerPaymentRepository; 
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	

    @RequestMapping("/CustomerMonthlyReport")
    public String CustomerMonthlyReport(ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException
    {

    	String branchId = (String)req.getSession().getAttribute("branchId");

    	 DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
	    Calendar now = Calendar.getInstance();
	    
	    String currentStartDate1="1/"+(now.get(Calendar.MONTH) + 1)+"/"+now.get(Calendar.YEAR);

	    Calendar calendar = Calendar.getInstance();
	    int lastDate = calendar.getActualMaximum(Calendar.DATE);
	    calendar.set(Calendar.DATE, lastDate);
	    
	    Date currentMonthStartDate=new SimpleDateFormat("d/M/yyyy").parse(currentStartDate1);
	    Date currentMonthEndDate=calendar.getTime();
	    
    	String customerName="";
    	Query query = new Query();

    	query = new Query();
    	List<CustomerPayment> customerPaymentDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(currentMonthStartDate).lt(currentMonthEndDate).and("branchId").is(branchId)), CustomerPayment.class);

   	 	String paidDate="";
   	 	
    	long totalPrincipalAmount=0;
    	long totalInterestAmount=0;
    	long totalNoticeCharge=0;
    	long totalAmount=0;
    	
    	query = new Query();
    	List<CustomerDetails> customerDetailsList1 = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
    	
    	for(int i=0;i<customerPaymentDetailsList.size();i++)
    	{
    		totalPrincipalAmount=totalPrincipalAmount+customerPaymentDetailsList.get(i).getPrincipalAmount();
    		totalInterestAmount=(long) (totalInterestAmount+customerPaymentDetailsList.get(i).getInterestAmount());
    		totalNoticeCharge=totalNoticeCharge+customerPaymentDetailsList.get(i).getNoticeCharge();
    		for(int j=0;j<customerDetailsList1.size();j++)
    		{
    			if(customerPaymentDetailsList.get(i).getCustomerId().equals(customerDetailsList1.get(j).getCustomerId()))
    			{
    				customerName=customerDetailsList1.get(j).getCustomerName();
    				customerPaymentDetailsList.get(i).setCustomerName(customerName);
    			}
    		}

			  paidDate = dateFormat.format(customerPaymentDetailsList.get(i).getCreationDate());
			  customerPaymentDetailsList.get(i).setPaidDate(paidDate);
    	}

    	totalAmount=totalPrincipalAmount+totalInterestAmount+totalNoticeCharge;

    	model.addAttribute("totalPrincipalAmount", totalPrincipalAmount);
    	model.addAttribute("totalInterestAmount", totalInterestAmount);
    	model.addAttribute("totalNoticeCharge", totalNoticeCharge);
    	model.addAttribute("totalAmount", totalAmount);

	    model.addAttribute("customerPaymentDetailsList", customerPaymentDetailsList);
    	return "CustomerMonthlyReport";
    }
    
         @ResponseBody
		 @RequestMapping("/searchCustomerCollectionReportDateWise")
		 public List<CustomerPayment> searchCustomerCollectionReportDateWise(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException
		 {
    			String branchId = (String)req.getSession().getAttribute("branchId");

    	    	DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
			    Date currentMonthStartDate=new SimpleDateFormat("M/d/yyyy").parse(startDate);
			    Date currentMonthEndDate=new SimpleDateFormat("M/d/yyyy").parse(endDate);
			    
			   // String todayDateInString = new SimpleDateFormat("d/M/yyyy").format(currentMonthStartDate);

		    	Query query = new Query();

		    	query = new Query();
		    	List<CustomerPayment> customerPaymentDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(currentMonthStartDate).lt(currentMonthEndDate).and("branchId").is(branchId)), CustomerPayment.class);

		   	 	String paidDate="";
		   	 	String customerName="";
		   	 	
		    	long totalPrincipalAmount=0;
		    	long totalInterestAmount=0;
		    	long totalNoticeCharge=0;
		    	long totalAmount=0;
		    	
		    	query = new Query();
		    	List<CustomerDetails> customerDetailsList1 = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
		    	
		    	for(int i=0;i<customerPaymentDetailsList.size();i++)
		    	{
		    		totalPrincipalAmount=totalPrincipalAmount+customerPaymentDetailsList.get(i).getPrincipalAmount();
		    		totalInterestAmount=(long) (totalInterestAmount+customerPaymentDetailsList.get(i).getInterestAmount());
		    		totalNoticeCharge=totalNoticeCharge+customerPaymentDetailsList.get(i).getNoticeCharge();
		    		for(int j=0;j<customerDetailsList1.size();j++)
		    		{
		    			if(customerPaymentDetailsList.get(i).getCustomerId().equals(customerDetailsList1.get(j).getCustomerId()))
		    			{
		    				customerName=customerDetailsList1.get(j).getCustomerName();
		    				customerPaymentDetailsList.get(i).setCustomerName(customerName);
		    			}
		    		}

					  paidDate = dateFormat.format(customerPaymentDetailsList.get(i).getCreationDate());
					  customerPaymentDetailsList.get(i).setPaidDate(paidDate);
		    	}

		    	totalAmount=totalPrincipalAmount+totalInterestAmount+totalNoticeCharge;


			    return customerPaymentDetailsList;
		 }
    

     	@ResponseBody
		@RequestMapping(value = "/CustomerMonthlyReport", method = RequestMethod.POST)
     	public ResponseEntity<byte[]> PrintCustomerMonthlyReport(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response )
     	{
     		int index=0;
     		JasperPrint print;
     		try 
     		{	
     			Query query = new Query();
     			
     			Collection c = new ArrayList();
     			HashMap jmap = new HashMap();
     			
     			 jmap = null;
     				

  		    		String branchId = (String)req.getSession().getAttribute("branchId");
     		    	String employeeName = (String)req.getSession().getAttribute("employeeName");
     		    	String branchName = (String)req.getSession().getAttribute("branchName");
     		    	query = new Query();
     		    	List<Company> companyDetails= companyRepository.findAll();

     		    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

     		    	
     		    	List<CustomerDetails> customerDetailsList1 =new ArrayList<CustomerDetails>();

     		      	List<CustomerPayment> customerPaymentDetailsList=new ArrayList<CustomerPayment>();
     		      	
     		      	long totalPrincipalAmount=0;
     		      	long totalInterestAmount=0;
     		      	long totalNoticeCharge=0;
     		      	long totalAmount=0;

     		      	String customerName="";
     		    	String reportDate="";
     		    	
     		    	if(endDate.equals("") || startDate.equals(""))
     		    	{

     		      	 DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
     		  	    Calendar now = Calendar.getInstance();
     		  	    
     		  	    String currentStartDate1="1/"+(now.get(Calendar.MONTH) + 1)+"/"+now.get(Calendar.YEAR);

     		  	    Calendar calendar = Calendar.getInstance();
     		  	    int lastDate = calendar.getActualMaximum(Calendar.DATE);
     		  	    calendar.set(Calendar.DATE, lastDate);
     		  	    
     		  	    Date currentMonthStartDate=new SimpleDateFormat("d/M/yyyy").parse(currentStartDate1);
     		  	    Date currentMonthEndDate=calendar.getTime();

	  				 String currentEndDate1 = dateFormat.format(currentMonthEndDate);
     		  	     reportDate=currentStartDate1+" To "+currentEndDate1;
     		  	    
     		  	     query = new Query();
  		    	     customerPaymentDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(currentMonthStartDate).lt(currentMonthEndDate).and("branchId").is(branchId)), CustomerPayment.class);
     		      	
     		    	}
     		    	else
     		    	{
     		    		
     	    	    	DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
     				    Date currentMonthStartDate=new SimpleDateFormat("M/d/yyyy").parse(startDate);
     				    Date currentMonthEndDate=new SimpleDateFormat("M/d/yyyy").parse(endDate);

     				   reportDate=dateFormat.format(currentMonthStartDate)+" To "+dateFormat.format(currentMonthEndDate);

       		  	     query = new Query();
    		    	 customerPaymentDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(currentMonthStartDate).lt(currentMonthEndDate).and("branchId").is(branchId)), CustomerPayment.class);
       		      	
     		    	}
     		    	
     		      query = new Query();
     		      customerDetailsList1 = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
     		      	
     		      	String paidDate;
         	    	DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
         	    	
     		      	for(int i=0;i<customerPaymentDetailsList.size();i++)
     		      	{
     		      		totalPrincipalAmount=totalPrincipalAmount+customerPaymentDetailsList.get(i).getPrincipalAmount();
     		      		totalInterestAmount=(long) (totalInterestAmount+customerPaymentDetailsList.get(i).getInterestAmount());
     		      		totalNoticeCharge=totalNoticeCharge+customerPaymentDetailsList.get(i).getNoticeCharge();
     		      		for(int j=0;j<customerDetailsList1.size();j++)
     		      		{
     		      			if(customerPaymentDetailsList.get(i).getCustomerId().equals(customerDetailsList1.get(j).getCustomerId()))
     		      			{
     		      				customerName=customerDetailsList1.get(j).getCustomerName();
     		      				customerPaymentDetailsList.get(i).setCustomerName(customerName);
     		      				
     							  paidDate = dateFormat.format(customerPaymentDetailsList.get(i).getCreationDate());
     							  customerPaymentDetailsList.get(i).setPaidDate(paidDate);
     		      			}
     		      		}
     		      	}

     		      	totalAmount=totalPrincipalAmount+totalInterestAmount+totalNoticeCharge;

     		   //  String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";	 
     		    	
     				c = new ArrayList();
     			 	 for(int i=0;i<customerPaymentDetailsList.size();i++)
     				 {
     			 		    jmap = new HashMap();
     						jmap.put("srno",""+(i+1));
     					    //jmap.put("srno","1");
     						jmap.put("customerId",""+customerPaymentDetailsList.get(i).getCustomerId());
     						jmap.put("customerName", ""+customerPaymentDetailsList.get(i).getCustomerName());
     						jmap.put("principalAmount",""+customerPaymentDetailsList.get(i).getPrincipalAmount());
     						jmap.put("interestAmount", ""+customerPaymentDetailsList.get(i).getInterestAmount());
     						jmap.put("paymentDue",""+customerPaymentDetailsList.get(i).getNoticeCharge());
     						jmap.put("paidDate",""+customerPaymentDetailsList.get(i).getPaidDate());
     						
     						c.add(jmap);
     						
     						jmap = null;
     				 }
     		      
     			 JRDataSource dataSource = new JRMapCollectionDataSource(c);
     			 Map<String, Object> parameterMap = new HashMap();
     			 
     			 Date date = new Date();  
     			 String strDate= createDateFormat.format(date);
     			 
     			/*1*/ parameterMap.put("reportDate",""+reportDate);
     				
     			/*2*/ parameterMap.put("totalPrincipalAmount",""+totalPrincipalAmount);
     			
     			/*3*/ parameterMap.put("date", ""+strDate);

     		    /*4*/parameterMap.put("userName", ""+employeeName);
     			      
     		    /*5*/parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());
     			      
     		    /*6*/parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress()+" ");
     		    
     		    /*6*/parameterMap.put("branchName", ""+branchName);
     			      
     			      
     			 InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerMonthlyReport.jasper");
     				
     				
     			 print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

     			 ByteArrayOutputStream baos = new ByteArrayOutputStream();
     			 JasperExportManager.exportReportToPdfStream(print, baos);
     				
     			 byte[] contents = baos.toByteArray();

     			 HttpHeaders headers = new HttpHeaders();
     			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
     			 String filename = "CustomerMonthlyReport.pdf";

     			 JasperExportManager.exportReportToPdfStream(print, baos);

     				 
     			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
     			 headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
     						 
     			 ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
     			 response.setHeader("Content-Disposition", "inline; filename=" + filename );
     					 
     			 return resp;					    
     		}
     		catch(Exception ex)
     		{
     			System.out.println(""+ex);
     			//redirectAttributes.addFlashAttribute("flagemail", 0);
     		}
     		
     				 return null;
     	}
     	
            
}
