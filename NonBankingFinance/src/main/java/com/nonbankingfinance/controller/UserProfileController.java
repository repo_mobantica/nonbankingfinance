package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.Branch;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.bean.Department;
import com.nonbankingfinance.bean.Designation;
import com.nonbankingfinance.bean.Employee;
import com.nonbankingfinance.bean.Login;
import com.nonbankingfinance.bean.UserModel;
import com.nonbankingfinance.repository.EmployeeRepository;
import com.nonbankingfinance.repository.LoginRepository;
import com.nonbankingfinance.repository.UserModelRepository;
import com.nonbankingfinance.services.EmailService;

@Controller
@RequestMapping("/")
public class UserProfileController extends EmailService  {

@Autowired
UserModelRepository usermodelRepository;
@Autowired
EmployeeRepository employeeRepository;
@Autowired
LoginRepository loginRepository;
@Autowired
MongoTemplate mongoTemplate;


@RequestMapping("/UserProfile")
public String UserProfile(ModelMap model, HttpServletRequest req, HttpServletResponse res)
{

	Query query = new Query();
	String employeeId = (String)req.getSession().getAttribute("employeeId");
	query = new Query();
    List<Login> loginDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(employeeId)),Login.class);

	query = new Query();
	Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(employeeId)),Employee.class);

	query = new Query();
	Department depatmentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("departmentId").is(employeeDetails.getDepartmentId())),Department.class);

	query = new Query();
	Designation designationDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("designationId").is(employeeDetails.getDesignationId())),Designation.class);

    model.addAttribute("designationName", designationDetails.getDesignationName());
    model.addAttribute("departmentName", depatmentDetails.getDepartmentName());
    model.addAttribute("loginDetails", loginDetails);
    model.addAttribute("employeeDetails", employeeDetails);
    
	return "UserProfile";
}

@ResponseBody
@RequestMapping("/UpdateUserPassword")
public List<Login> UpdateUserPassword(@RequestParam("newPassword") String newPassword, @RequestParam("userId") String userId, HttpSession session)
{
	try
	{
		Query query = new Query();
		
		query = new Query();
	    Login loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)),Login.class);

	    loginDetails.setPassWord(newPassword);
	    loginRepository.save(loginDetails);
	    
	    query = new Query();
	    Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), Employee.class);

		String emilId = employeeDetails.getEmployeeEmailId();

		String Msgbody1 = "Hi, "+employeeDetails.getEmployeefirstName()+" "+employeeDetails.getEmployeelastName();
		String Msgbody2 = "\n Your password update successfully. You can now login to non banking Finance.\n";
		String Msgbody3 = "\n You can use further login Credentials : ";
		String Msgbody4 =  "\n Username : "+emilId+"\n Password : "+newPassword;
		
	    sendLoginCredentialsMail(emilId,"Login Credentials", Msgbody1+"\n\t"+Msgbody2+"\n"+Msgbody3+"\n"+Msgbody4+" \n\n\t Please, do not reply to the mail.");
	    
       	return null;
	}
	catch(Exception e)
	{
		return null;
	}
	
} 

}
