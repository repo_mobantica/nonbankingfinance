package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.ChartOfAccount;
import com.nonbankingfinance.repository.ChartOfAccountRepository;

@Controller
@RequestMapping("/")
public class ChartOfAccountController {


	@Autowired
    ChartOfAccountRepository chartOfAccountRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String chartOfAccountCode;
	    
	    private String chartOfAccountCode()
	    {
	    	long chartOfAccountCount  = chartOfAccountRepository.count();
	        if(chartOfAccountCount<10)
	        {
	        	chartOfAccountCode = "CN000"+(chartOfAccountCount+1);
	        }
	        else if((chartOfAccountCount>=10) && (chartOfAccountCount<100))
	        {
	        	chartOfAccountCode = "CN00"+(chartOfAccountCount+1);
	        }
	        else if((chartOfAccountCount>=100) && (chartOfAccountCount<1000))
	        {
	        	chartOfAccountCode = "CN0"+(chartOfAccountCount+1);
	        }
	        else if(chartOfAccountCount>1000)
	        {
	        	chartOfAccountCode = "CN"+(chartOfAccountCount+1);
	        }
	         return chartOfAccountCode;
	    }
	    
	    @RequestMapping("/ChartOfAccountMaster")
	    public String ChartOfAccountMaster(ModelMap model)
	    {
	    	 List<ChartOfAccount> chartOfAccountList;
 	    	 chartOfAccountList = chartOfAccountRepository.findAll();
 	    	 
 	    	 model.addAttribute("chartOfAccountList", chartOfAccountList);
 	    	 
	    	return "ChartOfAccountMaster";
	    }
	    
	   
    @RequestMapping("/AddChartOfAccount")
    public String AddChartOfAccount(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	
    	model.addAttribute("chartOfAccountCode",chartOfAccountCode());
        return "AddChartOfAccount";
    }
    
    
	    @RequestMapping(value = "/AddChartOfAccount", method = RequestMethod.POST)
	    public String chartOfAccountSave(@ModelAttribute ChartOfAccount chartOfAccount, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
		        chartOfAccount = new ChartOfAccount(chartOfAccount.getChartOfAccountId(), chartOfAccount.getChartOfAccountName(),chartOfAccount.getChartOfAccountNumber(), chartOfAccount.getCreationDate(), chartOfAccount.getUpdateDate(), chartOfAccount.getUserId());
		        chartOfAccountRepository.save(chartOfAccount);
		        
		        model.addAttribute("chartOfAccountStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("chartOfAccountStatus", "Fail");
	    	}

	    	    model.addAttribute("chartOfAccountCode",chartOfAccountCode());
	    	
	    	 List<ChartOfAccount> chartOfAccountList;
	    	 chartOfAccountList = chartOfAccountRepository.findAll();
	    	 
	    	 model.addAttribute("chartOfAccountList", chartOfAccountList);
	    	
	        return "AddChartOfAccount";
	    }
	   

	    @RequestMapping(value="/EditChartOfAccount",method=RequestMethod.GET)
	    public String EditChartOfAccount(@RequestParam("chartOfAccountId") String chartOfAccountId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<ChartOfAccount> chartOfAccountDetails = mongoTemplate.find(query.addCriteria(Criteria.where("chartOfAccountId").is(chartOfAccountId)), ChartOfAccount.class);
	    	
	    	model.addAttribute("chartOfAccountDetails",chartOfAccountDetails);
	    	
	    	return "EditChartOfAccount";
	    }
	    
	    @RequestMapping(value="/EditChartOfAccount",method=RequestMethod.POST)
	    public String EditChartOfAccountPostMethod(@ModelAttribute ChartOfAccount chartOfAccount, ModelMap model)
	    {
	    	try
	    	{
	    		chartOfAccount = new ChartOfAccount(chartOfAccount.getChartOfAccountId(), chartOfAccount.getChartOfAccountName(), chartOfAccount.getChartOfAccountNumber(), chartOfAccount.getCreationDate(), chartOfAccount.getUpdateDate(), chartOfAccount.getUserId());
		        chartOfAccountRepository.save(chartOfAccount);
		        
		        model.addAttribute("chartOfAccountStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("chartOfAccountStatus", "Fail");
	    	}
	    	
	    	List<ChartOfAccount> chartOfAccountList = chartOfAccountRepository.findAll();
	    	 
	    	model.addAttribute("chartOfAccountList", chartOfAccountList);
	    	return "ChartOfAccountMaster";
	    } 
	    
}
