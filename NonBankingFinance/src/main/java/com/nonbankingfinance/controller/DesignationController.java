package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.Department;
import com.nonbankingfinance.bean.Designation;
import com.nonbankingfinance.repository.DepartmentRepository;
import com.nonbankingfinance.repository.DesignationRepository;

@Controller
@RequestMapping("/")
public class DesignationController {


	@Autowired
    DesignationRepository designationRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	   
	//Code For Auto generation for designation id
		String designationCode;
	    
	    private String DesignationCode()
	    {
	    	long designationCount  = designationRepository.count();
	    	
	        if(designationCount<10)
	        {
	        	designationCode = "DS000"+(designationCount+1);
	        }
	        else if((designationCount>=10) && (designationCount<100))
	        {
	        	designationCode = "DS00"+(designationCount+1);
	        }
	        else if((designationCount>=100) && (designationCount<1000))
	        {
	        	designationCode = "DS0"+(designationCount+1);
	        }
	        else if(designationCount>1000)
	        {
	        	designationCode = "DS"+(designationCount+1);
	        }
	        
	        return designationCode;
	    }
	    
	    @RequestMapping("/DesignationMaster")
	    public String DesignationMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {
	    	
	    	List<Designation> designationList= designationRepository.findAll();
	    	List<Department> departmentList= departmentRepository.findAll();
	    	
	    	for(int i=0;i<designationList.size();i++)
	    	{
	    		for(int j=0;j<departmentList.size();j++)
	    		{
	    			try
	    			{
	    			if(designationList.get(i).getDepartmentId().equals(departmentList.get(j).getDepartmentId()))
	    			{
	    				designationList.get(i).setDepartmentId(departmentList.get(j).getDepartmentName());
	    				break;
	    			}
	    			}catch (Exception e) {
						// TODO: handle exception
					}
	    		}
	    		
	    	}
	    	
	    	model.addAttribute("departmentList",departmentList);
	      	model.addAttribute("designationList", designationList);
	    	
	        return "DesignationMaster";
	    }
    @RequestMapping("/AddDesignation")
    public String addDesignation(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	List<Department> departmentList= departmentRepository.findAll();
    	
    	model.addAttribute("departmentList",departmentList);
    	model.addAttribute("designationCode",DesignationCode());
    	return "AddDesignation";
    }
    

    @RequestMapping(value = "/AddDesignation", method = RequestMethod.POST)
    public String designationSave(@ModelAttribute Designation designation, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
	        designation = new Designation(designation.getDesignationId(), designation.getDesignationName(), designation.getDepartmentId(), designation.getCreationDate(), designation.getUpdateDate(), designation.getUserId());
	        designationRepository.save(designation);
	        model.addAttribute("designationStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("designationStatus","Fail");
    	}

    	List<Department> departmentList= departmentRepository.findAll();
    	
    	model.addAttribute("departmentList",departmentList);
    	model.addAttribute("designationCode",DesignationCode());
    	
        return "AddDesignation";
    }
    
    
    @RequestMapping("/EditDesignation")
    public String EditDesignation(@RequestParam("designationId") String designationId, ModelMap model)
    {
    	Query query = new Query();
    	List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(designationId)),Designation.class);
    	
    	List<Department> departmentList= departmentRepository.findAll();
    	String departmentName = "";
    	
    	for(int i=0;i<departmentList.size();i++)
    	{
    		if(departmentList.get(i).getDepartmentId().equals(designationDetails.get(0).getDepartmentId()))
    		{
    			departmentName=departmentList.get(i).getDepartmentName();
    			break;
    		}
    	}
    	
    	model.addAttribute("departmentName",departmentName);
    	model.addAttribute("departmentList",departmentList);
    	model.addAttribute("designationDetails", designationDetails);
    	return "EditDesignation";
    }
    
    @RequestMapping(value="/EditDesignation", method=RequestMethod.POST)
    public String EditDesignationPostMethod(@ModelAttribute Designation designation, Model model)
    {
    	try
    	{
    		designation = new Designation(designation.getDesignationId(), designation.getDesignationName(), designation.getDepartmentId(), designation.getCreationDate(), designation.getUpdateDate(), designation.getUserId());
	        designationRepository.save(designation);
	        model.addAttribute("designationStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("designationStatus","Fail");
    	}

    	List<Designation> designationList= designationRepository.findAll();
    	List<Department> departmentList= departmentRepository.findAll();
    	
    	for(int i=0;i<designationList.size();i++)
    	{
    		for(int j=0;j<departmentList.size();j++)
    		{
    			try
    			{
    			if(designationList.get(i).getDepartmentId().equals(departmentList.get(j).getDepartmentId()))
    			{
    				designationList.get(i).setDepartmentId(departmentList.get(j).getDepartmentName());
    				break;
    			}
    			}catch (Exception e) {
					// TODO: handle exception
				}
    		}
    		
    	}
    	
    	model.addAttribute("departmentList",departmentList);
      	model.addAttribute("designationList", designationList);
    	
    	return "DesignationMaster";
    }

}
