package com.nonbankingfinance.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class EMICalculatorController {

	
    @RequestMapping("/EMICalculator")
    public String EMICalculator(ModelMap model)
    {
    	
    	return "EMICalculator";
    }
    
    
}
