package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.Bank;
import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.repository.BankRepository;

@Controller
@RequestMapping("/")
public class BankController {

	@Autowired
	BankRepository bankRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	String bankCode;
	private String BankCode()
	{
		long bankCount = bankRepository.count();
		
		if(bankCount<10)
		{
			bankCode = "BK000"+(bankCount+1);
		}
		else if((bankCount>=10) && (bankCount<100))
		{
			bankCode = "BK00"+(bankCount+1);
		}
		else if((bankCount>=100) && (bankCount<1000))
		{
			bankCode = "BK0"+(bankCount+1);
		}
		else if(bankCount>1000)
		{
			bankCode = "BK"+(bankCount+1);
		}
		
		return bankCode;
	} 
	
    @RequestMapping("/BankMaster")
    public String BankMaster(ModelMap model)
    {
    	List<Bank> bankList = bankRepository.findAll();
	    model.addAttribute("bankList", bankList);
    	return "BankMaster";
    }
    
    @RequestMapping("/AddBank")
    public String addBank(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	model.addAttribute("bankCode",BankCode());
        return "AddBank";
    }
  
    @RequestMapping(value = "/AddBank", method = RequestMethod.POST)
    public String AddBank(@ModelAttribute Bank bank, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		bank = new Bank(bank.getBankId(), bank.getBankName(), bank.getBankBranchName(), bank.getBankPhoneno(), bank.getBankifscCode(),
    				bank.getBankAddress(),  bank.getPinCode(),
    				bank.getCreationDate(), bank.getUpdateDate(), bank.getUserId());
    		bankRepository.save(bank);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}

    	model.addAttribute("bankCode",BankCode());
        return "AddBank";
    }
 

    @RequestMapping("/EditBank")
    public String EditBank(@RequestParam("bankId") String bankId, ModelMap model)
    {
      	
    	Query query = new Query();
    	List<Bank> bankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bankId").is(bankId)),Bank.class);
    	
    	query = new Query();
    	
    	model.addAttribute("bankDetails", bankDetails);
    	
    	return "EditBank";
    }
    
    @RequestMapping(value="/EditBank",method=RequestMethod.POST)
    public String EditBankPostMethod(@ModelAttribute Bank bank, Model model)
    {
    	try
    	{
    		bank = new Bank(bank.getBankId(), bank.getBankName(), bank.getBankBranchName(), bank.getBankPhoneno(), bank.getBankifscCode(),
    				bank.getBankAddress(), bank.getPinCode(),
    				bank.getCreationDate(), bank.getUpdateDate(), bank.getUserId());
    		bankRepository.save(bank);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}

    	List<Bank> bankList = bankRepository.findAll();
	    model.addAttribute("bankList", bankList);
    	return "BankMaster";
    }
    
}
