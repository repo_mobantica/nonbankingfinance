package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.bean.Occupation;
import com.nonbankingfinance.repository.OccupationRepository;

@Controller
@RequestMapping("/")
public class OccupationController {


	@Autowired
	OccupationRepository occupationRepository;  
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	String occupationCode;
	private String OccupationCode()
	{
		long occupationCount = occupationRepository.count();
		
		if(occupationCount<10)
		{
			occupationCode = "OC000"+(occupationCount+1);
		}
		else if((occupationCount>=10) && (occupationCount<100))
		{
			occupationCode = "OC00"+(occupationCount+1);
		}
		else if((occupationCount>=100) && (occupationCount<1000))
		{
			occupationCode = "OC0"+(occupationCount+1);
		}
		else if(occupationCount>1000)
		{
			occupationCode = "OC"+(occupationCount+1);
		}
		
		return occupationCode;
	} 
	
    @RequestMapping("/OccupationMaster")
    public String OccupationMaster(ModelMap model)
    {
    	List<Occupation> occupationList = occupationRepository.findAll();
	    model.addAttribute("occupationList", occupationList);
    	return "OccupationMaster";
    }

    @RequestMapping("/AddOccupation")
    public String AddOccupation(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	model.addAttribute("occupationCode",OccupationCode());
        return "AddOccupation";
    }
  

    @RequestMapping(value = "/AddOccupation", method = RequestMethod.POST)
    public String AddOccupation(@ModelAttribute Occupation occupation, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		occupation = new Occupation(occupation.getOccupationId(), occupation.getOccupationName(),
    				occupation.getCreationDate(), occupation.getUpdateDate(), occupation.getUserId());
    		occupationRepository.save(occupation);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}
    	
     	model.addAttribute("occupationCode",OccupationCode());
        return "AddOccupation";
    }


    @RequestMapping("/EditOccupation")
    public String EditOccupation(@RequestParam("occupationId") String occupationId, ModelMap model)
    {
      	
    	Query query = new Query();
    	List<Occupation> occupationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("occupationId").is(occupationId)),Occupation.class);
    	
    	model.addAttribute("occupationDetails",occupationDetails);
    	
    	return "EditOccupation";
    }

    @RequestMapping(value = "/EditOccupation", method = RequestMethod.POST)
    public String EditOccupation(@ModelAttribute Occupation occupation, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		occupation = new Occupation(occupation.getOccupationId(), occupation.getOccupationName(),
    				occupation.getCreationDate(), occupation.getUpdateDate(), occupation.getUserId());
    		occupationRepository.save(occupation);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}
    	

    	List<Occupation> occupationList = occupationRepository.findAll();
	    model.addAttribute("occupationList", occupationList);
    	return "OccupationMaster";
    }


    
}
