package com.nonbankingfinance.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.repository.CompanyRepository;
import com.nonbankingfinance.repository.CustomerDetailsRepository;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.GoldItemRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class CustomerEMIReportController {

	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
    GoldItemRepository goldItemRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	CustomerDetailsRepository customerDetailsRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	
	

    @RequestMapping("/CustomerEMIReport")
    public String CustomerEMIReport(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	DateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
    	Query query = new Query();
    	List<GoldLoanScheme> goldloanschemeDetails= goldloanschemeRepository.findAll();
    	query = new Query();
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
		query = new Query(); 
    	List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerLoanDetails.class);

    	String creationDate="";
    	
    	for(int i=0;i<customerLoanDetailsList.size();i++)
    	{
    		for(int j=0;j<customerDetailsList.size();j++)
    		{
    			if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
    			{
    				customerLoanDetailsList.get(i).setCustomerName(customerDetailsList.get(j).getCustomerName());
    				break;
    			}
    		}
    		
    		for(int j=0;j<goldloanschemeDetails.size();j++)
    		{
    			if(customerLoanDetailsList.get(i).getGoldloanschemeId().equalsIgnoreCase(goldloanschemeDetails.get(j).getGoldloanschemeId()))
    			{
    				customerLoanDetailsList.get(i).setGoldloanschemeId(goldloanschemeDetails.get(j).getGoldloanschemeName());
    				break;
    			}
    			
    		}

    		creationDate=createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate());
    		customerLoanDetailsList.get(i).setLoanDate(creationDate);
    		
    	}
    	
	    model.addAttribute("customerLoanDetailsList", customerLoanDetailsList);

    	return "CustomerEMIReport";
    }

    @RequestMapping("/CustomerPaidEMIReport")
    public String CustomerPaidEMIReport(@RequestParam("packetNumber") String packetNumber, ModelMap model)
    {
    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

    	Query query = new Query();
    	List<CustomerLoanDetails> customerLoanDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)),CustomerLoanDetails.class);
    	
    	query = new Query();
    	List<CustomerDetails> customerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerLoanDetails.get(0).getCustomerId())),CustomerDetails.class);
    	
    	query = new Query();
    	List<CustomerPayment> paymentcustomerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber).and("paymentStatus").ne("Canceled")),CustomerPayment.class);
    	long paidPrincipalAmount=0;
    	long paidInterestAmount=0;
    	
    	for(int i=0;i<paymentcustomerDetails.size();i++)
    	{
    		paymentcustomerDetails.get(i).setPaidDate(createDateFormat.format(paymentcustomerDetails.get(i).getCreationDate()));
    		paidPrincipalAmount=paidPrincipalAmount+paymentcustomerDetails.get(i).getPrincipalAmount();
    		paidInterestAmount=(long) (paidInterestAmount+paymentcustomerDetails.get(i).getInterestAmount());
    	}

    	String loanDate= createDateFormat.format( customerDetails.get(0).getCreationDate());
    	
    	model.addAttribute("loanDate", loanDate);
    	model.addAttribute("paidPrincipalAmount", paidPrincipalAmount);
    	model.addAttribute("paidInterestAmount", paidInterestAmount);
    	
    	model.addAttribute("paymentcustomerDetails", paymentcustomerDetails);
    	model.addAttribute("customerLoanDetails", customerLoanDetails);
    	model.addAttribute("customerDetails", customerDetails);
    	
    	return "CustomerPaidEMIReport";
    }
     
	@ResponseBody
	@RequestMapping(value="PrintCustomerPaidEMIReport")
	public ResponseEntity<byte[]> PrintCustomerPaidEMIReport(@RequestParam("packetNumber") String packetNumber, ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response )
	{
		int index=0;
		JasperPrint print;
		try 
		{	
			Query query = new Query();
			
			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			
			 jmap = null;
				

		    	String employeeName = (String)req.getSession().getAttribute("employeeName");
		    	query = new Query();
		    	List<Company> companyDetails= companyRepository.findAll();

		    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
				

		    	query = new Query();
		    	List<CustomerLoanDetails> customerLoanDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)),CustomerLoanDetails.class);
		    	
		    	query = new Query();
		    	List<CustomerDetails> customerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerLoanDetails.get(0).getCustomerId())),CustomerDetails.class);
		    	
		    	query = new Query();
		    	List<CustomerPayment> paymentcustomerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber).and("paymentStatus").ne("Canceled")),CustomerPayment.class);
		    	long paidPrincipalAmount=0;
		    	long paidInterestAmount=0;
		    	
		    	for(int i=0;i<paymentcustomerDetails.size();i++)
		    	{
		    		paymentcustomerDetails.get(i).setPaidDate(createDateFormat.format(paymentcustomerDetails.get(i).getCreationDate()));
		    		paidPrincipalAmount=paidPrincipalAmount+paymentcustomerDetails.get(i).getPrincipalAmount();
		    		paidInterestAmount=(long) (paidInterestAmount+paymentcustomerDetails.get(i).getInterestAmount());
		    	}

		    	String loanDate= createDateFormat.format( customerLoanDetails.get(0).getCreationDate());
		    	
		    	
		   //  String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";	 
		    	
				c = new ArrayList();
			 	 for(int i=0;i<paymentcustomerDetails.size();i++)
				 {
			 		    jmap = new HashMap();
						jmap.put("srno",""+(i+1));
					    //jmap.put("srno","1");
						jmap.put("principalAmount",""+paymentcustomerDetails.get(i).getPrincipalAmount());
						jmap.put("interestAmount", ""+paymentcustomerDetails.get(i).getInterestAmount());
						jmap.put("paymentDue",""+paymentcustomerDetails.get(i).getPaymentDue());
						jmap.put("paymentDescription", ""+paymentcustomerDetails.get(i).getInterestAmount());
						jmap.put("paidDate",""+paymentcustomerDetails.get(i).getPaidDate());
						
						c.add(jmap);
						
						jmap = null;
				 }
		      
			 JRDataSource dataSource = new JRMapCollectionDataSource(c);
			 Map<String, Object> parameterMap = new HashMap();
			 
			 Date date = new Date();  
			 String strDate= createDateFormat.format(date);
			 
			      parameterMap.put("customerId",""+customerDetails.get(0).getCustomerId());
				
			/*4*/ parameterMap.put("loanDate",""+loanDate);
			
			/*1*/ parameterMap.put("date", ""+strDate);

			/*2*/ parameterMap.put("customerName", ""+customerDetails.get(0).getCustomerName());
			
			/*3*/ parameterMap.put("mobileNumber",""+customerDetails.get(0).getMobileNumber());
			
			/*4*/ parameterMap.put("emailId",""+customerDetails.get(0).getEmailId());
			
			/*5*/ parameterMap.put("loanAmount",""+customerLoanDetails.get(0).getLoanAmount());
			
			/*6*/ parameterMap.put("interestRate",""+customerLoanDetails.get(0).getInterestRate());
			
			/*7*/ parameterMap.put("noOFMonth", "");
			
			/*9*/ parameterMap.put("paidPrincipalAmount", ""+paidPrincipalAmount);
			
			/*10*/parameterMap.put("paidInterestAmount", ""+paidInterestAmount);
		      
		    /*11*/parameterMap.put("userName", ""+employeeName);
			      
		    /*12*/parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());
			      
		    /*13*/parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress()+" ");
			      
			      
			 InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerPaidEMIReport.jasper");
				
				
			 print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			 ByteArrayOutputStream baos = new ByteArrayOutputStream();
			 JasperExportManager.exportReportToPdfStream(print, baos);
				
			 byte[] contents = baos.toByteArray();

			 HttpHeaders headers = new HttpHeaders();
			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
			 String filename = "CustomerPaidEMIReport.pdf";

			 JasperExportManager.exportReportToPdfStream(print, baos);

				 
			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
			 headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
						 
			 ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			 response.setHeader("Content-Disposition", "inline; filename=" + filename );
					 
			 return resp;					    
		}
		catch(Exception ex)
		{
			System.out.println(""+ex);
			//redirectAttributes.addFlashAttribute("flagemail", 0);
		}
		
				 return null;
	}
	
    
}
