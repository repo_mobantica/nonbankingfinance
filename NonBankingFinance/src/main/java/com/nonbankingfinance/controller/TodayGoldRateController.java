package com.nonbankingfinance.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.Login;
import com.nonbankingfinance.bean.TodayGoldRate;
import com.nonbankingfinance.repository.TodayGoldRateRepository;


@Controller
@RequestMapping("/")
public class TodayGoldRateController {

	
	@Autowired
	TodayGoldRateRepository todaygoldrateRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	

	String goldrateCode;
	private String TodayGoldRateCode()
	{
		long goldrateCount = todaygoldrateRepository.count();
		
		if(goldrateCount<10)
		{
			goldrateCode = "GR000"+(goldrateCount+1);
		}
		else if((goldrateCount>=10) && (goldrateCount<100))
		{
			goldrateCode = "GR00"+(goldrateCount+1);
		}
		else if((goldrateCount>=100) && (goldrateCount<1000))
		{
			goldrateCode = "GR0"+(goldrateCount+1);
		}
		else if(goldrateCount>1000)
		{
			goldrateCode = "GR"+(goldrateCount+1);
		}
		
		return goldrateCode;
	} 
	
    @RequestMapping("/GoldRateMaster")
    public String TodatGoldRateMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {

		int todayGoldRate=0;
		List<TodayGoldRate> goldRateList = todaygoldrateRepository.findAll();
		for(int i=0;i<goldRateList.size();i++)
		{
			todayGoldRate=goldRateList.get(i).getGoldRate();
		}
		HttpSession session = req.getSession(true);
		session.setAttribute("todayGoldRate",todayGoldRate);
	    model.addAttribute("goldRateList", goldRateList);
    	return "GoldRateMaster";
    }

    @RequestMapping("/AddTodayGoldRate")
    public String AddTodayGoldRate(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	model.addAttribute("goldRateId",TodayGoldRateCode());
        return "AddTodayGoldRate";
    }
  

    @RequestMapping(value = "/AddTodayGoldRate", method = RequestMethod.POST)
    public String AddTodayGoldRate(@ModelAttribute TodayGoldRate todaygoldRate, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		todaygoldRate = new TodayGoldRate(todaygoldRate.getGoldRateId(), todaygoldRate.getGoldRateBy99_1(), todaygoldRate.getGoldRate(),
    				todaygoldRate.getCreationDate(), todaygoldRate.getUpdateDate(), todaygoldRate.getUserId());
    		todaygoldrateRepository.save(todaygoldRate);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}


		int todayGoldRate=0;
		List<TodayGoldRate> goldRateList = todaygoldrateRepository.findAll();
		for(int i=0;i<goldRateList.size();i++)
		{
			todayGoldRate=goldRateList.get(i).getGoldRate();
		}
		HttpSession session = req.getSession(true);
		session.setAttribute("todayGoldRate",todayGoldRate);
	    model.addAttribute("goldRateList", goldRateList);
    	return "GoldRateMaster";
    }

    @RequestMapping("/EditTodayGoldRate")
    public String EditTodayGoldRate(@RequestParam("goldRateId") String goldRateId, ModelMap model)
    {
      	
    	Query query = new Query();
    	List<TodayGoldRate> todaygoldRateDetails = mongoTemplate.find(query.addCriteria(Criteria.where("goldRateId").is(goldRateId)),TodayGoldRate.class);
    	
    	model.addAttribute("todaygoldRateDetails", todaygoldRateDetails);
    	
    	return "EditTodayGoldRate";
    }
    
    
    
}
