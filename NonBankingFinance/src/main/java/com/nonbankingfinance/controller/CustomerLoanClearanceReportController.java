package com.nonbankingfinance.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.repository.CustomerDetailsRepository;
import com.nonbankingfinance.repository.GoldItemRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;

@Controller
@RequestMapping("/")
public class CustomerLoanClearanceReportController {

	@Autowired
	CustomerDetailsRepository customerDetailsRepository;

	@Autowired
    GoldItemRepository goldItemRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	
    @RequestMapping("/CustomerLoanClearanceReport")
    public String CustomerLoanClearanceReport(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	Query query = new Query();
    	List<GoldLoanScheme> goldloanschemeDetails= goldloanschemeRepository.findAll();
    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
    	String creationDate="";
    	String loanClearDate="";
    	/*   
    	    Date date = new Date(); 
    	    final Calendar cal = Calendar.getInstance();
    	    cal.add(Calendar.DATE, -1);
    	//cal.getTime();
   */
    	long totalPaidInterestAmount=0;
    	query = new Query();
    	List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("paymentStatus").is("Cleared")),CustomerLoanDetails.class);

    	query = new Query();
    	List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
    	
    	for(int i=0;i<customerLoanDetailsList.size();i++)
    	{
    		for(int j=0;j<customerDetailsList.size();j++)
    		{
    			if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
    			{
    				customerLoanDetailsList.get(i).setCustomerName(customerDetailsList.get(j).getCustomerName());
    				break;
    			}
    		}
    		
    		for(int j=0;j<goldloanschemeDetails.size();j++)
    		{
	    		if(customerLoanDetailsList.get(i).getGoldloanschemeId().equals(goldloanschemeDetails.get(j).getGoldloanschemeId()))
	    		{
	    			customerLoanDetailsList.get(i).setGoldloanschemeId(goldloanschemeDetails.get(j).getGoldloanschemeName());
	    			break;
	    		}
    		}

    		totalPaidInterestAmount=0;
    		query = new Query();
    		List<CustomerPayment> customerPaymentList= mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerLoanDetailsList.get(i).getCustomerId())),CustomerPayment.class);
    		for(int j=0;j<customerPaymentList.size();j++)
    		{
    			totalPaidInterestAmount=(long) (totalPaidInterestAmount+customerPaymentList.get(j).getInterestAmount());
    		}
    		
    		customerLoanDetailsList.get(i).setTotalPaidInterestAmount(totalPaidInterestAmount);
    		
    		creationDate=createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate());

    		customerLoanDetailsList.get(i).setLoanDate(creationDate);
    		loanClearDate=createDateFormat.format(customerPaymentList.get(customerPaymentList.size()-1).getCreationDate());
    		customerLoanDetailsList.get(i).setLoanClearDate(loanClearDate);
    	}
    	
	    model.addAttribute("customerLoanDetailsList", customerLoanDetailsList);

    	return "CustomerLoanClearanceReport";
    }
    
}
