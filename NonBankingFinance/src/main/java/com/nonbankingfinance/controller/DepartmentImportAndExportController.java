package com.nonbankingfinance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nonbankingfinance.bean.Department;
import com.nonbankingfinance.bean.Department;
import com.nonbankingfinance.repository.DepartmentRepository;

@Controller
@RequestMapping("/")
public class DepartmentImportAndExportController {

	@Autowired
	DepartmentRepository departmentRepository;  

	@Autowired
    ServletContext context;
	
	Department department = new Department();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;
	
	HSSFSheet worksheet;
	XSSFSheet worksheet1;
	
	@Autowired
	MongoTemplate mongoTemplate;

	String departmentCode;
	private String DepartmentCode()
	{
		long departmentCount = departmentRepository.count();
		
		if(departmentCount<10)
		{
			departmentCode = "DP000"+(departmentCount+1);
		}
		else if((departmentCount>=10) && (departmentCount<100))
		{
			departmentCode = "DP00"+(departmentCount+1);
		}
		else if((departmentCount>=100) && (departmentCount<1000))
		{
			departmentCode = "DP0"+(departmentCount+1);
		}
		else if(departmentCount>1000)
		{
			departmentCode = "DP"+(departmentCount+1);
		}
		
		return departmentCode;
	} 
	

    @RequestMapping(value="/ImportNewDepartment")
    public String importNewDepartment(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
        return "ImportNewDepartment";
    }
	
	//private static final String INTERNAL_FILE="Newdepartment.xlsx";
	@RequestMapping(value = "/DownloadDepartmentTemplate")
	public String downloadDepartmentTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{
		
		String filename = "NewDepartment.xlsx";
        response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");
 
		
		FileInputStream fileInputStream = new FileInputStream(filepath+filename);
 
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
		
		return "ImportNewDepartment";
	}
	
	

	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewDepartment", method = RequestMethod.POST)
	public String uploadDepartmentDetails(@RequestParam("department_excel") MultipartFile department_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

    	String employeeId = (String)request.getSession().getAttribute("employeeId");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		
		if (!department_excel.isEmpty() || department_excel != null )
		{
			try 
			{
				if(department_excel.getOriginalFilename().endsWith("xls") || department_excel.getOriginalFilename().endsWith("xlsx") || department_excel.getOriginalFilename().endsWith("csv"))
				{
					if(department_excel.getOriginalFilename().endsWith("xlsx"))
					{
						 InputStream stream = department_excel.getInputStream();
						 XSSFWorkbook workbook = new XSSFWorkbook(stream);
						 
						 XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet
						 
						 int firstRow = sheet.getFirstRowNum();
						 
						 XSSFRow firstrow = sheet.getRow(firstRow);
						 
						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();
						 
						@SuppressWarnings("unused")
						 Iterator<Row> rowIterator = sheet.iterator();   
						 int last_no=sheet.getLastRowNum();
						 
						 Department department = new Department();
						 for(int i=0;i<last_no;i++)
				         {
							try
							{
							 XSSFRow row = sheet.getRow(i+1);
							 
				                 	 // Skip read heading 
				                     if (row.getRowNum() == 0) 
				                     {
				                    	 continue;
				                     }
				                     
						                     department.setDepartmentId(DepartmentCode());
						                     
						                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
						                     department.setDepartmentName(row.getCell(0).getStringCellValue());
											
											 department.setCreationDate(todayDate);
											 department.setUpdateDate(todayDate);
											 department.setUserId(employeeId);
											 departmentRepository.save(department);
				         }
						 catch (Exception e) {
							// TODO: handle exception
						}       
									
				          }
				         		workbook.close();
					}
					
				}//if after inner if
				
			}
			catch(Exception e)
			{
				
			}
		}
	
		return "ImportNewDepartment";
	}
	
	
}
