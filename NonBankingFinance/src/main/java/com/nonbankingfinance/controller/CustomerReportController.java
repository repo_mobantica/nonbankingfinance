package com.nonbankingfinance.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.dto.CustomerReportDTO;
import com.nonbankingfinance.repository.CustomerLoanDetailsRepository;
import com.nonbankingfinance.services.CustomerReportServices;

@Controller
@RequestMapping("/")
public class CustomerReportController {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	CustomerLoanDetailsRepository customerloandetailsRepository;

	@Autowired
	CustomerReportServices customerReportService;

	@RequestMapping("/CustomerReport")
	public String CustomerReport(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		String branchId = (String)req.getSession().getAttribute("branchId");

		List<CustomerReportDTO> customerReportDTOList=customerReportService.GetCustomerReportList(branchId);
		model.addAttribute("customerReportDTOList", customerReportDTOList);
		return "CustomerReport";
	}

	@RequestMapping("/PendingCustomerReport")
	public String PendingCustomerReport(ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException
	{
		String branchId = (String)req.getSession().getAttribute("branchId");

		List<CustomerReportDTO> customerReportDTOList=customerReportService.GetPendingCustomerList(branchId);
		model.addAttribute("customerReportDTOList", customerReportDTOList);
		return "PendingCustomerReport";
	}

}
