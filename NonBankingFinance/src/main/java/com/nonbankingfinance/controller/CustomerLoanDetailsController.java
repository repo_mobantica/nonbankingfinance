package com.nonbankingfinance.controller;

import java.awt.Graphics;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;

import java.awt.FontMetrics;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.awt.Font;
import java.awt.Graphics2D;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.Bank;
import com.nonbankingfinance.bean.BankPaymentHistory;
import com.nonbankingfinance.bean.Branch;
import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.bean.CompanyBank;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.repository.CustomerLoanDetailsRepository;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.GoldItem;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.repository.CustomerDetailsRepository;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.EnquirySourceRepository;
import com.nonbankingfinance.repository.GoldItemRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;
import com.nonbankingfinance.repository.OccupationRepository;

@Controller
@RequestMapping("/")
public class CustomerLoanDetailsController implements Printable {


	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
    GoldItemRepository goldItemRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository; 
	@Autowired
	OccupationRepository occupationRepository;  
	@Autowired
	CustomerDetailsRepository customerDetailsRepository;
	@Autowired
	CustomerLoanDetailsRepository customerloandetailsRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	

	private String PacketCode(String customerId)
	{
		String packetNumber="";
		Query query = new Query();
    	List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerId)),CustomerLoanDetails.class);
    	
        packetNumber=customerId+"/CL";
		long customerCount = customerLoanDetailsList.size();
		
		if(customerCount<10)
		{
			packetNumber = packetNumber+"/00"+(customerCount+1);
		}
		else if((customerCount>=10) && (customerCount<100))
		{
			packetNumber = packetNumber+"/0"+(customerCount+1);
		}
		else 
		{
			packetNumber = packetNumber+"/"+(customerCount+1);
		}
		
		return packetNumber;
	} 
	
	

    @RequestMapping("/CustomerLoanMaster")
    public String CustomerLoanMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	
    	Query query = new Query();
    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
 
    	List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerLoanDetails.class);
    	query = new Query();
    	List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
    	List<GoldLoanScheme> goldLoanSchemeList = goldloanschemeRepository.findAll();
    	
    	
    	for(int i=0;i<customerLoanDetailsList.size();i++)
    	{
    		try {
	    		for(int j=0;j<customerDetailsList.size();j++)
	    		{
	    			if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
	    			{
	    				customerLoanDetailsList.get(i).setCustomerName(customerDetailsList.get(j).getCustomerName());
	    				break;
	    			}
	    			
	    		}
    		}catch (Exception e) {
				// TODO: handle exception
			}

        	for(int j=0;j<goldLoanSchemeList.size();j++)
        	{
        		if(customerLoanDetailsList.get(i).getGoldloanschemeId().equalsIgnoreCase(goldLoanSchemeList.get(j).getGoldloanschemeId()))
        		{
        			customerLoanDetailsList.get(i).setGoldloanschemeId(goldLoanSchemeList.get(j).getGoldloanschemeName());
        			break;
        		}
        	}
        	
    		customerLoanDetailsList.get(i).setLoanDate(createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate()));
    		
    	}
    	
    	
	    model.addAttribute("customerLoanDetailsList", customerLoanDetailsList);

    	return "CustomerLoanMaster";
    }
	
	
	
    @RequestMapping("/CustomerLoanDetailsMaster")
    public String CustomerLoanDetailsMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	
    	Query query = new Query();
    	List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
    	
	    model.addAttribute("customerDetailsList", customerDetailsList);

    	return "CustomerLoanDetailsMaster";
    }
	

    @RequestMapping("/AddCustomerLoanDetails")
    public String AddCustomerLoanDetails(@RequestParam("customerId") String customerId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {

    	String branchId = (String)req.getSession().getAttribute("branchId");
    	
    	Query query = new Query();
    	List<CustomerDetails> customerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerId)),CustomerDetails.class);

    	List<GoldLoanScheme> goldLoanSchemeList = goldloanschemeRepository.findAll();
    	List<GoldItem> goldItemList = goldItemRepository.findAll();    	

	    model.addAttribute("packetNumber", PacketCode(customerId));
	    model.addAttribute("goldItemList", goldItemList);
	    model.addAttribute("goldLoanSchemeList", goldLoanSchemeList);
	    model.addAttribute("customerDetails", customerDetails);
    	
    	return "AddCustomerLoanDetails";
    }
    


    @RequestMapping(value = "/AddCustomerLoanDetails", method = RequestMethod.POST)
    public String AddCustomerLoanDetails(@ModelAttribute CustomerLoanDetails customerloanDetails, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	  Query query = new Query();
    	  Date date = new Date(); 
    	//  List<CustomerDetails> customer = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerDetails.getCustomerId())),CustomerDetails.class);

  		String packetNumber=customerloanDetails.getPacketNumber();
    	try
    	{
    		CustomerLoanDetails customerloandetails = new CustomerLoanDetails();
    		customerloandetails.setPacketNumber(packetNumber);
    		customerloandetails.setCustomerId(customerloanDetails.getCustomerId());
    		
    		customerloandetails.setLoanAmount(customerloanDetails.getLoanAmount());
    		customerloandetails.setGoldloanschemeId(customerloanDetails.getGoldloanschemeId());
    		customerloandetails.setInterestRate(customerloanDetails.getInterestRate());
    	

    		customerloandetails.setProcessingFee(customerloanDetails.getProcessingFee());
    		customerloandetails.setStatus("Inprocess");
    		customerloandetails.setPaymentStatus("Remaining");
    		customerloandetails.setCreationDate(date);
    		customerloandetails.setUpdateDate(date);
    		customerloandetails.setBranchId(customerloanDetails.getBranchId());
    		customerloandetails.setUserId(customerloanDetails.getUserId());
    		
    		customerloandetailsRepository.save(customerloandetails);
	        model.addAttribute("stateStatus","Success");
	        
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}


/*
		try
		{
		BankPaymentHistory bankpaymenthistory=new BankPaymentHistory();
		
		bankpaymenthistory.setCompanyBankId(Integer.parseInt(companyBankId));
		bankpaymenthistory.setCustomerId(customerDetails.getCustomerId());
		bankpaymenthistory.setTotalAmount(customerDetails.getLoanAmount());
		bankpaymenthistory.setPaymentpourpose("Loan Amount");
		bankpaymenthistory.setPaymentStatus("Debited");
		bankpaymenthistory.setCreationDate(new Date());
		bankpaymenthistory.setBranchId(branchId);
		bankpaymenthistory.setUserId(customerDetails.getUserId());
		bankpaymenthistoryRepository.save(bankpaymenthistory);
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		*/
    	/*
		try
		{
		long totalBankAmount=0;
		query = new Query();
    	List<BankPaymentDetails> bankpaymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId))),BankPaymentDetails.class);
    	
    	if(bankpaymentDetails.size()!=0)
    	{
    		totalBankAmount=bankpaymentDetails.get(0).getTotalAmount()-customerDetails.getLoanAmount();
    		bankpaymentDetails.get(0).setTotalAmount(totalBankAmount);
    		bankpaymentdetailsRepository.save(bankpaymentDetails);
    	}
		}
		catch (Exception e) {
			System.out.println("eoor  = "+e);
			// TODO: handle exception
		}
		*/
    	
    	
    	
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	query = new Query();

    	List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
    	
	    model.addAttribute("customerDetailsList", customerDetailsList);

    	PrintCustomerLoanDetails1(packetNumber, model,req,res);
    	
    	return "CustomerLoanDetailsMaster";
    }
    
    
   
	 
	 
		static String packetNumber;
		static CustomerLoanDetails customerLoanDetails;
	    static CustomerDetails customerDetails;
	    static List<CustomerGoldItemDetails> customergolditemdetailsList=new ArrayList<CustomerGoldItemDetails>();
	    
	    static String customerAddress1;
	    static String customerAddress2;
	    static String schemeName;
	    
		@ResponseBody
		@RequestMapping(value="PrintCustomerLoanDetails")
		public ResponseEntity<byte[]> PrintCustomerLoanDetails(@RequestParam("packetNumber") String packetNumber, Model model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response )
		{

			PrintCustomerLoanDetails1(packetNumber, model,req,res);
			
			 return null;
		}
		void PrintCustomerLoanDetails1(String packetNumber, Model model, HttpServletRequest req, HttpServletResponse res)
		{
			this.packetNumber=packetNumber;
			try 
			{	
		    	Query query = new Query();
		    	CustomerLoanDetails customerLoanDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)),CustomerLoanDetails.class);
		    	
		    	query = new Query();
		    	CustomerDetails customerDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("customerId").is(customerLoanDetails.getCustomerId())),CustomerDetails.class);
		    	query = new Query();
			    GoldLoanScheme goldloanschemeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("goldloanschemeId").is(customerLoanDetails.getGoldloanschemeId())),GoldLoanScheme.class);
		    	query = new Query();
			    List<CustomerGoldItemDetails> customergolditemdetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)), CustomerGoldItemDetails.class);
			    /*
			    query = new Query();
		    	Village customerVillageDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("villageId").is(customerDetails.getVillageId())),Village.class);

		    	query = new Query();
		    	Tahsil customerTahsilDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("tahsilId").is(customerDetails.getTahsilId())),Tahsil.class);

		    	query = new Query();
		    	District customerDistrictDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("districtId").is(customerDetails.getDistrictId())),District.class);
*/

		       // String customerAddress1=customerDetails.getCustomerAddress()+", Village Name : "+customerVillageDetails.getVillageName();
		     //   String customerAddress2="Tahsil Name : "+customerTahsilDetails.getTahsilName()+", District Name : "+customerDistrictDetails.getDistrictName();
		        
		    	this.customerLoanDetails=customerLoanDetails;
		    	this.customerDetails=customerDetails;
		    	this.schemeName=goldloanschemeDetails.getGoldloanschemeName();
		    	this.customergolditemdetailsList=customergolditemdetailsList;
		    	this.customerAddress1=customerAddress1;
		    	this.customerAddress2=customerAddress2;
		    	
			        PrinterJob pj = PrinterJob.getPrinterJob();        
			            pj.setPrintable(new CustomerLoanDetailsController(), getPageFormat(pj));
			             try {
			                  pj.print();
			               
			             }
			              catch (PrinterException ex) {
			            	  
			            	  System.out.println("ERRE = "+ex);
			                      //ex.printStackTrace();
			             }
			             
			}
			catch(Exception ex)
			{
				System.out.println(""+ex);
			}
			
		}
		

	public PageFormat getPageFormat(PrinterJob pj)
	{
	    PageFormat pf = pj.defaultPage();
	    Paper paper = pf.getPaper();    

	    double middleHeight =32.0;  
	    double headerHeight = 2.0;                  
	    double footerHeight = 2.0;                  
	    double width = convert_CM_To_PPI(25);      //printer know only point per inch.default value is 72ppi
	    double height = convert_CM_To_PPI(headerHeight+middleHeight+footerHeight); 
	    paper.setSize(width, height);
	    paper.setImageableArea(                    
	        0,
	        20,
	        width,            
	        height - convert_CM_To_PPI(1)
	    );   //define boarder size    after that print area width is about 180 points
	            
	    pf.setOrientation(PageFormat.PORTRAIT);           //select orientation portrait or landscape but for this time portrait
	    pf.setPaper(paper);    

	    return pf;
	}
	    
	protected static double convert_CM_To_PPI(double cm) {            
		        return toPPI(cm * 0.393600787);            
	}
	 
	protected static double toPPI(double inch) {            
		        return inch * 72d;            
	}
		
	    
		public int print(Graphics graphics, PageFormat pageFormat,int pageIndex) 
		  {    
		      
		      int result = NO_SUCH_PAGE;    
		        if (pageIndex == 0) {                    
		        
		            Graphics2D g2d = (Graphics2D) graphics;                    

		            double width = pageFormat.getImageableWidth();                    
		           
		            g2d.translate((int) pageFormat.getImageableX(),(int) pageFormat.getImageableY()); 

		            ////////// code by alqama//////////////

		            FontMetrics metrics=g2d.getFontMetrics(new Font("Arial",Font.BOLD,7));
		        //    int idLength=metrics.stringWidth("000000");
		            //int idLength=metrics.stringWidth("00");
		            int idLength=metrics.stringWidth("000");
		            //System.out.println(idLength);
		            int amtLength=metrics.stringWidth("000000");
		            int qtyLength=metrics.stringWidth("00000");
		            int priceLength=metrics.stringWidth("000000");
		            int prodLength=(int)width - idLength - amtLength - qtyLength - priceLength-17;
		              
		        try{
		        	
		            /*Draw Header*/
		            int y=450;
		            int yShift = 18;
		            int headerRectHeight=15;
		            int headerRectHeighta=40;
		           
		          //   g2d.setFont(new Font("Monospaced",Font.PLAIN,10));
		             
		            g2d.setFont(new Font("Times New Roman",Font.PLAIN,14));

		            //customerDetails
		            g2d.drawString("Customer Id ",40,y);      g2d.drawString(":",150,y);	 g2d.drawString(" "+customerLoanDetails.getCustomerId()+"",160,y);   g2d.drawString("Packet Number : ",310,y);  g2d.drawString("  "+customerLoanDetails.getPacketNumber()+"",400,y);  y+=yShift;
		            g2d.drawString("Customer Name ",40,y);    g2d.drawString(":",150,y);	 g2d.drawString(" "+customerDetails.getCustomerName()+"",160,y);  y+=yShift;
		            g2d.drawString("Customer Address",40,y);  g2d.drawString(":",150,y);     g2d.drawString(" "+customerAddress1+"",160,y);  y+=yShift;
		            																		 g2d.drawString(" "+customerAddress2+"",160,y);  y+=yShift;
		            g2d.drawString("Scheme Name ",40,y);      g2d.drawString(":",150,y);	 g2d.drawString(" "+schemeName+"",160,y);  y+=yShift;
		            g2d.drawString("Loan Amount ",40,y);      g2d.drawString(":",150,y);     g2d.drawString(" "+customerLoanDetails.getLoanAmount()+"",160,y);   g2d.drawString("Interest %    : ",240,y);  g2d.drawString(" "+customerLoanDetails.getInterestRate()+"",310,y);  y+=yShift;
		            
		            y+=130;
		            for(int i=0;i<customergolditemdetailsList.size();i++)
		            {
		            	  g2d.drawString(" "+customergolditemdetailsList.get(i).getGoldItemName()+"",40,y);   g2d.drawString(" "+customergolditemdetailsList.get(i).getItemQty()+"",420,y);   g2d.drawString(" "+customergolditemdetailsList.get(i).getItemGrossWeight()+"",490,y);   g2d.drawString(" "+customergolditemdetailsList.get(i).getItemWeight()+"",600,y);   y+=yShift;
		            	
		            }
		            
		    }
		    catch(Exception r){
		        //System.out.println(r);
		   // r.printStackTrace();
		    }

		     result = PAGE_EXISTS;    
		   }    
		 return result;    
	    }
		  
		
		
	    @RequestMapping("/EditCustomerLoanDetails")
	    public String EditCustomerLoanDetails(@RequestParam("packetNumber") String packetNumber, ModelMap model)
	    {

	    	Query query = new Query();
	    	List<CustomerLoanDetails> customerLoanDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)),CustomerLoanDetails.class);
	    	
	    	query = new Query();
	    	List<CustomerDetails> customerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerLoanDetails.get(0).getCustomerId())),CustomerDetails.class);

	    	query = new Query();
	    	List<CustomerGoldItemDetails> customerGoldItemDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)),CustomerGoldItemDetails.class);

	    	double totalItemMarketValue=0;
	    	for(int i=0;i<customerGoldItemDetailsList.size();i++)
	    	{
	    		totalItemMarketValue=totalItemMarketValue+customerGoldItemDetailsList.get(i).getItemMarketValue();
	    	}
	    	
	    	List<GoldLoanScheme> goldLoanSchemeList = goldloanschemeRepository.findAll();
	    	
	    /*	query = new Query();
	    	District districtDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("districtId").is(customerDetails.get(0).getDistrictId())),District.class);
	    	query = new Query();
	    	Tahsil tahsilDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("tahsilId").is(customerDetails.get(0).getTahsilId())),Tahsil.class);
	    	query = new Query();
	    	Village villageDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("villageId").is(customerDetails.get(0).getVillageId())),Village.class);
		      */
	    	query = new Query();
	    	GoldLoanScheme goldloanschemeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("goldloanschemeId").is(customerLoanDetails.get(0).getGoldloanschemeId())),GoldLoanScheme.class);
		      
	    /*	model.addAttribute("districtName",districtDetails.getDistrictName());
	    	model.addAttribute("tahsilName",tahsilDetails.getTahsilName());
	    	model.addAttribute("villageName",villageDetails.getVillageName());
	    */	
	    	model.addAttribute("totalNumberOfGoldItems", customerGoldItemDetailsList.size());
	    	model.addAttribute("totalItemMarketValue", totalItemMarketValue);
	    	model.addAttribute("goldloanschemeDetails", goldloanschemeDetails);
	    	model.addAttribute("goldLoanSchemeList", goldLoanSchemeList);
	    	model.addAttribute("customerGoldItemDetailsList", customerGoldItemDetailsList);
	    	model.addAttribute("customerLoanDetails", customerLoanDetails);
	    	model.addAttribute("customerDetails", customerDetails);
	    	
	    	return "EditCustomerLoanDetails";
	    }
	    

	    @RequestMapping(value = "/EditCustomerLoanDetails", method = RequestMethod.POST)
	    public String EditCustomerLoanDetails(@ModelAttribute CustomerLoanDetails customerloanDetails, Model model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	Query query = new Query();
	    	Date date = new Date(); 
	    	CustomerLoanDetails customerLoan = mongoTemplate.findOne(query.addCriteria(Criteria.where("packetNumber").is(customerloanDetails.getPacketNumber())),CustomerLoanDetails.class);

	  		String packetNumber=customerloanDetails.getPacketNumber();
	    	try
	    	{
	    		CustomerLoanDetails customerloandetails = new CustomerLoanDetails();
	    		customerloandetails.setPacketNumber(packetNumber);
	    		customerloandetails.setCustomerId(customerloanDetails.getCustomerId());
	    		
	    		customerloandetails.setLoanAmount(customerloanDetails.getLoanAmount());
	    		customerloandetails.setGoldloanschemeId(customerloanDetails.getGoldloanschemeId());
	    		customerloandetails.setInterestRate(customerloanDetails.getInterestRate());



	    		customerloandetails.setProcessingFee(customerloanDetails.getProcessingFee());
	    		customerloandetails.setStatus(customerloanDetails.getStatus());
	    		customerloandetails.setPaymentStatus(customerloanDetails.getPaymentStatus());
	    		customerloandetails.setCreationDate(customerLoan.getCreationDate());
	    		customerloandetails.setUpdateDate(date);
	    		customerloandetails.setBranchId(customerloanDetails.getBranchId());
	    		customerloandetails.setUserId(customerloanDetails.getUserId());
	    		
	    		customerloandetailsRepository.save(customerloandetails);
		        model.addAttribute("stateStatus","Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("stateStatus","Fail");
	    	}



	    	String branchId = (String)req.getSession().getAttribute("branchId");
	    	
	    	query = new Query();
	    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
	 
	    	List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerLoanDetails.class);
	    	query = new Query();
	    	List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
	    	List<GoldLoanScheme> goldLoanSchemeList = goldloanschemeRepository.findAll();
	    	
	    	
	    	for(int i=0;i<customerLoanDetailsList.size();i++)
	    	{
	    		try {
		    		for(int j=0;j<customerDetailsList.size();j++)
		    		{
		    			if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
		    			{
		    				customerLoanDetailsList.get(i).setCustomerName(customerDetailsList.get(j).getCustomerName());
		    				break;
		    			}
		    			
		    		}
	    		}catch (Exception e) {
					// TODO: handle exception
				}

	        	for(int j=0;j<goldLoanSchemeList.size();j++)
	        	{
	        		if(customerLoanDetailsList.get(i).getGoldloanschemeId().equalsIgnoreCase(goldLoanSchemeList.get(j).getGoldloanschemeId()))
	        		{
	        			customerLoanDetailsList.get(i).setGoldloanschemeId(goldLoanSchemeList.get(j).getGoldloanschemeName());
	        			break;
	        		}
	        	}
	        	
	    		customerLoanDetailsList.get(i).setLoanDate(createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate()));
	    		
	    	}
	    	
	    	
		    model.addAttribute("customerLoanDetailsList", customerLoanDetailsList);

	    	return "CustomerLoanMaster";
	    }
	    
	    
	    
		
	    
	    
}
