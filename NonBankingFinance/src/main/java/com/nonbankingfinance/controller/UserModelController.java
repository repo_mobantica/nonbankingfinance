package com.nonbankingfinance.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.repository.UserModelRepository;
import com.nonbankingfinance.services.EmailService;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.bean.Employee;
import com.nonbankingfinance.bean.EnquirySource;
import com.nonbankingfinance.bean.Login;
import com.nonbankingfinance.bean.Occupation;
import com.nonbankingfinance.bean.UserModel;
import com.nonbankingfinance.repository.EmployeeRepository;
import com.nonbankingfinance.repository.LoginRepository;

@Controller
@RequestMapping("/")
public class UserModelController extends EmailService {

@Autowired
UserModelRepository usermodelRepository;
@Autowired
EmployeeRepository employeeRepository;
@Autowired
LoginRepository loginRepository;
@Autowired
MongoTemplate mongoTemplate;


protected String generatePassword()
{
    String RandChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    StringBuilder pass = new StringBuilder();
    Random rnd = new Random();
    
    while ( pass.length()<=8 ) 
    { 
      int index = (int) (rnd.nextFloat() * RandChar.length());
      pass.append(RandChar.charAt(index));
    }
    
    String passwordString = pass.toString();
    
    return passwordString;
}
String userCode;
private String UserCode()
{
	long userCount = usermodelRepository.count();
	
	if(userCount<10)
	{
		userCode = "UM000"+(userCount+1);
	}
	else if((userCount>=10) && (userCount<100))
	{
		userCode = "UM00"+(userCount+1);
	}
	else if((userCount>=100) && (userCount<1000))
	{
		userCode = "UM0"+(userCount+1);
	}
	else if(userCount>1000)
	{
		userCode = "UM"+(userCount+1);
	}
	
	return userCode;
} 

String userId = "";
public String UserId()
{
	   long count = loginRepository.count();
		 
	   if(count<10)
	   {
		   userId = "U000"+(count+1);	 
	   }
	   else if(count>=10 && count<100)
	   {
		   userId = "U00"+(count+1);
	   }
	   else if(count>=100 && count<1000)
	   {
		   userId = "U0"+(count+1);
	   }
	   else
	   {
		   userId = "U"+(count+1);
	   }
		 
	     return userId;
}


    @RequestMapping("/UserMaster")
    public String UserMaster(ModelMap model)
    {
    	List<UserModel> userList = usermodelRepository.findAll();
    	List<Employee> employeeList = employeeRepository.findAll();
    	
    	for(int i=0;i<userList.size();i++)
    	{
    		for(int j=0;j<employeeList.size();j++)
    		{
    			if(userList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
    			{
    				userList.get(i).setEmployeeId(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeelastName());
    				break;
    			}
    			
    		}
    	}
    	
	    model.addAttribute("userList", userList);
    	return "UserMaster";
    }
    
    @RequestMapping("/AddUserModel")
    public String AddUserModel(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
 	   int f=0;
 	   Query query = new Query();
    	//List<Employee> employeeList= employeeRepository.findAll();

	   List<Login> loginDetails = loginRepository.findAll();
 	   List<Employee> employeeList1 = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), Employee.class);
 	
 	  List<Employee> employeeList = new ArrayList<Employee>();
 	  Employee employee = new Employee();
	   
	   for(int i=0;i<employeeList1.size();i++)
	   {
		   f=0;
		   employee = new Employee();
		   for(int j=0;j<loginDetails.size();j++)
		   {
			   if(employeeList1.get(i).getEmployeeId().equals(loginDetails.get(j).getEmployeeId()))
			   {
				   f=1;
				   break;
			   }
		   }
		   
		   if(f==0)
		   {
			   employee.setEmployeeId(employeeList1.get(i).getEmployeeId());
			   employee.setEmployeefirstName(employeeList1.get(i).getEmployeefirstName());
			   employee.setEmployeemiddleName(employeeList1.get(i).getEmployeemiddleName());
			   employee.setEmployeelastName(employeeList1.get(i).getEmployeelastName());
			   
			   employeeList.add(employee);
		   }
	   }
 	   
    	model.addAttribute("employeeList",employeeList);
    	model.addAttribute("userCode",UserCode());
        return "AddUserModel";
    }

    @RequestMapping(value = "/AddUserModel", method = RequestMethod.POST)
    public String AddUserModel(@ModelAttribute UserModel userModel, Model model, HttpServletRequest req, HttpServletResponse res)
    {

 	   Login loginUser = new Login();
  	  Date date = new Date(); 
    	try
    	{
    		UserModel usermodel = new UserModel();
    		usermodel.setUserModelId(userModel.getUserModelId());
    		usermodel.setEmployeeId(userModel.getEmployeeId());
    		usermodel.setUserStatus("Active");
    		
    		usermodel.setCreationDate(date);
    		usermodel.setUpdateDate(date);
    		usermodel.setUserId(usermodel.getUserId());
    		
    		usermodelRepository.save(usermodel);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}
    	catch (Exception e) {
			// TODO: handle exception
		}
		 Query query = new Query();
		 
		 try
		 {
			Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(userModel.getEmployeeId())), Employee.class);

			String emilId = employeeDetails.getEmployeeEmailId();
			 loginUser = new Login();
			 
			 loginUser.setUserId(UserId());
			 loginUser.setUserName(emilId);
			 String pass = generatePassword();
			 loginUser.setPassWord(pass);
			 loginUser.setEmployeeId(userModel.getEmployeeId());
			 loginUser.setStatus("Active");
			 
			 loginRepository.save(loginUser);
			 
			String Msgbody1 = "Hi, "+employeeDetails.getEmployeefirstName()+" "+employeeDetails.getEmployeelastName();
			String Msgbody2 = "\nYour account registered successfully. You can now login to non banking Finance/";
			String Msgbody3 = "\nYou can use further login Credentials : ";
			String Msgbody4 =  "\nUsername : "+emilId+"\n Password : "+pass;
			
			
		    sendLoginCredentialsMail(emilId,"Login Credentials", Msgbody1+"\n\t"+Msgbody2+"\n"+Msgbody3+"\n"+Msgbody4+" \n\n\t Please, do not reply to the mail.");
		 }
		 catch(Exception e)
		 {
			 System.out.println("Mail Sending Exception = "+e.toString());
		 }
		 

	    	List<UserModel> userList = usermodelRepository.findAll();
	    	List<Employee> employeeList = employeeRepository.findAll();
	    	
	    	for(int i=0;i<userList.size();i++)
	    	{
	    		for(int j=0;j<employeeList.size();j++)
	    		{
	    			if(userList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
	    			{
	    				userList.get(i).setEmployeeId(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeelastName());
	    				break;
	    			}
	    			
	    		}
	    	}
	    	
	    model.addAttribute("userList", userList);
        return "UserMaster";
    }
 
    

    @RequestMapping("/EditUserModel")
    public String EditUserModel(@RequestParam("userModelId") String userModelId, ModelMap model)
    {
      	
    	Query query = new Query();
    	UserModel userDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userModelId").is(userModelId)),UserModel.class);
    try
    {
    	query = new Query();
    	Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(userDetails.getEmployeeId())),Employee.class);
    	
    	model.addAttribute("employeeName", employeeDetails.getEmployeefirstName()+" "+employeeDetails.getEmployeelastName());
    }
    catch (Exception e) {
		// TODO: handle exception
	}
    	model.addAttribute("userDetails", userDetails);
    	
    	return "EditUserModel";
    }

    @RequestMapping(value = "/EditUserModel", method = RequestMethod.POST)
    public String EditUserModel(@ModelAttribute UserModel userModel, Model model, HttpServletRequest req, HttpServletResponse res)
    {

  	  Date date = new Date(); 

		Query query = new Query();
    	try
    	{
    		query = new Query();
    		UserModel userDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userModelId").is(userModel.getUserModelId())),UserModel.class);
        	
    		userDetails.setUserStatus(userModel.getUserStatus());
    		userDetails.setUpdateDate(date);
    		
    		usermodelRepository.save(userDetails);
    	}
    	catch(Exception de)
    	{
    	}
    	
    	try
    	{
    	query = new Query();
    	Login loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(userModel.getEmployeeId())),Login.class);
    	
    	loginDetails.setStatus(userModel.getUserStatus());
		 
		 loginRepository.save(loginDetails);
    	}
    	catch (Exception e) {
			// TODO: handle exception
		}

    	List<UserModel> userList = usermodelRepository.findAll();
    	List<Employee> employeeList = employeeRepository.findAll();
    	
    	for(int i=0;i<userList.size();i++)
    	{
    		for(int j=0;j<employeeList.size();j++)
    		{
    			if(userList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
    			{
    				userList.get(i).setEmployeeId(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeelastName());
    				break;
    			}
    			
    		}
    	}
    	
	    model.addAttribute("userList", userList);
        return "UserMaster";
    }
 
}
