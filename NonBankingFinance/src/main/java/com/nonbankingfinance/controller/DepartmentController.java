package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.Department;
import com.nonbankingfinance.repository.DepartmentRepository;

@Controller
@RequestMapping("/")
public class DepartmentController {

	@Autowired
	DepartmentRepository departmentRepository;  
	@Autowired
	MongoTemplate mongoTemplate;
	

	String departmentCode;
	private String DepartmentCode()
	{
		long departmentCount = departmentRepository.count();
		
		if(departmentCount<10)
		{
			departmentCode = "DP000"+(departmentCount+1);
		}
		else if((departmentCount>=10) && (departmentCount<100))
		{
			departmentCode = "DP00"+(departmentCount+1);
		}
		else if((departmentCount>=100) && (departmentCount<1000))
		{
			departmentCode = "DP0"+(departmentCount+1);
		}
		else if(departmentCount>1000)
		{
			departmentCode = "DP"+(departmentCount+1);
		}
		
		return departmentCode;
	} 
	
    @RequestMapping("/DepartmentMaster")
    public String DepartmentMaster(ModelMap model)
    {
    	List<Department> departmentList = departmentRepository.findAll();
	    model.addAttribute("departmentList", departmentList);
    	return "DepartmentMaster";
    }

    @RequestMapping("/AddDepartment")
    public String AddDepartment(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	model.addAttribute("departmentCode",DepartmentCode());
        return "AddDepartment";
    }

    @RequestMapping(value = "/AddDepartment", method = RequestMethod.POST)
    public String AddDepartment(@ModelAttribute Department department, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		department = new Department(department.getDepartmentId(), department.getDepartmentName(),
    				department.getCreationDate(), department.getUpdateDate(), department.getUserId());
    		departmentRepository.save(department);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}
    	
     	model.addAttribute("departmentCode",DepartmentCode());
        return "AddDepartment";
    }


    @RequestMapping("/EditDepartment")
    public String EditDepartment(@RequestParam("departmentId") String departmentId, ModelMap model)
    {
      	
    	Query query = new Query();
    	List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(departmentId)),Department.class);
    	
    	model.addAttribute("departmentDetails",departmentDetails);
    	
    	return "EditDepartment";
    }

    @RequestMapping(value = "/EditDepartment", method = RequestMethod.POST)
    public String EditDepartment(@ModelAttribute Department department, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		department = new Department(department.getDepartmentId(), department.getDepartmentName(),
    				department.getCreationDate(), department.getUpdateDate(), department.getUserId());
    		departmentRepository.save(department);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}
    	

    	List<Department> departmentList = departmentRepository.findAll();
	    model.addAttribute("departmentList", departmentList);
    	return "DepartmentMaster";
    }

}
