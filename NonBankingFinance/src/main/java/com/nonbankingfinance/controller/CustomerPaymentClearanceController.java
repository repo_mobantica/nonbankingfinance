package com.nonbankingfinance.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.Bank;
import com.nonbankingfinance.bean.BankPaymentDetails;
import com.nonbankingfinance.bean.BankPaymentHistory;
import com.nonbankingfinance.bean.CompanyBank;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.bean.CustomerPaymentStatusHistory;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.CustomerLoanDetailsRepository;
import com.nonbankingfinance.repository.CustomerPaymentRepository;
import com.nonbankingfinance.repository.BankRepository;
import com.nonbankingfinance.repository.CompanyBankRepository;
import com.nonbankingfinance.repository.CustomerDetailsRepository;
import com.nonbankingfinance.repository.CustomerPaymentStatusHistoryRepository;
import com.nonbankingfinance.repository.BankPaymentDetailsRepository;
import com.nonbankingfinance.repository.BankPaymentHistoryRepository;

@Controller
@RequestMapping("/")
public class CustomerPaymentClearanceController {

	@Autowired
	CustomerLoanDetailsRepository customerloandetailsRepository;
	@Autowired
	BankPaymentHistoryRepository bankpaymenthistoryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
	CustomerDetailsRepository customerdetailsRepository;
	@Autowired
	CustomerPaymentRepository customerPaymentRepository; 

	@Autowired
	CustomerPaymentStatusHistoryRepository customerpaymentstatushistoryRepository;
	@Autowired
	CompanyBankRepository companybankRepository;
	@Autowired
	BankPaymentDetailsRepository bankpaymentdetailsRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	
	
    @RequestMapping("/CustomerPaymentClearanceMaster")
    public String CustomerPaymentClearanceMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {

     	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	Query query = new Query();
    	List<CustomerDetails> customerDetails= customerdetailsRepository.findAll();
  
    	List<CustomerPayment> customerPaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("paymentStatus").is("Uncleared")),CustomerPayment.class);
   
    	String customerName="";
    	long totalAmount=0;
    	for(int i=0;i<customerPaymentList.size();i++)
    	{
    		totalAmount=0;
    		for(int j=0;j<customerDetails.size();j++)
    		{
    			if(customerPaymentList.get(i).getCustomerId().equals(customerDetails.get(j).getCustomerId()))
    			{
    				customerName=customerDetails.get(j).getCustomerName();
    				customerPaymentList.get(i).setCustomerName(customerName);
    			}
    		}
    		totalAmount=(long) (customerPaymentList.get(i).getPrincipalAmount()+customerPaymentList.get(i).getInterestAmount()+customerPaymentList.get(i).getPaymentDue());
    		customerPaymentList.get(i).setTotalAmount(totalAmount);
    		customerPaymentList.get(i).setPaidDate(createDateFormat.format(customerPaymentList.get(i).getCreationDate()));
    	}
    	
    	
	    model.addAttribute("customerPaymentList", customerPaymentList);

    	return "CustomerPaymentClearanceMaster";
    }

    @RequestMapping("/UpdateCustomerPaymentStatus")
    public String UpdateCustomerPaymentStatus(@RequestParam("customerPaymentId") String customerPaymentId, ModelMap model) throws ParseException
    {
     	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
     	List<CompanyBank> CompanybankList = companybankRepository.findAll();

     	List<Bank> bankList = bankRepository.findAll();
     	
     	for(int i=0;i<CompanybankList.size();i++)
     	{
     		for(int j=0;j<bankList.size();j++)
     		{
     			if(CompanybankList.get(i).getBankId().equals(bankList.get(j).getBankId()))
     			{
     				CompanybankList.get(i).setBankName(bankList.get(j).getBankName());
     			}
     		}
     		
     	}
     	
    	long totalAmount=0;
    	Query query = new Query();
    	List<CustomerPayment> paymentcustomerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("customerPaymentId").is(customerPaymentId)),CustomerPayment.class);
    
    	query = new Query();
    	List<CustomerDetails> customerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(paymentcustomerDetails.get(0).getCustomerId())),CustomerDetails.class);

    	String customerLoanDate=createDateFormat.format(customerDetails.get(0).getCreationDate());
    	totalAmount=(long) (paymentcustomerDetails.get(0).getPrincipalAmount()+paymentcustomerDetails.get(0).getInterestAmount()+paymentcustomerDetails.get(0).getPaymentDue());
    	
    	
	    model.addAttribute("CompanybankList", CompanybankList);
    	model.addAttribute("totalAmount", totalAmount);
    	model.addAttribute("customerLoanDate", customerLoanDate);
    	model.addAttribute("customerDetails", customerDetails);
    	model.addAttribute("paymentcustomerDetails", paymentcustomerDetails);
    	
    	return "UpdateCustomerPaymentStatus";
    }

    @RequestMapping(value="/UpdateCustomerPaymentStatus", method = RequestMethod.POST)
    public String UpdateCustomerPaymentStatus(@RequestParam("packetNumber") String packetNumber, @RequestParam("customerPaymentId") String customerPaymentId, @RequestParam("customerId") String customerId, @RequestParam("companyBankId") String companyBankId, @RequestParam("paymentStatus") String paymentStatus, @RequestParam("clearanceDate") String clearanceDate, @RequestParam("totalAmount") long totalAmount, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	Query query = new Query();
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	String userId = (String)req.getSession().getAttribute("userId");

    	Date date=new Date();
    	try
    	{
    	
    		CustomerPaymentStatusHistory customerpayment=new CustomerPaymentStatusHistory();
    		//customerpayment.setHistoryId("");
    		customerpayment.setCustomerPaymentId(customerPaymentId);
    		customerpayment.setPacketNumber(packetNumber);
    		customerpayment.setCustomerId(customerId);
    		customerpayment.setCompanyBankId(companyBankId);
    		customerpayment.setPaymentStatus(paymentStatus);
    		customerpayment.setClearanceDate(clearanceDate);
    		customerpayment.setTotalAmount(totalAmount);
	    		
	    	customerpayment.setCreationDate(date);
	    	customerpayment.setUserId(userId);
	    	customerpayment.setBranchId(branchId);
    	
	    	customerpaymentstatushistoryRepository.save(customerpayment);
    	
    	CustomerPayment	customerpayment1 = mongoTemplate.findOne(Query.query(Criteria.where("customerPaymentId").is(customerPaymentId)), CustomerPayment.class);
    	customerpayment1.setPaymentStatus(paymentStatus);
    	customerPaymentRepository.save(customerpayment1);
    	
    	//CompanyBank	companybankDetails = mongoTemplate.findOne(Query.query(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId))), CompanyBank.class);
    	//String companyBankId=companybankDetails.getBankId();
    	if(paymentStatus.equals("Cleared"))
    	{
    		try
    		{
    		BankPaymentHistory bankpaymenthistory=new BankPaymentHistory();
    		
    		bankpaymenthistory.setCompanyBankId(Integer.parseInt(companyBankId));
    		bankpaymenthistory.setCustomerId(customerId);
    		bankpaymenthistory.setPacketNumber(packetNumber);
    		bankpaymenthistory.setTotalAmount(totalAmount);
    		bankpaymenthistory.setPaymentpourpose("EMI Paid");
    		bankpaymenthistory.setPaymentStatus("Credited");
    		bankpaymenthistory.setCreationDate(new Date());
    		bankpaymenthistory.setBranchId(branchId);
    		bankpaymenthistory.setUserId(userId);
    		bankpaymenthistoryRepository.save(bankpaymenthistory);
    		}
    		catch (Exception e) {
				// TODO: handle exception
			}
    		
    		try
    		{
    		long totalBankAmount=0;
    		query = new Query();
        	List<BankPaymentDetails> bankpaymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId))),BankPaymentDetails.class);
        	
        	if(bankpaymentDetails.size()!=0)
        	{
        		totalBankAmount=bankpaymentDetails.get(0).getTotalAmount()+totalAmount;
        		bankpaymentDetails.get(0).setTotalAmount(totalBankAmount);
        		bankpaymentdetailsRepository.save(bankpaymentDetails);
        	}
        	else
        	{
        		BankPaymentDetails bankpaymentdetails=new BankPaymentDetails();
        		
        		bankpaymentdetails.setCompanyBankId(companyBankId);
        		bankpaymentdetails.setTotalAmount(totalAmount);
        		bankpaymentdetailsRepository.save(bankpaymentdetails);
        	}
    		}
    		catch (Exception e) {
    			System.out.println("eoor  = "+e);
				// TODO: handle exception
			}
    		
    	}
    	
    	}
    	catch (Exception e) {
    		
    		System.out.println("eoore  = "+e);
			// TODO: handle exception
		}
    	
    	query = new Query();
    	CustomerLoanDetails	customerLoanDetails = mongoTemplate.findOne(Query.query(Criteria.where("packetNumber").is(packetNumber)), CustomerLoanDetails.class);
    	
    	long customerLoanAmount=customerLoanDetails.getLoanAmount();
    	long customerPaidPrincipalAmount=0;
    	query = new Query();
    	List<CustomerPayment> paymentcustomerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber).and("paymentStatus").is("Cleared")),CustomerPayment.class);
    	
    	for(int i=0;i<paymentcustomerDetails.size();i++)
    	{
    		customerPaidPrincipalAmount=customerPaidPrincipalAmount+paymentcustomerDetails.get(i).getPrincipalAmount();
    	}
    	if(customerLoanAmount<=customerPaidPrincipalAmount)
    	{
    		customerLoanDetails.setPaymentStatus("Cleared");
    		customerloandetailsRepository.save(customerLoanDetails);
    		
    	}
    	
    	
    	
     	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		
    	query = new Query();
    	List<CustomerDetails> customerDetails= customerdetailsRepository.findAll();
  
    	List<CustomerPayment> customerPaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("paymentStatus").is("Uncleared")),CustomerPayment.class);
   
    	String customerName="";
    	long totalAmount1=0;
    	for(int i=0;i<customerPaymentList.size();i++)
    	{
    		totalAmount=0;
    		for(int j=0;j<customerDetails.size();j++)
    		{
    			if(customerPaymentList.get(i).getCustomerId().equals(customerDetails.get(j).getCustomerId()))
    			{
    				customerName=customerDetails.get(j).getCustomerName();
    				customerPaymentList.get(i).setCustomerName(customerName);
    			}
    		}
    		totalAmount1=(long) (customerPaymentList.get(i).getPrincipalAmount()+customerPaymentList.get(i).getInterestAmount()+customerPaymentList.get(i).getPaymentDue());
    		customerPaymentList.get(i).setTotalAmount(totalAmount1);
    		customerPaymentList.get(i).setPaidDate(createDateFormat.format(customerPaymentList.get(i).getCreationDate()));
    	}
    	
    	
	    model.addAttribute("customerPaymentList", customerPaymentList);

    	return "CustomerPaymentClearanceMaster";
    }
    
    
    @ResponseBody
    @RequestMapping("/getCompanyBankDetailsByBankIdWise")
    public List<Bank> getCompanyBankDetailsByBankIdWise(@RequestParam("companyBankId") String companyBankId, HttpSession session)
    {
    	try
    	{
    		CompanyBank	companybankDetails = mongoTemplate.findOne(Query.query(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId))), CompanyBank.class);
        	String bankId=companybankDetails.getBankId();
    		Query query = new Query();
	       	List<Bank> bankList = mongoTemplate.find(query.addCriteria(Criteria.where("bankId").is(bankId)), Bank.class);
	       	return bankList;
    	}
    	catch(Exception e)
    	{
    		System.out.println("err  = "+e);
    		return null;
    	}
    	
    }  
      

}
