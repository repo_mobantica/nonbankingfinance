package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;

@Controller
@RequestMapping("/")
public class GoldLoanSchemeController {


	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;  
	@Autowired
	MongoTemplate mongoTemplate;
	

	String goldloanschemeCode;
	private String GoldLoanSchemeCode()
	{
		long goldloanschemeCount = goldloanschemeRepository.count();
		
		if(goldloanschemeCount<10)
		{
			goldloanschemeCode = "GLS000"+(goldloanschemeCount+1);
		}
		else if((goldloanschemeCount>=10) && (goldloanschemeCount<100))
		{
			goldloanschemeCode = "GLS00"+(goldloanschemeCount+1);
		}
		else if((goldloanschemeCount>=100) && (goldloanschemeCount<1000))
		{
			goldloanschemeCode = "GLS0"+(goldloanschemeCount+1);
		}
		else if(goldloanschemeCount>1000)
		{
			goldloanschemeCode = "GLS"+(goldloanschemeCount+1);
		}
		
		return goldloanschemeCode;
	} 
	
    @RequestMapping("/GoldLoanSchemeMaster")
    public String GoldLoanSchemeMaster(ModelMap model)
    {
    	List<GoldLoanScheme> goldLoanSchemeList = goldloanschemeRepository.findAll();
	    model.addAttribute("goldLoanSchemeList", goldLoanSchemeList);
    	return "GoldLoanSchemeMaster";
    }
    

    @RequestMapping("/AddGoldLoanScheme")
    public String AddGoldLoanScheme(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	model.addAttribute("goldloanschemeCode",GoldLoanSchemeCode());
        return "AddGoldLoanScheme";
    }
  

    @RequestMapping(value = "/AddGoldLoanScheme", method = RequestMethod.POST)
    public String AddGoldLoanScheme(@ModelAttribute GoldLoanScheme goldloanscheme, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		goldloanscheme = new GoldLoanScheme(goldloanscheme.getGoldloanschemeId(), goldloanscheme.getGoldloanschemeName(), goldloanscheme.getInterestRate(), 
    				goldloanscheme.getMinimumLoanAmount(), goldloanscheme.getMaximumLoanAmount(),goldloanscheme.getLateFine(),goldloanscheme.getMaximumTenure(),
    				goldloanscheme.getInterestRate_3_6_month(),goldloanscheme.getInterestRate_6_9_month(),
    				goldloanscheme.getCreationDate(), goldloanscheme.getUpdateDate(), goldloanscheme.getUserId());
    		goldloanschemeRepository.save(goldloanscheme);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}
    	model.addAttribute("goldloanschemeCode",GoldLoanSchemeCode());
        return "AddGoldLoanScheme";
    }


    @RequestMapping("/EditGoldLoanScheme")
    public String EditBranch(@RequestParam("goldloanschemeId") String goldloanschemeId, ModelMap model)
    {
      	
    	Query query = new Query();
    	List<GoldLoanScheme> goldloanschemeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("goldloanschemeId").is(goldloanschemeId)),GoldLoanScheme.class);
    	
    	model.addAttribute("goldloanschemeDetails",goldloanschemeDetails);
    	
    	return "EditGoldLoanScheme";
    }
    

    @RequestMapping(value = "/EditGoldLoanScheme", method = RequestMethod.POST)
    public String EditGoldLoanScheme(@ModelAttribute GoldLoanScheme goldloanscheme, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		goldloanscheme = new GoldLoanScheme(goldloanscheme.getGoldloanschemeId(), goldloanscheme.getGoldloanschemeName(), goldloanscheme.getInterestRate(), 
    				goldloanscheme.getMinimumLoanAmount(), goldloanscheme.getMaximumLoanAmount(),goldloanscheme.getLateFine(),goldloanscheme.getMaximumTenure(),
    				goldloanscheme.getInterestRate_3_6_month(),goldloanscheme.getInterestRate_6_9_month(),
    				goldloanscheme.getCreationDate(), goldloanscheme.getUpdateDate(), goldloanscheme.getUserId());
    		goldloanschemeRepository.save(goldloanscheme);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}

    	List<GoldLoanScheme> goldLoanSchemeList = goldloanschemeRepository.findAll();
	    model.addAttribute("goldLoanSchemeList", goldLoanSchemeList);
    	return "GoldLoanSchemeMaster";
    }

}
