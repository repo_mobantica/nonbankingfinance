package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.GoldItem;
import com.nonbankingfinance.repository.GoldItemRepository;

@Controller
@RequestMapping("/")
public class GoldItemController {

	@Autowired
    GoldItemRepository goldItemRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String goldItemCode;
	    
	    private String goldItemCode()
	    {
	    	long goldItemCount  = goldItemRepository.count();
	        if(goldItemCount<10)
	        {
	        	goldItemCode = "GI000"+(goldItemCount+1);
	        }
	        else if((goldItemCount>=10) && (goldItemCount<100))
	        {
	        	goldItemCode = "GI00"+(goldItemCount+1);
	        }
	        else if((goldItemCount>=100) && (goldItemCount<1000))
	        {
	        	goldItemCode = "GI0"+(goldItemCount+1);
	        }
	        else if(goldItemCount>1000)
	        {
	        	goldItemCode = "GI"+(goldItemCount+1);
	        }
	         return goldItemCode;
	    }
	    
	    @RequestMapping("/GoldItemMaster")
	    public String GoldItemMaster(ModelMap model)
	    {
	    	 List<GoldItem> goldItemList = goldItemRepository.findAll();
 	    	 
 	    	 model.addAttribute("goldItemList", goldItemList);
 	    	 
	    	return "GoldItemMaster";
	    }
	    
	   
    @RequestMapping("/AddGoldItem")
    public String AddGoldItem(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	
    	model.addAttribute("goldItemCode",goldItemCode());
        return "AddGoldItem";
    }
    
    
	    @RequestMapping(value = "/AddGoldItem", method = RequestMethod.POST)
	    public String goldItemSave(@ModelAttribute GoldItem goldItem, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
		        goldItem = new GoldItem(goldItem.getGoldItemId(), goldItem.getGoldItemName(), goldItem.getCreationDate(), goldItem.getUpdateDate(), goldItem.getUserId());
		        goldItemRepository.save(goldItem);
		        
		        model.addAttribute("goldItemStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("goldItemStatus", "Fail");
	    	}

	    	    model.addAttribute("goldItemCode",goldItemCode());
	    	
	    	 List<GoldItem> goldItemList;
	    	 goldItemList = goldItemRepository.findAll();
	    	 
	    	 model.addAttribute("goldItemList", goldItemList);
	    	
	        return "AddGoldItem";
	    }
	   

	    @RequestMapping(value="/EditGoldItem",method=RequestMethod.GET)
	    public String EditGoldItem(@RequestParam("goldItemId") String goldItemId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<GoldItem> goldItemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("goldItemId").is(goldItemId)), GoldItem.class);
	    	
	    	model.addAttribute("goldItemDetails",goldItemDetails);
	    	
	    	return "EditGoldItem";
	    }
	    
	    @RequestMapping(value="/EditGoldItem",method=RequestMethod.POST)
	    public String EditGoldItemPostMethod(@ModelAttribute GoldItem goldItem, ModelMap model)
	    {
	    	try
	    	{
	    		goldItem = new GoldItem(goldItem.getGoldItemId(), goldItem.getGoldItemName(), goldItem.getCreationDate(), goldItem.getUpdateDate(), goldItem.getUserId());
		        goldItemRepository.save(goldItem);
		        
		        model.addAttribute("goldItemStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("goldItemStatus", "Fail");
	    	}
	    	
	    	List<GoldItem> goldItemList = goldItemRepository.findAll();
	    	 
	    	model.addAttribute("goldItemList", goldItemList);
	    	return "GoldItemMaster";
	    } 
	    
}
