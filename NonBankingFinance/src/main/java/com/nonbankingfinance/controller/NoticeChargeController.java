package com.nonbankingfinance.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.dto.NoticeChargeReportDTO;
import com.nonbankingfinance.repository.CompanyRepository;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.CustomerPaymentRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;
import com.nonbankingfinance.services.SmsServices;

@Controller
@RequestMapping("/")
public class NoticeChargeController {


	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	CustomerPaymentRepository customerPaymentRepository; 
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	@RequestMapping("/NoticeChargeReport")
	public String NoticeChargeReport(@RequestParam("noticeDays") long noticeDays, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<NoticeChargeReportDTO> noticeChargeReportDTOList=new ArrayList<NoticeChargeReportDTO>();

			String branchId = (String)req.getSession().getAttribute("branchId");
			noticeChargeReportDTOList =GetCustomerNoticeChargeList(branchId, noticeDays);
			model.addAttribute("noticeDays", noticeDays);
			model.addAttribute("noticeChargeReportDTOList", noticeChargeReportDTOList);

			return "NoticeChargeReport";
		}catch (Exception e) {
			return "login";
			// TODO: handle exception
		}
	}

	public List<NoticeChargeReportDTO> GetCustomerNoticeChargeList(String branchId, long noticeDays)
	{
		List<NoticeChargeReportDTO> noticeChargeReportDTOList=new ArrayList<NoticeChargeReportDTO>();

		try {
			//long noticeDays=270;
			long nextNoticeDays=noticeDays+90;


			if(noticeDays<360)
			{
				nextNoticeDays=noticeDays+90;
			}
			else
			{
				nextNoticeDays=noticeDays*90;
			}


			Query query = new Query();

			List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("paymentStatus").ne("Cleared")),CustomerLoanDetails.class);

			NoticeChargeReportDTO noticeChargeReportDTO=new NoticeChargeReportDTO();

			Date loanCreationDate;
			Date lastPaymentdate;

			SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
			String todayDate1=createDateFormat.format(new Date());
			Date firstDate = null;
			Date secondDate = null;

			for(int i=0;i<customerLoanDetailsList.size();i++)
			{

				query = new Query();
				List<CustomerPayment> paymentcustomerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(customerLoanDetailsList.get(i).getPacketNumber()).and("paymentStatus").ne("Canceled")),CustomerPayment.class);

				long customerPaidPrincipalAmount=0;
				double paidInterestAmount=0;

				for(int j=0;j<paymentcustomerDetails.size();j++)
				{
					paidInterestAmount=paidInterestAmount+paymentcustomerDetails.get(j).getInterestAmount();
					customerPaidPrincipalAmount=customerPaidPrincipalAmount+paymentcustomerDetails.get(j).getPrincipalAmount();
					paymentcustomerDetails.get(j).setPaidDate(createDateFormat.format(paymentcustomerDetails.get(j).getCreationDate()));
				}

				loanCreationDate = customerLoanDetailsList.get(i).getCreationDate();
				if(customerPaidPrincipalAmount==0)
				{
					lastPaymentdate=loanCreationDate;
				}
				else
				{
					lastPaymentdate=paymentcustomerDetails.get(paymentcustomerDetails.size()-1).getCreationDate();
				}

				long totalDays=0;

				firstDate = null;
				secondDate = null;

				String loanDate=createDateFormat.format(lastPaymentdate);
				firstDate = createDateFormat.parse(loanDate);
				secondDate = createDateFormat.parse(todayDate1);

				long diff = secondDate.getTime() - firstDate.getTime();

				totalDays = diff / (24 * 60 * 60 * 1000);

				if(totalDays>=noticeDays && totalDays<nextNoticeDays)
				{
					double interestPer=CustomerPaymentController.FindInterestPer(totalDays);
					long interestAmount;
					long noticeCharge=0;
					long principalAmount=customerLoanDetailsList.get(i).getLoanAmount();

					double remainingPrincipal=principalAmount-customerPaidPrincipalAmount;

					if(totalDays<=7 && paymentcustomerDetails.size()==0)
					{
						interestAmount=(long) ((remainingPrincipal/100)*(interestPer/30)*7);
					}
					else
					{
						interestAmount=(long) ((remainingPrincipal/100)*(interestPer/30)*totalDays);
					}

					if(totalDays>=90 && totalDays<=180)
					{
						noticeCharge=50;
					}
					else if(totalDays>180 && totalDays<=270)
					{
						noticeCharge=100;
					}
					else if(totalDays>270 && totalDays<=360)
					{
						noticeCharge=150;
					}
					else if(totalDays>360)
					{
						noticeCharge=200;
					}

					long totalRemainingAmount=(long) (interestAmount+remainingPrincipal)+noticeCharge;

					query = new Query();
					CustomerDetails customerDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("customerId").is(customerLoanDetailsList.get(i).getCustomerId())),CustomerDetails.class);


					noticeChargeReportDTO=new NoticeChargeReportDTO();
					noticeChargeReportDTO.setCustomerId(customerLoanDetailsList.get(i).getCustomerId());
					noticeChargeReportDTO.setPacketNumber(customerLoanDetailsList.get(i).getPacketNumber());
					noticeChargeReportDTO.setCustomerName(customerDetails.getCustomerName());
					noticeChargeReportDTO.setCustomerMobileNo(customerDetails.getMobileNumber());
					noticeChargeReportDTO.setCustomerEmailId(customerDetails.getEmailId());
					noticeChargeReportDTO.setLoanAmount(customerLoanDetailsList.get(i).getLoanAmount());
					noticeChargeReportDTO.setGoldloanschemeId(customerLoanDetailsList.get(i).getGoldloanschemeId());
					noticeChargeReportDTO.setInterestRate(customerLoanDetailsList.get(i).getInterestRate());
					noticeChargeReportDTO.setLoanDate(createDateFormat.format(lastPaymentdate));
					noticeChargeReportDTO.setUserId(customerLoanDetailsList.get(i).getUserId());
					noticeChargeReportDTO.setBranchId(customerLoanDetailsList.get(i).getBranchId());

					noticeChargeReportDTO.setTotalPaidPrincipalAmount(customerPaidPrincipalAmount);
					noticeChargeReportDTO.setRemainingPrincipal((long)remainingPrincipal);
					noticeChargeReportDTO.setInterestAmount(interestAmount);
					noticeChargeReportDTO.setInterestPer(interestPer);
					noticeChargeReportDTO.setNoticeCharge(noticeCharge);
					noticeChargeReportDTO.setTotalRemainingAmount(totalRemainingAmount);

					noticeChargeReportDTOList.add(noticeChargeReportDTO);
				}

			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return noticeChargeReportDTOList;

	}


	@ResponseBody
	@RequestMapping("/SendCustomerNotice")
	public void SendCustomerNotice(@RequestParam("noticeDays") long noticeDays, ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException
	{
		List<NoticeChargeReportDTO> noticeChargeReportDTOList=new ArrayList<NoticeChargeReportDTO>();

		String branchId = (String)req.getSession().getAttribute("branchId");
		noticeChargeReportDTOList =GetCustomerNoticeChargeList(branchId, noticeDays);
		
		for(int i=0;i<noticeChargeReportDTOList.size();i++)
		{
			try {
				String sms="";
				sms="Dear "+noticeChargeReportDTOList.get(i).getCustomerName()+", Please pay .\r\n" + 
						""+".\r\n" ;
				SmsServices.sendSMS(noticeChargeReportDTOList.get(i).getCustomerMobileNo(),sms);
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		}
	}

}
