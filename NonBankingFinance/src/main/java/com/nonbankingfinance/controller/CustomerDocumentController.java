package com.nonbankingfinance.controller;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.repository.BankPaymentDetailsRepository;
import com.nonbankingfinance.repository.BankPaymentHistoryRepository;
import com.nonbankingfinance.repository.BankRepository;
import com.nonbankingfinance.repository.CompanyBankRepository;
import com.nonbankingfinance.repository.CustomerDetailsRepository;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.CustomerLoanDetailsRepository;
import com.nonbankingfinance.repository.CustomerPaymentStatusHistoryRepository;
import com.nonbankingfinance.repository.EnquirySourceRepository;
import com.nonbankingfinance.repository.GoldItemRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;
import com.nonbankingfinance.repository.OccupationRepository;

@Controller
@RequestMapping("/")
public class CustomerDocumentController {

	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
	GoldItemRepository goldItemRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository; 
	@Autowired
	OccupationRepository occupationRepository;  
	@Autowired
	CustomerDetailsRepository customerDetailsRepository;
	@Autowired
	CustomerLoanDetailsRepository customerloandetailsRepository;

	@Autowired
	CustomerPaymentStatusHistoryRepository customerpaymentstatushistoryRepository;
	@Autowired
	CompanyBankRepository companybankRepository;
	@Autowired
	BankPaymentDetailsRepository bankpaymentdetailsRepository;

	@Autowired
	BankPaymentHistoryRepository bankpaymenthistoryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@RequestMapping("/UploadCustomerDocument")
	public String UploadCustomerDocument(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		
		String branchId = (String)req.getSession().getAttribute("branchId");
		Query query = new Query();
		List<GoldLoanScheme> goldloanschemeDetails= goldloanschemeRepository.findAll();
		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		String creationDate="";
		/*   
    	    Date date = new Date(); 
    	    final Calendar cal = Calendar.getInstance();
    	    cal.add(Calendar.DATE, -1);
    	//cal.getTime();
		 */
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

		model.addAttribute("customerDetailsList", customerDetailsList);

		return "UploadCustomerDocument";
	}

}
