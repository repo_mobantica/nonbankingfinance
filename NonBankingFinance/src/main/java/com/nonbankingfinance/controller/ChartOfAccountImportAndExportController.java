package com.nonbankingfinance.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nonbankingfinance.bean.ChartOfAccount;
import com.nonbankingfinance.bean.Designation;
import com.nonbankingfinance.repository.ChartOfAccountRepository;
import com.nonbankingfinance.repository.DesignationRepository;

@Controller
@RequestMapping("/")
public class ChartOfAccountImportAndExportController {


	@Autowired
    DesignationRepository designationRepository;
	@Autowired
	ChartOfAccountRepository chartOfAccountRepository;

	@Autowired
    ServletContext context;
	
	Designation designation = new Designation();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;
	
	HSSFSheet worksheet;
	XSSFSheet worksheet1;
	
	@Autowired
	MongoTemplate mongoTemplate;

	
	 String chartOfAccountCode;
	    
	    private String ChartOfAccountCode()
	    {
	    	long chartOfAccountCount  = chartOfAccountRepository.count();
	        if(chartOfAccountCount<10)
	        {
	        	chartOfAccountCode = "CN000"+(chartOfAccountCount+1);
	        }
	        else if((chartOfAccountCount>=10) && (chartOfAccountCount<100))
	        {
	        	chartOfAccountCode = "CN00"+(chartOfAccountCount+1);
	        }
	        else if((chartOfAccountCount>=100) && (chartOfAccountCount<1000))
	        {
	        	chartOfAccountCode = "CN0"+(chartOfAccountCount+1);
	        }
	        else if(chartOfAccountCount>1000)
	        {
	        	chartOfAccountCode = "CN"+(chartOfAccountCount+1);
	        }
	         return chartOfAccountCode;
	    }
	    
    @RequestMapping(value="/ImportNewChartOfAccount")
    public String importNewChartOfAccount(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	model.addAttribute("progressBarPer",0);
    	model.addAttribute("statusFlag",0);
        return "ImportNewChartOfAccount";
    }
	
	//private static final String INTERNAL_FILE="NewchartOfAccount.xlsx";
	@RequestMapping(value = "/DownloadChartOfAccountTemplate")
	public String downloadChartOfAccountTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{
		
		String filename = "NewChartOfAccount.xlsx";
        response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");
 
		
		FileInputStream fileInputStream = new FileInputStream(filepath+filename);
 
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
		
		return "ImportNewChartOfAccount";
	}
	
	

	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewChartOfAccount", method = RequestMethod.POST)
	public String uploadChartOfAccountDetails(@RequestParam("chartOfAccount_excel") MultipartFile chartOfAccount_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

    	String employeeId = (String)request.getSession().getAttribute("employeeId");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		int flag1=0;
		int progressBarPer=0;
    	List<ChartOfAccount> chartofaccountList= chartOfAccountRepository.findAll();
		List<ChartOfAccount> chartofaccountList1=new ArrayList<ChartOfAccount>();
    	
		if (!chartOfAccount_excel.isEmpty() || chartOfAccount_excel != null )
		{
			try 
			{
				if(chartOfAccount_excel.getOriginalFilename().endsWith("xls") || chartOfAccount_excel.getOriginalFilename().endsWith("xlsx") || chartOfAccount_excel.getOriginalFilename().endsWith("csv"))
				{
					if(chartOfAccount_excel.getOriginalFilename().endsWith("xlsx"))
					{
						 InputStream stream = chartOfAccount_excel.getInputStream();
						 XSSFWorkbook workbook = new XSSFWorkbook(stream);
						 
						 XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet
						 
						 int firstRow = sheet.getFirstRowNum();
						 
						 XSSFRow firstrow = sheet.getRow(firstRow);
						 
						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();
						 
						@SuppressWarnings("unused")
						 Iterator<Row> rowIterator = sheet.iterator();   
						 int last_no=sheet.getLastRowNum();
						 
						 ChartOfAccount chartOfAccount = new ChartOfAccount();
						 for(int i=0;i<last_no;i++)
				         {
							 flag1=0;
							 try
							 {
							 XSSFRow row = sheet.getRow(i+1);
							 
				                 	 // Skip read heading 
				                     if (row.getRowNum() == 0) 
				                     {
				                    	 continue;
				                     }
				                     
				                     for(int j=0;j<chartofaccountList.size();j++)
				                     {

					                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
				                    	 if(chartofaccountList.get(j).getChartOfAccountName().equalsIgnoreCase(row.getCell(0).getStringCellValue()))
				                    	 {
				                    		 flag1=1;
				                    		 break;
				                    	 }
				                     }
				                     
				                     
				                     if(flag1==0)
				                     {
				                    	 chartOfAccount = new ChartOfAccount();
						                     chartOfAccount.setChartOfAccountId(ChartOfAccountCode());
						                     
						                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
						                     chartOfAccount.setChartOfAccountName(row.getCell(0).getStringCellValue());
						                     
						                     row.getCell(1).setCellType(Cell.CELL_TYPE_NUMERIC);
						                     chartOfAccount.setChartOfAccountNumber((int) row.getCell(1).getNumericCellValue());
						                     
											 chartOfAccount.setCreationDate(todayDate);
											 chartOfAccount.setUpdateDate(todayDate);
											 chartOfAccount.setUserId(employeeId);
											 chartOfAccountRepository.save(chartOfAccount);
				                     }	 
				                     else
				                     {
				                    	 chartOfAccount = new ChartOfAccount();
					                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
					                     chartOfAccount.setChartOfAccountName(row.getCell(0).getStringCellValue());
					                     
					                     row.getCell(1).setCellType(Cell.CELL_TYPE_NUMERIC);
					                     chartOfAccount.setChartOfAccountNumber((int) row.getCell(1).getNumericCellValue());
					                     
					                     chartOfAccount.setStatus("Account Already exit");
					                     chartofaccountList1.add(chartOfAccount);
					                     
				                     }
											 
							 }
							 catch (Exception e) {
								// TODO: handle exception
							}
									
				          }

						 progressBarPer= ((last_no-chartofaccountList1.size())*100)/last_no;
				         		workbook.close();
					}
					
				}//if after inner if
				
			}
			catch(Exception e)
			{
				
			}
		}
	

		int statusFlag;
		if(chartofaccountList1.size()!=0)
		{
			statusFlag=1;	
		}
		else
		{
			statusFlag=2;
		}
		
    	model.addAttribute("progressBarPer",progressBarPer);
    	
    	model.addAttribute("statusFlag",statusFlag);
    	model.addAttribute("chartofaccountList1",chartofaccountList1);
		return "ImportNewChartOfAccount";
	}
	 
}
