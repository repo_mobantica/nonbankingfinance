package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.Branch;
import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.repository.BranchRepository;
import com.nonbankingfinance.repository.CompanyRepository;


@Controller
@RequestMapping("/")
public class BranchController {

	@Autowired
	BranchRepository branchRepository;  

	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	

	String branchCode;
	private String BranchCode()
	{
		long branchCount = branchRepository.count();
		
		if(branchCount<10)
		{
			branchCode = "BR000"+(branchCount+1);
		}
		else if((branchCount>=10) && (branchCount<100))
		{
			branchCode = "BR00"+(branchCount+1);
		}
		else if((branchCount>=100) && (branchCount<1000))
		{
			branchCode = "BR0"+(branchCount+1);
		}
		else if(branchCount>1000)
		{
			branchCode = "BR"+(branchCount+1);
		}
		
		return branchCode;
	} 
	
    @RequestMapping("/BranchMaster")
    public String BranchMaster(ModelMap model)
    {
    	
    	List<Branch> branchList = branchRepository.findAll();
    	List<Company> companyList = companyRepository.findAll();
    	
    	for(int i=0;i<branchList.size();i++)
    	{
    		for(int j=0;j<companyList.size();j++)
    		{
    			if(branchList.get(i).getCompanyId().equalsIgnoreCase(companyList.get(j).getCompanyId()))
    			{
    				branchList.get(i).setCompanyId(companyList.get(j).getCompanyName());
    				break;
    			}
    		}

    		
    	}
    	
    	
	    model.addAttribute("branchList", branchList);
    	return "BranchMaster";
    }
    

    @RequestMapping("/AddBranch")
    public String AddBranch(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	

    	List<Company> companyList = companyRepository.findAll();
	    model.addAttribute("companyList", companyList);
    	model.addAttribute("branchCode",BranchCode());
        return "AddBranch";
    }
  

    @RequestMapping(value = "/AddBranch", method = RequestMethod.POST)
    public String AddBranch(@ModelAttribute Branch branch, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		branch = new Branch(branch.getBranchId(), branch.getBranchName(), branch.getCompanyId(), branch.getBranchAddress(), 
    				branch.getPinCode(),branch.getPhoneNumber(),
    				branch.getCreationDate(), branch.getUpdateDate(), branch.getUserId());
    		branchRepository.save(branch);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}

    	List<Branch> branchList = branchRepository.findAll();
	    model.addAttribute("branchList", branchList);
    	return "BranchMaster";
    }


    @RequestMapping("/EditBranch")
    public String EditBranch(@RequestParam("branchId") String branchId, ModelMap model)
    {
      	
    	Query query = new Query();
    	List<Branch> branchDetails = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),Branch.class);
    	
    	List<Company> companyList = companyRepository.findAll();
	  
    	String companyName = "";
     	
    	for(int i=0;i<companyList.size();i++)
    	{
    		if(companyList.get(i).getCompanyId().equals(branchDetails.get(0).getCompanyId()))
    		{

    			companyName=companyList.get(i).getCompanyName();
        		break;
    		}
    		
    	}
    	
    	model.addAttribute("companyName",companyName);
    	
    	  
        model.addAttribute("companyList", companyList);
    	model.addAttribute("branchDetails", branchDetails);
    	
    	return "EditBranch";
    }
    
    

    @RequestMapping(value = "/EditBranch", method = RequestMethod.POST)
    public String EditBranch(@ModelAttribute Branch branch, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{

    		branch = new Branch(branch.getBranchId(), branch.getBranchName(), branch.getCompanyId(), branch.getBranchAddress(), 
    				branch.getPinCode(),branch.getPhoneNumber(),
    				branch.getCreationDate(), branch.getUpdateDate(), branch.getUserId());
    		branchRepository.save(branch);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}

    	List<Branch> branchList = branchRepository.findAll();
	    model.addAttribute("branchList", branchList);
    	return "BranchMaster";
    }

    
    
}
