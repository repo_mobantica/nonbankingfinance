package com.nonbankingfinance.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mongodb.BasicDBObjectBuilder;
import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.repository.CompanyRepository;
import com.nonbankingfinance.repository.CustomerDetailsRepository;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.CustomerLoanDetailsRepository;
import com.nonbankingfinance.repository.CustomerPaymentRepository;
import com.nonbankingfinance.repository.GoldItemRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class CustomerDailyPaymentReportController {


	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
    GoldItemRepository goldItemRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	CustomerDetailsRepository customerDetailsRepository;
	@Autowired
	CustomerPaymentRepository customerPaymentRepository; 
	@Autowired
	CustomerLoanDetailsRepository customerloandetailsRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	

    @RequestMapping("/CustomerDailyPaymentReport")
    public String CustomerDailyPaymentReport(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {
    	
    	int i=0;
    	Date todayDate=new Date();
    	
    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

    	String todayDate1=createDateFormat.format(todayDate);
    	String creationDate="";
    	
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	Query query = new Query();
    
    	//.and("creationDate").regex(todayDate1)
    	
    	List<CustomerPayment> customerPaymentList=new ArrayList<CustomerPayment>();
    	
    	List<CustomerPayment> customerPaymentDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("paymentStatus").is("Cleared")),CustomerPayment.class);

    	CustomerPayment customerpayment=new CustomerPayment();
    	
    	for(i=0;i<customerPaymentDetailsList.size();i++)
    	{
    		creationDate=createDateFormat.format(customerPaymentDetailsList.get(i).getCreationDate());
    		
    		if(creationDate.equals(todayDate1))
    		{
    			 customerpayment=new CustomerPayment();
    			 
    			 customerpayment.setCustomerPaymentId(customerPaymentDetailsList.get(i).getCustomerPaymentId());
    			 customerpayment.setCustomerId(customerPaymentDetailsList.get(i).getCustomerId());
    			 customerpayment.setPrincipalAmount(customerPaymentDetailsList.get(i).getPrincipalAmount());
    			 customerpayment.setInterestAmount(customerPaymentDetailsList.get(i).getInterestAmount());
    			 customerpayment.setPaymentDue(customerPaymentDetailsList.get(i).getPaymentDue());
    			 customerpayment.setPaymentMode(customerPaymentDetailsList.get(i).getPaymentMode());
    			 customerpayment.setBankName(customerPaymentDetailsList.get(i).getBankName());
    			 customerpayment.setBranchName(customerPaymentDetailsList.get(i).getBranchName());
    			 customerpayment.setRefNumber(customerPaymentDetailsList.get(i).getRefNumber());
    			 customerpayment.setNarration(customerPaymentDetailsList.get(i).getNarration());
    			 
    			 customerPaymentList.add(customerpayment);
    		}
    		
    	}
    	long totalPrincipalAmount=0;
    	long totalInterestAmount=0;
    	long totalNoticeCharge=0;
    	long totalAmount=0;
    	String customerName="";
    	query = new Query();
    	List<CustomerDetails> customerDetailsList1 = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
    	
    	for(i=0;i<customerPaymentList.size();i++)
    	{
    		totalPrincipalAmount=totalPrincipalAmount+customerPaymentList.get(i).getPrincipalAmount();
    		totalInterestAmount=(long) (totalInterestAmount+customerPaymentList.get(i).getInterestAmount());
    		totalNoticeCharge=totalNoticeCharge+customerPaymentList.get(i).getNoticeCharge();
    		for(int j=0;j<customerDetailsList1.size();j++)
    		{
    			if(customerPaymentList.get(i).getCustomerId().equals(customerDetailsList1.get(j).getCustomerId()))
    			{
    				customerName=customerDetailsList1.get(j).getCustomerName();
    				customerPaymentList.get(i).setCustomerName(customerName);
    			}
    		}
    		
    		
    	}
    	totalAmount=totalPrincipalAmount+totalInterestAmount+totalNoticeCharge;

    	
    	model.addAttribute("todayDate1", todayDate1);
    	model.addAttribute("totalPrincipalAmount", totalPrincipalAmount);
    	model.addAttribute("totalInterestAmount", totalInterestAmount);
    	model.addAttribute("totalNoticeCharge", totalNoticeCharge);
    	model.addAttribute("totalAmount", totalAmount);

	    model.addAttribute("customerPaymentList", customerPaymentList);

    	return "CustomerDailyPaymentReport";
    }

    @RequestMapping("/CustomerDailyLoanReport")
    public String CustomerDailyLoanReport(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {
    	int i=0;
    	Date todayDate=new Date();
    	
    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

    	String todayDate1=createDateFormat.format(todayDate);
    	String creationDate="";
    	
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	Query query = new Query();
    
    	//.and("creationDate").regex(todayDate1)
    	List<GoldLoanScheme> goldLoanSchemeList = goldloanschemeRepository.findAll();
    	long totalLoanAmount=0;
    	query = new Query();
    	List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
    	
    	query = new Query();
    	List<CustomerLoanDetails> customerLoanDetailsList1 = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerLoanDetails.class);
    
    	List<CustomerLoanDetails> customerLoanDetailsList=new ArrayList<CustomerLoanDetails>();
    	
    	CustomerLoanDetails customerLoanDetails=new CustomerLoanDetails();
    	
    	for(i=0;i<customerLoanDetailsList1.size();i++)
    	{
    		creationDate=createDateFormat.format(customerLoanDetailsList1.get(i).getCreationDate());
    		if(creationDate.equals(todayDate1))
    		{
    			totalLoanAmount=totalLoanAmount+customerLoanDetailsList1.get(i).getLoanAmount();
    			
    			customerLoanDetails=new CustomerLoanDetails();
    			customerLoanDetails.setPacketNumber(customerLoanDetailsList1.get(i).getPacketNumber());
    			customerLoanDetails.setCustomerId(customerLoanDetailsList1.get(i).getPacketNumber());
    			customerLoanDetails.setLoanAmount(customerLoanDetailsList1.get(i).getLoanAmount());
    			customerLoanDetails.setInterestRate(customerLoanDetailsList1.get(i).getInterestRate());


    			
    			for(int j=0;j<goldLoanSchemeList.size();j++)
    			{
    				if(customerLoanDetailsList1.get(i).getGoldloanschemeId().equals(goldLoanSchemeList.get(j).getGoldloanschemeId()))
    				{

    					customerLoanDetails.setGoldloanschemeId(goldLoanSchemeList.get(j).getGoldloanschemeName());
    	    			break;
    				}
    				
    			}
    			
    			for(int j=0;j<customerDetailsList.size();j++)
    			{
    				if(customerLoanDetailsList1.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
    				{
    					customerLoanDetails.setCustomerName(customerDetailsList.get(j).getCustomerName());
    					break;
    				}
    			}
    			
    			customerLoanDetailsList.add(customerLoanDetails);
    		}
    		
    	}
    	
    	
    	model.addAttribute("todayDate1", todayDate1);
    	model.addAttribute("totalLoanAmount", totalLoanAmount);

 	    model.addAttribute("customerLoanDetailsList", customerLoanDetailsList);

    	return "CustomerDailyLoanReport";
    }

    
    
    

    @RequestMapping("/TodayCollectionReport")
    public String TodayCollectionReport(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {

    	String branchId = (String)req.getSession().getAttribute("branchId");

    	int i=0,j=0;
    	int count=0;
    	
    	Date todayDate=new Date();
    	Date emiDate;
    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
    	String todayDate1=createDateFormat.format(todayDate);
		Calendar cal = Calendar.getInstance();
		
    	//		customerPaymentList.get(i).setPaidDate(createDateFormat.format(customerPaymentList.get(i).getCreationDate()));
    	
    	Query query = new Query();
    	List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("loanType").is("EMI")),CustomerLoanDetails.class);
    	
    	query = new Query();
    	List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
    	
    	
    	//query.put("dateAdded", BasicDBObjectBuilder.start("$gte", fromDate).add("$lte", toDate).get());
    	
    	// query = new Query();
     	//List<CustomerDetails> customerDetails = mongoTemplate.find(query.put("creationDate", BasicDBObjectBuilder.start("$gte", todayDate).add("$lte", todayDate).get()));
    	
    	List<CustomerPayment> customerPaymentList =new ArrayList<CustomerPayment>();
    	long totalPaidPrincipalAmount=0;
    	
    	CustomerLoanDetails customerLoanDetails=new CustomerLoanDetails();
    	List<CustomerLoanDetails> customerLoanList =new ArrayList<CustomerLoanDetails>();
    	
    	for(i=0;i<customerLoanDetailsList.size();i++)
    	{
    		try
    		{
    		
    		totalPaidPrincipalAmount=0;
    			
    		customerPaymentList =new ArrayList<CustomerPayment>();
    		customerLoanDetails=new CustomerLoanDetails();
    		query = new Query();
    		customerPaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerLoanDetailsList.get(i).getCustomerId()).and("paymentStatus").is("Cleared")),CustomerPayment.class);
    		count=  customerPaymentList.size();
    		count=count+1;
    		
    		cal = Calendar.getInstance();
    		cal.setTime(customerLoanDetailsList.get(i).getCreationDate());
    		cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)+count));
    		emiDate = cal.getTime();
    		
    		for(j=0;j<customerPaymentList.size();j++)
    		{
    			totalPaidPrincipalAmount=totalPaidPrincipalAmount+customerPaymentList.get(j).getPrincipalAmount();
    		}
    		
    		if(emiDate.compareTo(todayDate)<=0)
    		{
    			customerLoanDetails.setCustomerId(customerLoanDetailsList.get(i).getCustomerId());
    			
    			for(j=0;j<customerDetailsList.size();j++)
    			{
	    			if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))	
	    			{
	    				customerLoanDetails.setCustomerName(customerDetailsList.get(j).getCustomerName());
	    				break; 
	    			}
    			}
    			
    			customerLoanDetails.setPacketNumber(customerLoanDetailsList.get(i).getPacketNumber());
    			customerLoanDetails.setLoanAmount(customerLoanDetailsList.get(i).getLoanAmount());
    			customerLoanDetails.setGoldloanschemeId(customerLoanDetailsList.get(i).getGoldloanschemeId());
    			customerLoanDetails.setInterestRate(customerLoanDetailsList.get(i).getInterestRate());


    			customerLoanDetails.setProcessingFee(customerLoanDetailsList.get(i).getProcessingFee());
    			customerLoanDetails.setPaymentStatus(customerLoanDetailsList.get(i).getPaymentStatus());
    			customerLoanDetails.setStatus(customerLoanDetailsList.get(i).getStatus());
    			customerLoanDetails.setCreationDate(customerLoanDetailsList.get(i).getCreationDate());
    			customerLoanDetails.setUserId(customerLoanDetailsList.get(i).getUserId());
    			customerLoanDetails.setBranchId(customerLoanDetailsList.get(i).getBranchId());
    			
    			customerLoanDetails.setTotalPaidPrincipalAmount(totalPaidPrincipalAmount);
    			
    			customerLoanList.add(customerLoanDetails);
    		
    		}
    		
    		
    		}
    		catch (Exception e) {
    			System.out.println("error = "+e);
				// TODO: handle exception
			}
    		
    	}
    	model.addAttribute("customerLoanList", customerLoanList);
    	return "TodayCollectionReport";
    }

	@ResponseBody
	@RequestMapping(value="PrintCustomerDailyPaymentReport")
	public ResponseEntity<byte[]> PrintCustomerDailyPaymentReport(ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response )
	{
		int index=0;
		JasperPrint print;
		try 
		{	
			Query query = new Query();
			
			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			
			 jmap = null;
				
		    	query = new Query();
		    	List<Company> companyDetails= companyRepository.findAll();

		    	int i=0;
		    	Date todayDate=new Date();
		    	
		    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

		    	String todayDate1=createDateFormat.format(todayDate);
		    	String creationDate="";
		    	
		    	String branchId = (String)req.getSession().getAttribute("branchId");
		    	String branchName = (String)req.getSession().getAttribute("branchName");
		    	 query = new Query();
		    
		    	//.and("creationDate").regex(todayDate1)
		    	
		    	List<CustomerPayment> customerPaymentList=new ArrayList<CustomerPayment>();
		    	
		    	List<CustomerPayment> customerPaymentDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("paymentStatus").is("Cleared")),CustomerPayment.class);

		    	CustomerPayment customerpayment=new CustomerPayment();
		    	
		    	for(i=0;i<customerPaymentDetailsList.size();i++)
		    	{
		    		creationDate=createDateFormat.format(customerPaymentDetailsList.get(i).getCreationDate());
		    		
		    		if(creationDate.equals(todayDate1))
		    		{
		    			 customerpayment=new CustomerPayment();
		    			 
		    			 customerpayment.setCustomerPaymentId(customerPaymentDetailsList.get(i).getCustomerPaymentId());
		    			 customerpayment.setCustomerId(customerPaymentDetailsList.get(i).getCustomerId());
		    			 customerpayment.setPrincipalAmount(customerPaymentDetailsList.get(i).getPrincipalAmount());
		    			 customerpayment.setInterestAmount(customerPaymentDetailsList.get(i).getInterestAmount());
		    			 customerpayment.setPaymentDue(customerPaymentDetailsList.get(i).getPaymentDue());
		    			 customerpayment.setPaymentMode(customerPaymentDetailsList.get(i).getPaymentMode());
		    			 customerpayment.setBankName(customerPaymentDetailsList.get(i).getBankName());
		    			 customerpayment.setBranchName(customerPaymentDetailsList.get(i).getBranchName());
		    			 customerpayment.setRefNumber(customerPaymentDetailsList.get(i).getRefNumber());
		    			 customerpayment.setNarration(customerPaymentDetailsList.get(i).getNarration());
		    			 
		    			 customerPaymentList.add(customerpayment);
		    		}
		    		
		    	}
		    	long totalPrincipalAmount=0;
		    	long totalInterestAmount=0;
		    	long totalDueAmount=0;
		    	long totalAmount=0;
		    	String customerName="";
		    	query = new Query();
		    	List<CustomerDetails> customerDetailsList1 = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
		    	
		    	for(i=0;i<customerPaymentList.size();i++)
		    	{
		    		totalPrincipalAmount=totalPrincipalAmount+customerPaymentList.get(i).getPrincipalAmount();
		    		totalInterestAmount=(long) (totalInterestAmount+customerPaymentList.get(i).getInterestAmount());
		    		totalDueAmount=totalDueAmount+customerPaymentList.get(i).getPaymentDue();
		    		for(int j=0;j<customerDetailsList1.size();j++)
		    		{
		    			if(customerPaymentList.get(i).getCustomerId().equals(customerDetailsList1.get(j).getCustomerId()))
		    			{
		    				customerName=customerDetailsList1.get(j).getCustomerName();
		    				customerPaymentList.get(i).setCustomerName(customerName);
		    			}
		    		}
		    		
		    		
		    	}
		    	totalAmount=totalPrincipalAmount+totalInterestAmount+totalDueAmount;

		    	
		   //  String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";	 
		    	
				c = new ArrayList();
			 	 for(i=0;i<customerPaymentList.size();i++)
				 {
			 		    jmap = new HashMap();
						jmap.put("srno",""+(i+1));
					    //jmap.put("srno","1");
						jmap.put("customerId",""+customerPaymentList.get(i).getCustomerId());
						jmap.put("customerName", ""+customerPaymentList.get(i).getCustomerName());
						jmap.put("principalAmount",""+customerPaymentList.get(i).getPrincipalAmount());
						jmap.put("interestAmount", ""+customerPaymentList.get(i).getInterestAmount());
						jmap.put("paymentDue",""+customerPaymentList.get(i).getPaymentDue());
						
						c.add(jmap);
						
						jmap = null;
				 }
		      
			 JRDataSource dataSource = new JRMapCollectionDataSource(c);
			 Map<String, Object> parameterMap = new HashMap();
			 
			 Date date = new Date();  
			 String strDate= createDateFormat.format(date);
			 

			/*1*/ parameterMap.put("date", ""+strDate);
				
			/*2*/ parameterMap.put("totalPrincipalAmount",""+totalPrincipalAmount);
				
			/*3*/ parameterMap.put("totalInterestAmount",""+totalInterestAmount);
			
			/*4*/ parameterMap.put("totalDueAmount", ""+totalDueAmount);
			
			/*5*/ parameterMap.put("totalAmount",""+totalAmount);
			
			/*5*/ parameterMap.put("totalAmount",""+totalAmount);
			
		      parameterMap.put("branchName", ""+branchName);
			
		      parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());
		      
		      parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress()+" ");			
			
		      
			 InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerDailyPaymentReport.jasper");
				
				
			 print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			 ByteArrayOutputStream baos = new ByteArrayOutputStream();
			 JasperExportManager.exportReportToPdfStream(print, baos);
				
			 byte[] contents = baos.toByteArray();

			 HttpHeaders headers = new HttpHeaders();
			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
			 String filename = "CustomerDailyPaymentReport.pdf";

			 JasperExportManager.exportReportToPdfStream(print, baos);

				 
			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
			 headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
						 
			 ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			 response.setHeader("Content-Disposition", "inline; filename=" + filename );
					 
			 return resp;					    
		}
		catch(Exception ex)
		{
			System.out.println(""+ex);
			//redirectAttributes.addFlashAttribute("flagemail", 0);
		}
		
				 return null;
	}
	
    

	@ResponseBody
	@RequestMapping(value="PrintCustomerDailyLoanReport")
	public ResponseEntity<byte[]> PrintCustomerDailyLoanReport(ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response )
	{
		
		int index=0;
		JasperPrint print;
		try 
		{	
			Query query = new Query();
			
			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			
			 jmap = null;
				
		    	query = new Query();
		    	List<Company> companyDetails= companyRepository.findAll();

		    	int i=0;
		    	Date todayDate=new Date();
		    	
		    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

		    	String todayDate1=createDateFormat.format(todayDate);
		    	String creationDate="";
		    	
		    	String branchId = (String)req.getSession().getAttribute("branchId");
		    	String branchName = (String)req.getSession().getAttribute("branchName");
		    	 query = new Query();
		    
		     	List<GoldLoanScheme> goldLoanSchemeList = goldloanschemeRepository.findAll();
		     	long totalLoanAmount=0;
		     	query = new Query();
		     	List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
		     	
		     	query = new Query();
		     	List<CustomerLoanDetails> customerLoanDetailsList1 = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerLoanDetails.class);
		     
		     	List<CustomerLoanDetails> customerLoanDetailsList=new ArrayList<CustomerLoanDetails>();
		     	
		     	CustomerLoanDetails customerLoanDetails=new CustomerLoanDetails();
		     	
		     	for(i=0;i<customerLoanDetailsList1.size();i++)
		     	{
		     		creationDate=createDateFormat.format(customerLoanDetailsList1.get(i).getCreationDate());
		     		if(creationDate.equals(todayDate1))
		     		{
		     			totalLoanAmount=totalLoanAmount+customerLoanDetailsList1.get(i).getLoanAmount();
		     			
		     			customerLoanDetails=new CustomerLoanDetails();
		     			customerLoanDetails.setPacketNumber(customerLoanDetailsList1.get(i).getPacketNumber());
		     			customerLoanDetails.setCustomerId(customerLoanDetailsList1.get(i).getPacketNumber());
		     			customerLoanDetails.setLoanAmount(customerLoanDetailsList1.get(i).getLoanAmount());
		     			customerLoanDetails.setInterestRate(customerLoanDetailsList1.get(i).getInterestRate());

	
		     			for(int j=0;j<goldLoanSchemeList.size();j++)
		     			{
		     				if(customerLoanDetailsList1.get(i).getGoldloanschemeId().equals(goldLoanSchemeList.get(j).getGoldloanschemeId()))
		     				{

		     					customerLoanDetails.setGoldloanschemeId(goldLoanSchemeList.get(j).getGoldloanschemeName());
		     	    			break;
		     				}
		     				
		     			}
		     			
		     			for(int j=0;j<customerDetailsList.size();j++)
		     			{
		     				if(customerLoanDetailsList1.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
		     				{
		     					customerLoanDetails.setCustomerName(customerDetailsList.get(j).getCustomerName());
		     					break;
		     				}
		     			}
		     			
		     			customerLoanDetailsList.add(customerLoanDetails);
		     		}
		     		
		     	}
		     	
		   //  String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";	 
		    	
				c = new ArrayList();
			 	 for(i=0;i<customerDetailsList.size();i++)
				 {
			 		    jmap = new HashMap();
						jmap.put("srno",""+(i+1));
					    //jmap.put("srno","1");
						jmap.put("customerId",""+customerDetailsList.get(i).getCustomerId());
						jmap.put("customerName", ""+customerLoanDetailsList.get(i).getCustomerName());
						jmap.put("goldloanschemeId",""+customerLoanDetailsList.get(i).getGoldloanschemeId());
						jmap.put("loanAmount", ""+customerLoanDetailsList.get(i).getLoanAmount());
						jmap.put("interestRate",""+customerLoanDetailsList.get(i).getInterestRate());
						jmap.put("noOFMonth","");
						
						c.add(jmap);
						
						jmap = null;
				 }
		      
			 JRDataSource dataSource = new JRMapCollectionDataSource(c);
			 Map<String, Object> parameterMap = new HashMap();
			 
			 Date date = new Date();  
			 String strDate= createDateFormat.format(date);
			 

			/*1*/ parameterMap.put("date", ""+strDate);
				
			/*2*/ parameterMap.put("totalLoanAmount",""+totalLoanAmount);
				
			
		      parameterMap.put("branchName", ""+branchName);
			
		      parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());
		      
		      parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress()+" ");			
			
		      
			 InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerDailyLoanReport.jasper");
				
				
			 print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			 ByteArrayOutputStream baos = new ByteArrayOutputStream();
			 JasperExportManager.exportReportToPdfStream(print, baos);
				
			 byte[] contents = baos.toByteArray();

			 HttpHeaders headers = new HttpHeaders();
			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
			 String filename = "CustomerDailyLoanReport.pdf";

			 JasperExportManager.exportReportToPdfStream(print, baos);

				 
			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
			 headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
						 
			 ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			 response.setHeader("Content-Disposition", "inline; filename=" + filename );
					 
			 return resp;					    
		}
		catch(Exception ex)
		{
			System.out.println(""+ex);
			//redirectAttributes.addFlashAttribute("flagemail", 0);
		}
		
				 return null;
	}
	
    
}
