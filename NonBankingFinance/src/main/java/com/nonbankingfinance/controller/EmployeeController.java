package com.nonbankingfinance.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.nonbankingfinance.bean.Department;
import com.nonbankingfinance.bean.Designation;
import com.nonbankingfinance.bean.Employee;
import com.nonbankingfinance.repository.DepartmentRepository;
import com.nonbankingfinance.repository.EmployeeRepository;

@Controller
@RequestMapping("/")
public class EmployeeController 
{


	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	//Code generation for company code
	String employeeCode;
	private String employeeCode()
	{
		long employeeCount = employeeRepository.count();

		if(employeeCount<10)
		{
			employeeCode = "EM000"+(employeeCount+1);
		}
		else if((employeeCount>=10) && (employeeCount<100))
		{
			employeeCode = "EM00"+(employeeCount+1);
		}
		else if((employeeCount>=100) && (employeeCount<1000))
		{
			employeeCode = "EM0"+(employeeCount+1);
		}
		else if(employeeCount>1000)
		{
			employeeCode = "EM"+(employeeCount+1);
		}

		return employeeCode;
	}


	//code for retriving all designations
	private List<Department> getAllDepartment()
	{
		List<Department> departmentList;

		departmentList = departmentRepository.findAll(new Sort(Sort.Direction.ASC,"departmentName"));
		return departmentList;
	}

	//Retrive all employee in Employee Master
	@RequestMapping("/EmployeeMaster")
	public String employeeMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Employee> employeeList = employeeRepository.findAll(); 
		Query query = new Query();
		for(int i=0;i<employeeList.size();i++)
		{
			try {

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

				employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		model.addAttribute("departmentList", getAllDepartment());
		model.addAttribute("employeeList",employeeList);
		return "EmployeeMaster";
	}

	//Request Mapping For Add Employee
	@RequestMapping("/AddEmployee")
	public String addEmployee(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Employee> employeeList = employeeRepository.findAll(); 

		model.addAttribute("employeeCode",employeeCode());
		model.addAttribute("departmentList", getAllDepartment());
		model.addAttribute("employeeList",employeeList);
		return "AddEmployee";
	}

	@RequestMapping(value="/AddEmployee", method=RequestMethod.POST)
	public String employeeSave(@ModelAttribute Employee employee, Model model)
	{

		try
		{
			employee = new Employee(employee.getEmployeeId(),employee.getEmployeefirstName(),employee.getEmployeemiddleName(),employee.getEmployeelastName(),
					employee.getEmployeeGender(),employee.getEmployeeMarried(),employee.getEmployeeDob(),
					employee.getEmployeeAnniversaryDate(),employee.getEmployeeSpouseName(),employee.getEmployeeEducation(),employee.getDepartmentId(),employee.getDesignationId(),
					employee.getEmployeePancardno(),employee.getEmployeeAadharcardno(),employee.getEmployeeAddress(),
					employee.getPinCode(),
					employee.getEmployeeEmailId(),employee.getEmployeeMobileno(),employee.getCompanyEmailId(),
					employee.getCompanyMobileno(),employee.getEmployeeBasicpay(),employee.getEmployeeHra(),employee.getEmployeeDa(),employee.getEmployeeCa(),
					employee.getEmployeePfacno(),employee.getEmployeeEsino(),employee.getEmployeePLleaves(),employee.getEmployeeSLleaves(),employee.getEmployeeCLleaves(),
					employee.getEmployeeBankName(),employee.getBranchName(),employee.getBankifscCode(),employee.getEmployeeBankacno(),
					employee.getStatus(),employee.getCreationDate(),employee.getUpdateDate(),employee.getUserId());

			employeeRepository.save(employee);

			model.addAttribute("employeeStatus","Success");
		}
		catch(DuplicateKeyException de)
		{
			model.addAttribute("employeeStatus","Fail");
		}

		model.addAttribute("employeeCode",employeeCode());
		model.addAttribute("departmentList", getAllDepartment());
		return "AddEmployee";
	}


	@RequestMapping("/EditEmployee")
	public String EditEmployee(@RequestParam("employeeId") String employeeId, ModelMap model)
	{

		Query query = new Query();
		List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(employeeId)),Employee.class);
		try {
			query = new Query();
			List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeDetails.get(0).getDepartmentId())),Department.class);

			query = new Query();
			List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeDetails.get(0).getDesignationId())),Designation.class);

			model.addAttribute("departmentName",departmentDetails.get(0).getDepartmentName());
			model.addAttribute("designationName",designationDetails.get(0).getDesignationName());


		}
		catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("employeeDetails",employeeDetails);
		model.addAttribute("departmentList", getAllDepartment());

		return "EditEmployee";
	}


	@RequestMapping(value="/EditEmployee", method=RequestMethod.POST)
	public String EditEmployeePostMethod(@ModelAttribute Employee employee, Model model)
	{
		try
		{
			employee = new Employee(employee.getEmployeeId(),employee.getEmployeefirstName(),employee.getEmployeemiddleName(),employee.getEmployeelastName(),
					employee.getEmployeeGender(),employee.getEmployeeMarried(),employee.getEmployeeDob(),
					employee.getEmployeeAnniversaryDate(),employee.getEmployeeSpouseName(),employee.getEmployeeEducation(),employee.getDepartmentId(),employee.getDesignationId(),
					employee.getEmployeePancardno(),employee.getEmployeeAadharcardno(),employee.getEmployeeAddress(),
					employee.getPinCode(),
					employee.getEmployeeEmailId(),employee.getEmployeeMobileno(),employee.getCompanyEmailId(),
					employee.getCompanyMobileno(),employee.getEmployeeBasicpay(),employee.getEmployeeHra(),employee.getEmployeeDa(),employee.getEmployeeCa(),
					employee.getEmployeePfacno(),employee.getEmployeeEsino(),employee.getEmployeePLleaves(),employee.getEmployeeSLleaves(),employee.getEmployeeCLleaves(),
					employee.getEmployeeBankName(),employee.getBranchName(),employee.getBankifscCode(),employee.getEmployeeBankacno(),
					employee.getStatus(),employee.getCreationDate(),employee.getUpdateDate(),employee.getUserId());

			employeeRepository.save(employee);
			model.addAttribute("employeeStatus","Success");
		}
		catch(DuplicateKeyException de)
		{
			model.addAttribute("employeeStatus","Fail");
		}

		List<Employee> employeeList = employeeRepository.findAll(); 
		Query query = new Query();
		for(int i=0;i<employeeList.size();i++)
		{
			try {

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

				employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		model.addAttribute("departmentList", getAllDepartment());
		model.addAttribute("employeeList",employeeList);
		return "EmployeeMaster";
	}


	@ResponseBody
	@RequestMapping("/getDesignationList")
	public List<Designation> getDesignationList(@RequestParam("departmentId") String departmentId, HttpSession session)
	{
		Query query = new Query();
		List<Designation> designationList = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(departmentId)), Designation.class);

		return designationList;
	}


	@ResponseBody
	@RequestMapping("/getDepartmentWiseEmployeeList")
	public List<Employee> getDepartmentWiseEmployeeList(@RequestParam("departmentId") String departmentId, HttpSession session)
	{
		Query query = new Query();
		List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(departmentId)), Employee.class);

		query = new Query();
		for(int i=0;i<employeeList.size();i++)
		{
			try {

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

				employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return employeeList;
	}


	@ResponseBody
	@RequestMapping("/getDesignationWiseEmployeeList")
	public List<Employee> getDesignationWiseEmployeeList(@RequestParam("departmentId") String departmentId, @RequestParam("designationId") String designationId, HttpSession session)
	{
		Query query = new Query();
		List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(departmentId).and("designationId").is(designationId)), Employee.class);

		query = new Query();
		for(int i=0;i<employeeList.size();i++)
		{
			try {

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

				employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return employeeList;
	}

	@ResponseBody
	@RequestMapping("/EmployeeCheckMailId")
	public List<String> EmployeeCheckMailId(@RequestParam("employeeEmailId") String employeeEmailId, HttpSession session)
	{
		Query query = new Query();
		List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("employeeEmailId").is(employeeEmailId)), Employee.class);
		String emialStatus="";
		if(employeeList.size()!=0)
		{
			emialStatus="Present";
		}
		else
		{
			emialStatus="Absent";
		}

		List<String> employeeDetails = new ArrayList();
		employeeDetails.add(emialStatus);
		return employeeDetails;

	}

}