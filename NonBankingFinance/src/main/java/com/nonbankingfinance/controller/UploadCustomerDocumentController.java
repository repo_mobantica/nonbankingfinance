package com.nonbankingfinance.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.nonbankingfinance.bean.CustomerDocument;
import com.nonbankingfinance.repository.CustomerDocumentRepository;

@Controller
@RequestMapping("/")
public class UploadCustomerDocumentController {
	
	@Autowired
	CustomerDocumentRepository customerdocumentRepository;
	
    @ResponseBody
    @RequestMapping("/checkDocumentFormat")
    public String getFileExtension(@RequestParam("fileName") String fileName)
    {
    	
    	System.out.println("checkDocumentFormat= "+fileName);
    	
       try
       {
 		  int dotIndex = fileName.lastIndexOf('.');
 		  String extension =  (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
 		  if(extension.equalsIgnoreCase("pdf") || extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("jpeg") || extension.equalsIgnoreCase("png"))
 		  {
 			  return "OK";
 		  }
 		  else
 		  {
 			  return "NOT OK";
 		  }
       }
       catch(Exception e)
       {
     	 e.toString();
     	 
     	 return "NOT OK";
       }
 	}
      
    

    public String knowFileExtension(@RequestParam("fileName") String fileName)
    {
       try
       {
 		  String fileName1 = new File(fileName).getName();
 		  int dotIndex = fileName1.lastIndexOf('.');
 		  String extension =  (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
 		  
 		  return extension;
       }
       catch(Exception e)
       {
     	 e.toString();
     	 
     	 return null;
       }
 	}
    
    
    public boolean UploadFileToPath(MultipartFile file, String path)
    {
 	   	try
 	   	{
 	   			if (!file.isEmpty())
 	   			{
 	   				try 
 	   				{
 	   				   byte[] bytes = file.getBytes();
 	   				  
 	   				   BufferedOutputStream bout=new BufferedOutputStream(new FileOutputStream(path));  
 	   				   bout.write(bytes);
 	   				   bout.flush();  
 	   		           bout.close();  
 	   				   
 	   				}
 	   				catch(IOException ioe)
 	   				{

 	   				}
 	   			}
 	   	}
 	    catch(Exception e)
 	   	{

 	   	}
 		
 		File f = new File(path);
 		
 		if (f.exists() && !f.isDirectory()) 
 		{
 			return true;
 		} 
 		else 
 		{
 			return false;
 		}
    }
     

    @ResponseBody
    @RequestMapping("/uploadCustomerDocument")
	public List<CustomerDocument> uploadCustomerDocument(@RequestParam("customerId") String customerId, @RequestParam("documentName") String documentName, @RequestParam("document1") MultipartFile file, @RequestParam("userId") String userId, Model model, HttpSession session)
    {
    	System.out.println("INNNNNNn");
    	System.out.println("customerId = "+customerId);
    	try
    	{
    	//String extension="."+knowFileExtension(file.getOriginalFilename());
    	}
    	catch (Exception e) {
			// TODO: handle exception
		}
    	try
    	{
    		
    		//String path = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/customer_documents/" + documentDetails.getCompanyId()+"_"+documentDetails.getDocumentNumber()+"."+knowFileExtension(file.getOriginalFilename());
    		//UploadFileToPath(file, path);
    		
    	}
    	catch(Exception e)
    	{
    		System.out.println("1.File upload exception = "+e.toString());
    	}
    	
    	try
    	{
    		CustomerDocument CustomerDocument=new CustomerDocument();
    	   
    	   model.addAttribute("documentStatus", "Success");
    	}
    	catch(Exception e)
    	{

    	}
    	/*
    	Query query = new Query();
    	List<CompanyDocuments> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(documentDetails.getCompanyId())), CompanyDocuments.class);
    	*/
    	
    	return null;
    }
    
    
    
}
