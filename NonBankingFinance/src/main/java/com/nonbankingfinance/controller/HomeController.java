package com.nonbankingfinance.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.repository.CustomerPaymentRepository;

@Controller
@RequestMapping("/")
public class HomeController 
{

	@Autowired
	CustomerPaymentRepository customerPaymentRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	
    //Request Mapping For Home
    @RequestMapping("/home")
    public String homeRedirect(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	
    	int i=0;
    	long todayPaymentCollection=0;
    	long todayLoanAmount=0;
    	Date todayDate=new Date();
    	
    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

    	String todayDate1=createDateFormat.format(todayDate);
    	String creationDate="";
    	try {
    	String branchId = (String)req.getSession().getAttribute("branchId");
    	Query query = new Query();

    	List<CustomerPayment> customerPaymentDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("paymentStatus").is("Cleared")),CustomerPayment.class);

    	for(i=0;i<customerPaymentDetailsList.size();i++)
    	{
    		creationDate=createDateFormat.format(customerPaymentDetailsList.get(i).getCreationDate());
    		
    		if(creationDate.equals(todayDate1))
    		{
    			todayPaymentCollection=(long) (todayPaymentCollection+customerPaymentDetailsList.get(i).getPrincipalAmount()+customerPaymentDetailsList.get(i).getInterestAmount()+customerPaymentDetailsList.get(i).getPaymentDue());
    		}
    		
    	}

    	query = new Query();
    	List<CustomerLoanDetails> customerLoanDetailsList1 = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerLoanDetails.class);

    	for(i=0;i<customerLoanDetailsList1.size();i++)
    	{
    		creationDate=createDateFormat.format(customerLoanDetailsList1.get(i).getCreationDate());
    		if(creationDate.equals(todayDate1))
    		{
    			todayLoanAmount=todayLoanAmount+customerLoanDetailsList1.get(i).getLoanAmount();
    			
    		}
    		
    	}
    	}catch (Exception e) {
    		System.out.println("exception = "+e);
			// TODO: handle exception
		}
    	model.addAttribute("todayLoanAmount", todayLoanAmount);
	    model.addAttribute("todayPaymentCollection", todayPaymentCollection);
        return "home";
    }
 
}
