package com.nonbankingfinance.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;  

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.Bank;
import com.nonbankingfinance.bean.BankPaymentDetails;
import com.nonbankingfinance.bean.BankPaymentHistory;
import com.nonbankingfinance.bean.Branch;
import com.nonbankingfinance.bean.BranchPayment;
import com.nonbankingfinance.bean.BranchPaymentHistory;
import com.nonbankingfinance.bean.CompanyBank;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.EnquirySource;
import com.nonbankingfinance.bean.GoldItem;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.bean.Occupation;
import com.nonbankingfinance.bean.TodayGoldRate;
import com.nonbankingfinance.repository.CustomerDetailsRepository;
import com.nonbankingfinance.repository.BankPaymentDetailsRepository;
import com.nonbankingfinance.repository.BankPaymentHistoryRepository;
import com.nonbankingfinance.repository.BankRepository;
import com.nonbankingfinance.repository.BranchPaymentHistoryRepository;
import com.nonbankingfinance.repository.BranchPaymentRepository;
import com.nonbankingfinance.repository.CompanyBankRepository;
import com.nonbankingfinance.repository.EnquirySourceRepository;
import com.nonbankingfinance.repository.GoldItemRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;
import com.nonbankingfinance.repository.OccupationRepository;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.CustomerLoanDetailsRepository;
import com.nonbankingfinance.repository.CustomerPaymentStatusHistoryRepository;


@Controller
@RequestMapping("/")
public class CustomerController {

	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
	GoldItemRepository goldItemRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository; 
	@Autowired
	OccupationRepository occupationRepository;  
	@Autowired
	CustomerDetailsRepository customerDetailsRepository;
	@Autowired
	CustomerLoanDetailsRepository customerloandetailsRepository;

	@Autowired
	BranchPaymentRepository branchPaymentRepository;
	@Autowired
	BranchPaymentHistoryRepository branchPaymentHistoryRepository;
	@Autowired
	CustomerPaymentStatusHistoryRepository customerpaymentstatushistoryRepository;
	@Autowired
	CompanyBankRepository companybankRepository;
	@Autowired
	BankPaymentDetailsRepository bankpaymentdetailsRepository;

	@Autowired
	BankPaymentHistoryRepository bankpaymenthistoryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	private String CustomerCode(String branchId)
	{
		String customerCode="KHTD/";
		String branchName1="";
		String[] branchName;
		Query query = new Query();
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
		query = new Query();
		List<Branch> BranchDetails = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),Branch.class);
		branchName1=BranchDetails.get(0).getBranchName();
		try
		{
			branchName  = branchName1.split(" ");

			if(branchName.length==1)
			{
				for(int i=0; i<branchName[0].length() && i<4;i++)
				{
					customerCode = customerCode + branchName[0].charAt(i);
				}
			}
			else
			{
				for(int i=0; i<branchName[0].length() && i<3;i++)
				{
					customerCode = customerCode + branchName[0].charAt(i);
				}
				customerCode=customerCode+"_";
				for(int i=0; i<branchName[1].length() && i<3;i++)
				{
					customerCode = customerCode + branchName[1].charAt(i);
				}
			}

		}
		catch (Exception e) {
			System.out.println("error===  "+e);
			// TODO: handle exception
		}
		long customerCount = customerDetailsList.size();

		if(customerCount<10)
		{
			customerCode = customerCode+"/000"+(customerCount+1);
		}
		else if((customerCount>=10) && (customerCount<100))
		{
			customerCode = customerCode+"/00"+(customerCount+1);
		}
		else if((customerCount>=100) && (customerCount<1000))
		{
			customerCode = customerCode+"/0"+(customerCount+1);
		}
		else if(customerCount>1000)
		{
			customerCode = customerCode+"/"+(customerCount+1);
		}

		return customerCode;
	} 

	private String PacketCode(String customerId)
	{
		String packetNumber="";
		Query query = new Query();
		List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerId)),CustomerLoanDetails.class);

		packetNumber=customerId+"/CL";
		long customerCount = customerLoanDetailsList.size();

		if(customerCount<10)
		{
			packetNumber = packetNumber+"/00"+(customerCount+1);
		}
		else if((customerCount>=10) && (customerCount<100))
		{
			packetNumber = packetNumber+"/0"+(customerCount+1);
		}
		else 
		{
			packetNumber = packetNumber+"/"+(customerCount+1);
		}

		return packetNumber;
	} 

	public long Generate_CustomerGoldItemDetailsId()
	{
		long customergolditemdetailsId = 0;

		List<CustomerGoldItemDetails> customergolditemdetailsList = customergolditemdetailsRepository.findAll();

		int size = customergolditemdetailsList.size();

		if(size!=0)
		{
			customergolditemdetailsId = customergolditemdetailsList.get(size-1).getCustomergolditemdetailsId();
		}

		return customergolditemdetailsId+1;
	}
	@RequestMapping("/CustomerMaster")
	public String CustomerMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		String branchId = (String)req.getSession().getAttribute("branchId");
		Query query = new Query();
		List<GoldLoanScheme> goldloanschemeDetails= goldloanschemeRepository.findAll();
		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		String creationDate="";
		/*   
    	    Date date = new Date(); 
    	    final Calendar cal = Calendar.getInstance();
    	    cal.add(Calendar.DATE, -1);
    	//cal.getTime();
		 */
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

		model.addAttribute("customerDetailsList", customerDetailsList);

		return "CustomerMaster";
	}

	@RequestMapping("/AddCustomer")
	public String AddCustomer(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		String branchId = (String)req.getSession().getAttribute("branchId");
		try {
			List<CompanyBank> CompanybankList = companybankRepository.findAll();

			List<Bank> bankList = bankRepository.findAll();

			for(int i=0;i<CompanybankList.size();i++)
			{
				for(int j=0;j<bankList.size();j++)
				{
					if(CompanybankList.get(i).getBankId().equals(bankList.get(j).getBankId()))
					{
						CompanybankList.get(i).setBankName(bankList.get(j).getBankName());
					}
				}

			}


			String customerId = CustomerCode(branchId);
			Query query = new Query();
			List<CustomerGoldItemDetails> customergolditemdetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerId)), CustomerGoldItemDetails.class);

			if(customergolditemdetailsList.size()!=0)
			{
				query = new Query();
				mongoTemplate.remove(query.addCriteria(Criteria.where("customerId").is(customerId)), CustomerGoldItemDetails.class);	
			}

			List<Occupation> occupationList = occupationRepository.findAll();
			List<GoldLoanScheme> goldLoanSchemeList = goldloanschemeRepository.findAll();
			List<GoldItem> goldItemList = goldItemRepository.findAll();


			model.addAttribute("goldItemList", goldItemList);
			model.addAttribute("goldLoanSchemeList", goldLoanSchemeList);
			model.addAttribute("occupationList", occupationList);

			model.addAttribute("customerCode",customerId);
			model.addAttribute("packetNumber", PacketCode(customerId));
			return "AddCustomer";
		}
		catch (Exception e) {
			return "AddCustomer";
			// TODO: handle exception
		}
	}


	@RequestMapping(value = "/AddCustomer", method = RequestMethod.POST)
	public String AddCustomer(@ModelAttribute CustomerDetails customerDetails, @ModelAttribute CustomerLoanDetails customerloanDetails, Model model, HttpServletRequest req, HttpServletResponse res)
	{

		Date date = new Date(); 

		try
		{
			CustomerDetails customerdetails = new CustomerDetails();

			customerdetails.setCustomerId(customerDetails.getCustomerId());
			customerdetails.setCustomerName(customerDetails.getCustomerName().toUpperCase());
			customerdetails.setMobileNumber(customerDetails.getMobileNumber());
			customerdetails.setCustomerPhoneno(customerDetails.getCustomerPhoneno());
			customerdetails.setEmailId(customerDetails.getEmailId());
			customerdetails.setCustomerPancardno(customerDetails.getCustomerPancardno().toUpperCase());
			customerdetails.setCustomerAadharcardno(customerDetails.getCustomerAadharcardno());
			customerdetails.setOccupationName(customerDetails.getOccupationName());
			customerdetails.setCustomerAddress(customerDetails.getCustomerAddress());

			customerdetails.setPinCode(customerDetails.getPinCode());

			customerdetails.setCustomerBankName(customerDetails.getCustomerBankName().toUpperCase());
			customerdetails.setBankBranchName(customerDetails.getBankBranchName());
			customerdetails.setBanckIFSC(customerDetails.getBanckIFSC());
			customerdetails.setBankAccountNumber(customerDetails.getBankAccountNumber());

			customerdetails.setUpdateDate(date);

			Query query = new Query();
			CustomerDetails customer = mongoTemplate.findOne(query.addCriteria(Criteria.where("customerId").is(customerDetails.getCustomerId())),CustomerDetails.class);
			try {
				if(customer==null)
				{

					customerdetails.setCreationDate(date);
					customerdetails.setUserId(customerDetails.getUserId());
					customerdetails.setBranchId(customerDetails.getBranchId());
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
			customerDetailsRepository.save(customerdetails);


			// add customer Loan Details 
			CustomerLoanDetails customerloandetails = new CustomerLoanDetails();
			customerloandetails.setPacketNumber(customerloanDetails.getPacketNumber());
			customerloandetails.setCustomerId(customerloanDetails.getCustomerId());

			customerloandetails.setLoanAmount(customerloanDetails.getLoanAmount());
			customerloandetails.setGoldloanschemeId(customerloanDetails.getGoldloanschemeId());
			customerloandetails.setInterestRate(customerloanDetails.getInterestRate());

			customerloandetails.setProcessingFee(customerloanDetails.getProcessingFee());
			customerloandetails.setStatus("Inprocess");
			customerloandetails.setPaymentStatus("Remaining");
			customerloandetails.setCreationDate(date);
			customerloandetails.setUpdateDate(date);
			customerloandetails.setBranchId(customerloanDetails.getBranchId());
			customerloandetails.setUserId(customerloanDetails.getUserId());

			customerloandetailsRepository.save(customerloandetails);

			// add Amount in Branch Amount
			BranchPayment branchPayment=branchPaymentRepository.findByBranchId(customerloanDetails.getBranchId());
			if(branchPayment==null)
			{
				BranchPayment branchPayment1=new BranchPayment();
				branchPayment1.setBranchId(customerloanDetails.getBranchId());
				branchPayment1.setAmount(customerloanDetails.getLoanAmount());
				branchPaymentRepository.save(branchPayment1);
			}
			else
			{
				branchPayment.setAmount(branchPayment.getAmount()-customerloanDetails.getLoanAmount());
				branchPaymentRepository.save(branchPayment);
			}
			double loanAmount=customerloanDetails.getLoanAmount();

			BranchPaymentHistory branchPaymentHistory=new BranchPaymentHistory();
			branchPaymentHistory.setBranchId(customerloanDetails.getBranchId());
			branchPaymentHistory.setAmount(loanAmount);
			branchPaymentHistory.setPaymentType(1);
			branchPaymentHistory.setDebiteOrcredit(2);
			branchPaymentHistory.setPacketOrbranchId(customerloanDetails.getPacketNumber());
			branchPaymentHistory.setCreateDate(new Date());
			branchPaymentHistory.setUserId(customerloanDetails.getUserId());
			branchPaymentHistory.setStatus(1);

			branchPaymentHistoryRepository.save(branchPaymentHistory);
			model.addAttribute("stateStatus","Success");
		}
		catch(DuplicateKeyException de)
		{
			model.addAttribute("stateStatus","Fail");
		}


		String branchId = (String)req.getSession().getAttribute("branchId");
		Query query = new Query();


		query = new Query();
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

		model.addAttribute("customerDetailsList", customerDetailsList);

		return "CustomerMaster";
	}

	@RequestMapping("/EditCustomer")
	public String EditCustomer(@RequestParam("customerId") String customerId, ModelMap model)
	{

		Query query = new Query();
		List<CustomerDetails> customerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerId)),CustomerDetails.class);

		SimpleDateFormat createDate = new SimpleDateFormat("dd/MM/yyyy");
		String creationDate = createDate.format( customerDetails.get(0).getCreationDate());


		List<Occupation> occupationList = occupationRepository.findAll();

		model.addAttribute("creationDate",creationDate);

		model.addAttribute("occupationList", occupationList);
		model.addAttribute("customerDetails", customerDetails);

		return "EditCustomer";
	}


	@RequestMapping(value = "/EditCustomer", method = RequestMethod.POST)
	public String EditCustomer(@ModelAttribute CustomerDetails customerDetails, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		Query query = new Query();
		Date date = new Date(); 
		CustomerDetails customer = mongoTemplate.findOne(query.addCriteria(Criteria.where("customerId").is(customerDetails.getCustomerId())),CustomerDetails.class);

		try
		{
			CustomerDetails customerdetails = new CustomerDetails();

			customerdetails.setCustomerId(customerDetails.getCustomerId());
			customerdetails.setCustomerName(customerDetails.getCustomerName().toUpperCase());
			customerdetails.setMobileNumber(customerDetails.getMobileNumber());
			customerdetails.setCustomerPhoneno(customerDetails.getCustomerPhoneno());
			customerdetails.setEmailId(customerDetails.getEmailId());
			customerdetails.setCustomerPancardno(customerDetails.getCustomerPancardno().toUpperCase());
			customerdetails.setCustomerAadharcardno(customerDetails.getCustomerAadharcardno());
			customerdetails.setOccupationName(customerDetails.getOccupationName());
			customerdetails.setCustomerAddress(customerDetails.getCustomerAddress());

			customerdetails.setPinCode(customerDetails.getPinCode());

			customerdetails.setCustomerBankName(customerDetails.getCustomerBankName().toUpperCase());
			customerdetails.setBankBranchName(customerDetails.getBankBranchName());
			customerdetails.setBanckIFSC(customerDetails.getBanckIFSC().toUpperCase());
			customerdetails.setBankAccountNumber(customerDetails.getBankAccountNumber());

			customerdetails.setCreationDate(customer.getCreationDate());
			customerdetails.setUpdateDate(date);

			customerdetails.setUserId(customerDetails.getUserId());
			customerdetails.setBranchId(customerDetails.getBranchId());

			customerDetailsRepository.save(customerdetails);
			model.addAttribute("stateStatus","Success");

		}
		catch(DuplicateKeyException de)
		{
			model.addAttribute("stateStatus","Fail");
		}


		String branchId = (String)req.getSession().getAttribute("branchId");
		query = new Query();

		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

		model.addAttribute("customerDetailsList", customerDetailsList);

		return "CustomerMaster";
	}

	@ResponseBody
	@RequestMapping("/AddCustomerGoldItemList")
	public List<CustomerGoldItemDetails> AddCustomerGoldItemList(@RequestParam("packetNumber") String packetNumber, @RequestParam("customerId") String customerId, @RequestParam("goldItemName") String goldItemName, @RequestParam("itemGrossWeight") double itemGrossWeight, @RequestParam("itemQty") double itemQty, @RequestParam("itemDWeight") double itemDWeight, @RequestParam("itemWeight") double itemWeight, @RequestParam("purityPer") double purityPer, @RequestParam("purWeight") double purWeight, @RequestParam("itemMarketValue") double itemMarketValue, HttpSession session)
	{
		try
		{
			CustomerGoldItemDetails customergolditemdetails = new CustomerGoldItemDetails();
			customergolditemdetails.setCustomergolditemdetailsId(Generate_CustomerGoldItemDetailsId());
			customergolditemdetails.setPacketNumber(packetNumber);
			customergolditemdetails.setCustomerId(customerId);
			customergolditemdetails.setGoldItemName(goldItemName);
			customergolditemdetails.setItemGrossWeight(itemGrossWeight);
			customergolditemdetails.setItemQty(itemQty);
			customergolditemdetails.setItemDWeight(itemDWeight);
			customergolditemdetails.setItemWeight(itemWeight);
			customergolditemdetails.setPurityPer(purityPer);
			customergolditemdetails.setPurWeight(purWeight);
			customergolditemdetails.setItemMarketValue(itemMarketValue);
			customergolditemdetailsRepository.save(customergolditemdetails);

			Query query = new Query();
			List<CustomerGoldItemDetails> customergolditemdetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerId).and("packetNumber").is(packetNumber)), CustomerGoldItemDetails.class);
			return customergolditemdetailsList;
		}
		catch(Exception e)
		{
			return null;
		}

	}	 
	@ResponseBody
	@RequestMapping("/DeleteCustomerItem")
	public List<CustomerGoldItemDetails> DeleteCustomerItem(@RequestParam("customerId") String customerId, @RequestParam("packetNumber") String packetNumber, @RequestParam("customergolditemdetailsId") String customergolditemdetailsId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("customergolditemdetailsId").is(Integer.parseInt(customergolditemdetailsId)).and("customerId").is(customerId)), CustomerGoldItemDetails.class);

			query = new Query();
			List<CustomerGoldItemDetails> customergolditemdetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerId).and("packetNumber").is(packetNumber)), CustomerGoldItemDetails.class);
			return customergolditemdetailsList;
		}
		catch(Exception e)
		{
			return null;
		}

	} 


	@ResponseBody
	@RequestMapping(value="/getGoldloanschemeDetails")
	public List<GoldLoanScheme> getGoldloanschemeDetails(@RequestParam("goldloanschemeId") String goldloanschemeId)
	{
		Query query =new Query();
		List<GoldLoanScheme> goldloanschemeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("goldloanschemeId").is(goldloanschemeId)), GoldLoanScheme.class);
		return goldloanschemeDetails;
	}

	//get Customer Details by Aadhar Number
	@ResponseBody
	@RequestMapping("/getCustomerDetailsByAdharNumber")
	public CustomerDetails getCustomerDetailsByAdharNumber(@RequestParam("customerAadharcardno") String customerAadharcardno, HttpSession session)
	{
		try
		{
			Query query = new Query();
			CustomerDetails customer = mongoTemplate.findOne(query.addCriteria(Criteria.where("customerAadharcardno").is(customerAadharcardno)),CustomerDetails.class);

			if(customer!=null)
			{
				String packetNumber=PacketCode(customer.getCustomerId());
				customer.setPacketNumber(packetNumber);

			}
			return customer;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}

	} 

}
