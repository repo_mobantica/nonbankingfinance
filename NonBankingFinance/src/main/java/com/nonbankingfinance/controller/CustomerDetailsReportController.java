package com.nonbankingfinance.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.bean.Employee;
import com.nonbankingfinance.bean.EnquirySource;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.bean.Occupation;
import com.nonbankingfinance.repository.BankPaymentDetailsRepository;
import com.nonbankingfinance.repository.BankPaymentHistoryRepository;
import com.nonbankingfinance.repository.BankRepository;
import com.nonbankingfinance.repository.CompanyBankRepository;
import com.nonbankingfinance.repository.CompanyRepository;
import com.nonbankingfinance.repository.CustomerDetailsRepository;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.CustomerPaymentStatusHistoryRepository;
import com.nonbankingfinance.repository.EnquirySourceRepository;
import com.nonbankingfinance.repository.GoldItemRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;
import com.nonbankingfinance.repository.OccupationRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class CustomerDetailsReportController {


	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
	GoldItemRepository goldItemRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository; 
	@Autowired
	OccupationRepository occupationRepository;  
	@Autowired
	CustomerDetailsRepository customerDetailsRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	CustomerPaymentStatusHistoryRepository customerpaymentstatushistoryRepository;
	@Autowired
	CompanyBankRepository companybankRepository;
	@Autowired
	BankPaymentDetailsRepository bankpaymentdetailsRepository;

	@Autowired
	BankPaymentHistoryRepository bankpaymenthistoryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	@ResponseBody
	@RequestMapping(value="PrintCustomerDetails")
	public ResponseEntity<byte[]> PrintCustomerDetails(@RequestParam("packetNumber") String packetNumber, ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response )
	{


		int index=0;
		JasperPrint print;
		try 
		{	
			Query query = new Query();

			Collection c = new ArrayList();
			HashMap jmap = new HashMap();

			jmap = null;


			String employeeName = (String)req.getSession().getAttribute("employeeName");
			query = new Query();
			List<Company> companyDetails= companyRepository.findAll();

			SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

			query = new Query();
			CustomerLoanDetails customerLoanDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)),CustomerLoanDetails.class);

			query = new Query();
			CustomerDetails customerDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("customerId").is(customerLoanDetails.getCustomerId())),CustomerDetails.class);


			SimpleDateFormat createDate = new SimpleDateFormat("dd/MM/yyyy");


			query = new Query();
			List<CustomerGoldItemDetails> customergolditemdetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)), CustomerGoldItemDetails.class);

			query = new Query();
			List<GoldLoanScheme> goldloanschemeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("goldloanschemeId").is(customerLoanDetails.getGoldloanschemeId())),GoldLoanScheme.class);
			query = new Query();
			List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(customerLoanDetails.getUserId())),Employee.class);

			//  String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";	 

			c = new ArrayList();
			for(int i=0;i<customergolditemdetailsList.size();i++)
			{
				jmap = new HashMap();
				jmap.put("srno",""+(i+1));
				//jmap.put("srno","1");
				jmap.put("goldItemName",""+customergolditemdetailsList.get(i).getGoldItemName());
				jmap.put("itemQty", ""+customergolditemdetailsList.get(i).getItemQty());
				jmap.put("itemWeight",""+customergolditemdetailsList.get(i).getItemWeight());
				jmap.put("purityPer", ""+customergolditemdetailsList.get(i).getPurityPer());
				jmap.put("purWeight",""+customergolditemdetailsList.get(i).getPurWeight());

				c.add(jmap);

				jmap = null;
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			Date date = new Date();  
			String strDate= createDateFormat.format(date);

			String loanDate= createDateFormat.format( customerLoanDetails.getCreationDate());

			parameterMap.put("customerId",""+customerLoanDetails.getCustomerId());

			parameterMap.put("loanDate",""+loanDate);

			parameterMap.put("date", ""+strDate);

			parameterMap.put("customerName", ""+customerDetails.getCustomerName());

			parameterMap.put("mobileNumber",""+customerDetails.getMobileNumber());

			parameterMap.put("emailId",""+customerDetails.getEmailId());

			parameterMap.put("customerPancardno",""+customerDetails.getCustomerPancardno());

			parameterMap.put("customerAadharcardno",""+customerDetails.getCustomerAadharcardno());


			parameterMap.put("loanPurpose", "");


			parameterMap.put("occupationName", ""+customerDetails.getOccupationName());

			parameterMap.put("enquirySourceName", "");

			parameterMap.put("customerAddress",""+customerDetails.getCustomerAddress());

			parameterMap.put("countryName","");

			parameterMap.put("addressDetails1", "");

			parameterMap.put("addressDetails2", " Pin Code :"+customerDetails.getPinCode());

			parameterMap.put("loanAmount",""+customerLoanDetails.getLoanAmount());

			parameterMap.put("goldloanschemeName", ""+goldloanschemeDetails.get(0).getGoldloanschemeName());

			parameterMap.put("interestRate", ""+customerLoanDetails.getInterestRate());

			parameterMap.put("noOFMonth", "");

			parameterMap.put("monthlyEmi", "");

			parameterMap.put("processingFee", ""+customerLoanDetails.getProcessingFee());

			parameterMap.put("packetNumber", ""+customerLoanDetails.getPacketNumber());

			parameterMap.put("userName", ""+employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeelastName());

			parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());

			parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress());


			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerLoanDetails.jasper");


			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "CustomerLoanDetails.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			System.out.println(""+ex);
			//redirectAttributes.addFlashAttribute("flagemail", 0);
		}

		return null;
	}



	@RequestMapping("/CustomerDetailsMonthlyReport")
	public String CustomerDetailsMonthlyReport(ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException
	{

		String branchId = (String)req.getSession().getAttribute("branchId");

		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		Calendar now = Calendar.getInstance();

		String currentStartDate1="1/"+(now.get(Calendar.MONTH) + 1)+"/"+now.get(Calendar.YEAR);

		Calendar calendar = Calendar.getInstance();
		int lastDate = calendar.getActualMaximum(Calendar.DATE);
		calendar.set(Calendar.DATE, lastDate);

		Date currentMonthStartDate=new SimpleDateFormat("d/M/yyyy").parse(currentStartDate1);
		Date currentMonthEndDate=calendar.getTime();

		String creationDate="";
		Query query = new Query();

		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);
		query = new Query();
		List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(currentMonthStartDate).lt(currentMonthEndDate).and("branchId").is(branchId)), CustomerLoanDetails.class);

		List<GoldLoanScheme> goldloanschemeDetails= goldloanschemeRepository.findAll();

		for(int i=0;i<customerLoanDetailsList.size();i++)
		{
			for(int j=0;j<customerDetailsList.size();j++)
			{
				if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
				{
					customerLoanDetailsList.get(i).setCustomerName(customerDetailsList.get(j).getCustomerName());
					break;
				}
			}

			for(int j=0;j<goldloanschemeDetails.size();j++)
			{
				if(customerLoanDetailsList.get(i).getGoldloanschemeId().equalsIgnoreCase(goldloanschemeDetails.get(j).getGoldloanschemeId()))
				{
					customerLoanDetailsList.get(i).setGoldloanschemeId(goldloanschemeDetails.get(j).getGoldloanschemeName());
					break;
				}

			}

			creationDate=createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate());
			customerLoanDetailsList.get(i).setLoanDate(creationDate);

		}

		model.addAttribute("goldloanschemeDetails", goldloanschemeDetails);
		model.addAttribute("customerLoanDetailsList", customerLoanDetailsList);
		return "CustomerDetailsMonthlyReport";
	}




	@ResponseBody
	@RequestMapping("/getMonthlyCustomerDetailsReport")
	public List<CustomerLoanDetails> getMonthlyCustomerDetailsReport(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, @RequestParam("goldloanschemeId") String goldloanschemeId, ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException
	{
		String branchId = (String)req.getSession().getAttribute("branchId");

		DateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		Date currentMonthStartDate=new SimpleDateFormat("M/d/yyyy").parse(startDate);
		Date currentMonthEndDate=new SimpleDateFormat("M/d/yyyy").parse(endDate);

		Query query = new Query();
		// String todayDateInString = new SimpleDateFormat("d/M/yyyy").format(currentMonthStartDate);
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

		String creationDate="";
		query = new Query();
		List<CustomerLoanDetails> customerLoanDetailsList;
		if(goldloanschemeId.equals("Default")) 
		{
			query = new Query();
			customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(currentMonthStartDate).lt(currentMonthEndDate).and("branchId").is(branchId)), CustomerLoanDetails.class);
		}
		else
		{
			query = new Query();
			customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(currentMonthStartDate).lt(currentMonthEndDate).and("branchId").is(branchId).and("goldloanschemeId").is(goldloanschemeId)), CustomerLoanDetails.class);

		}

		List<GoldLoanScheme> goldloanschemeDetails= goldloanschemeRepository.findAll();

		for(int i=0;i<customerLoanDetailsList.size();i++)
		{
			for(int j=0;j<customerDetailsList.size();j++)
			{
				if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
				{
					customerLoanDetailsList.get(i).setCustomerName(customerDetailsList.get(j).getCustomerName());
					break;
				}
			}

			for(int j=0;j<goldloanschemeDetails.size();j++)
			{
				if(customerLoanDetailsList.get(i).getGoldloanschemeId().equalsIgnoreCase(goldloanschemeDetails.get(j).getGoldloanschemeId()))
				{
					customerLoanDetailsList.get(i).setGoldloanschemeId(goldloanschemeDetails.get(j).getGoldloanschemeName());
					break;
				}

			}

			creationDate=createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate());
			customerLoanDetailsList.get(i).setLoanDate(creationDate);

		}


		return customerLoanDetailsList;
	}


	@ResponseBody
	@RequestMapping(value = "/CustomerDetailsMonthlyReport", method = RequestMethod.POST)
	public ResponseEntity<byte[]> CustomerDetailsMonthlyReport(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, @RequestParam("goldloanschemeId") String goldloanschemeId,  ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response )
	{

		int index=0;
		JasperPrint print;
		try 
		{	
			Query query = new Query();

			Collection c = new ArrayList();
			HashMap jmap = new HashMap();

			jmap = null;


			String employeeName = (String)req.getSession().getAttribute("employeeName");

			String branchId = (String)req.getSession().getAttribute("branchId");

			String branchName = (String)req.getSession().getAttribute("branchName");

			String reportDate="";
			query = new Query();
			List<Company> companyDetails= companyRepository.findAll();

			
			DateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
			Date currentMonthStartDate=new SimpleDateFormat("M/d/yyyy").parse(startDate);
			Date currentMonthEndDate=new SimpleDateFormat("M/d/yyyy").parse(endDate);

			String currentStartDate1 = createDateFormat.format(currentMonthStartDate);
			String currentEndDate1 = createDateFormat.format(currentMonthEndDate);
			reportDate=currentStartDate1+" To "+currentEndDate1;

			// String todayDateInString = new SimpleDateFormat("d/M/yyyy").format(currentMonthStartDate);


			query = new Query();
			// String todayDateInString = new SimpleDateFormat("d/M/yyyy").format(currentMonthStartDate);
			List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

			String creationDate="";
			query = new Query();
			List<CustomerLoanDetails> customerLoanDetailsList;
			if(goldloanschemeId.equals("Default")) 
			{
				query = new Query();
				customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(currentMonthStartDate).lt(currentMonthEndDate).and("branchId").is(branchId)), CustomerLoanDetails.class);
			}
			else
			{
				query = new Query();
				customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(currentMonthStartDate).lt(currentMonthEndDate).and("branchId").is(branchId).and("goldloanschemeId").is(goldloanschemeId)), CustomerLoanDetails.class);

			}

			List<GoldLoanScheme> goldloanschemeDetails= goldloanschemeRepository.findAll();

			for(int i=0;i<customerLoanDetailsList.size();i++)
			{
				for(int j=0;j<customerDetailsList.size();j++)
				{
					if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId()))
					{
						customerLoanDetailsList.get(i).setCustomerName(customerDetailsList.get(j).getCustomerName());
						break;
					}
				}

				for(int j=0;j<goldloanschemeDetails.size();j++)
				{
					if(customerLoanDetailsList.get(i).getGoldloanschemeId().equalsIgnoreCase(goldloanschemeDetails.get(j).getGoldloanschemeId()))
					{
						customerLoanDetailsList.get(i).setGoldloanschemeId(goldloanschemeDetails.get(j).getGoldloanschemeName());
						break;
					}

				}

				creationDate=createDateFormat.format(customerLoanDetailsList.get(i).getCreationDate());
				customerLoanDetailsList.get(i).setLoanDate(creationDate);

			}




			//  String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";	 

			c = new ArrayList();
			for(int i=0;i<customerDetailsList.size();i++)
			{
				jmap = new HashMap();
				jmap.put("srno",""+(i+1));
				//jmap.put("srno","1");
				jmap.put("customerId",""+customerLoanDetailsList.get(i).getCustomerId());
				jmap.put("packetNumber",""+customerLoanDetailsList.get(i).getPacketNumber());
				jmap.put("customerName", ""+customerLoanDetailsList.get(i).getCustomerName());
				jmap.put("goldloanschemeId",""+customerLoanDetailsList.get(i).getGoldloanschemeId());
				jmap.put("loanAmount", ""+customerLoanDetailsList.get(i).getLoanAmount());
				jmap.put("interestRate",""+customerLoanDetailsList.get(i).getInterestRate());

				jmap.put("noOFMonth", "");
				jmap.put("loanDate",""+customerLoanDetailsList.get(i).getLoanDate());

				c.add(jmap);

				jmap = null;
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			Date date = new Date();  
			String strDate= createDateFormat.format(date);

			/*1*/ parameterMap.put("date", ""+strDate);

			/*1*/ parameterMap.put("reportDate", ""+reportDate);

			/*1*/ parameterMap.put("branchName", ""+branchName);

			/*12*/parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());

			/*13*/parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress()+", "+companyDetails.get(0).getPinCode());


			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/MonthlyCustomerDetailsReport.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "MonthlyCustomerDetailsReport.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			System.out.println(""+ex);
			//redirectAttributes.addFlashAttribute("flagemail", 0);
		}

		return null;
	}

}
