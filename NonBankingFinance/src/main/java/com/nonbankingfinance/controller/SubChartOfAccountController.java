package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.ChartOfAccount;
import com.nonbankingfinance.bean.SubChartOfAccount;
import com.nonbankingfinance.repository.ChartOfAccountRepository;
import com.nonbankingfinance.repository.SubChartOfAccountRepository;

@Controller
@RequestMapping("/")
public class SubChartOfAccountController {



	@Autowired
    SubChartOfAccountRepository subChartOfAccountRepository;
	@Autowired
	ChartOfAccountRepository chartOfAccountRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	  
		//Code For Auto generation for subChartOfAccount id
		String subChartOfAccountCode;
	    
	    private String SubChartOfAccountCode()
	    {
	    	long subChartOfAccountCount  = subChartOfAccountRepository.count();
	    	
	        if(subChartOfAccountCount<10)
	        {
	        	subChartOfAccountCode = "SC000"+(subChartOfAccountCount+1);
	        }
	        else if((subChartOfAccountCount>=10) && (subChartOfAccountCount<100))
	        {
	        	subChartOfAccountCode = "SC00"+(subChartOfAccountCount+1);
	        }
	        else if((subChartOfAccountCount>=100) && (subChartOfAccountCount<1000))
	        {
	        	subChartOfAccountCode = "SC0"+(subChartOfAccountCount+1);
	        }
	        else if(subChartOfAccountCount>1000)
	        {
	        	subChartOfAccountCode = "SC"+(subChartOfAccountCount+1);
	        }
	        
	        return subChartOfAccountCode;
	    }
	    
	    @RequestMapping("/SubChartOfAccountMaster")
	    public String SubChartOfAccountMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {
	    	
	    	List<SubChartOfAccount> subChartOfAccountList= subChartOfAccountRepository.findAll();
	    	List<ChartOfAccount> chartOfAccountList= chartOfAccountRepository.findAll();
	    	
	    	for(int i=0;i<subChartOfAccountList.size();i++)
	    	{
	    		for(int j=0;j<chartOfAccountList.size();j++)
	    		{
	    			try
	    			{
	    			if(subChartOfAccountList.get(i).getChartOfAccountId().equals(chartOfAccountList.get(j).getChartOfAccountId()))
	    			{
	    				subChartOfAccountList.get(i).setChartOfAccountId(chartOfAccountList.get(j).getChartOfAccountName());
	    				break;
	    			}
	    			}catch (Exception e) {
						// TODO: handle exception
					}
	    		}
	    		
	    	}
	    	
	    	model.addAttribute("chartOfAccountList",chartOfAccountList);
	      	model.addAttribute("subChartOfAccountList", subChartOfAccountList);
	    	
	        return "SubChartOfAccountMaster";
	    }
    @RequestMapping("/AddSubChartOfAccount")
    public String addSubChartOfAccount(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	List<ChartOfAccount> chartOfAccountList= chartOfAccountRepository.findAll();
    	
    	model.addAttribute("chartOfAccountList",chartOfAccountList);
    	model.addAttribute("subChartOfAccountCode",SubChartOfAccountCode());
    	return "AddSubChartOfAccount";
    }
    

    @RequestMapping(value = "/AddSubChartOfAccount", method = RequestMethod.POST)
    public String subChartOfAccountSave(@ModelAttribute SubChartOfAccount subChartOfAccount, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
	        subChartOfAccount = new SubChartOfAccount(subChartOfAccount.getSubChartOfAccountId(), subChartOfAccount.getSubChartOfAccountNumber(), subChartOfAccount.getSubChartOfAccountName(), subChartOfAccount.getChartOfAccountId(), subChartOfAccount.getChartOfAccountNumber(),
	        		subChartOfAccount.getCreationDate(), subChartOfAccount.getUpdateDate(), subChartOfAccount.getUserId());
	        subChartOfAccountRepository.save(subChartOfAccount);
	        model.addAttribute("subChartOfAccountStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("subChartOfAccountStatus","Fail");
    	}

    	List<ChartOfAccount> chartOfAccountList= chartOfAccountRepository.findAll();
    	
    	model.addAttribute("chartOfAccountList",chartOfAccountList);
    	model.addAttribute("subChartOfAccountCode",SubChartOfAccountCode());
    	
        return "AddSubChartOfAccount";
    }
    
    
    @RequestMapping("/EditSubChartOfAccount")
    public String EditSubChartOfAccount(@RequestParam("subChartOfAccountId") String subChartOfAccountId, ModelMap model)
    {
    	Query query = new Query();
    	List<SubChartOfAccount> subChartOfAccountDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subChartOfAccountId").is(subChartOfAccountId)),SubChartOfAccount.class);
    	
    	List<ChartOfAccount> chartOfAccountList= chartOfAccountRepository.findAll();
    	String chartOfAccountName = "";
    	for(int i=0;i<chartOfAccountList.size();i++)
    	{
    		if(chartOfAccountList.get(i).getChartOfAccountId().equals(subChartOfAccountDetails.get(0).getChartOfAccountId()))
    		{
    			chartOfAccountName=chartOfAccountList.get(i).getChartOfAccountName();
    			break;
    		}
    	}
    	model.addAttribute("chartOfAccountName",chartOfAccountName);
    	model.addAttribute("chartOfAccountList",chartOfAccountList);
    	model.addAttribute("subChartOfAccountDetails", subChartOfAccountDetails);
    	return "EditSubChartOfAccount";
    }
    
    @RequestMapping(value="/EditSubChartOfAccount", method=RequestMethod.POST)
    public String EditSubChartOfAccountPostMethod(@ModelAttribute SubChartOfAccount subChartOfAccount, Model model)
    {
    	try
    	{
    		subChartOfAccount = new SubChartOfAccount(subChartOfAccount.getSubChartOfAccountId(), subChartOfAccount.getSubChartOfAccountNumber(), subChartOfAccount.getSubChartOfAccountName(), subChartOfAccount.getChartOfAccountId(), subChartOfAccount.getChartOfAccountNumber(), 
    				subChartOfAccount.getCreationDate(), subChartOfAccount.getUpdateDate(), subChartOfAccount.getUserId());
	        subChartOfAccountRepository.save(subChartOfAccount);
	        model.addAttribute("subChartOfAccountStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("subChartOfAccountStatus","Fail");
    	}


    	List<SubChartOfAccount> subChartOfAccountList= subChartOfAccountRepository.findAll();
    	List<ChartOfAccount> chartOfAccountList= chartOfAccountRepository.findAll();
    	
    	for(int i=0;i<subChartOfAccountList.size();i++)
    	{
    		for(int j=0;j<chartOfAccountList.size();j++)
    		{
    			try
    			{
    			if(subChartOfAccountList.get(i).getChartOfAccountId().equals(chartOfAccountList.get(j).getChartOfAccountId()))
    			{
    				subChartOfAccountList.get(i).setChartOfAccountId(chartOfAccountList.get(j).getChartOfAccountName());
    				break;
    			}
    			}catch (Exception e) {
					// TODO: handle exception
				}
    		}
    		
    	}
    	
    	model.addAttribute("chartOfAccountList",chartOfAccountList);
      	model.addAttribute("subChartOfAccountList", subChartOfAccountList);
    	
    	return "SubChartOfAccountMaster";
    }


    @ResponseBody
    @RequestMapping("/getCOAWiseNumber")
	public List<ChartOfAccount> getCOAWiseNumber(@RequestParam("chartOfAccountId") String chartOfAccountId, HttpSession session)
	{
	       Query query = new Query();
	       List<ChartOfAccount> chartOfAccountList = mongoTemplate.find(query.addCriteria(Criteria.where("chartOfAccountId").is(chartOfAccountId)), ChartOfAccount.class);
	      
	      return chartOfAccountList;
	}
    
}
