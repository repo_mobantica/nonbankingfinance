package com.nonbankingfinance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nonbankingfinance.bean.ChartOfAccount;
import com.nonbankingfinance.bean.SubChartOfAccount;
import com.nonbankingfinance.repository.ChartOfAccountRepository;
import com.nonbankingfinance.repository.SubChartOfAccountRepository;

@Controller
@RequestMapping("/")
public class SubChartOfAccountImportAndExportController {


	@Autowired
    SubChartOfAccountRepository subChartOfAccountRepository;
	@Autowired
	ChartOfAccountRepository chartOfAccountRepository;

	@Autowired
    ServletContext context;
	
	SubChartOfAccount subChartOfAccount = new SubChartOfAccount();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;
	
	HSSFSheet worksheet;
	XSSFSheet worksheet1;
	
	@Autowired
	MongoTemplate mongoTemplate;
	   
	//Code For Auto generation for subChartOfAccount id
	String subChartOfAccountCode;
    
    private String SubChartOfAccountCode()
    {
    	long subChartOfAccountCount  = subChartOfAccountRepository.count();
    	
        if(subChartOfAccountCount<10)
        {
        	subChartOfAccountCode = "SC000"+(subChartOfAccountCount+1);
        }
        else if((subChartOfAccountCount>=10) && (subChartOfAccountCount<100))
        {
        	subChartOfAccountCode = "SC00"+(subChartOfAccountCount+1);
        }
        else if((subChartOfAccountCount>=100) && (subChartOfAccountCount<1000))
        {
        	subChartOfAccountCode = "SC0"+(subChartOfAccountCount+1);
        }
        else if(subChartOfAccountCount>1000)
        {
        	subChartOfAccountCode = "SC"+(subChartOfAccountCount+1);
        }
        
        return subChartOfAccountCode;
    }
    
		    @RequestMapping(value="/ImportNewSubChartOfAccount")
		    public String importNewSubChartOfAccount(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
		    {

		    	model.addAttribute("progressBarPer",0);
		    	model.addAttribute("statusFlag",0);
		    	
		        return "ImportNewSubChartOfAccount";
		    }
			
			//private static final String INTERNAL_FILE="NewsubChartOfAccount.xlsx";
			@RequestMapping(value = "/DownloadSubChartOfAccountTemplate")
			public String downloadSubChartOfAccountTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
			{
				
				String filename = "NewSubChartOfAccount.xlsx";
		        response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				
				String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
				response.setContentType("APPLICATION/OCTET-STREAM");
				response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");
		 
				
				FileInputStream fileInputStream = new FileInputStream(filepath+filename);
		 
				int i;
				while ((i = fileInputStream.read()) != -1) {
					out.write(i);
				}
				fileInputStream.close();
				out.close();
				
				return "ImportNewSubChartOfAccount";
			}
			
			

			
			@SuppressWarnings({ "deprecation"})
			@RequestMapping(value = "/ImportNewSubChartOfAccount", method = RequestMethod.POST)
			public String uploadSubChartOfAccountDetails(@RequestParam("subChartOfAccount_excel") MultipartFile subChartOfAccount_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
			{

		    	List<ChartOfAccount> chartOfAccountList= chartOfAccountRepository.findAll();
		    	List<SubChartOfAccount> subchartofaccountList= subChartOfAccountRepository.findAll();
		    	
		    	List<SubChartOfAccount> subchartofaccountList1= new ArrayList<SubChartOfAccount>();
		    	
		    	String employeeId = (String)request.getSession().getAttribute("employeeId");
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
				LocalDate localDate = LocalDate.now();
				String todayDate=dtf.format(localDate);

				int progressBarPer=0;
				int flag1=0,flag2=0;
				String chartOfAccountId="";
				int chartOfAccountNumber=0;
				
				if (!subChartOfAccount_excel.isEmpty() || subChartOfAccount_excel != null )
				{
					try 
					{
						if(subChartOfAccount_excel.getOriginalFilename().endsWith("xls") || subChartOfAccount_excel.getOriginalFilename().endsWith("xlsx") || subChartOfAccount_excel.getOriginalFilename().endsWith("csv"))
						{
							if(subChartOfAccount_excel.getOriginalFilename().endsWith("xlsx"))
							{
								 InputStream stream = subChartOfAccount_excel.getInputStream();
								 XSSFWorkbook workbook = new XSSFWorkbook(stream);
								 
								 XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet
								 
								 int firstRow = sheet.getFirstRowNum();
								 
								 XSSFRow firstrow = sheet.getRow(firstRow);
								 
								@SuppressWarnings("unused")
								int lastColumnCount = firstrow.getLastCellNum();
								 
								@SuppressWarnings("unused")
								 Iterator<Row> rowIterator = sheet.iterator();   
								 int last_no=sheet.getLastRowNum();
								 
								 SubChartOfAccount subChartOfAccount = new SubChartOfAccount();
								 for(int i=0;i<last_no;i++)
						         {
									 flag1=0;
									 flag2=0;
									 try
									 {
									 XSSFRow row = sheet.getRow(i+1);
									 
						                 	 // Skip read heading 
						                     if (row.getRowNum() == 0) 
						                     {
						                    	 continue;
						                     }
						                     
						                     for(int k=0;k<chartOfAccountList.size();k++)
											 {
						                    	 if(chartOfAccountList.get(k).getChartOfAccountName().equalsIgnoreCase(row.getCell(2).getStringCellValue()))
						                    	 {
						                    		 flag1=1;
						                    		 chartOfAccountId=chartOfAccountList.get(k).getChartOfAccountId();
						                    		 chartOfAccountNumber=chartOfAccountList.get(k).getChartOfAccountNumber();
								                     break;
						                    	 }
												 
											 }
						                  for(int j=0;j<subchartofaccountList.size();j++)
						                  {
						                	  if(subchartofaccountList.get(j).getChartOfAccountId().equals(chartOfAccountId) && subchartofaccountList.get(j).getSubChartOfAccountName().equalsIgnoreCase(row.getCell(0).getStringCellValue()))
						                	  {
						                		  flag2=1;
						                	  }
						                	  
						                  }
						                     
						                     
						                     if(flag1==1 && flag2==0)
						                     {
					                    		 subChartOfAccount = new SubChartOfAccount();
							                     subChartOfAccount.setSubChartOfAccountId(SubChartOfAccountCode());
							                     
							                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
							                     subChartOfAccount.setSubChartOfAccountName(row.getCell(0).getStringCellValue());
							                     
							                     row.getCell(1).setCellType(Cell.CELL_TYPE_NUMERIC);
							                     subChartOfAccount.setSubChartOfAccountNumber((int) row.getCell(1).getNumericCellValue());
							                     
							                     subChartOfAccount.setChartOfAccountId(chartOfAccountId);
							                     
							                     subChartOfAccount.setChartOfAccountNumber(chartOfAccountNumber);
							                     
												 subChartOfAccount.setCreationDate(todayDate);
												 subChartOfAccount.setUpdateDate(todayDate);
												 subChartOfAccount.setUserId(employeeId);
												 subChartOfAccountRepository.save(subChartOfAccount); 
						                     }
						                     else
						                     {

					                    		 subChartOfAccount = new SubChartOfAccount();
							                     subChartOfAccount.setSubChartOfAccountId(SubChartOfAccountCode());
							                     
							                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
							                     subChartOfAccount.setSubChartOfAccountName(row.getCell(0).getStringCellValue());
							                     
							                     row.getCell(1).setCellType(Cell.CELL_TYPE_NUMERIC);
							                     subChartOfAccount.setSubChartOfAccountNumber((int) row.getCell(1).getNumericCellValue());
							                     row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
							                     subChartOfAccount.setChartOfAccountId(row.getCell(2).getStringCellValue());
							                     
							                     if(flag1==0)
							                     {
							                    	 subChartOfAccount.setStatus("COA Not present");
							                     }
							                     else 
							                     {
							                    	 subChartOfAccount.setStatus("Sub COA already exit");
							                     }
							                     subchartofaccountList1.add(subChartOfAccount);
						                    	 
						                     }
						                     
									 }
									 catch (Exception e) {
										// TODO: handle exception
									}
						          }

								 progressBarPer= ((last_no-subchartofaccountList1.size())*100)/last_no;
						         	
								 workbook.close();
							}
							
						}//if after inner if
						
					}
					catch(Exception e)
					{
						
					}
				}

				int statusFlag;
				if(subchartofaccountList1.size()!=0)
				{
					statusFlag=1;	
				}
				else
				{
					statusFlag=2;
				}
				
		    	model.addAttribute("progressBarPer",progressBarPer);
		    	
		    	model.addAttribute("statusFlag",statusFlag);
		    	model.addAttribute("subchartofaccountList1",subchartofaccountList1);
				return "ImportNewSubChartOfAccount";
			}
			
			
}
