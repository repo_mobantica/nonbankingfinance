package com.nonbankingfinance.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.Bank;
import com.nonbankingfinance.bean.Branch;
import com.nonbankingfinance.bean.BranchPayment;
import com.nonbankingfinance.bean.BranchPaymentHistory;
import com.nonbankingfinance.bean.CompanyBank;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.GoldItem;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.bean.Occupation;
import com.nonbankingfinance.repository.BranchPaymentHistoryRepository;
import com.nonbankingfinance.repository.BranchPaymentRepository;
import com.nonbankingfinance.repository.BranchRepository;
import com.nonbankingfinance.services.*;

@Controller
public class VoucherController {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	BranchRepository branchRepository;  
	@Autowired
	BranchPaymentRepository branchPaymentRepository;
	@Autowired
	BranchPaymentHistoryRepository branchPaymentHistoryRepository;
	@Autowired
	VoucherServices voucherServices;

	@RequestMapping("/VoucherMaster")
	public String CustomerMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		String branchId = (String)req.getSession().getAttribute("branchId");
		List<BranchPaymentHistory> branchPaymentHistoryList=voucherServices.GetVoucherList(2, 1, branchId, 0);

		model.addAttribute("branchPaymentHistoryList", branchPaymentHistoryList);

		return "VoucherMaster";
	}

	@RequestMapping("/AddVoucher")
	public String AddVoucher(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		String branchId = (String)req.getSession().getAttribute("branchId");
		String branchName=(String)req.getSession().getAttribute("branchName");
		List<Branch> branchList = branchRepository.findAll();

		model.addAttribute("branchId",branchId);
		model.addAttribute("branchName",branchName);
		model.addAttribute("branchList", branchList);
		return "AddVoucher";

	}


	@RequestMapping(value = "/AddVoucher", method = RequestMethod.POST)
	public String AddCustomer(@RequestParam("toBranchId") String toBranchId, @RequestParam("userId") String userId, @RequestParam("amount") double amount, Model model, HttpServletRequest req, HttpServletResponse res)
	{

		String branchId = (String)req.getSession().getAttribute("branchId");

		BranchPaymentHistory branchPaymentHistory=new BranchPaymentHistory();
		branchPaymentHistory.setBranchId(branchId);
		branchPaymentHistory.setAmount(amount);
		branchPaymentHistory.setPaymentType(2);
		branchPaymentHistory.setDebiteOrcredit(1);
		branchPaymentHistory.setPacketOrbranchId(toBranchId);
		branchPaymentHistory.setCreateDate(new Date());
		branchPaymentHistory.setUserId(userId);
		branchPaymentHistory.setStatus(0);

		branchPaymentHistoryRepository.save(branchPaymentHistory);



		List<BranchPaymentHistory> branchPaymentHistoryList=voucherServices.GetVoucherList(2, 1, branchId, 0);
		model.addAttribute("branchPaymentHistoryList", branchPaymentHistoryList);

		return "VoucherMaster";
	}

}
