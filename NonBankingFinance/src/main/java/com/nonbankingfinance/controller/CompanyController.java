package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.nonbankingfinance.bean.Bank;
import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.bean.CompanyBank;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.repository.BankRepository;
import com.nonbankingfinance.repository.CompanyBankRepository;
import com.nonbankingfinance.repository.CompanyRepository;

@Controller
@RequestMapping("/")
public class CompanyController {

	@Autowired
	BankRepository bankRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	CompanyBankRepository companybankRepository;
	
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	String companyCode;
	private String CompanyCode()
	{
		long companyCount = companyRepository.count();
		
		if(companyCount<10)
		{
			companyCode = "CO000"+(companyCount+1);
		}
		else if((companyCount>=10) && (companyCount<100))
		{
			companyCode = "CO00"+(companyCount+1);
		}
		else if((companyCount>=100) && (companyCount<1000))
		{
			companyCode = "CO0"+(companyCount+1);
		}
		else if(companyCount>1000)
		{
			companyCode = "CO"+(companyCount+1);
		}
		
		return companyCode;
	} 
	
    @RequestMapping("/CompanyMaster")
    public String CompanyMaster(ModelMap model)
    {
    	List<Company> companyList = companyRepository.findAll();
	    model.addAttribute("companyList", companyList);
    	return "CompanyMaster";
    }
    
  //Request Mapping For Add Company
    
    @RequestMapping("/AddCompany")
    public String addCompany(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	List<Bank> bankList = bankRepository.findAll();
    	
	    model.addAttribute("bankList", bankList);
    	model.addAttribute("companyCode",CompanyCode());
        return "AddCompany";
    }
  
    

    @RequestMapping(value = "/AddCompany", method = RequestMethod.POST)
    public String AddCompany(@ModelAttribute Company company, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		company = new Company(company.getCompanyId(), company.getCompanyName(), company.getCompanyType(), company.getCompanyPanno(), company.getCompanyRegistrationno(),
    				company.getCompanyTanno(), company.getCompanyPfno(), company.getCompanyGstno(), company.getCompanyAddress(),
    				 company.getPinCode(),company.getPhoneNumber(),
    				company.getCreationDate(), company.getUpdateDate(), company.getUserId());
    		companyRepository.save(company);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}

    	
    	model.addAttribute("companyCode",CompanyCode());
        return "AddCompany";
    }


    @RequestMapping("/EditCompany")
    public String EditCompany(@RequestParam("companyId") String companyId, ModelMap model)
    {

    	List<Bank> bankList = bankRepository.findAll();
    	Query query = new Query();
    	List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId)),Company.class);
    	
    	
    	
    	List<CompanyBank> companyBankList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId)), CompanyBank.class);
	      
  
    	model.addAttribute("bankList", bankList);
    	model.addAttribute("companyBankList", companyBankList);
    	model.addAttribute("companyDetails", companyDetails);
    	
    	return "EditCompany";
    }
    

    @RequestMapping(value="/EditCompany",method=RequestMethod.POST)
    public String EditCompanyPostMethod(@ModelAttribute Company company, Model model)
    {
    	try
    	{
    		company = new Company(company.getCompanyId(), company.getCompanyName(), company.getCompanyType(), company.getCompanyPanno(), company.getCompanyRegistrationno(),
    				company.getCompanyTanno(), company.getCompanyPfno(), company.getCompanyGstno(), company.getCompanyAddress(), 
    				company.getPinCode(),company.getPhoneNumber(),
    				company.getCreationDate(), company.getUpdateDate(), company.getUserId());
    		companyRepository.save(company);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("companyStatus","Fail");
    	}
    	
    	List<Company> companyList = companyRepository.findAll();
	    model.addAttribute("companyList", companyList);
    	return "CompanyMaster";
    }
    
    @ResponseBody
    @RequestMapping("/getBankDetailsByBankIdWise")
    public List<Bank> getBankDetailsByBankIdWise(@RequestParam("bankId") String bankId, HttpSession session)
    {
    	try
    	{
    		Query query = new Query();
	       	List<Bank> bankList = mongoTemplate.find(query.addCriteria(Criteria.where("bankId").is(bankId)), Bank.class);
	       	return bankList;
    	}
    	catch(Exception e)
    	{
    		return null;
    	}
    	
    }  
    
    
	
	 public long GenerateCompanyBankId()
	 {
		 long customerbankId = 0;
	    	
	   	List<CompanyBank> companyBankList = companybankRepository.findAll();
	   	
	   	int size = companyBankList.size();
	   	
	   	if(size!=0)
	   	{
	   		customerbankId = companyBankList.get(size-1).getCompanyBankId();
	   	}
	   	
	   	return customerbankId+1;
	 }
	 
    
	 @ResponseBody
	 @RequestMapping("/AddCompantAccountDetails")
	 public List<CompanyBank> AddCompantAccountDetails(@RequestParam("companyId") String companyId, @RequestParam("bankId") String bankId, @RequestParam("branchName") String branchName, @RequestParam("bankIfsc") String bankIfsc, @RequestParam("bankAccountNumber") String bankAccountNumber, HttpSession session)
	 {
	   	try
	   	{
	   		CompanyBank compantbank = new CompanyBank();
	   		compantbank.setCompanyBankId(GenerateCompanyBankId());
	   		compantbank.setCompanyId(companyId);
	   		
	   		compantbank.setBankId(bankId);
	   		compantbank.setBranchName(branchName);
	   		compantbank.setBankIfsc(bankIfsc);
	   		compantbank.setBankAccountNumber(bankAccountNumber);
	   		
	   		companybankRepository.save(compantbank);
	   		
	   		Query query = new Query();
	       	List<CompanyBank> companyBankList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId)), CompanyBank.class);
	       	return companyBankList;
	   	}
	   	catch(Exception e)
	   	{
	   		return null;
	   	}
	    	
     }	 
    
	    @ResponseBody
	    @RequestMapping("/DeleteCompanyAccount")
	    public List<CompanyBank> DeleteCompanyAccount(@RequestParam("companyBankId") String companyBankId, @RequestParam("companyId") String companyId, HttpSession session)
	    {
	    	try
	    	{
	    		Query query = new Query();
	    		mongoTemplate.remove(query.addCriteria(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId)).and("companyId").is(companyId)), CompanyBank.class);

		   		 query = new Query();
		       	List<CompanyBank> companyBankList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId)), CompanyBank.class);
		       	return companyBankList;
	    	}
	    	catch(Exception e)
	    	{
	    		return null;
	    	}
	    	
	    } 
	
}
