package com.nonbankingfinance.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nonbankingfinance.bean.EnquirySource;
import com.nonbankingfinance.bean.Occupation;
import com.nonbankingfinance.repository.EnquirySourceRepository;

@Controller
@RequestMapping("/")
public class EnquirySourceImportAndExportController {


	@Autowired
	EnquirySourceRepository enquirySourceRepository;  
	@Autowired
    ServletContext context;
	
	EnquirySource enquirySource = new EnquirySource();
	
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;
	
	HSSFSheet worksheet;
	XSSFSheet worksheet1;
	
	@Autowired
	MongoTemplate mongoTemplate;
	   

	String enquirySourceCode;
	private String EnquirySourceCode()
	{
		long enquirySourceCount = enquirySourceRepository.count();
		
		if(enquirySourceCount<10)
		{
			enquirySourceCode = "EN000"+(enquirySourceCount+1);
		}
		else if((enquirySourceCount>=10) && (enquirySourceCount<100))
		{
			enquirySourceCode = "EN00"+(enquirySourceCount+1);
		}
		else if((enquirySourceCount>=100) && (enquirySourceCount<1000))
		{
			enquirySourceCode = "EN0"+(enquirySourceCount+1);
		}
		else if(enquirySourceCount>1000)
		{
			enquirySourceCode = "EN"+(enquirySourceCount+1);
		}
		
		return enquirySourceCode;
	} 
	

    @RequestMapping(value="/ImportNewEnquirySource")
    public String importNewDesignation(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
        return "ImportNewEnquirySource";
    }
	
	//private static final String INTERNAL_FILE="Newdesignation.xlsx";
	@RequestMapping(value = "/DownloadEnquirySourceTemplate")
	public String downloadDesignationTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{
		
		String filename = "NewEnquirySource.xlsx";
        response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");
 
		
		FileInputStream fileInputStream = new FileInputStream(filepath+filename);
 
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
		
		return "ImportNewEnquirySource";
	}
	

	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewEnquirySource", method = RequestMethod.POST)
	public String ImportNewEnquirySource(@RequestParam("enquirySource_excel") MultipartFile enquirySource_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

    	
    	String employeeId = (String)request.getSession().getAttribute("employeeId");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		

		int flag1=0;
		int progressBarPer=0;
    	List<EnquirySource> enquirysourceList= enquirySourceRepository.findAll();
		List<EnquirySource> enquirysourceList1=new ArrayList<EnquirySource>();
		
		if (!enquirySource_excel.isEmpty() || enquirySource_excel != null )
		{
			try 
			{
				if(enquirySource_excel.getOriginalFilename().endsWith("xls") || enquirySource_excel.getOriginalFilename().endsWith("xlsx") || enquirySource_excel.getOriginalFilename().endsWith("csv"))
				{
					if(enquirySource_excel.getOriginalFilename().endsWith("xlsx"))
					{
						 InputStream stream = enquirySource_excel.getInputStream();
						 XSSFWorkbook workbook = new XSSFWorkbook(stream);
						 
						 XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet
						 
						 int firstRow = sheet.getFirstRowNum();
						 
						 XSSFRow firstrow = sheet.getRow(firstRow);
						 
						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();
						 
						@SuppressWarnings("unused")
						 Iterator<Row> rowIterator = sheet.iterator();   
						 int last_no=sheet.getLastRowNum();
						 
						 EnquirySource enquirySource = new EnquirySource();
						 for(int i=0;i<last_no;i++)
				         {
							 flag1=0;
							 try
							 {
							 XSSFRow row = sheet.getRow(i+1);
							 
				                 	 // Skip read heading 
				                     if (row.getRowNum() == 0) 
				                     {
				                    	 continue;
				                     }
				                    	for(int j=0;j<enquirysourceList.size();j++)
				                    	{
				                    		if(enquirysourceList.get(j).getEnquirySourceName().equalsIgnoreCase(row.getCell(0).getStringCellValue()))
				                    		{
				                    			flag1=1;
				                    		}
				                    	}
				                     if(flag1==0) {
				                    	 enquirySource = new EnquirySource();
				                    	 enquirySource.setEnquirySourceId(EnquirySourceCode());
					                     
					                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
					                     enquirySource.setEnquirySourceName(row.getCell(0).getStringCellValue());
					                     
					                     enquirySource.setCreationDate(todayDate);
					                     enquirySource.setUpdateDate(todayDate);
					                     enquirySource.setUserId(employeeId);
					                     enquirySourceRepository.save(enquirySource);
				                     }
				                     else
				                     {

				                    	 enquirySource = new EnquirySource();
					                     
					                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
					                     enquirySource.setEnquirySourceName(row.getCell(0).getStringCellValue());
					                     enquirySource.setStatus("This is Already exit");
					                     enquirysourceList1.add(enquirySource);
					                     
				                     }
				         }
						 catch (Exception e) {
							// TODO: handle exception
						}	
				          }

						 progressBarPer= ((last_no-enquirysourceList1.size())*100)/last_no;	
						 workbook.close();
					}
					
				}//if after inner if
				
			}
			catch(Exception e)
			{
			}
		}

		int statusFlag;
		if(enquirysourceList1.size()!=0)
		{
			statusFlag=1;	
		}
		else
		{
			statusFlag=2;
		}
		
    	model.addAttribute("progressBarPer",progressBarPer);
    	
    	model.addAttribute("statusFlag",statusFlag);
    	model.addAttribute("enquirysourceList1",enquirysourceList1);
		return "ImportNewEnquirySource";
	}
	
}
