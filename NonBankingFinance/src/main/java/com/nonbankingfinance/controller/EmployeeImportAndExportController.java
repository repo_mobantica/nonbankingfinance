package com.nonbankingfinance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nonbankingfinance.bean.Department;
import com.nonbankingfinance.bean.Designation;
import com.nonbankingfinance.bean.Employee;
import com.nonbankingfinance.repository.DepartmentRepository;
import com.nonbankingfinance.repository.DesignationRepository;
import com.nonbankingfinance.repository.EmployeeRepository;

@Controller
@RequestMapping("/")
public class EmployeeImportAndExportController {

	@Autowired
    DesignationRepository designationRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
    EmployeeRepository employeeRepository;

	@Autowired
    ServletContext context;
	
	Employee employee = new Employee();
	
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;
	
	HSSFSheet worksheet;
	XSSFSheet worksheet1;
	
	@Autowired
	MongoTemplate mongoTemplate;
	   

	//Code generation for company code
	String employeeCode;
	private String employeeCode()
	{
		long employeeCount = employeeRepository.count();
	
		if(employeeCount<10)
		{
			employeeCode = "EM000"+(employeeCount+1);
		}
		else if((employeeCount>=10) && (employeeCount<100))
		{
			employeeCode = "EM00"+(employeeCount+1);
		}
		else if((employeeCount>=100) && (employeeCount<1000))
		{
			employeeCode = "EM0"+(employeeCount+1);
		}
		else if(employeeCount>1000)
		{
			employeeCode = "EM"+(employeeCount+1);
		}
		
		return employeeCode;
	}

    @RequestMapping(value="/ImportNewEmployee")
    public String importNewDesignation(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {

    	model.addAttribute("progressBarPer",0);
    	model.addAttribute("statusFlag",0);
        return "ImportNewEmployee";
    }
	
	//private static final String INTERNAL_FILE="Newdesignation.xlsx";
	@RequestMapping(value = "/DownloadEmployeeTemplate")
	public String downloadDesignationTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{
		
		String filename = "NewEmployee.xlsx";
        response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");
 
		
		FileInputStream fileInputStream = new FileInputStream(filepath+filename);
 
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
		
		return "ImportNewEmployee";
	}
	
	

	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewEmployee", method = RequestMethod.POST)
	public String ImportNewEmployee(@RequestParam("employee_excel") MultipartFile employee_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

    	List<Department> departmentList= departmentRepository.findAll();
    	List<Designation> designationList= designationRepository.findAll();
    	List<Employee> employeeList= employeeRepository.findAll();
    	
    	String employeeId = (String)request.getSession().getAttribute("employeeId");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		int flag1=0,flag2=0;
		
		String departmentId="";
		String designationId="";
		String employeeMailId="";

		int progressBarPer=0;
		
		Employee employee1=new Employee();
		List<Employee> employeeList1=new ArrayList<Employee>();
		
		
		if (!employee_excel.isEmpty() || employee_excel != null )
		{
			try 
			{
				if(employee_excel.getOriginalFilename().endsWith("xls") || employee_excel.getOriginalFilename().endsWith("xlsx") || employee_excel.getOriginalFilename().endsWith("csv"))
				{
					if(employee_excel.getOriginalFilename().endsWith("xlsx"))
					{
						 InputStream stream = employee_excel.getInputStream();
						 XSSFWorkbook workbook = new XSSFWorkbook(stream);
						 
						 XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet
						 
						 int firstRow = sheet.getFirstRowNum();
						 
						 XSSFRow firstrow = sheet.getRow(firstRow);
						 
						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();
						 
						@SuppressWarnings("unused")
						 Iterator<Row> rowIterator = sheet.iterator();   
						 int last_no=sheet.getLastRowNum();
						 
						 Employee employee = new Employee();
						 for(int i=0;i<last_no;i++)
				         {
							 try
							 {
							 flag1=0;
							 flag2=0;
							 employee1=new Employee();
							 
							 XSSFRow row = sheet.getRow(i+1);
							 
				                 	 // Skip read heading 
				                     if (row.getRowNum() == 0) 
				                     {
				                    	 continue;
				                     }

				                   for(int j=0;j<employeeList.size();j++)
				                   {
					                   row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
				                	   employeeMailId=row.getCell(6).getStringCellValue();
				                	   if(employeeMailId.equalsIgnoreCase(employeeList.get(j).getEmployeeEmailId()))
				                	   {
				                		   flag2=1;
				                		   break;
				                	   }
				                	   
				                   }
				                     for(int j=0;j<designationList.size();j++)
				                     {

				                    	 if(designationList.get(j).getDesignationName().equalsIgnoreCase(row.getCell(7).getStringCellValue()))
				                    	 {
						                     for(int k=0;k<departmentList.size();k++)
											 {
						                    	 if(departmentList.get(k).getDepartmentName().equalsIgnoreCase(row.getCell(8).getStringCellValue()))
						                    	 {
						                    		 
							                    		flag1=1;
							                    		departmentId=departmentList.get(k).getDepartmentId();
						                    		 	designationId=designationList.get(j).getDesignationId();
									                    break;
						                    	 }
				                    		 }
				                    	 }
				                     }
				                  if(flag1==1 && flag2==0)
				                  {			
				                	  employee = new Employee();
				                	  employee.setEmployeeId(employeeCode());
					                     
					                     row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
					                     employee.setEmployeefirstName(row.getCell(0).getStringCellValue());
					                     employee.setEmployeemiddleName(row.getCell(1).getStringCellValue());
					                     employee.setEmployeelastName(row.getCell(2).getStringCellValue());

					                     row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
					                     employee.setEmployeePancardno(row.getCell(3).getStringCellValue());

					                     row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
					                     employee.setEmployeeAadharcardno(row.getCell(4).getStringCellValue());

					                     row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
					                     employee.setEmployeeMobileno(row.getCell(5).getStringCellValue());

					                     row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
					                     employee.setEmployeeEmailId(row.getCell(6).getStringCellValue());
					                     

					                     employee.setDesignationId(designationId);//7
					                     employee.setDepartmentId(departmentId);//8

					                     row.getCell(9).setCellType(Cell.CELL_TYPE_STRING);
					                     employee.setEmployeeBasicpay(row.getCell(9).getStringCellValue());

					                     row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
					                     employee.setEmployeeHra(row.getCell(10).getStringCellValue());

					                     row.getCell(11).setCellType(Cell.CELL_TYPE_STRING);
					                     employee.setEmployeeDa(row.getCell(11).getStringCellValue());

					                     row.getCell(12).setCellType(Cell.CELL_TYPE_STRING);
					                     employee.setEmployeeCa(row.getCell(12).getStringCellValue());
					                     
					                     
					                     employee.setCreationDate(todayDate);
					                     employee.setUpdateDate(todayDate);
					                     employee.setUserId(employeeId);
					                     employeeRepository.save(employee);
				                  }
				                  else
				                  {
				                	  
				                	 if(flag1==0) {
					                	  
				                	  employee1=new Employee();
				                	  employee1.setEmployeefirstName(row.getCell(0).getStringCellValue());
				                	  employee1.setEmployeemiddleName(row.getCell(1).getStringCellValue());
				                	  
				                	  employee1.setEmployeelastName(row.getCell(2).getStringCellValue());
				                	  employee1.setDepartmentId(row.getCell(8).getStringCellValue());
				                	  employee1.setDesignationId(row.getCell(7).getStringCellValue());
				                	  employee1.setEmployeeEmailId(row.getCell(6).getStringCellValue());

					                  row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
				                	  employee1.setEmployeeMobileno(row.getCell(5).getStringCellValue());
				                	  employee1.setStatus("Depart Or Designation not present");
				                	  employeeList1.add(employee1);
				                	  }
				                	 else if(flag2==1)
				                	  {
										  employee1=new Employee();
					                	  employee1.setEmployeefirstName(row.getCell(0).getStringCellValue());
					                	  employee1.setEmployeemiddleName(row.getCell(1).getStringCellValue());
					                	  
					                	  employee1.setEmployeelastName(row.getCell(2).getStringCellValue());
					                	  employee1.setDepartmentId(row.getCell(8).getStringCellValue());
					                	  employee1.setDesignationId(row.getCell(7).getStringCellValue());
					                	  employee1.setEmployeeEmailId(row.getCell(6).getStringCellValue());

						                  row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
					                	  employee1.setEmployeeMobileno(row.getCell(5).getStringCellValue());
					                	  employee1.setStatus("Given mail id already present");
					                	  employeeList1.add(employee1);
				                	  }
				                	  
				                  }
				                     
				         }
						 catch (Exception e) {
							// TODO: handle exception
						}	
				          }
						 
						 progressBarPer= ((last_no-employeeList1.size())*100)/last_no;
				         		workbook.close();
					}
					
				}//if after inner if
				
			}
			catch(Exception e)
			{
			}
		}

		int statusFlag;
		if(employeeList1.size()!=0)
		{
			statusFlag=1;	
		}
		else
		{
			statusFlag=2;
		}
		
    	model.addAttribute("progressBarPer",progressBarPer);
    	
    	model.addAttribute("statusFlag",statusFlag);
    	model.addAttribute("employeeList1",employeeList1);
		return "ImportNewEmployee";
	}
	
	
	
}
