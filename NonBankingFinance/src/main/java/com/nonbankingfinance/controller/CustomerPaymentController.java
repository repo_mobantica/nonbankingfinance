package com.nonbankingfinance.controller;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.NumberToWordConversion;
import com.nonbankingfinance.bean.Branch;
import com.nonbankingfinance.bean.BranchPayment;
import com.nonbankingfinance.bean.BranchPaymentHistory;
import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.bean.CustomerLoanDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.bean.GoldLoanScheme;
import com.nonbankingfinance.repository.BranchPaymentHistoryRepository;
import com.nonbankingfinance.repository.BranchPaymentRepository;
import com.nonbankingfinance.repository.CompanyRepository;
import com.nonbankingfinance.repository.CustomerGoldItemDetailsRepository;
import com.nonbankingfinance.repository.GoldLoanSchemeRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

import com.nonbankingfinance.repository.CustomerPaymentRepository;

@Controller
@RequestMapping("/")
public class CustomerPaymentController implements Printable  {

	@Autowired
	CustomerGoldItemDetailsRepository customergolditemdetailsRepository;
	@Autowired
	GoldLoanSchemeRepository goldloanschemeRepository;
	@Autowired
	CustomerPaymentRepository customerPaymentRepository; 
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	BranchPaymentRepository branchPaymentRepository;
	@Autowired
	BranchPaymentHistoryRepository branchPaymentHistoryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	static CustomerPayment paymentcustomerDetails;
	static CustomerDetails customerDetails;
	static CustomerLoanDetails customerLoanDetails;

	static Double totalAmount;
	static String paymentDate;
	static String paymentAmountinWords;

	static double paidPrincipalAmount;
	static double paidInterestAmount;
	static double paidDueAmount;

	static String customerAddress1;
	static String customerAddress2;

	static String branchName;


	String recieptCode;
	private String CustomerPaymentCode(String packetNumber)
	{
		Query query = new Query();
		List<CustomerPayment> paymentcustomerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)),CustomerPayment.class);

		recieptCode=packetNumber+"/LP/"+(paymentcustomerDetails.size()+1);

		return recieptCode;
	} 


	@RequestMapping("/CustomerPaymentMaster")
	public String CustomerMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		String branchId = (String)req.getSession().getAttribute("branchId");
		Query query = new Query();
		List<GoldLoanScheme> goldloanschemeDetails= goldloanschemeRepository.findAll();

		List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("paymentStatus").ne("Cleared")),CustomerLoanDetails.class);
		query = new Query();
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

		for(int i=0;i<customerLoanDetailsList.size();i++)
		{
			for(int j=0;j<customerDetailsList.size();j++)
			{
				if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId())) 
				{
					customerLoanDetailsList.get(i).setCustomerName(customerDetailsList.get(j).getCustomerName());
					break;
				}
			}
			for(int j=0;j<goldloanschemeDetails.size();j++)
			{
				if(customerLoanDetailsList.get(i).getGoldloanschemeId().equals(goldloanschemeDetails.get(j).getGoldloanschemeId()))
				{
					customerLoanDetailsList.get(i).setGoldloanschemeId(goldloanschemeDetails.get(j).getGoldloanschemeName());
					break;
				}
			}
		}

		model.addAttribute("customerLoanDetailsList", customerLoanDetailsList);

		return "CustomerPaymentMaster";
	}

	static double FindInterestPer(long totalDays)
	{
		double interest=0.0;
		if(totalDays<=30)
		{
			interest=2.15;
		}
		else if(totalDays<=90 && totalDays>30)
		{
			interest=2.35;
		}
		else if(totalDays<=120 && totalDays>90) 
		{
			interest=2.6;
		}
		else 
		{
			interest=2.85;
		}
		return interest;

	}

	@RequestMapping("/AddCustomerPayment")
	public String AddCustomerPayment(@RequestParam("packetNumber") String packetNumber, ModelMap model) throws ParseException
	{
		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

		Query query = new Query();
		CustomerLoanDetails customerLoanDetailsList = mongoTemplate.findOne(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)),CustomerLoanDetails.class);

		query = new Query();
		CustomerDetails customerDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("customerId").is(customerLoanDetailsList.getCustomerId())),CustomerDetails.class);

		query = new Query();
		List<CustomerPayment> paymentcustomerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber).and("paymentStatus").ne("Canceled")),CustomerPayment.class);

		long customerPaidPrincipalAmount=0;
		double paidInterestAmount=0;

		for(int i=0;i<paymentcustomerDetails.size();i++)
		{
			paidInterestAmount=paidInterestAmount+paymentcustomerDetails.get(i).getInterestAmount();
			customerPaidPrincipalAmount=customerPaidPrincipalAmount+paymentcustomerDetails.get(i).getPrincipalAmount();
			paymentcustomerDetails.get(i).setPaidDate(createDateFormat.format(paymentcustomerDetails.get(i).getCreationDate()));
		}

		query = new Query();
		GoldLoanScheme schemeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("goldloanschemeId").is(customerLoanDetailsList.getGoldloanschemeId())),GoldLoanScheme.class);

		String goldloanschemeName=schemeDetails.getGoldloanschemeName();

		Date todayDate=new Date();
		Date loanCreationDate = customerLoanDetailsList.getCreationDate();

		LocalDate today = LocalDate.now();                          //Today's date
		LocalDate loanDate1 = loanCreationDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		Period p = Period.between(loanDate1, today);

		//Now access the values as below
		int totalMonths=p.getMonths();
		if(p.getYears()>0)
		{
			totalMonths=p.getYears()*12+p.getMonths();
		}

		long totalInterest=0;
		long principalAmount=customerLoanDetailsList.getLoanAmount();

		double remainingPrincipal=principalAmount-customerPaidPrincipalAmount;

		double currentMonthInterest=0;

		Date lastPaymentdate;
		if(customerPaidPrincipalAmount==0)
		{
			lastPaymentdate=loanCreationDate;
		}
		else
		{
			lastPaymentdate=paymentcustomerDetails.get(paymentcustomerDetails.size()-1).getCreationDate();
		}

		long totalDays=0;

		String todayDate1=createDateFormat.format(new Date());
		Date firstDate = null;
		Date secondDate = null;

		String loanDate=createDateFormat.format(lastPaymentdate);
		firstDate = createDateFormat.parse(loanDate);
		secondDate = createDateFormat.parse(todayDate1);

		long diff = secondDate.getTime() - firstDate.getTime();

		totalDays = diff / (24 * 60 * 60 * 1000);

		double interestPer=FindInterestPer(totalDays);
		long interestAmount;
		long noticeCharge=0;

		if(totalDays<=7 && paymentcustomerDetails.size()==0)
		{
			interestAmount=(long) ((remainingPrincipal/100)*(interestPer/30)*7);
		}
		else
		{
			interestAmount=(long) ((remainingPrincipal/100)*(interestPer/30)*totalDays);
		}

		if(totalDays>=90 && totalDays<=180)
		{
			noticeCharge=50;
		}
		else if(totalDays>180 && totalDays<=270)
		{
			noticeCharge=100;
		}
		else if(totalDays>270 && totalDays<=360)
		{
			noticeCharge=150;
		}
		else if(totalDays>360)
		{
			noticeCharge=200;
		}

		long totalRemainingAmount=(long) (interestAmount+remainingPrincipal)+noticeCharge;

		/*
    	if(customerLoanDetailsList.get(0).getLoanType().equalsIgnoreCase("Fixed"))
    	{
    		if(p.getDays()>0)
    		{
    			totalInterest=(long) (principalAmount*(totalMonths+1)*interestPerMonth/100);
    		}
    		else
    		{
    			totalInterest=(long) (principalAmount*(totalMonths)*interestPerMonth/100);
    		}

    		currentMonthInterest=(long) (totalInterest-paidInterestAmount);
    		currentMonthPrincipal=principalAmount-customerPaidPrincipalAmount;
    	}
    	else
    	{
    		if(customerLoanDetailsList.get(0).getEmiType().equalsIgnoreCase("Fixed"))
    		{
        		if(p.getDays()>0)
        		{
        			totalInterest=(long) (principalAmount*(totalMonths+1)*interestPerMonth/100);
        		}
        		else
        		{
        			totalInterest=(long) (principalAmount*(totalMonths)*interestPerMonth/100);
        		}

        		currentMonthInterest=(long) (totalInterest-paidInterestAmount);
        		currentMonthPrincipal=principalAmount-customerPaidPrincipalAmount;
    		}
    		else
    		{

    	    	Date newDate = DateUtils.addMonths(customerLoanDetailsList.get(0).getCreationDate(), paymentcustomerDetails.size()+1);
    	    	Date date=new Date();
    	    	long diff1 = date.getTime() - newDate.getTime();

    	    	long fineDays = diff1 / (24 * 60 * 60 * 1000);

    	    	if(fineDays<0)
    	    	{
    	    		overDueDays=0;
    	    	}
    	    	else
    	    	{
    	    		overDueDays=fineDays;
    	    	}


    	    	cutsomerPrincipalAmount=customerLoanDetailsList.get(0).getLoanAmount()-customerPaidPrincipalAmount;

    	    	double r = ((customerLoanDetailsList.get(0).getInterestRate() / 12) / 100);

    	    	emi= (cutsomerPrincipalAmount * r * (Math.pow((1 + r), remainingMonths)) / ((Math.pow((1 + r), remainingMonths)) - 1));

    	    	emi=Math.round(emi);

    	    	currentMonthInterest = (cutsomerPrincipalAmount * r);

    	    	if(totalDays<=7)
    	    	{
    	    		double R = ((customerLoanDetailsList.get(0).getInterestRate() / 365) / 100);
    	    		currentMonthInterest=(cutsomerPrincipalAmount * R)*7;
    	    	}

    	    	currentMonthPrincipal=emi - currentMonthInterest;

    	    	currentMonthPrincipal=Math.round(currentMonthPrincipal);
    	    	currentMonthInterest=Math.round(currentMonthInterest);
    		}


    	}
		 */

		model.addAttribute("noticeCharge",noticeCharge);
		model.addAttribute("remainingPrincipal",remainingPrincipal);
		model.addAttribute("interestPer", interestPer);
		model.addAttribute("interestAmount", interestAmount);
		model.addAttribute("totalRemainingAmount",totalRemainingAmount);

		model.addAttribute("customerPaidPrincipalAmount", customerPaidPrincipalAmount);

		model.addAttribute("totalDays", totalDays);
		model.addAttribute("loanDate", loanDate);
		model.addAttribute("goldloanschemeName", goldloanschemeName);
		model.addAttribute("customerLoanDetailsList", customerLoanDetailsList);
		model.addAttribute("customerDetails", customerDetails);
		model.addAttribute("paymentcustomerDetails", paymentcustomerDetails);

		return "AddCustomerPayment";
	}


	@RequestMapping(value="/AddCustomerPayment", method = RequestMethod.POST)
	public String AddCustomerPayment(@RequestParam("packetNumber") String packetNumber, @RequestParam("customerId") String customerId, @RequestParam("paymentAmount") long paymentAmount, @RequestParam("interestAmount") double interestAmount, @RequestParam("noticeCharge") long noticeCharge,  @RequestParam("bankName") String bankName, @RequestParam("branchName") String branchName, @RequestParam("paymentMode") String paymentMode,  @RequestParam("refNumber") String refNumber,  @RequestParam("narration") String narration, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		String branchId = (String)req.getSession().getAttribute("branchId");
		String userId = (String)req.getSession().getAttribute("userId");
		String customerPaymentId=CustomerPaymentCode(packetNumber);

		Date date = new Date(); 
		try
		{

			long principalAmount=(long) (paymentAmount-interestAmount)-noticeCharge;

			CustomerPayment customerpayment=new CustomerPayment();
			customerpayment.setCustomerPaymentId(customerPaymentId);
			customerpayment.setPacketNumber(packetNumber);
			customerpayment.setCustomerId(customerId);
			customerpayment.setPrincipalAmount(principalAmount);
			customerpayment.setInterestAmount(interestAmount);
			//customerpayment.setPaymentDue(paymentDue);
			customerpayment.setNoticeCharge(noticeCharge);
			customerpayment.setBankName(bankName);
			customerpayment.setBranchName(branchName);
			customerpayment.setPaymentMode(paymentMode);
			customerpayment.setRefNumber(refNumber);
			customerpayment.setNarration(narration);

			customerpayment.setCreationDate(date);
			customerpayment.setUserId(userId);
			customerpayment.setBranchId(branchId);
			customerpayment.setPaymentStatus("Uncleared");

			customerPaymentRepository.save(customerpayment);

			long totalAmount=(long) (principalAmount+interestAmount+noticeCharge);
			// add Amount in Branch Amount
			BranchPayment branchPayment=branchPaymentRepository.findByBranchId(branchId);

			branchPayment.setAmount(branchPayment.getAmount()+totalAmount);
			branchPaymentRepository.save(branchPayment);


			BranchPaymentHistory branchPaymentHistory=new BranchPaymentHistory();
			branchPaymentHistory.setBranchId(branchId);
			branchPaymentHistory.setAmount(principalAmount+interestAmount+noticeCharge);
			branchPaymentHistory.setPaymentType(1);
			branchPaymentHistory.setDebiteOrcredit(2);
			branchPaymentHistory.setPacketOrbranchId(packetNumber);
			branchPaymentHistory.setCreateDate(new Date());
			branchPaymentHistory.setUserId(userId);
			branchPaymentHistory.setStatus(1);

			branchPaymentHistoryRepository.save(branchPaymentHistory);


		}
		catch (Exception e) {

			System.out.println("eoore  = "+e);
			// TODO: handle exception
		}

		Query query = new Query();
		List<GoldLoanScheme> goldloanschemeDetails= goldloanschemeRepository.findAll();

		List<CustomerLoanDetails> customerLoanDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId).and("paymentStatus").ne("Cleared")),CustomerLoanDetails.class);
		query = new Query();
		List<CustomerDetails> customerDetailsList = mongoTemplate.find(query.addCriteria(Criteria.where("branchId").is(branchId)),CustomerDetails.class);

		for(int i=0;i<customerLoanDetailsList.size();i++)
		{
			for(int j=0;j<customerDetailsList.size();j++)
			{
				if(customerLoanDetailsList.get(i).getCustomerId().equalsIgnoreCase(customerDetailsList.get(j).getCustomerId())) 
				{
					customerLoanDetailsList.get(i).setCustomerName(customerDetailsList.get(j).getCustomerName());
					break;
				}
			}
			for(int j=0;j<goldloanschemeDetails.size();j++)
			{
				if(customerLoanDetailsList.get(i).getGoldloanschemeId().equals(goldloanschemeDetails.get(j).getGoldloanschemeId()))
				{
					customerLoanDetailsList.get(i).setGoldloanschemeId(goldloanschemeDetails.get(j).getGoldloanschemeName());
					break;
				}
			}
		}



		PrintCustomerPaymentReceipt1(customerPaymentId, model,req,res);

		model.addAttribute("customerLoanDetailsList", customerLoanDetailsList);

		return "CustomerPaymentMaster";
	}



	@RequestMapping("/CustomerPaymentReceipt")
	public String CustomerPaymentReceipt(@RequestParam("packetNumber") String packetNumber, ModelMap model) throws ParseException
	{
		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

		Query query = new Query();
		List<CustomerLoanDetails> customerLoanDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber)),CustomerLoanDetails.class);

		query = new Query();
		List<CustomerPayment> paymentcustomerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("packetNumber").is(packetNumber).and("paymentStatus").ne("Canceled")),CustomerPayment.class);

		query = new Query();


		List<CustomerDetails> customerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("customerId").is(customerLoanDetails.get(0).getCustomerId())),CustomerDetails.class);
		long customerPaidPrincipalAmount=0;

		String loanDate=createDateFormat.format(customerLoanDetails.get(0).getCreationDate());

		for(int i=0;i<paymentcustomerDetails.size();i++)
		{
			customerPaidPrincipalAmount=customerPaidPrincipalAmount+paymentcustomerDetails.get(i).getPrincipalAmount();
			paymentcustomerDetails.get(i).setPaidDate(createDateFormat.format(paymentcustomerDetails.get(i).getCreationDate()));
		}

		query = new Query();
		GoldLoanScheme schemeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("goldloanschemeId").is(customerLoanDetails.get(0).getGoldloanschemeId())),GoldLoanScheme.class);

		String goldloanschemeName=schemeDetails.getGoldloanschemeName();

		model.addAttribute("loanDate", loanDate);
		model.addAttribute("customerDetails", customerDetails);
		model.addAttribute("paymentcustomerDetails", paymentcustomerDetails);
		model.addAttribute("customerLoanDetails", customerLoanDetails);
		model.addAttribute("goldloanschemeName", goldloanschemeName);
		model.addAttribute("customerPaidPrincipalAmount", customerPaidPrincipalAmount);

		return "CustomerPaymentReceipt";
	}


	@ResponseBody
	@RequestMapping(value="PrintCustomerPaymentReceipt")
	public ResponseEntity<byte[]> PrintCustomerPaymentReceipt(@RequestParam("customerPaymentId") String customerPaymentId, Model model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response )
	{

		PrintCustomerPaymentReceipt1(customerPaymentId, model,req,res);
		return null;
	}

	void PrintCustomerPaymentReceipt1(String customerPaymentId, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try 
		{	
			Query query = new Query();

			String employeeName = (String)req.getSession().getAttribute("employeeName");
			query = new Query();

			SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

			query = new Query();
			CustomerPayment paymentcustomerDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("customerPaymentId").is(customerPaymentId)),CustomerPayment.class);

			query = new Query();
			CustomerDetails customerDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("customerId").is(paymentcustomerDetails.getCustomerId())),CustomerDetails.class);

			query = new Query();
			CustomerLoanDetails customerLoanDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("packetNumber").is(paymentcustomerDetails.getPacketNumber())),CustomerLoanDetails.class);

			double paidPrincipalAmount=paymentcustomerDetails.getPrincipalAmount();
			double paidInterestAmount=paymentcustomerDetails.getInterestAmount();
			double paidDueAmount=paymentcustomerDetails.getPaymentDue();

			Double totalAmount=(paymentcustomerDetails.getPrincipalAmount()+paymentcustomerDetails.getInterestAmount()+paymentcustomerDetails.getPaymentDue());

			String paymentDate= createDateFormat.format(paymentcustomerDetails.getCreationDate());

			NumberToWordConversion run = new NumberToWordConversion();
			String paymentAmountinWords = run.convertToIndianCurrency(totalAmount.toString());

			String customerAddress1=customerDetails.getCustomerAddress();
			String customerAddress2="";


			query = new Query();
			Branch branchDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("branchId").is(paymentcustomerDetails.getBranchId())),Branch.class);

			String branchName=branchDetails.getBranchName();


			this.paymentcustomerDetails=paymentcustomerDetails;
			this.customerDetails=customerDetails;
			this.customerLoanDetails=customerLoanDetails;

			this.totalAmount=totalAmount;
			this.paymentDate=paymentDate;
			this.paymentAmountinWords=paymentAmountinWords;

			this.paidPrincipalAmount=paidPrincipalAmount;
			this.paidInterestAmount=paidInterestAmount;
			this.paidDueAmount=paidDueAmount;

			this.customerAddress1=customerAddress1;
			this.customerAddress2=customerAddress2;

			this.branchName=branchName;

			PrinterJob pj = PrinterJob.getPrinterJob();        
			pj.setPrintable(new CustomerPaymentController(), getPageFormat(pj));
			try {
				pj.print();

			}
			catch (PrinterException ex) {

				System.out.println("ERRE = "+ex);
				//ex.printStackTrace();
			}


		}
		catch(Exception ex)
		{
			System.out.println(""+ex);
			//redirectAttributes.addFlashAttribute("flagemail", 0);
		}

	}



	public PageFormat getPageFormat(PrinterJob pj)
	{
		PageFormat pf = pj.defaultPage();
		Paper paper = pf.getPaper();    

		double middleHeight =14.0;  
		double headerHeight = 2.0;                  
		double footerHeight = 2.0;                  
		double width = convert_CM_To_PPI(16);      //printer know only point per inch.default value is 72ppi
		double height = convert_CM_To_PPI(headerHeight+middleHeight+footerHeight); 
		paper.setSize(width, height);
		paper.setImageableArea(                    
				0,
				20,
				width,            
				height - convert_CM_To_PPI(1)
				);   //define boarder size    after that print area width is about 180 points

		pf.setOrientation(PageFormat.PORTRAIT);           //select orientation portrait or landscape but for this time portrait
		pf.setPaper(paper);    

		return pf;
	}

	protected static double convert_CM_To_PPI(double cm) {            
		return toPPI(cm * 0.393600787);            
	}

	protected static double toPPI(double inch) {            
		return inch * 72d;            
	}


	public int print(Graphics graphics, PageFormat pageFormat,int pageIndex) 
	{    

		int result = NO_SUCH_PAGE;      
		if (pageIndex == 0) {                    

			Graphics2D g2d = (Graphics2D) graphics;                    

			double width = pageFormat.getImageableWidth();                    

			g2d.translate((int) pageFormat.getImageableX(),(int) pageFormat.getImageableY()); 

			////////// code by alqama//////////////

			FontMetrics metrics=g2d.getFontMetrics(new Font("Arial",Font.BOLD,7));
			//    int idLength=metrics.stringWidth("000000");
			//int idLength=metrics.stringWidth("00");
			int idLength=metrics.stringWidth("000");
			//System.out.println(idLength);
			int amtLength=metrics.stringWidth("000000");
			int qtyLength=metrics.stringWidth("00000");
			int priceLength=metrics.stringWidth("000000");
			int prodLength=(int)width - idLength - amtLength - qtyLength - priceLength-17;

			try{

				/*Draw Header*/
				int y=100;
				int yShift = 18;

				//   g2d.setFont(new Font("Monospaced",Font.PLAIN,10));

				g2d.setFont(new Font("Times New Roman",Font.PLAIN,14));

				//paymentDate
				g2d.drawString(" "+paymentDate+"",320,y); y+=yShift;
				g2d.drawString(" "+branchName+"",50,y); y+=yShift;

				y=160; 
				//customerDetails
				g2d.drawString("Customer Id ",12,y);         g2d.drawString(":",120,y);   g2d.drawString(""+customerLoanDetails.getCustomerId()+"",130,y);   y+=yShift;
				g2d.drawString("Voucher No ",12,y);          g2d.drawString(":",120,y);   g2d.drawString(""+paymentcustomerDetails.getCustomerPaymentId()+"",130,y);   y+=yShift;
				g2d.drawString("Packet Number",12,y);        g2d.drawString(":",120,y);   g2d.drawString(""+customerLoanDetails.getPacketNumber()+"",130,y);  y+=yShift;
				g2d.drawString("Customer Name",12,y);        g2d.drawString(":",120,y);   g2d.drawString(""+customerDetails.getCustomerName()+"",130,y);  y+=yShift;
				g2d.drawString("Customer Address",12,y);     g2d.drawString(":",120,y);   g2d.drawString(""+customerAddress1+"",130,y);  y+=yShift;
				g2d.drawString(""+customerAddress2+"",130,y);  y+=yShift;
				g2d.drawString("Loan Amount",12,y);          g2d.drawString(":",120,y);   g2d.drawString(""+customerLoanDetails.getLoanAmount()+"",130,y);   g2d.drawString("Interest % :",220,y);  g2d.drawString(" "+customerLoanDetails.getInterestRate()+" %",300,y);  y+=yShift;

				y+=yShift;

				g2d.drawString("Principal Amount Paid ",30,y);    g2d.drawString(":",190,y);  g2d.drawString(" "+paidPrincipalAmount+"",220,y);  y+=yShift;
				g2d.drawString("Interest Amount ",30,y);          g2d.drawString(":",190,y);  g2d.drawString(" "+paidInterestAmount+"",220,y);    y+=yShift;

				if(paidDueAmount!=0)
				{
					g2d.drawString("Due Amount  ",30,y);              g2d.drawString(":",190,y);  g2d.drawString(" "+paidDueAmount+"",220,y);    y+=yShift;
				}

				g2d.drawString("-----------------------------------------------------------",30,y);   y+=yShift; 

				g2d.drawString("Total ",30,y);                   g2d.drawString(":",190,y);  g2d.drawString(" "+totalAmount+"",220,y);    y+=yShift;  
				g2d.drawString(" ("+paymentAmountinWords+")",30,y);    y+=yShift;     											  

			}
			catch(Exception r){
				//System.out.println(r);
				// r.printStackTrace();
			}

			result = PAGE_EXISTS;    
		}    
		return result;    
	}



}
