package com.nonbankingfinance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nonbankingfinance.bean.EnquirySource;
import com.nonbankingfinance.bean.EnquirySource;
import com.nonbankingfinance.repository.EnquirySourceRepository;


@Controller
@RequestMapping("/")
public class EnquirySourceController {


	@Autowired
	EnquirySourceRepository enquirySourceRepository;  
	@Autowired
	MongoTemplate mongoTemplate;
	

	String enquirySourceCode;
	private String EnquirySourceCode()
	{
		long enquirySourceCount = enquirySourceRepository.count();
		
		if(enquirySourceCount<10)
		{
			enquirySourceCode = "EN000"+(enquirySourceCount+1);
		}
		else if((enquirySourceCount>=10) && (enquirySourceCount<100))
		{
			enquirySourceCode = "EN00"+(enquirySourceCount+1);
		}
		else if((enquirySourceCount>=100) && (enquirySourceCount<1000))
		{
			enquirySourceCode = "EN0"+(enquirySourceCount+1);
		}
		else if(enquirySourceCount>1000)
		{
			enquirySourceCode = "EN"+(enquirySourceCount+1);
		}
		
		return enquirySourceCode;
	} 
	
    @RequestMapping("/EnquirySourceMaster")
    public String EnquirySourceMaster(ModelMap model)
    {
    	List<EnquirySource> enquirySourceList = enquirySourceRepository.findAll();
	    model.addAttribute("enquirySourceList", enquirySourceList);
    	return "EnquirySourceMaster";
    }

    @RequestMapping("/AddEnquirySource")
    public String AddEnquirySource(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    	model.addAttribute("enquirySourceCode",EnquirySourceCode());
        return "AddEnquirySource";
    }

    @RequestMapping(value = "/AddEnquirySource", method = RequestMethod.POST)
    public String AddEnquirySource(@ModelAttribute EnquirySource enquirySource, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		enquirySource = new EnquirySource(enquirySource.getEnquirySourceId(), enquirySource.getEnquirySourceName(),
    				enquirySource.getCreationDate(), enquirySource.getUpdateDate(), enquirySource.getUserId());
    		enquirySourceRepository.save(enquirySource);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}
    	
     	model.addAttribute("enquirySourceCode",EnquirySourceCode());
        return "AddEnquirySource";
    }


    @RequestMapping("/EditEnquirySource")
    public String EditEnquirySource(@RequestParam("enquirySourceId") String enquirySourceId, ModelMap model)
    {
      	
    	Query query = new Query();
    	List<EnquirySource> enquirySourceDetails = mongoTemplate.find(query.addCriteria(Criteria.where("enquirySourceId").is(enquirySourceId)),EnquirySource.class);
    	
    	model.addAttribute("enquirySourceDetails",enquirySourceDetails);
    	
    	return "EditEnquirySource";
    }

    @RequestMapping(value = "/EditEnquirySource", method = RequestMethod.POST)
    public String EditEnquirySource(@ModelAttribute EnquirySource enquirySource, Model model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    		enquirySource = new EnquirySource(enquirySource.getEnquirySourceId(), enquirySource.getEnquirySourceName(),
    				enquirySource.getCreationDate(), enquirySource.getUpdateDate(), enquirySource.getUserId());
    		enquirySourceRepository.save(enquirySource);
	        model.addAttribute("stateStatus","Success");
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("stateStatus","Fail");
    	}
    	

    	List<EnquirySource> enquirySourceList = enquirySourceRepository.findAll();
	    model.addAttribute("enquirySourceList", enquirySourceList);
    	return "EnquirySourceMaster";
    }

}
