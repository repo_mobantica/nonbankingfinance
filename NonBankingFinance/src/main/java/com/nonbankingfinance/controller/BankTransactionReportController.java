package com.nonbankingfinance.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nonbankingfinance.bean.Bank;
import com.nonbankingfinance.bean.BankPaymentHistory;
import com.nonbankingfinance.bean.Company;
import com.nonbankingfinance.bean.CompanyBank;
import com.nonbankingfinance.bean.CustomerDetails;
import com.nonbankingfinance.bean.CustomerGoldItemDetails;
import com.nonbankingfinance.bean.CustomerPayment;
import com.nonbankingfinance.bean.EnquirySource;
import com.nonbankingfinance.bean.Occupation;
import com.nonbankingfinance.repository.BankPaymentHistoryRepository;
import com.nonbankingfinance.repository.BankRepository;
import com.nonbankingfinance.repository.CompanyBankRepository;
import com.nonbankingfinance.repository.CompanyRepository;
import com.nonbankingfinance.repository.CustomerDetailsRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class BankTransactionReportController {


	@Autowired
	CustomerDetailsRepository customerDetailsRepository;
	@Autowired
	BankPaymentHistoryRepository bankpaymenthistoryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	CompanyBankRepository companybankRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	MongoTemplate mongoTemplate;

    @RequestMapping("/BankTransactionReport")
    public String BankTransactionReport(ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {

    	Query query = new Query();
    	List<CompanyBank> companybankList = companybankRepository.findAll();
    	
    	List<Bank> bankList = bankRepository.findAll();
    	try
    	{
    	int i=0,j=0;
    	long creditedAmount=0;
    	long debitedAmount=0;
    	for(i=0;i<companybankList.size();i++)
    	{
    		for(j=0;j<bankList.size();j++)
    		{
    			if(companybankList.get(i).getBankId().equals(bankList.get(j).getBankId()))
    			{
    				companybankList.get(i).setBankName(bankList.get(j).getBankName());
    				companybankList.get(i).setBankBranchName(bankList.get(j).getBankBranchName());
    			}
    		}
    		creditedAmount=0;
    		debitedAmount=0;
    		try
    		{
    		query = new Query();
        	List<BankPaymentHistory> bankpaymenthistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(companybankList.get(i).getCompanyBankId())),BankPaymentHistory.class);
        	
        	
        	for(j=0;j<bankpaymenthistoryList.size();j++)
        	{
        		
        		if(bankpaymenthistoryList.get(j).getPaymentStatus().equals("Credited"))
        		{
        		creditedAmount =creditedAmount + bankpaymenthistoryList.get(j).getTotalAmount();
        		}
        		else
        		{
        			debitedAmount=debitedAmount+bankpaymenthistoryList.get(j).getTotalAmount();
        		}
        	}
    		}
    		catch (Exception e) {
			}
        	companybankList.get(i).setCreditedAmount(creditedAmount);
        	companybankList.get(i).setDebitedAmount(debitedAmount);
    	}
    	}
    	catch (Exception e) {
		}
	    model.addAttribute("companybankList", companybankList);

	   
    	return "BankTransactionReport";
    }


    @RequestMapping("/BankTransactionDetailsReport")
    public String BankTransactionDetailsReport(@RequestParam("companyBankId") String companyBankId, ModelMap model)
    {
    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

      	try
      	{
    	Query query = new Query();
    	List<BankPaymentHistory> bankpaymenthistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId))),BankPaymentHistory.class);
        
    	List<CustomerDetails> customerList = customerDetailsRepository.findAll();
    	String customerName="";
    	
    	for(int i=0;i<bankpaymenthistoryList.size();i++)
    	{
    		
    		for(int j=0;j<customerList.size();j++)
    		{
    			if(bankpaymenthistoryList.get(i).getCustomerId().equals(customerList.get(j).getCustomerId()))
    			{
    				customerName=customerList.get(j).getCustomerName()+" ";
    				bankpaymenthistoryList.get(i).setCustomerName(customerName);
    			}
    			
    		}
    		
    		bankpaymenthistoryList.get(i).setDate(createDateFormat.format(bankpaymenthistoryList.get(i).getCreationDate()));
    		
    	}
    	
    	 query = new Query();
     	List<CompanyBank> companybankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId))),CompanyBank.class);
     	 query = new Query();
      	List<Bank> bankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bankId").is(companybankDetails.get(0).getBankId())),Bank.class);
          
      	model.addAttribute("bankDetails", bankDetails);
    	model.addAttribute("companybankDetails", companybankDetails);
    	model.addAttribute("bankpaymenthistoryList", bankpaymenthistoryList);
    	model.addAttribute("companyBankId", companyBankId);
      	}
      	catch (Exception e) {
			// TODO: handle exception
		}
    	return "BankTransactionDetailsReport";
    }
    

    @ResponseBody
	 @RequestMapping("/BankTransactionSearchByDateWise")
	 public List<BankPaymentHistory> BankTransactionSearchByDateWise(@RequestParam("companyBankId") String companyBankId, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException
	 {

    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

      	try
      	{
    	Query query = new Query();
    	List<BankPaymentHistory> bankpaymenthistoryList1 = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId))),BankPaymentHistory.class);
        
    	List<CustomerDetails> customerList = customerDetailsRepository.findAll();
    	String customerName="";

	    Date currentMonthStartDate=new SimpleDateFormat("M/d/yyyy").parse(startDate);
	    Date currentMonthEndDate=new SimpleDateFormat("M/d/yyyy").parse(endDate);
	    

    	List<BankPaymentHistory> bankpaymenthistoryList=new ArrayList<BankPaymentHistory>();
    	BankPaymentHistory bankpaymentHistory=new BankPaymentHistory();

    	for(int i=0;i<bankpaymenthistoryList1.size();i++)
    	{

			  if(currentMonthStartDate.compareTo(bankpaymenthistoryList1.get(i).getCreationDate())<=0  && currentMonthEndDate.compareTo(bankpaymenthistoryList1.get(i).getCreationDate())>=0 )
			  {
				  bankpaymentHistory=new BankPaymentHistory();
				  bankpaymentHistory.setBankpaymentHistoryId(bankpaymenthistoryList1.get(i).getBankpaymentHistoryId());
				  bankpaymentHistory.setCompanyBankId(bankpaymenthistoryList1.get(i).getCompanyBankId());
				  bankpaymentHistory.setCustomerId(bankpaymenthistoryList1.get(i).getCustomerId());
				  bankpaymentHistory.setTotalAmount(bankpaymenthistoryList1.get(i).getTotalAmount());
				  bankpaymentHistory.setPaymentpourpose(bankpaymenthistoryList1.get(i).getPaymentpourpose());
				  bankpaymentHistory.setPaymentStatus(bankpaymenthistoryList1.get(i).getPaymentStatus());
				  bankpaymentHistory.setCreationDate(bankpaymenthistoryList1.get(i).getCreationDate());
				  bankpaymentHistory.setUserId(bankpaymenthistoryList1.get(i).getUserId());
				  bankpaymentHistory.setBranchId(bankpaymenthistoryList1.get(i).getBranchId());
			  
		
	    		for(int j=0;j<customerList.size();j++)
	    		{
	    			if(bankpaymenthistoryList1.get(i).getCustomerId().equals(customerList.get(j).getCustomerId()))
	    			{
	    				customerName=customerList.get(j).getCustomerName();
	    				bankpaymentHistory.setCustomerName(customerName);
	    			}
	    			
	    		}
	    		
	    		bankpaymentHistory.setDate(createDateFormat.format(bankpaymenthistoryList1.get(i).getCreationDate()));

			    bankpaymenthistoryList.add(bankpaymentHistory);
			  }
    	}
    	
	    return bankpaymenthistoryList;
      	}
      	catch (Exception e) {
      		System.out.println("eee == "+e);
      		return null;
			// TODO: handle exception
		}
	 }

 	@ResponseBody
	@RequestMapping(value = "/BankTransactionDetailsReport", method = RequestMethod.POST)
 	public ResponseEntity<byte[]> PrintBankTransactionDetailsReport(@RequestParam("companyBankId") String companyBankId, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response )
 	{
 		int index=0;
 		JasperPrint print;
 		try 
 		{	
 			Query query = new Query();
 			
 			Collection c = new ArrayList();
 			HashMap jmap = new HashMap();
 			
 			 jmap = null;
 				

		    	String branchId = (String)req.getSession().getAttribute("branchId");
 		    	String employeeName = (String)req.getSession().getAttribute("employeeName");
 		    	String branchName = (String)req.getSession().getAttribute("branchName");
 		    	query = new Query();
 		    	List<Company> copmanyDetails= companyRepository.findAll();

 		    	SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

 		    	query = new Query();
 		    	List<BankPaymentHistory> bankpaymenthistoryList1 = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId))),BankPaymentHistory.class);
 		        
 		    	List<CustomerDetails> customerList = customerDetailsRepository.findAll();
 		    	String customerName="";

 		    	List<BankPaymentHistory> bankpaymenthistoryList=new ArrayList<BankPaymentHistory>();
 		    	BankPaymentHistory bankpaymentHistory=new BankPaymentHistory();

 		      	long totalPrincipalAmount=0;
 		      	long totalInterestAmount=0;
 		      	long totalDueAmount=0;
 		      	long totalAmount=0;

 		    	String reportDate="";
 		    	
 		    	if(endDate.equals("") || startDate.equals(""))
 		    	{
 		    		reportDate="All Date Report";
 		    		bankpaymenthistoryList.addAll(bankpaymenthistoryList1);

 			    	for(int i=0;i<bankpaymenthistoryList.size();i++)
 			    	{

				    		for(int j=0;j<customerList.size();j++)
				    		{
				    			if(bankpaymenthistoryList.get(i).getCustomerId().equals(customerList.get(j).getCustomerId()))
				    			{
				    				customerName=customerList.get(j).getCustomerName();
				    				bankpaymenthistoryList.get(i).setCustomerName(customerName);
				    			}
				    			
				    		}
				    		
				    		bankpaymenthistoryList.get(i).setDate(createDateFormat.format(bankpaymenthistoryList1.get(i).getCreationDate()));

 			    	}

 		    	}
 		    	else
 		    	{
 		    		reportDate=startDate+" To "+endDate+" Date Report";
 	    	    	DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
 				    Date currentMonthStartDate=new SimpleDateFormat("M/d/yyyy").parse(startDate);
 				    Date currentMonthEndDate=new SimpleDateFormat("M/d/yyyy").parse(endDate);

 				   reportDate=dateFormat.format(currentMonthStartDate)+" To "+dateFormat.format(currentMonthEndDate);
 				   // String todayDateInString = new SimpleDateFormat("d/M/yyyy").format(currentMonthStartDate);

 			    	for(int i=0;i<bankpaymenthistoryList1.size();i++)
 			    	{

 						  if(currentMonthStartDate.compareTo(bankpaymenthistoryList1.get(i).getCreationDate())<=0  && currentMonthEndDate.compareTo(bankpaymenthistoryList1.get(i).getCreationDate())>=0 )
 						  {
 							  bankpaymentHistory=new BankPaymentHistory();
 							  bankpaymentHistory.setBankpaymentHistoryId(bankpaymenthistoryList1.get(i).getBankpaymentHistoryId());
 							  bankpaymentHistory.setCompanyBankId(bankpaymenthistoryList1.get(i).getCompanyBankId());
 							  bankpaymentHistory.setCustomerId(bankpaymenthistoryList1.get(i).getCustomerId());
 							  bankpaymentHistory.setTotalAmount(bankpaymenthistoryList1.get(i).getTotalAmount());
 							  bankpaymentHistory.setPaymentpourpose(bankpaymenthistoryList1.get(i).getPaymentpourpose());
 							  bankpaymentHistory.setPaymentStatus(bankpaymenthistoryList1.get(i).getPaymentStatus());
 							  bankpaymentHistory.setCreationDate(bankpaymenthistoryList1.get(i).getCreationDate());
 							  bankpaymentHistory.setUserId(bankpaymenthistoryList1.get(i).getUserId());
 							  bankpaymentHistory.setBranchId(bankpaymenthistoryList1.get(i).getBranchId());
 						  
 					
 				    		for(int j=0;j<customerList.size();j++)
 				    		{
 				    			if(bankpaymenthistoryList1.get(i).getCustomerId().equals(customerList.get(j).getCustomerId()))
 				    			{
 				    				customerName=customerList.get(j).getCustomerName();
 				    				bankpaymentHistory.setCustomerName(customerName);
 				    			}
 				    			
 				    		}
 				    		
 				    		bankpaymentHistory.setDate(createDateFormat.format(bankpaymenthistoryList1.get(i).getCreationDate()));

 						    bankpaymenthistoryList.add(bankpaymentHistory);
 						  }
 		    		
 		    	}
 		    	
 		    	}
 		    	
 		   //  String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";	 
 		    	
 				c = new ArrayList();
 			 	 for(int i=0;i<bankpaymenthistoryList.size();i++)
 				 {
 			 		    jmap = new HashMap();
 						jmap.put("srno",""+(i+1));
 					    //jmap.put("srno","1");
 						jmap.put("bankpaymentHistoryId",""+bankpaymenthistoryList.get(i).getBankpaymentHistoryId());
 						jmap.put("companyBankId", ""+bankpaymenthistoryList.get(i).getCompanyBankId());
 						jmap.put("customerId",""+bankpaymenthistoryList.get(i).getCustomerId());
 						jmap.put("totalAmount", ""+bankpaymenthistoryList.get(i).getTotalAmount());
 						jmap.put("paymentpourpose",""+bankpaymenthistoryList.get(i).getPaymentpourpose());
 						jmap.put("paymentStatus",""+bankpaymenthistoryList.get(i).getPaymentStatus());
 						
 						jmap.put("customerName",""+bankpaymenthistoryList.get(i).getCustomerName());
 						jmap.put("date",""+bankpaymenthistoryList.get(i).getDate());
 						
 						c.add(jmap);
 						
 						jmap = null;
 				 }

 		    	 query = new Query();
 		     	List<CompanyBank> companybankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(Integer.parseInt(companyBankId))),CompanyBank.class);
 		     	 query = new Query();
 		      	List<Bank> bankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bankId").is(companybankDetails.get(0).getBankId())),Bank.class);
 		          
 			 JRDataSource dataSource = new JRMapCollectionDataSource(c);
 			 Map<String, Object> parameterMap = new HashMap();
 			 
 			 Date date = new Date();  
 			 String strDate= createDateFormat.format(date);
 			 
 			/*1*/ parameterMap.put("date",""+strDate);
 				
 			/*2*/ parameterMap.put("bankName",""+bankDetails.get(0).getBankName());
 			
 			/*3*/ parameterMap.put("bankBranchName", ""+bankDetails.get(0).getBankBranchName());

 		    /*4*/parameterMap.put("bankAccountNumber", ""+companybankDetails.get(0).getBankAccountNumber());
 		    
 		    /*4*/parameterMap.put("bankIfsc", ""+companybankDetails.get(0).getBankIfsc());
 			      
 		    /*5*/parameterMap.put("companyName", ""+copmanyDetails.get(0).getCompanyName());
 			      
 		    /*6*/parameterMap.put("companyAddress", ""+copmanyDetails.get(0).getCompanyAddress()+" ");
 		    
 		    /*6*/parameterMap.put("branchName", ""+branchName);
 		    
 		    /*6*/parameterMap.put("reportDate", ""+reportDate);
 			      
 			      
 			 InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/BankTransactionDetailsReport.jasper");
 				
 				
 			 print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

 			 ByteArrayOutputStream baos = new ByteArrayOutputStream();
 			 JasperExportManager.exportReportToPdfStream(print, baos);
 				
 			 byte[] contents = baos.toByteArray();

 			 HttpHeaders headers = new HttpHeaders();
 			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
 			 String filename = "BankTransactionDetailsReport.pdf";

 			 JasperExportManager.exportReportToPdfStream(print, baos);

 				 
 			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
 			 headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
 						 
 			 ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
 			 response.setHeader("Content-Disposition", "inline; filename=" + filename );
 					 
 			 return resp;					    
 		}
 		catch(Exception ex)
 		{
 			System.out.println(""+ex);
 			//redirectAttributes.addFlashAttribute("flagemail", 0);
 		}
 		
 				 return null;
 	}
 	
        
}
