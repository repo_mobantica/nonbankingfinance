<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">

<!-- App Favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/images/favicon.ico">

<!-- App title -->
<title>Add Customer</title>

<!-- Switchery css -->
<link
	href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css"
	rel="stylesheet" />

<!-- Bootstrap CSS -->
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- App CSS -->
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet" type="text/css" />

<!-- Modernizr js -->
<script
	src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


</head>


<body class="fixed-left" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

	<div id="wrapper">

		<%@ include file="headerpage.jsp"%>

		<%@ include file="menu.jsp"%>

		<div class="content-page">

			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-xl-12">
							<div class="page-title-box">
								<h4 class="page-title float-left">Add Customer</h4>

								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="home">Home</a></li>
									<li class="breadcrumb-item"><a href="CustomerMaster">Customer
											Master</a></li>
									<li class="breadcrumb-item active">Add Customer</li>
								</ol>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<form name="customerform"
						action="${pageContext.request.contextPath}/AddCustomer"
						onSubmit="return validate()" method="post"
						enctype="multipart/form-data">

						<div class="row">

							<div class="col-12">

								<div class="card-box">

									<div class="row">

										<div class="col-xl-3">
											<div class="form-group">
												<label for="customerId">Customer Id<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="customerId" name="customerId"
													value="${customerCode}" readonly>
											</div>
										</div>


										<div class="col-xl-3">
											<div class="form-group">
												<label for="customerAadharcardno">Aadhar No<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="customerAadharcardno"
													name="customerAadharcardno" placeholder="Aadhar card No"
													style="text-transform: uppercase;"
													onchange="getCustomerDetails(this.value)">
											</div>
											<span id="customerAadharcardnoSpan" style="color: #FF0000"></span>
										</div>
										<!-- 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="packetNumber">Packet Number<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="packetNumber" name="packetNumber">
                      </div>
					 </div>
				  -->
									</div>

									<div class="row">

										<div class="col-xl-3">
											<div class="form-group">
												<label for="customerName">Customer Name<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="customerName" name="customerName"
													placeholder="Customer First Name">
											</div>
											<span id="customerNameSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-3">
											<div class="form-group">
												<label for="mobileNumber">Mobile No.<span
													class="text-danger">*</span></label> <input type="text"
													id="mobileNumber" name="mobileNumber"
													placeholder="Mobile Number" data-mask="9999999999"
													class="form-control">
											</div>
											<span id="mobileNumberSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-3">
											<div class="form-group">
												<label for="customerPhoneno">Phone No.</label> <input
													type="text" id="customerPhoneno" name="customerPhoneno"
													placeholder="" data-mask="(999) 999-99999"
													class="form-control">
											</div>
											<span id="customerPhonenoSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-3">
											<div class="form-group">
												<label>Email ID<span class="text-danger">*</span></label>
												<div class="input-group">
													<input type="text" class="form-control"
														placeholder="Email Id" id="emailId" name="emailId">
													<div class="input-group-append">
														<span class="input-group-text"><i class="ti-email"></i></span>
													</div>
												</div>
											</div>

											<span id="emailIdSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-3">
											<div class="form-group">
												<label for="customerPancardno">PAN Number<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="customerPancardno" name="customerPancardno"
													placeholder="PAN Number" style="text-transform: uppercase">
											</div>
											<span id="customerPancardnoSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-3">
											<div class="form-group">
												<label for="countryId">Occupation Name<span
													class="text-danger">*</span></label> <select class="form-control"
													name="occupationName" id="occupationName">
													<option selected="selected" value="Default">-Select
														Occupation-</option>
													<c:forEach var="occupationList" items="${occupationList}">
														<option value="${occupationList.occupationName}">${occupationList.occupationName}</option>
													</c:forEach>
												</select>
											</div>
											<span id="occupationNameSpan" style="color: #FF0000"></span>
										</div>

										<!-- 
                    </div>
                    
					 <div class="row">
	                   -->
										<div class="col-xl-4">
											<div class="form-group">
												<label for="customerAddress">Address<span
													class="text-danger">*</span></label>
												<textarea class="form-control" id="customerAddress"
													name="customerAddress" rows="1"></textarea>
											</div>
											<span id="customerAddressSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-2">
											<div class="form-group">
												<label for="pinCode">Pin Code<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="pinCode" name="pinCode"
													placeholder="Pin Code">
											</div>
											<span id="pinCodeSpan" style="color: #FF0000"></span>
										</div>

									</div>

									<div class="row">

										<div class="col-xl-3">
											<div class="form-group">
												<label for="customerBankName">Bank Name</label> <input
													class="form-control" type="text" id="customerBankName"
													name="customerBankName" placeholder="Bank Name">
											</div>
											<span id="customerBankNameSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-3">
											<div class="form-group">
												<label for="bankBranchName">Branch Name</label> <input
													class="form-control" type="text" id="bankBranchName"
													name="bankBranchName" placeholder="Branch Name">
											</div>
											<span id="bankBranchNameSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-3">
											<div class="form-group">
												<label for="banckIFSC">IFSC</label> <input
													class="form-control" type="text" id="banckIFSC"
													name="banckIFSC" placeholder="IFSC">
											</div>
											<span id="banckIFSCSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-3">
											<div class="form-group">
												<label for="bankAccountNumber">REF No.</label> <input
													class="form-control" type="text" id="bankAccountNumber"
													name="bankAccountNumber" placeholder="Account Number">
											</div>
											<span id="bankAccountNumberSpan" style="color: #FF0000"></span>
										</div>

									</div>


									<!-- 
					 <div class="row">
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      
						<label for="documentName">Document Name/Caption</label>
						<input type="text" class="form-control" id="documentName" placeholder="Document Name Or Caption" name="documentName" style="text-transform: capitalize;">
												
                      </div>
                      <span id="documentNameSpan" style="color:#FF0000"></span>
                      
					 </div>
					 
					  <div class="col-xl-4">
                      <div class="form-group">
                      
						<label>Select File</label>
	                      <div class="input-group">
	                      
						<input type="file" class="form-control" id="document" name="document" accept="application/pdf,image/x-png,image/gif,image/jpeg" onchange="return getFileExtension()">
						
		                    
						<span class="input-group-btn">
						 <button type="button" class="btn btn-success" onclick="return uploadCustomerDocument()" ><i class="fa fa-upload"></i>Upload</button>
						</span>
						<br/>
						
						<p class="help-block">Only select PDF, PNG, JPEG, and JPG formats(with size maximum 500KB).</p>
						 
	                      </div>
                      </div>

                      <span id="documentSpan" style="color:#FF0000"></span>
					 </div>
	                   
					 </div>
                     -->

									<div class="row">

										<div class="col-xl-2">
											<div class="form-group">
												<label for="loanAmount">Loan Amount<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="loanAmount" name="loanAmount"
													placeholder="Loan Amount"
													onchange="getCheckLoanAmountIsInScheme(this.value)">
											</div>
											<span id="loanAmountSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-2">
											<div class="form-group">
												<label for="goldloanschemeId">Scheme Name<span
													class="text-danger">*</span></label> <select class="form-control"
													name="goldloanschemeId" id="goldloanschemeId">
													<option selected="selected" value="Default">-Select
														Scheme-</option>
													<c:forEach var="goldLoanSchemeList"
														items="${goldLoanSchemeList}">
														<option value="${goldLoanSchemeList.goldloanschemeId}">${goldLoanSchemeList.goldloanschemeName}</option>
													</c:forEach>
												</select>
											</div>
											<span id="goldloanschemeIdSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-2">
											<div class="form-group">
												<label for="interestRate">Interest Rate/Year<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="interestRate" name="interestRate"
													placeholder="Interest Rate">
											</div>
											<span id="interestRateSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-2">
											<div class="form-group">
												<label for="processingFee">Processing Fee<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="processingFee" name="processingFee"
													placeholder="processing Fee">
											</div>
											<span id="processingFeeSpan" style="color: #FF0000"></span>
										</div>

									</div>


									<div class="row">
										<div class="col-xl-3">
											<div class="form-group">
												<label for="goldItemName">Item Name<span
													class="text-danger">*</span></label> <select class="form-control"
													name="goldItemName" id="goldItemName">
													<option selected="selected" value="Default">-Select
														Scheme-</option>
													<c:forEach var="goldItemList" items="${goldItemList}">
														<option value="${goldItemList.goldItemName}">${goldItemList.goldItemName}</option>
													</c:forEach>
												</select>
											</div>
											<span id="goldItemNameSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-1">
											<div class="form-group">
												<label for="itemQty">Qty<span class="text-danger">*</span></label>
												<input class="form-control" type="text" id="itemQty"
													name="itemQty">
											</div>
											<span id="itemQtySpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-1">
											<div class="form-group">
												<label for="itemDWeight">D. Wt<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="itemDWeight" name="itemDWeight"
													onchange="getCheckPurWeight(this.value)">
											</div>
											<span id="itemDWeightSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-1">
											<div class="form-group">
												<label for="itemGrossWeight">G. Wt<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="itemGrossWeight" name="itemGrossWeight"
													onchange="getCheckPurWeight(this.value)">
											</div>
											<span id="itemGrossWeightSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-1">
											<div class="form-group">
												<label for="itemWeight">N. Wt<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="itemWeight" name="itemWeight"
													onchange="getCheckPurWeight(this.value)">
											</div>
											<span id="itemWeightSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-1">
											<div class="form-group">
												<label for="purityPer">Purity %<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="purityPer" name="purityPer"
													onchange="getCheckPurWeight(this.value)">
											</div>
											<span id="purityPerSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-1">
											<div class="form-group">
												<label for="purWeight">Pur Wt<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="purWeight" name="purWeight"
													readonly="readonly">
											</div>
											<span id="purWeightSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemMarketValue">Aprx<span
													class="text-danger">*</span></label>
												<div class="input-group">
													<input class="form-control" type="text"
														id="itemMarketValue" name="itemMarketValue"
														readonly="readonly"> <span class="input-group-btn">
														<button type="button" class="btn btn-success"
															onclick="return AddCustomerGoldItemList()">
															<i class="fa fa-plus"></i>Add
														</button>
													</span>
												</div>
											</div>

											<span id="itemMarketValueSpan" style="color: #FF0000"></span>
										</div>

									</div>


									<div class="row">

										<div class="col-xl-10">
											<div class="card-box table-responsive">
												<h4 class="m-t-0 header-title">Item Table</h4>
												<span id="totalNumberOfGoldItemsSpan" style="color: #FF0000"></span>

												<table id="customerGoldItemListTable"
													class="table table-striped table-bordered table mb-0 table-sm">
													<thead>
														<tr>
															<th>#</th>
															<th>Item</th>
															<th>Qty</th>
															<th>D Wt</th>
															<th>Gross Wt</th>
															<th>N. Wt</th>
															<th>Purity %</th>
															<th>Pur Wt</th>
															<th>Aprx</th>
															<th style="width: 50px">Action</th>
														</tr>
													</thead>
													<tbody>

													</tbody>
												</table>

											</div>
										</div>
									</div>



									<br />
									<div class="row">
										<div class="col-xl-2"></div>
										<div class="col-xl-3">
											<a href="CustomerMaster"><button type="button"
													class="btn btn-secondary" value="reset" style="width: 90px">Back</button></a>
										</div>

										<div class="col-xl-3">
											<button type="reset" class="btn btn-default">Reset</button>
										</div>

										<div class="col-xl-3">
											<button class="btn btn-primary" type="submit">Submit</button>
										</div>
									</div>

									<input type="hidden" id="packetNumber" name="packetNumber"
										value="${packetNumber}">
										
										
										 <input type="hidden"
										id="totalItemMarketValue" name="totalItemMarketValue" value="">
									<input type="hidden" id="totalNumberOfGoldItems"
										name="totalNumberOfGoldItems" value=""> <input
										type="hidden" id="minimumLoanAmount" name="minimumLoanAmount"
										value=""> <input type="hidden" id="maximumLoanAmount"
										name="maximumLoanAmount" value=""> <input
										type="hidden" id="maximumTenure" name="maximumTenure" value="">

									<input type="hidden" id="todayGoldRate" name="todayGoldRate"
										value="<%= session.getAttribute("todayGoldRate") %>">


									<input type="hidden" id="goldRateBy99_1" name="goldRateBy99_1"
										value="<%= session.getAttribute("goldRateBy99_1") %>">



									<input type="hidden" id="userId" name="userId"
										value="<%= session.getAttribute("employeeId") %>"> <input
										type="hidden" id="branchId" name="branchId"
										value="<%= session.getAttribute("branchId") %>">


								</div>

							</div>
							<!-- end col-->

						</div>

					</form>

				</div>
				<!-- container -->
			</div>

			<%@ include file="RightSidebar.jsp"%>

			<%@ include file="footer.jsp"%>

		</div>
	</div>
	<script>
            var resizefunc = [];
        </script>

	<!-- jQuery  -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
	<!-- Tether for Bootstrap -->
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js"
		type="text/javascript"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js"
		type="text/javascript"></script>

	<script>

function getCheckPurWeight()
{

	 var itemWeight = Number($('#itemWeight').val());
	 var purityPer = Number($('#purityPer').val());
	 var todayGoldRate = Number($('#todayGoldRate').val());
	 var goldRateBy99_1=Number($('#goldRateBy99_1').val());
	 var goldRateBy99_1PerGram=goldRateBy99_1/10;
	 
	// var todayGoldRatePerGram=todayGoldRate/10;
	 
	 var purWeight=(itemWeight/100)*purityPer;
	 var itemMarketValue=purWeight*goldRateBy99_1PerGram;

	 document.customerform.purWeight.value=purWeight.toFixed(2);
	 document.customerform.itemMarketValue.value=itemMarketValue.toFixed(0);
}

function AddCustomerGoldItemList()
{
	
	if(document.customerform.goldItemName.value=="Default")
	{
		$('#goldItemNameSpan').html('Please, select Item Name..!');
		document.customerform.goldItemName.focus();
		return false;
	}

	 
	  //  validation for itemQty
	  if(document.customerform.itemQty.value=="")
		{
		  $('#itemQtySpan').html('Please, Enter item Qty..!');
			document.customerform.itemQty.focus();
			return false;
		}
		else if(document.customerform.itemQty.value.match(/^[\s]+$/))
		{
			$('#itemQtySpan').html('Please, Enter item Qty..!');
			document.customerform.itemQty.value="";
			document.customerform.itemQty.focus();
			return false; 	
		}
		else if(!document.customerform.itemQty.value.match(/^[0-9]+$/))
		 {
			$('#itemQtySpan').html('Please, use only digit value for item Qty..!');
			document.customerform.itemQty.value="";
			document.customerform.itemQty.focus();
		    return false;
		 }

	  //  validation for itemWeight
	  if(document.customerform.itemGrossWeight.value=="")
		{
		  $('#itemGrossWeightSpan').html('Please, Enter item Gross Weight..!');
			document.customerform.itemGrossWeight.focus();
			return false;
		}
		else if(document.customerform.itemGrossWeight.value.match(/^[\s]+$/))
		{
			$('#itemGrossWeightSpan').html('Please, Enter item Gross Weight..!');
			document.customerform.itemGrossWeight.value="";
			document.customerform.itemGrossWeight.focus();
			return false; 	
		}
		else if(!document.customerform.itemGrossWeight.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.customerform.itemGrossWeight.value.match(/^[0-9]+$/))
				 {
					$('#itemGrossWeightSpan').html('Please, use only digit value for item Gross Weight..! eg:21.36 OR 30');
					document.customerform.itemGrossWeight.value="";
					document.customerform.itemGrossWeight.focus();
					return false;
				}
		}
	 
	 
	  //  validation for itemWeight
	  if(document.customerform.itemWeight.value=="")
		{
		  $('#itemWeightSpan').html('Please, Enter item Weight..!');
			document.customerform.itemWeight.focus();
			return false;
		}
		else if(document.customerform.itemWeight.value.match(/^[\s]+$/))
		{
			$('#itemWeightSpan').html('Please, Enter item Weight..!');
			document.customerform.itemWeight.value="";
			document.customerform.itemWeight.focus();
			return false; 	
		}
		else if(!document.customerform.itemWeight.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.customerform.itemWeight.value.match(/^[0-9]+$/))
				 {
					$('#itemWeightSpan').html('Please, use only digit value for item Weight..! eg:21.36 OR 30');
					document.customerform.itemWeight.value="";
					document.customerform.itemWeight.focus();
					return false;
				}
		}

		 
	  //  validation for purityPer
	  if(document.customerform.purityPer.value=="")
		{
		  $('#purityPerSpan').html('Please, Enter purity Per..!');
			document.customerform.purityPer.focus();
			return false;
		}
		else if(document.customerform.purityPer.value.match(/^[\s]+$/))
		{
			$('#purityPerSpan').html('Please, Enter purity Per..!');
			document.customerform.purityPer.value="";
			document.customerform.purityPer.focus();
			return false; 	
		}
		else if(!document.customerform.purityPer.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.customerform.purityPer.value.match(/^[0-9]+$/))
				 {
					$('#purityPerSpan').html('Please, use only digit value for purity Per..! eg:21.36 OR 30');
					document.customerform.purityPer.value="";
					document.customerform.purityPer.focus();
					return false;
				}
		}


     var totalItemMarketValue=0;
     var itemMarketValue=0;

	 var packetNumber = $('#packetNumber').val();
	 var customerId = $('#customerId').val();
	 var goldItemName = $('#goldItemName').val();
	 
	 
	 var itemQty = $('#itemQty').val();
	 var itemDWeight= $('#itemDWeight').val();
	 var itemGrossWeight = $('#itemGrossWeight').val();
	 var itemWeight = $('#itemWeight').val();
	 var purityPer = $('#purityPer').val();
	 var purWeight= $('#purWeight').val();
	 var itemMarketValue=$('#itemMarketValue').val();
	 
	 $('#totalNumberOfGoldItems').val("");
	 $('#totalItemMarketValue').val("");
	 
	 $('#customerGoldItemListTable tr').detach();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddCustomerGoldItemList',
		type : 'Post',
		data : { packetNumber : packetNumber, customerId : customerId, goldItemName : goldItemName, itemQty : itemQty, itemDWeight : itemDWeight, itemGrossWeight : itemGrossWeight, itemWeight : itemWeight, purityPer : purityPer, purWeight : purWeight, itemMarketValue : itemMarketValue},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#customerGoldItemListTable').append('<tr><th>#</th><th>Item</th><th>Qty</th><th> D Wt</th><th> Gross Wt</th><th> N.Wt</th><th>Purity%</th><th>Pur Wt</th><th>Aprx</th><th style="width:100px">Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].customergolditemdetailsId;
									$('#customerGoldItemListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].goldItemName+'</td><td>'+result[i].itemQty+'</td><td>'+result[i].itemDWeight+'</td><td>'+result[i].itemGrossWeight+'</td><td>'+result[i].itemWeight+'</td><td>'+result[i].purityPer+'</td><td>'+result[i].purWeight+'</td><td>'+result[i].itemMarketValue+'</td><td><a onclick="DeleteCustomerItem('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].customergolditemdetailsId;
									$('#customerGoldItemListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].goldItemName+'</td><td>'+result[i].itemQty+'</td><td>'+result[i].itemDWeight+'</td><td>'+result[i].itemGrossWeight+'</td><td>'+result[i].itemWeight+'</td><td>'+result[i].purityPer+'</td><td>'+result[i].purWeight+'</td><td>'+result[i].itemMarketValue+'</td><td><a onclick="DeleteCustomerItem('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								
								//itemMarketValue=result[i].purWeight*todayGoldRatePerGram;
								totalItemMarketValue=totalItemMarketValue+itemMarketValue;

								document.customerform.totalItemMarketValue.value=totalItemMarketValue;
								document.customerform.totalNumberOfGoldItems.value=i+1;
							 } 

							if(result.length==0)
								{
								document.customerform.totalItemMarketValue.value=0;
								document.customerform.totalNumberOfGoldItems.value=0;
								}
							 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#itemQty').val("");
	 $('#itemWeight').val("");
	 $('#purityPer').val("");
	 $('#purWeight').val("");
	 $('#itemMarketValue').val("");

}

function DeleteCustomerItem(id)
{

    var totalItemMarketValue=0;
    var itemMarketValue=0;

	var packetNumber = $('#packetNumber').val();
	var customerId = $('#customerId').val();
    var customergolditemdetailsId = id;
    $('#totalNumberOfGoldItems').val("");
    $('#customerGoldItemListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteCustomerItem',
		type : 'Post',
		data : { customerId : customerId, packetNumber : packetNumber, customergolditemdetailsId : customergolditemdetailsId },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#customerGoldItemListTable').append('<tr><th>#</th><th>Item</th><th>Qty</th><th> D Wt</th><th> Gross Wt</th><th> N.Wt</th><th>Purity%</th><th>Pur Wt</th><th>Aprx</th><th style="width:100px">Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].customergolditemdetailsId;
									$('#customerGoldItemListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].goldItemName+'</td><td>'+result[i].itemQty+'</td><td>'+result[i].itemDWeight+'</td><td>'+result[i].itemGrossWeight+'</td><td>'+result[i].itemWeight+'</td><td>'+result[i].purityPer+'</td><td>'+result[i].purWeight+'</td><td>'+result[i].itemMarketValue+'</td><td><a onclick="DeleteCustomerItem('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].customergolditemdetailsId;
									$('#customerGoldItemListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].goldItemName+'</td><td>'+result[i].itemQty+'</td><td>'+result[i].itemDWeight+'</td><td>'+result[i].itemGrossWeight+'</td><td>'+result[i].itemWeight+'</td><td>'+result[i].purityPer+'</td><td>'+result[i].purWeight+'</td><td>'+result[i].itemMarketValue+'</td><td><a onclick="DeleteCustomerItem('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								
								//itemMarketValue=result[i].purWeight*todayGoldRatePerGram;
								totalItemMarketValue=totalItemMarketValue+itemMarketValue;

								document.customerform.totalItemMarketValue.value=totalItemMarketValue;
								document.customerform.totalNumberOfGoldItems.value=i+1;
							 } 

							if(result.length==0)
								{
								document.customerform.totalItemMarketValue.value=0;
								document.customerform.totalNumberOfGoldItems.value=0;
								}
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});

	
}

function getCustomerDetails()
{
	

	   var customerAadharcardno = $('#customerAadharcardno').val();

	    $.ajax({

			 url : '${pageContext.request.contextPath}/getCustomerDetailsByAdharNumber',
			type : 'Post',
			data : { customerAadharcardno : customerAadharcardno },
			dataType : 'json',
			success : function(result)
					  {
						   if (result) 
						   { 
								document.customerform.customerId.value=result.customerId;
								document.customerform.customerName.value=result.customerName;
								document.customerform.mobileNumber.value=result.mobileNumber;
								document.customerform.customerPhoneno.value=result.customerPhoneno;
								document.customerform.emailId.value=result.emailId;
								document.customerform.customerPancardno.value=result.customerPancardno;
								document.customerform.occupationName.value=result.occupationName;
								document.customerform.customerAddress.value=result.customerAddress;
								document.customerform.pinCode.value=result.pinCode;
								document.customerform.customerBankName.value=result.customerBankName;
								document.customerform.bankBranchName.value=result.bankBranchName;
								document.customerform.banckIFSC.value=result.banckIFSC;
								document.customerform.bankAccountNumber.value=result.bankAccountNumber;

								document.customerform.packetNumber.value=result.packetNumber;
							} 
							else
							{
								alert("failure111");
							}
					  } 

			});

		
}


function clearAll()
{
	$('#packetNumberSpan').html('');
	$('#customerNameSpan').html('');
	$('#mobileNumberSpan').html('');
	$('#customerPhonenoSpan').html('');
	$('#emailIdSpan').html('');
	$('#customerPancardnoSpan').html('');
	$('#customerAadharcardnoSpan').html('');
	//$('#loanPurposeSpan').html('');
	$('#occupationNameSpan').html('');
		
	$('#customerAddressSpan').html('');
	$('#pinCodeSpan').html('');
	/* 
	$('#noOFMonthSpan').html('');
	$('#loanAmountSpan').html('');
	$('#goldloanschemeIdSpan').html('');
	$('#interestRateSpan').html('');
	$('#netWeightSpan').html('');
	$('#processingFeeSpan').html('');
	
	$('#totalNumberOfGoldItemsSpan').html('');
	$('#companyBankIdSpan').html('');

	$('#refNumberSpan').html('');
	$('#narrationSpan').html('');
	$('#paymentModeSpan').html('');
	 */
	
}

function init()
{
	clearAll();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	document.customerform.customerName.focus();	
	
}

function validate()
{
	
	clearAll();
	
	  if(document.customerform.customerName.value=="")
		{
		  $('#customerNameSpan').html('Please, Enter customer name..!');
			document.customerform.customerName.focus();
			return false;
		}
		else if(document.customerform.customerName.value.match(/^[\s]+$/))
		{
			$('#customerNameSpan').html('Please, Enter customer name..!');
			document.customerform.customerName.value="";
			document.customerform.customerName.focus();
			return false; 	
		}

	  
		//validation for mobileNumber mobile number------------------------------
		if(document.customerform.mobileNumber.value=="")
		{
			$('#mobileNumberSpan').html('Please, Enter Customer mobile number..!');
			document.customerform.mobileNumber.value="";
			document.customerform.mobileNumber.focus();
			return false;
		}
		else if(!document.customerform.mobileNumber.value.match(/^[0-9]{10}$/))
		{
			$('#mobileNumberSpan').html(' Enter valid Customer mobile number..!');
			document.customerform.mobileNumber.value="";
			document.customerform.mobileNumber.focus();
			return false;	
		}
	    
		  //validation for customerform email address
		if(document.customerform.emailId.value=="")
		{
			$('#emailIdSpan').html('Email Id should not be blank..!');
			document.customerform.emailId.focus();
			return false;
		}
		else if(!document.customerform.emailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			$('#emailIdSpan').html(' Enter valid email id..!');
			document.customerform.emailId.value="";
			document.customerform.emailId.focus();
			return false;
		}
		  
		//validation for customerPancardno
		if(document.customerform.customerPancardno.value=="")
		{
			$('#customerPancardnoSpan').html('Please, Enter Pancard number..!');
			document.customerform.customerPancardno.focus();
			return false;
		}
		else if(!document.customerform.customerPancardno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
		{
			$('#customerPancardnoSpan').html('PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
			document.customerform.customerPancardno.value="";
			document.customerform.customerPancardno.focus();
			return false;
		}
		
		//validation for customerAadharcardno
		if(document.customerform.customerAadharcardno.value=="")
		{
			$('#customerAadharcardnoSpan').html('Please, Enter Aadhar card number..!');
			document.customerform.customerAadharcardno.focus();
			return false;
		}
		else if(!document.customerform.customerAadharcardno.value.match(/^\d{4}\d{4}\d{4}$/))
		{
			$('#customerAadharcardnoSpan').html('Aadhar card number should be 12 digit number only..!');
			document.customerform.customerAadharcardno.value="";
			document.customerform.customerAadharcardno.focus();
			return false;
		}
		
		
		if(document.customerform.occupationName.value=="Default")
		{
			$('#occupationNameSpan').html('Please, select occupation Name..!');
			document.customerform.occupationName.focus();
			return false;
		}
	  
	  if(document.customerform.customerAddress.value=="")
		{
		  $('#customerAddressSpan').html('Please, Enter customer address..!');
			document.customerform.customerAddress.focus();
			return false;
		}
		else if(document.customerform.customerAddress.value.match(/^[\s]+$/))
		{
			$('#customerAddressSpan').html('Please, Enter customer address..!');
			document.customerform.customerAddress.value="";
			document.customerform.customerAddress.focus();
			return false; 	
		}
	  
}
</script>
</body>
</html>