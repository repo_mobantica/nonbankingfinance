<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Bank Details</title>

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Multi Item Selection examples -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

        <link href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/mjolnic-bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Bank Details</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="BankTransactionReport">Bank Report</a></li>
                                        <li class="breadcrumb-item active">Bank Details</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form target="_blank"  name="customerPaymentform" action="${pageContext.request.contextPath}/BankTransactionDetailsReport" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                <div class="row">
	                 
					  <div class="col-xl-3">
                      <div class="form-group">
	                      <label>Start Date</label>
	                      <div class="input-group">
		                      <div class="input-group-append">
		                      <span class="input-group-text"><i class="icon-calender"></i></span>
	                      </div>
	                      
		                      <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="startDate" name="startDate">
	                      </div>
                      </div>

                      <span id="startDateSpan" style="color:#FF0000"></span>
					 </div>
					
					
					  <div class="col-xl-3">
                      <div class="form-group">
	                      <label>End Date</label>
	                      <div class="input-group">
		                      <div class="input-group-append">
		                      <span class="input-group-text"><i class="icon-calender"></i></span>
		                      </div>
		                      
		                      <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="endDate" name="endDate">
		                   
	                       </div><!-- input-group -->
                      </div>

                      <span id="endDateSpan" style="color:#FF0000"></span>
					 </div>
					
					  <div class="col-xl-1">
                      <div class="form-group">
	                      <label></label>
	                      <div class="input-group">
		                      
		                       <span class="input-group-btn">
			            	    <button type="button" class="btn btn-success" onclick="return BankTransactionSearchByDateWise()"><i class="ti-search"></i></button>
			              	   </span>
	                       </div><!-- input-group -->
                      </div>

                      <span id="endDateSpan" style="color:#FF0000"></span>
					 </div>
					
					
					  <div class="col-xl-1">
                      <div class="form-group">
                      
	                      <label></label>
	                      <div class="input-group">
                      		<a  href="${pageContext.request.contextPath}/BankTransactionDetailsReport?companyBankId=${companyBankId}"><button type="button" class="btn btn-default" value="reset">Reset</button></a>
	                       </div><!-- input-group -->
                      </div>

					 </div>
					
                      </div>
                       
                
	                 <div class="row">
	                 
                      <div class="col-xl-12">
                		 <table class="table mb-0 table-sm">
                              <thead>
                                <tr>
                                  <th><th>
                                  <th>Bank Name : ${bankDetails[0].bankName}</th>
                                  <td></td>
                                   
                                  <th><th>
                                  <th>Branch Name : ${bankDetails[0].bankBranchName}</th>
                                  <td></td>
                                         
                                  <th><th>
                                  <th>A/C No. : ${companybankDetails[0].bankAccountNumber}</th>
                                  <td></td>
                                          
                                  <th><th>
                                  <th>IFSC. : ${companybankDetails[0].bankIfsc}</th>
                                  <td></td>
                                         
                                  <th><th>
                                  <th><button class="btn btn-success btn-sm" type="submit"><i class="ion-printer"></i> Print</button></th>
                                  <td></td>
                                              
                                  <th><th>
                                  <th></th>
                                  <td></td>
                                            
                                </tr>
                          
                              
                            </thead>      
                           </table>       
                               
                      </div>
	                 
	                 </div>
	                 
	        <br/>         
	                 
	      <div class="row">
	       <div class="col-xl-12">
               <table class="table table-striped table-bordered table mb-0 table-sm" id="bankNameList">
                  <thead>
                   <tr>
                    <th>#</th>
                    <th>Customer Name</th>
                    <th>Amount</th>
                    <th>Payment</th>
                    <th>Credited/Debited</th>
                    <th>Date</th>
                   </tr>
                 </thead>
                       
                    <tbody>
					<c:forEach items="${bankpaymenthistoryList}" var="bankpaymenthistoryList" varStatus="loopStatus">
					    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
						  <td>${loopStatus.index+1}</td>
						  <td>${bankpaymenthistoryList.customerName}</td>
						  <td>${bankpaymenthistoryList.totalAmount} </td>
						  <td>${bankpaymenthistoryList.paymentpourpose} </td>
						  <td>${bankpaymenthistoryList.paymentStatus} </td>
						  <td>${bankpaymenthistoryList.date} </td>
                        </tr>
			 	    </c:forEach>
                    </tbody>       
               </table>
	       </div>
				 
               </div>   
                 
                 <input type="hidden" id="companyBankId" name="companyBankId" value="${companyBankId}">
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="BankTransactionReport"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                    </div>   
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
  <script>
function BankTransactionSearchByDateWise()
{


	$('#endDateSpan').html('');
	$('#startDateSpan').html('');
	 
	var startDate = $('#startDate').val();
	var endDate = $('#endDate').val();
	var companyBankId=$('#companyBankId').val();
	if(startDate=="")
		{
		  $('#endDateSpan').html('Please, enter start Date..!');
		  return false;
		}

	if(endDate=="")
		{
		  $('#datepickerSpan').html('Please, enter end Date..!');
		  return false;
		}
	

	$("#bankNameList tr").detach();

	$.ajax({

		url : '${pageContext.request.contextPath}/BankTransactionSearchByDateWise',
		type : 'Post',
		data : { companyBankId : companyBankId, startDate : startDate, endDate : endDate},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 

							$('#bankNameList').append('<tr>	<th>#</th><th>Customer Name</th><th>Amount</th><th>Payment</th><th>Credited/Debited</th><th>Date</th></tr>');
							
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#bankNameList').append('<tr><td>'+(i+1)+'</td><td>'+result[i].customerName+'</td><td>'+result[i].totalAmount+'</td><td> '+result[i].paymentpourpose+'</td><td>'+result[i].paymentStatus+'</td><td>'+result[i].date+'</td></tr>');
								}
								else
								{
									$('#bankNameList').append('<tr><td>'+(i+1)+'</td><td>'+result[i].customerName+'</td><td>'+result[i].totalAmount+'</td><td>'+result[i].paymentpourpose+'</td><td>'+result[i].paymentStatus+'</td><td>'+result[i].date+'</td></tr>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
						
					}
		});
	

}

</script>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/jszip.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

        <!-- jQuery  -->

     
        <script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/mjolnic-bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/clockpicker/bootstrap-clockpicker.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {


            	 $('#startDate').datepicker({
            	      autoclose: true
            	    })

            	 $('#endDate').datepicker({
            	      autoclose: true
            	    })

            	
                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#bankNameList').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#bankNameList_wrapper .col-md-6:eq(0)');
            } );

        </script>



</body>
</html>