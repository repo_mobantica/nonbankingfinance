<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Add Village</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Form elements</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Uplon</a></li>
                                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                                        <li class="breadcrumb-item active">Form elements</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="villageform" action="${pageContext.request.contextPath}/EditVillage" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="villageId">Village Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="villageId" name="villageId" value="${villageDetails[0].villageId}" readonly>
                      </div>
					 </div>
				 
					 </div>
					 
					 <div class="row">
	                
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="countryId">Country Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="countryId"  id="countryId" onchange="getStateList(this.value)">
						  <option selected="selected" value="${villageDetails[0].countryId}">${countryName}</option>
							<c:forEach var="countryList" items="${countryList}">
							  	<c:if test="${villageDetails[0].countryId ne countryList.countryId}">
			                    <option value="${countryList.countryId}">${countryList.countryName}</option>
			                    </c:if>
			                </c:forEach>
		                   </select>
                      </div>
                       <span id="countryIdSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="stateId">State Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="stateId"  id="stateId" onchange="getStateWiseDistrictList(this.value)">
						   <option selected="selected" value="${villageDetails[0].stateId}">${stateName}</option>
							<c:forEach var="stateDetails" items="${stateDetails}">
							  	<c:if test="${villageDetails[0].stateId ne stateDetails.stateId}">
			                    <option value="${stateDetails.stateId}">${stateDetails.stateName}</option>
			                    </c:if>
			                </c:forEach>
		                  </select>
                      </div>
                       <span id="stateIdSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="districtId">District Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="districtId"  id="districtId" onchange="getDistrictWiseTahsilList(this.value)">
						  <option selected="selected" value="${villageDetails[0].districtId}">${districtName}</option>
							<c:forEach var="districtDetails" items="${districtDetails}">
							  	<c:if test="${villageDetails[0].districtId ne districtDetails.districtId}">
			                    <option value="${districtDetails.districtId}">${districtDetails.districtName}</option>
			                    </c:if>
			                </c:forEach>
		                  </select>
                      </div>
                       <span id="districtIdSpan" style="color:#FF0000"></span>
					 </div>
					 	 
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="tahsilId">Tahsil Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="tahsilId"  id="tahsilId" >
						   <option selected="selected" value="${villageDetails[0].tahsilId}">${tahsilName}</option>
							<c:forEach var="tahsilDetails" items="${tahsilDetails}">
							  	<c:if test="${villageDetails[0].tahsilId ne tahsilDetails.tahsilId}">
			                    <option value="${tahsilDetails.tahsilId}">${tahsilDetails.tahsilName}</option>
			                    </c:if>
			                </c:forEach>
		                  </select>
                      </div>
                       <span id="tahsilIdSpan" style="color:#FF0000"></span>
					 </div>
					 
					 </div>
					 
					 <div class="row">
	                
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="villageName">Village Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="villageName" name="villageName" placeholder="Village Name" value="${villageDetails[0].villageName}" style="text-transform: capitalize;">
                      </div>
                      <span id="villageNameSpan" style="color:#FF0000"></span>
					 </div>
					   
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="pinCode">Pin Code<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="pinCode" name="pinCode" placeholder="" data-v-max="999999" value="${villageDetails[0].pinCode}" >
                      </div>
                      <span id="pinCodeSpan" style="color:#FF0000"></span>
					 </div>
                    </div>
                    
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="VillageMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="${villageDetails[0].creationDate}">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

<script>
function getDistrictWiseTahsilList()
{
	
	 $("#tahsilId").empty();
	 var countryId = $('#countryId').val();
	 var stateId = $('#stateId').val();
	 var districtId = $('#districtId').val();	
	$.ajax({

		url : '${pageContext.request.contextPath}/getDistrictWiseTahsilList',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId, districtId : districtId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Tahsil-");
							$("#tahsilId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].tahsilId).text(result[i].tahsilName);
							    $("#tahsilId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	

}

function getStateWiseDistrictList()
{
	 $("#tahsilId").empty();
	 $("#districtId").empty();
	 var countryId = $('#countryId').val();
	 var stateId = $('#stateId').val();
		
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateWiseDistrictList',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Tahsil-");
							$("#tahsilId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select District-");
							$("#districtId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].districtId).text(result[i].districtName);
							    $("#districtId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	

}

function getStateList()
{
	
	 $("#stateId").empty();
	 $("#districtId").empty();
	 $("#tahsilId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getCountryWiseStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select District-");
							$("#districtId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List

function init()
{

	$('#pinCodeSpan').html('');
	$('#villageNameSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#districtIdSpan').html('');
	$('#tahsilIdSpan').html('');
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	//document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.villageform.villageName.focus();	
}

function validate()
{
	$('#pinCodeSpan').html('');
	$('#villageNameSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#districtIdSpan').html('');
	$('#tahsilIdSpan').html('');
	
	if(document.villageform.countryId.value=="Default")
	{
		$('#countryIdSpan').html('Please, select Country Name..!');
		document.villageform.countryId.focus();
		return false;
	}

	if(document.villageform.stateId.value=="Default")
	{
		$('#stateIdSpan').html('Please, select State Name..!');
		document.villageform.stateId.focus();
		return false;
	}

	if(document.villageform.districtId.value=="Default")
	{
		$('#districtIdSpan').html('Please, select District Name..!');
		document.villageform.districtId.focus();
		return false;
	}

	if(document.villageform.tahsilId.value=="Default")
	{
		$('#tahsilIdSpan').html('Please, select Tahsil Name..!');
		document.villageform.tahsilId.focus();
		return false;
	}
	
	  if(document.villageform.villageName.value=="")
		{
		  $('#villageNameSpan').html('Please, enter village name..!');
			document.villageform.villageName.focus();
			return false;
		}
		else if(document.villageform.villageName.value.match(/^[\s]+$/))
		{
			$('#villageNameSpan').html('Please, enter village name..!');
			document.villageform.villageName.value="";
			document.villageform.villageName.focus();
			return false; 	
		}
	  if(document.villageform.pinCode.value=="")
		{
			$('#pinCodeSpan').html('Please, enter valid 6 digit pincode');
			document.villageform.pinCode.focus();
			return false;
		}
}
</script>
    </body>
</html>