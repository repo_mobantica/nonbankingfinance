<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>EMI Calculator</title>

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Multi Item Selection examples -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">EMI Calculator</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item active">EMI Calculator</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="emicalculatorform" action="${pageContext.request.contextPath}/EMICalculator" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="principalAmount">Principal Amount<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="principalAmount" name="principalAmount" placeholder="Principal Amount">
                      </div>
                        <span id="principalAmountSpan" style="color:#FF0000"></span>
					 </div>
				 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="interestRate">Interest Rate/Year<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="interestRate" name="interestRate" placeholder="Interest Rate">
                      </div>
                      <span id="interestRateSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="noOfMonths">No. of Months<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="noOfMonths" name="noOfMonths" placeholder="No Of Months">
                      </div>
                      <span id="noOfMonthsSpan" style="color:#FF0000"></span>
					 </div>
					 	
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="emiType">EMI Type<span class="text-danger">*</span></label>
                          <select class="form-control" name="emiType"  id="emiType" >
						  <option selected=selected value="Default">-Select Type-</option>
			              <option value="Fixed">Fixed</option>
			              <option value="Floating">Floating</option>
		                  </select>
                      </div>
                       <span id="emiTypeSpan" style="color:#FF0000"></span>
					 </div>
					 
                     <div class="col-xl-2">
                      	<br/>
                     	<button type="button" class="btn btn-success" onclick="return CalculateTotalEMI()">Submit</button>
					 </div>
						 
					 <div class="col-xl-2">
					 	  <br/>
				  		  <a href="EMICalculator">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
	                  </div> 
	                  
					 </div>
			
					 <div class="row">
		                 
	                      <div class="col-xl-2">
	                      <div class="form-group">
	                      		<label for="totalInt">Total Interest<span class="text-danger">*</span></label>
	                           	<input class="form-control" type="text" id="totalInt" name="totalInt" placeholder="Total Interest" readonly>
	                      </div>
						 </div>
						
	                      <div class="col-xl-2">
	                      <div class="form-group">
	                      		<label for="totalAmt">Total Amount<span class="text-danger">*</span></label>
	                           	<input class="form-control" type="text" id="totalAmt" name="totalAmt" placeholder="Total Amount" readonly>
	                      </div>
						 </div>
						 
		              </div>
				
						<div class="card-box table-responsive">

                          <table id="emiCalculatorList" class="table table-striped table-bordered table mb-0 table-sm" cellspacing="0" width="100%">
                               <thead>
                                <tr>
                                   <th>Month</th>
                                   <th>Principal Per Month</th>
                    		       <th>Interest Per Month</th>
                    			   <th>Total</th>
                    			   <th>Balance Principal</th>
                                </tr>
                               </thead>

                               <tbody>
                                  
                               </tbody>
                          </table>
                       </div>
			
				</div>
				
                </div>

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/jszip.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

<script>

function clearAll()
{
	$('#principalAmountSpan').html('');
	$('#interestRateSpan').html('')
	$('#noOfMonthsSpan').html('');
	$('#emiTypeSpan').html('');
}


function calcEmi(principalAmount, interestRate, noOfMonths)
{

	var r =(interestRate/ 12) / 100;
	var emi=(principalAmount * r * (Math.pow((1 + r), noOfMonths)) / ((Math.pow((1 + r), noOfMonths)) - 1));

	return emi;
}
 
function CalculateTotalEMI()
{
	clearAll();
	  //  validation for principalAmount
	  if(document.emicalculatorform.principalAmount.value=="")
		{
		  $('#principalAmountSpan').html('Please, enter principal Amount..!');
			document.emicalculatorform.principalAmount.focus();
			return false;
		}
		else if(document.emicalculatorform.principalAmount.value.match(/^[\s]+$/))
		{
			$('#principalAmountSpan').html('Please, enter principal Amount..!');
			document.emicalculatorform.principalAmount.value="";
			document.emicalculatorform.principalAmount.focus();
			return false; 	
		}
		else if(!document.emicalculatorform.principalAmount.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.emicalculatorform.principalAmount.value.match(/^[0-9]+$/))
				 {
					$('#principalAmountSpan').html('Please, use only digit value for principal Amount');
					document.emicalculatorform.principalAmount.value="";
					document.emicalculatorform.principalAmount.focus();
					return false;
				}
		}


	  //  validation for interestRate
	  if(document.emicalculatorform.interestRate.value=="")
		{
		  $('#interestRateSpan').html('Please, enter interest Rate..!');
			document.emicalculatorform.interestRate.focus();
			return false;
		}
		else if(document.emicalculatorform.interestRate.value.match(/^[\s]+$/))
		{
			$('#interestRateSpan').html('Please, enter interest Rate..!');
			document.emicalculatorform.interestRate.value="";
			document.emicalculatorform.interestRate.focus();
			return false; 	
		}
		else if(!document.emicalculatorform.interestRate.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.emicalculatorform.interestRate.value.match(/^[0-9]+$/))
				 {
					$('#interestRateSpan').html('Please, use only digit value for interest Rate');
					document.emicalculatorform.interestRate.value="";
					document.emicalculatorform.interestRate.focus();
					return false;
				}
		}


	  //  validation for noOfMonths
	  if(document.emicalculatorform.noOfMonths.value=="")
		{
		  $('#noOfMonthsSpan').html('Please, enter No Of Months..!');
			document.emicalculatorform.noOfMonths.focus();
			return false;
		}
		else if(document.emicalculatorform.noOfMonths.value.match(/^[\s]+$/))
		{
			$('#noOfMonthsSpan').html('Please, enter No Of Months..!');
			document.emicalculatorform.noOfMonths.value="";
			document.emicalculatorform.noOfMonths.focus();
			return false; 	
		}
		else if(!document.emicalculatorform.noOfMonths.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.emicalculatorform.noOfMonths.value.match(/^[0-9]+$/))
				 {
					$('#noOfMonthsSpan').html('Please, use only digit value for No Of Months');
					document.emicalculatorform.noOfMonths.value="";
					document.emicalculatorform.noOfMonths.focus();
					return false;
				}
		}

		if(document.emicalculatorform.emiType.value=="Default")
		{
			$('#emiTypeSpan').html('Please, select EMI Type..!');
			document.emicalculatorform.emiType.focus();
			return false;
		}
	
	 var principalAmount = Number($('#principalAmount').val()); //p
	 var interestRate = Number($('#interestRate').val());
	 var noOfMonths = Number($('#noOfMonths').val());
	 var emiType=($('#emiType').val());

	 $("#emiCalculatorList tr").detach();
	 
	   var R = (interestRate/12 ) / 100;
       var principal = principalAmount; //P
       var emi = calcEmi(principal, interestRate, noOfMonths);
        
      if(emiType=="Floating")
       {
       var totalInt = Math.round((emi * noOfMonths) - principalAmount);
       
       var totalAmt = Math.round((emi * noOfMonths));
       
       document.emicalculatorform.totalInt.value=totalInt;
       document.emicalculatorform.totalAmt.value=totalAmt;
       
       var intPerMonth = Math.round(totalInt / noOfMonths);

       
		$('#emiCalculatorList').append('<tr><th>Month</th> <th>Principal Per Month</th><th>Interest Per Month</th><th>Total</th><th>Balance Principal</th></tr>');
			
			for(var i=1;i<=noOfMonths;i++)
			{
				 
				 intPerMonth = (principal * R);
				
				 principal = ((principal) - ((emi) - (intPerMonth)));
				 
				var interestPerMonth=Math.round(intPerMonth);
				var principalPerMonths1=emi - intPerMonth;
				var principalPerMonths=Math.round(principalPerMonths1);
				var balancePrincipal=Math.round(principal);
				var total=principalPerMonths+interestPerMonth;
				
				 $('#emiCalculatorList').append('<tr><td>'+i+'</td><td>'+principalPerMonths+'</td><td>'+interestPerMonth+'</td><td>'+total+'</td><td>'+balancePrincipal+'</td></tr>');
					 
				
			}  
				 
       }
      else
    	  {

          var intPerMonth = (principalAmount * R);
          var totalInt = Math.round(intPerMonth*noOfMonths);
          
          var totalAmt = Math.round(totalInt+principalAmount);
          
          var emi1=Math.round(totalAmt/noOfMonths);
          document.emicalculatorform.totalInt.value=totalInt;
          document.emicalculatorform.totalAmt.value=totalAmt;

          var principalPerMonths= Math.round(principalAmount/noOfMonths);
          var interestPerMonth= Math.round(intPerMonth);
          var total= Math.round(principalPerMonths+interestPerMonth);
          var balancePrincipal= Math.round(principalAmount);
  		$('#emiCalculatorList').append('<tr><th>Month</th> <th>Principal Per Month</th><th>Interest Per Month</th><th>Total</th><th>Balance Principal</th></tr>');
  			
  			for(var i=1;i<=noOfMonths;i++)
  			{
  				if(i==noOfMonths)
  				{
  					principalPerMonths= Math.round(principalAmount-principalPerMonths*(noOfMonths-1));
  					balancePrincipal=0;
  					 total= Math.round(principalPerMonths+interestPerMonth);
  				}
  				else
  				{
  					balancePrincipal= Math.round(balancePrincipal-principalPerMonths);
  				}
  				
  				 $('#emiCalculatorList').append('<tr><td>'+i+'</td><td>'+principalPerMonths+'</td><td>'+interestPerMonth+'</td><td>'+total+'</td><td>'+balancePrincipal+'</td></tr>');
  					 
  				
  			} 
          
    	  }
}




function init()
{
/* 
	$('#branchNameSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#districtIdSpan').html('');
	$('#tahsilIdSpan').html('');
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	 */
	 //document.branchform.branchName.focus();	
}


function validate()
{
	
}
</script>
    </body>
</html>