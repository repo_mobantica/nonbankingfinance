<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Import New States</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Import States</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="StateMaster">States Master</a></li>
                                        <li class="breadcrumb-item active">Import States</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="stateform" action="${pageContext.request.contextPath}/ImportNewState" onSubmit="return validate()" method="post"  enctype="multipart/form-data">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
					 <div class="row">
					 
                      <div class="col-xl-4">
                      
                      </div>
                      <div class="col-xl-8">
                      <div class="form-group">
								<label for="profile_img1" class="col-sm-4 control-label">Excel File</label>
								<div class="col-sm-5">
									<input type="file" accept=".xls,.xlsx,.csv" class="form-control" id="state_excel" name="state_excel" required="required"/>
									<label id="profile_img1-error"></label>
								</div>
                     
                      </div>
                      <span id="stateNameSpan" style="color:#FF0000"></span>
					 </div>
					
                    </div>
                   
					 <div class="row">
					 	     
                      <div class="col-xl-3">
                      
                      </div>
                      
                      <div class="col-xl-3">
                      	<a href="StateMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
					 </div> 
				
					 <div class="row">
					 
                      <div class="col-xl-8">	 
				  <div class="box-header with-border">
	                
	                  <h4>  <i class="fa fa-text-width"></i> Import Instructions</h4>
	                </div>
	                <div class="box-body">
	                  <dl class="dl-horizontal">
	                    <dt>Template</dt>
	                    <dd>Use Standard Template only for Import.
	                     You can download it from <a href="DownloadStateTemplate">here</a>. </dd>
	                  
	                  </dl>
	                 
	                </div>
                </div>
                
					 </div>						 
                    
                  <br/>
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>
function clearAll()
{

	$('#stateNameSpan').html('');
	$('#stateBranchNameSpan').html('');
	$('#stateAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#districtIdSpan').html('');
	$('#tahsilIdSpan').html('');
	$('#villageIdSpan').html('');
	$('#pinCodeSpan').html('');
	$('#statePhonenoSpan').html('');
	$('#stateifscCodeSpan').html('');
	
}

function init()
{
	//clearAll();
		
}

function validate()
{
	/* 
	clearAll();

	  if(document.stateform.stateName.value=="")
		{
		  $('#stateNameSpan').html('Please, enter state name..!');
			document.stateform.stateName.focus();
			return false;
		}
		else if(document.stateform.stateName.value.match(/^[\s]+$/))
		{
			$('#stateNameSpan').html('Please, enter state name..!');
			document.stateform.stateName.value="";
			document.stateform.stateName.focus();
			return false; 	
		}
	
 */  

	
}
</script>
    </body>
</html>