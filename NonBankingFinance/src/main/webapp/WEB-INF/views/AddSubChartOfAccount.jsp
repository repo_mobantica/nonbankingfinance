<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Add SubChartOfAccount</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add SubChartOfAccount</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item"><a href="SubChartOfAccountMaster">SubChartOfAccount Master</a></li>
                                        <li class="breadcrumb-item active">Add SubChartOfAccount</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="subChartOfAccountform" action="${pageContext.request.contextPath}/AddSubChartOfAccount" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="subChartOfAccountId">SubChartOfAccount Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="subChartOfAccountId" name="subChartOfAccountId"  value="${subChartOfAccountCode}" readonly>
                      </div>
					 </div>
				 
					 </div>
					 
					 <div class="row">
	                
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="chartOfAccountId">COA Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="chartOfAccountId"  id="chartOfAccountId" onchange="getCOAWiseNumber(this.value)">
						  <option selected="selected" value="Default">-Select ChartOfAccount-</option>
							  <c:forEach var="chartOfAccountList" items="${chartOfAccountList}">
			                    <option value="${chartOfAccountList.chartOfAccountId}">${chartOfAccountList.chartOfAccountName}</option>
			                  </c:forEach>
		                   </select>
                      </div>
                       <span id="chartOfAccountIdSpan" style="color:#FF0000"></span>
					 </div>
					  
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="chartOfAccountNumber">COA Number<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="chartOfAccountNumber" name="chartOfAccountNumber"  placeholder="COA Number" readonly>
                      </div>
                       <span id="chartOfAccountNumberSpan" style="color:#FF0000"></span>
					 </div>
					 
					 </div>
					 
					 <div class="row">
	                  
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="subChartOfAccountNumber">Sub COA Number<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="subChartOfAccountNumber" name="subChartOfAccountNumber"  placeholder="Sub-COA Number" >
                      </div>
                       <span id="subChartOfAccountNumberSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="subChartOfAccountName">Sub COA Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="subChartOfAccountName" name="subChartOfAccountName" placeholder="SubChartOfAccount Name" style="text-transform: capitalize;">
                      </div>
                      <span id="subChartOfAccountNameSpan" style="color:#FF0000"></span>
					 </div>
					 
                    </div>
                    
                  <br/>
	                 <div class="row">
					     
                      <div class="col-xl-3">
                      	<a href="SubChartOfAccountMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

<script>
function getCOAWiseNumber()
{
	 $("#chartOfAccountNumber").empty();
	 var chartOfAccountId = $('#chartOfAccountId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getCOAWiseNumber',
		type : 'Post',
		data : { chartOfAccountId : chartOfAccountId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								$('#chartOfAccountNumber').val(result[i].chartOfAccountNumber);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function init()
{
	$('#subChartOfAccountNameSpan').html('');
	$('#chartOfAccountIdSpan').html('');
	$('#subChartOfAccountNumberSpan').html('');
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.subChartOfAccountform.subChartOfAccountNumber.focus();	
}

function validate()
{
	$('#subChartOfAccountNameSpan').html('');
	$('#chartOfAccountIdSpan').html('');
	$('#subChartOfAccountNumberSpan').html('');

		if(document.subChartOfAccountform.chartOfAccountId.value=="Default")
		{
			$('#chartOfAccountIdSpan').html('Please, select ChartOfAccount Name..!');
			document.subChartOfAccountform.chartOfAccountId.focus();
			return false;
		}

	  if(document.subChartOfAccountform.subChartOfAccountNumber.value=="")
		{
		  $('#subChartOfAccountNumberSpan').html('Please, enter Sub-COA Number..!');
			document.subChartOfAccountform.subChartOfAccountNumber.focus();
			return false;
		}
		else if(document.subChartOfAccountform.subChartOfAccountNumber.value.match(/^[\s]+$/))
		{
			$('#subChartOfAccountNumberSpan').html('Please, enter Sub-COA Number..!');
			document.subChartOfAccountform.subChartOfAccountNumber.value="";
			document.subChartOfAccountform.subChartOfAccountNumber.focus();
			return false; 	
		}
		else if(!document.subChartOfAccountform.subChartOfAccountNumber.value.match(/^[0-9]+$/))
			 {
				$('#subChartOfAccountNumberSpan').html('Please, use only digit value for Sub-COA..! eg: 30');
				document.subChartOfAccountform.subChartOfAccountNumber.value="";
				document.subChartOfAccountform.subChartOfAccountNumber.focus();
				return false;
			}
	  if(document.subChartOfAccountform.subChartOfAccountName.value=="")
		{
		  $('#subChartOfAccountNameSpan').html('Please, enter subChartOfAccount name..!');
			document.subChartOfAccountform.subChartOfAccountName.focus();
			return false;
		}
		else if(document.subChartOfAccountform.subChartOfAccountName.value.match(/^[\s]+$/))
		{
			$('#subChartOfAccountNameSpan').html('Please, enter subChartOfAccount name..!');
			document.subChartOfAccountform.subChartOfAccountName.value="";
			document.subChartOfAccountform.subChartOfAccountName.focus();
			return false; 	
		}

}
</script>
    </body>
</html>