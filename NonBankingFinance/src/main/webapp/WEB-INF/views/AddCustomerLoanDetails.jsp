<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Add Customer</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Customer</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="CustomerMaster">Customer Master</a></li>
                                        <li class="breadcrumb-item active">Add Customer</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="customerLoanform" action="${pageContext.request.contextPath}/AddCustomerLoanDetails" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">

                      <div class="col-xl-12">
	                  <div class="card-box table-responsive">

                         <table id="customerNameList" class="table table-striped table-bordered table mb-0 table-sm">
                           <tbody>
                           
                              <tr> 
                              <td>Customer Id</td>
						      <td>: ${customerDetails[0].customerId}</td>
						      
                              <td>Customer Name</td>
						      <td>: ${customerDetails[0].cutsomerName}</td>
                              </tr>
                              
                              <tr> 
                              <td>Mobile Number</td>
						      <td>: ${customerDetails[0].mobileNumber}</td>
						      
                              <td>Email Id</td>
						      <td>: ${customerDetails[0].emailId}</td>
						      
                              <td>PAN Number</td>
						      <td>: ${customerDetails[0].customerPancardno}</td>
						      
                              <td>Aadhar Number</td>
						      <td>: ${customerDetails[0].customerAadharcardno}</td>
						      
                              </tr>
                              
                           </tbody>
                                        
                         </table>
                      </div>
                     </div>             
	                 <br/>
	                 
	                 <br/>
	                 
                     
					 </div>
                    
                    
					 <div class="row">
	                  
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="loanAmount">Loan Amount<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="loanAmount" name="loanAmount" placeholder="Loan Amount" onchange="getCheckLoanAmountIsInScheme(this.value)">
                      </div>
                      <span id="loanAmountSpan" style="color:#FF0000"></span>
					 </div>
	                   
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="goldloanschemeId">Scheme Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="goldloanschemeId"  id="goldloanschemeId" onchange="getGoldloanschemeDetails(this.value)">
						  <option selected="selected" value="Default">-Select Scheme-</option>
							  <c:forEach var="goldLoanSchemeList" items="${goldLoanSchemeList}">
			                    <option value="${goldLoanSchemeList.goldloanschemeId}">${goldLoanSchemeList.goldloanschemeName}</option>
			                  </c:forEach>
		                   </select>
                      </div>
                       <span id="goldloanschemeIdSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="interestRate">Interest Rate/Year<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="interestRate" name="interestRate" placeholder="Interest Rate" >
                      </div>
                      <span id="interestRateSpan" style="color:#FF0000"></span>
					 </div>
	                       
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="noOFMonth">No. of Month<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="noOFMonth" name="noOFMonth" placeholder="No of Months" onchange="CalculateEMI(this.value)" >
                      </div>
                      <span id="noOFMonthSpan" style="color:#FF0000"></span>
					 </div>
	                        
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="loanType"> Type<span class="text-danger">*</span></label>
                          <select class="form-control" name="loanType"  id="loanType" onchange="CalculateEMI(this.value)">
						  <option selected=selected value="Default">-Select Type-</option>
			              <option value="Fixed">Fixed</option>
			              <option value="EMI">EMI</option>
		                  </select>
                      </div>
                       <span id="loanTypeSpan" style="color:#FF0000"></span>
					 </div>
 	
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="emiType">EMI Type<span class="text-danger">*</span></label>
                          <select class="form-control" name="emiType"  id="emiType" onchange="CalculateEMI(this.value)">
						  <option selected=selected value="Default">-Select Type-</option>
			              <option value="Fixed">Fixed</option>
			              <option value="Floating">Floating</option>
		                  </select>
                      </div>
                       <span id="emiTypeSpan" style="color:#FF0000"></span>
					 </div>
					 	                        
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="monthlyEmi">Monthly EMI<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="monthlyEmi" name="monthlyEmi" readonly="readonly" >
                      </div>
                      <span id="monthlyEmiSpan" style="color:#FF0000"></span>
					 </div>
	                         
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="totalInterest">Total EMI Interest<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="totalInterest" name="totalInterest" readonly="readonly" >
                      </div>
                      <span id="totalInterestSpan" style="color:#FF0000"></span>
					 </div>
	                        
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="totalAmount">Total EMI Amount<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="totalAmount" name="totalAmount" readonly="readonly" >
                      </div>
                      <span id="totalAmountSpan" style="color:#FF0000"></span>
					 </div>
	                  
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="processingFee">Processing Fee<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="processingFee" name="processingFee" placeholder="processing Fee" >
                      </div>
                      <span id="processingFeeSpan" style="color:#FF0000"></span>
					 </div>

                    </div>
                   <!-- 
					 <div class="row">
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="customerBankName">Bank Name</label>
                           	<input class="form-control" type="text" id="customerBankName" name="customerBankName"  placeholder="Bank Name">
                      </div>
                       <span id="customerBankNameSpan" style="color:#FF0000"></span>
					 </div>
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="bankBranchName">Branch Name</label>
                           	<input class="form-control" type="text" id="bankBranchName" name="bankBranchName"  placeholder="Branch Name">
                      </div>
                      <span id="bankBranchNameSpan" style="color:#FF0000"></span>
					 </div>
						
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="banckIFSC">IFSC</label>
                           	<input class="form-control" type="text" id="banckIFSC" name="banckIFSC"  placeholder="IFSC" >
                      </div>
                       <span id="banckIFSCSpan" style="color:#FF0000"></span>
					 </div>
					 
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="bankAccountNumber">REF No.</label>
                           	<input class="form-control" type="text" id="bankAccountNumber" name="bankAccountNumber"  placeholder="Account Number">
                      </div>
                      <span id="bankAccountNumberSpan" style="color:#FF0000"></span>
					 </div>
					 
					 </div>
                     -->
                    
					 <div class="row">
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="goldItemName">Item Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="goldItemName"  id="goldItemName">
						  <option selected="selected" value="Default">-Select Scheme-</option>
							  <c:forEach var="goldItemList" items="${goldItemList}">
			                    <option value="${goldItemList.goldItemName}">${goldItemList.goldItemName}</option>
			                  </c:forEach>
		                   </select>
                      </div>
                       <span id="goldItemNameSpan" style="color:#FF0000"></span>
					 </div>
	                       
                      <div class="col-xl-1">
                      <div class="form-group">
                      		<label for="itemQty">Qty<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="itemQty" name="itemQty" >
                      </div>
                      <span id="itemQtySpan" style="color:#FF0000"></span>
					 </div>
	                       
                      <div class="col-xl-1">
                      <div class="form-group">
                      		<label for="itemGrossWeight1">D. Wt<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="itemGrossWeight1" name="itemGrossWeight1" onchange="getCheckPurWeight(this.value)">
                      </div>
                      <span id="itemGrossWeight1Span" style="color:#FF0000"></span>
					 </div>
	                         
                      <div class="col-xl-1">
                      <div class="form-group">
                      		<label for="itemGrossWeight">G. Wt<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="itemGrossWeight" name="itemGrossWeight" onchange="getCheckPurWeight(this.value)">
                      </div>
                      <span id="itemGrossWeightSpan" style="color:#FF0000"></span>
					 </div>
	                          
                      <div class="col-xl-1">
                      <div class="form-group">
                      		<label for="itemWeight">N. Wt<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="itemWeight" name="itemWeight" onchange="getCheckPurWeight(this.value)">
                      </div>
                      <span id="itemWeightSpan" style="color:#FF0000"></span>
					 </div>
	                       
                      <div class="col-xl-1">
                      <div class="form-group">
                      		<label for="purityPer">Purity %<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="purityPer" name="purityPer" onchange="getCheckPurWeight(this.value)">
                      </div>
                      <span id="purityPerSpan" style="color:#FF0000"></span>
					 </div>
	                  
	                       
                      <div class="col-xl-1">
                      <div class="form-group">
                      		<label for="purWeight">Pur Wt<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="purWeight" name="purWeight" readonly="readonly">
                      </div>
                      <span id="purWeightSpan" style="color:#FF0000"></span>
					 </div>
	                     
					  <div class="col-xl-2">
                      <div class="form-group">
	                     <label for="itemMarketValue">Aprx<span class="text-danger">*</span></label>
	                      <div class="input-group">
		                     	<input class="form-control" type="text" id="itemMarketValue" name="itemMarketValue" readonly="readonly">
		                       <span class="input-group-btn">
			            	    <button type="button" class="btn btn-success" onclick="return AddCustomerGoldItemList()"><i class="fa fa-plus"></i>Add</button>
			              	   </span>
	                      </div>
                      </div>

                      <span id="itemMarketValueSpan" style="color:#FF0000"></span>
					 </div>
	                   
                    </div>
                    
					
					 <div class="row">
					      
					  <div class="col-xl-8">
	                     <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title">Item Table</h4> <span id="totalNumberOfGoldItemsSpan" style="color:#FF0000"></span>

									 <table id="customerGoldItemListTable"  class="table table-striped table-bordered table mb-0 table-sm">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Item</th>
                                            <th>Qty</th>
                                            <th>Gross Wt</th>
                                            <th>Wt</th>
                                            <th>Per</th>
                                            <th>Pur Wt</th>
                                            <th>Aprx</th>
                                            <th style="width:50px">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                    </table>

                           </div>
                         </div>
					 </div>
					  
                    
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="CustomerLoanDetailsMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
                
                
               <input type="hidden" id="customerId" name="customerId" value="${customerDetails[0].customerId}">
               <input type="hidden" id="packetNumber" name="packetNumber" value="${packetNumber}">
               
                  <input type="hidden" id="totalItemMarketValue" name="totalItemMarketValue" value="">
                  <input type="hidden" id="totalNumberOfGoldItems" name="totalNumberOfGoldItems" value="">
                  
                 <input type="hidden" id="minimumLoanAmount" name="minimumLoanAmount" value="">
				 <input type="hidden" id="maximumLoanAmount" name="maximumLoanAmount" value="">
				 <input type="hidden" id="maximumTenure" name="maximumTenure" value="">
				 
				 <input type="hidden" id="todayGoldRate" name="todayGoldRate" value="<%= session.getAttribute("todayGoldRate") %>">
               
				 
				 <input type="hidden" id="goldRateBy99_1" name="goldRateBy99_1" value="<%= session.getAttribute("goldRateBy99_1") %>">
               
                
				 <input type="hidden" id="userId" name="userId"  value="<%= session.getAttribute("employeeId") %>">	
                 <input type="hidden" id="branchId" name="branchId"  value="<%= session.getAttribute("branchId") %>">	    
				</div>
				
                </div>

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>
 
function getBankWiseBranchName()
{
	
	 $("#branchName").empty();
	 var companyBankId = $('#companyBankId').val();

		$.ajax({

			url : '${pageContext.request.contextPath}/getCompanyBankDetailsByBankIdWise',
			type : 'Post',
			data : { companyBankId : companyBankId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								for(var i=0;i<result.length;i++)
								{
									document.customerLoanform.branchName.value=result[i].bankBranchName;
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
	 
}

function AddCustomerGoldItemList()
{
	
	$('#goldItemNameSpan').html('');
	$('#itemQtySpan').html('');
	$('#itemWeightSpan').html('');
	$('#purityPerSpan').html('');
	$('#purWeightSpan').html('');

	$('#totalNumberOfGoldItemsSpan').html('');
	
	if(document.customerLoanform.goldItemName.value=="Default")
	{
		$('#goldItemNameSpan').html('Please, select Item Name..!');
		document.customerLoanform.goldItemName.focus();
		return false;
	}

	 
	  //  validation for itemQty
	  if(document.customerLoanform.itemQty.value=="")
		{
		  $('#itemQtySpan').html('Please, Enter item Qty..!');
			document.customerLoanform.itemQty.focus();
			return false;
		}
		else if(document.customerLoanform.itemQty.value.match(/^[\s]+$/))
		{
			$('#itemQtySpan').html('Please, Enter item Qty..!');
			document.customerLoanform.itemQty.value="";
			document.customerLoanform.itemQty.focus();
			return false; 	
		}
		else if(!document.customerLoanform.itemQty.value.match(/^[0-9]+$/))
		 {
			$('#itemQtySpan').html('Please, use only digit value for item Qty..!');
			document.customerLoanform.itemQty.value="";
			document.customerLoanform.itemQty.focus();
		    return false;
		 }

	  //  validation for itemWeight
	  if(document.customerLoanform.itemGrossWeight.value=="")
		{
		  $('#itemGrossWeightSpan').html('Please, Enter item Gross Weight..!');
			document.customerLoanform.itemGrossWeight.focus();
			return false;
		}
		else if(document.customerLoanform.itemGrossWeight.value.match(/^[\s]+$/))
		{
			$('#itemGrossWeightSpan').html('Please, Enter item Gross Weight..!');
			document.customerLoanform.itemGrossWeight.value="";
			document.customerLoanform.itemGrossWeight.focus();
			return false; 	
		}
		else if(!document.customerLoanform.itemGrossWeight.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.customerLoanform.itemGrossWeight.value.match(/^[0-9]+$/))
				 {
					$('#itemGrossWeightSpan').html('Please, use only digit value for item Gross Weight..! eg:21.36 OR 30');
					document.customerLoanform.itemGrossWeight.value="";
					document.customerLoanform.itemGrossWeight.focus();
					return false;
				}
		}
	 
	 
	  //  validation for itemWeight
	  if(document.customerLoanform.itemWeight.value=="")
		{
		  $('#itemWeightSpan').html('Please, Enter item Weight..!');
			document.customerLoanform.itemWeight.focus();
			return false;
		}
		else if(document.customerLoanform.itemWeight.value.match(/^[\s]+$/))
		{
			$('#itemWeightSpan').html('Please, Enter item Weight..!');
			document.customerLoanform.itemWeight.value="";
			document.customerLoanform.itemWeight.focus();
			return false; 	
		}
		else if(!document.customerLoanform.itemWeight.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.customerLoanform.itemWeight.value.match(/^[0-9]+$/))
				 {
					$('#itemWeightSpan').html('Please, use only digit value for item Weight..! eg:21.36 OR 30');
					document.customerLoanform.itemWeight.value="";
					document.customerLoanform.itemWeight.focus();
					return false;
				}
		}

		 
	  //  validation for purityPer
	  if(document.customerLoanform.purityPer.value=="")
		{
		  $('#purityPerSpan').html('Please, Enter purity Per..!');
			document.customerLoanform.purityPer.focus();
			return false;
		}
		else if(document.customerLoanform.purityPer.value.match(/^[\s]+$/))
		{
			$('#purityPerSpan').html('Please, Enter purity Per..!');
			document.customerLoanform.purityPer.value="";
			document.customerLoanform.purityPer.focus();
			return false; 	
		}
		else if(!document.customerLoanform.purityPer.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.customerLoanform.purityPer.value.match(/^[0-9]+$/))
				 {
					$('#purityPerSpan').html('Please, use only digit value for purity Per..! eg:21.36 OR 30');
					document.customerLoanform.purityPer.value="";
					document.customerLoanform.purityPer.focus();
					return false;
				}
		}


     var todayGoldRate = Number($('#todayGoldRate').val());
     var todayGoldRatePerGram=todayGoldRate/10;
     var totalItemMarketValue=0;
     var itemMarketValue=0;

	 var packetNumber = $('#packetNumber').val();
	 var customerId = $('#customerId').val();
	 var goldItemName = $('#goldItemName').val();
	 
	 
	 var itemQty = $('#itemQty').val();
	 var itemGrossWeight = $('#itemGrossWeight').val();
	 var itemWeight = $('#itemWeight').val();
	 var purityPer = $('#purityPer').val();
	 var purWeight= $('#purWeight').val();
	 var itemMarketValue=$('#itemMarketValue').val();
	 
	 $('#totalNumberOfGoldItems').val("");
	 $('#totalItemMarketValue').val("");
	 
	 $('#customerGoldItemListTable tr').detach();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddCustomerGoldItemList',
		type : 'Post',
		data : { packetNumber : packetNumber, customerId : customerId, goldItemName : goldItemName, itemQty : itemQty, itemGrossWeight : itemGrossWeight, itemWeight : itemWeight, purityPer : purityPer, purWeight : purWeight, itemMarketValue : itemMarketValue},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#customerGoldItemListTable').append('<tr><th>#</th><th>Item</th><th >Qty</th>Gross Wt<th></th><th>Wt</th><th>Per</th><th>Pur Wt</th><th>Aprx</th><th style="width:100px">Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].customergolditemdetailsId;
									$('#customerGoldItemListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].goldItemName+'</td><td>'+result[i].itemGrossWeight+'</td><td>'+result[i].itemQty+'</td><td>'+result[i].itemWeight+'</td><td>'+result[i].purityPer+'</td><td>'+result[i].purWeight+'</td><td>'+result[i].itemMarketValue+'</td><td><a onclick="DeleteCustomerItem('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].customergolditemdetailsId;
									$('#customerGoldItemListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].goldItemName+'</td><td>'+result[i].itemGrossWeight+'</td><td>'+result[i].itemQty+'</td><td>'+result[i].itemWeight+'</td><td>'+result[i].purityPer+'</td><td>'+result[i].purWeight+'</td><td>'+result[i].itemMarketValue+'</td><td><a onclick="DeleteCustomerItem('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								
								itemMarketValue=result[i].purWeight*todayGoldRatePerGram;
								totalItemMarketValue=totalItemMarketValue+itemMarketValue;

								document.customerLoanform.totalItemMarketValue.value=totalItemMarketValue;
								document.customerLoanform.totalNumberOfGoldItems.value=i+1;
							 } 

							if(result.length==0)
								{
								document.customerLoanform.totalItemMarketValue.value=0;
								document.customerLoanform.totalNumberOfGoldItems.value=0;
								}
							 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#itemQty').val("");
	 $('#itemWeight').val("");
	 $('#purityPer').val("");
	 $('#purWeight').val("");
	 $('#itemMarketValue').val("");

}
function DeleteCustomerItem(id)
{

    var todayGoldRate = Number($('#todayGoldRate').val());
    var todayGoldRatePerGram=todayGoldRate/10;
    var totalItemMarketValue=0;
    var itemMarketValue=0;

	var packetNumber = $('#packetNumber').val();
	var customerId = $('#customerId').val();
    var customergolditemdetailsId = id;
    $('#totalNumberOfGoldItems').val("");
    $('#customerGoldItemListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteCustomerItem',
		type : 'Post',
		data : { customerId : customerId, packetNumber : packetNumber, customergolditemdetailsId : customergolditemdetailsId },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#customerGoldItemListTable').append('<tr><th>#</th><th>Item</th><th >Qty</th>Gross Wt<th></th><th>Wt</th><th>Per</th><th>Pur Wt</th><th>Aprx</th><th style="width:100px">Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].customergolditemdetailsId;
									$('#customerGoldItemListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].goldItemName+'</td><td>'+result[i].itemGrossWeight+'</td><td>'+result[i].itemQty+'</td><td>'+result[i].itemWeight+'</td><td>'+result[i].purityPer+'</td><td>'+result[i].purWeight+'</td><td>'+result[i].itemMarketValue+'</td><td><a onclick="DeleteCustomerItem('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].customergolditemdetailsId;
									$('#customerGoldItemListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].goldItemName+'</td><td>'+result[i].itemGrossWeight+'</td><td>'+result[i].itemQty+'</td><td>'+result[i].itemWeight+'</td><td>'+result[i].purityPer+'</td><td>'+result[i].purWeight+'</td><td>'+result[i].itemMarketValue+'</td><td><a onclick="DeleteCustomerItem('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								
								itemMarketValue=result[i].purWeight*todayGoldRatePerGram;
								totalItemMarketValue=totalItemMarketValue+itemMarketValue;

								document.customerLoanform.totalItemMarketValue.value=totalItemMarketValue;
								document.customerLoanform.totalNumberOfGoldItems.value=i+1;
							 } 
							if(result.length==0)
								{
								document.customerLoanform.totalItemMarketValue.value=0;
								document.customerLoanform.totalNumberOfGoldItems.value=0;
								}
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});

	
}


function getCheckPurWeight()
{

	 var itemWeight = Number($('#itemWeight').val());
	 var purityPer = Number($('#purityPer').val());
	 var todayGoldRate = Number($('#todayGoldRate').val());
	 var goldRateBy99_1=Number($('#goldRateBy99_1').val());
	 var goldRateBy99_1PerGram=goldRateBy99_1/10;
	 
	// var todayGoldRatePerGram=todayGoldRate/10;
	 
	 var purWeight=(itemWeight/100)*purityPer;
	 var itemMarketValue=purWeight*goldRateBy99_1PerGram;

	 document.customerLoanform.purWeight.value=purWeight.toFixed(2);
	 document.customerLoanform.itemMarketValue.value=itemMarketValue.toFixed(0);
}


function calcEmi(principalAmount, interestRate, noOfMonths)
{

	var r =(interestRate / 12) / 100;
	var emi=(principalAmount * r * (Math.pow((1 + r), noOfMonths)) / ((Math.pow((1 + r), noOfMonths)) - 1));

	return emi;
}

function CalculateEMI()
{
	
	 var principalAmount = Number($('#loanAmount').val());
	 var interestRate = Number($('#interestRate').val());
	 var noOfMonths = Number($('#noOFMonth').val());
	 

	 var loanType=($('#loanType').val());
	 var emiType=($('#emiType').val());
	 
	 
	 var R = (interestRate/12 ) / 100;
     var principal = principalAmount; //P
     var emi = calcEmi(principal, interestRate, noOfMonths);
     var totalInt;
     var totalAmt;
     
	 if(principalAmount != 0 && interestRate != 0 && noOfMonths != 0)
		 {
		 
		 if(loanType == "EMI")
			 {
			 if(emiType =="Floating")
				 {
			     totalInt = Math.round((emi * noOfMonths) - principalAmount);
			     totalAmt = Math.round((emi * noOfMonths));

			     document.customerLoanform.monthlyEmi.value=Math.round(emi);
			     document.customerLoanform.totalInterest.value=totalInt;
			     document.customerLoanform.totalAmount.value=totalAmt;
			     
			     var intPerMonth = Math.round(totalInt / noOfMonths);
				 }
			 else
				 {
				 
		          var intPerMonth = (principalAmount * R);
		           totalInt = Math.round(intPerMonth*noOfMonths);
		           totalAmt = Math.round(totalInt+principalAmount);
		          
		          var emi1=Math.round(totalAmt/noOfMonths);

				   document.customerLoanform.monthlyEmi.value=emi1;
				   document.customerLoanform.totalInterest.value=totalInt;
				   document.customerLoanform.totalAmount.value=totalAmt;
				 
				 }
			 
			 }
		 else if(loanType == "Fixed")
			 {
	          var intPerMonth = (principalAmount * R);
	           totalInt = Math.round(intPerMonth*noOfMonths);
	           totalAmt = Math.round(totalInt+principalAmount);
	          
	           //var emi1=Math.round(totalAmt/noOfMonths);

			   document.customerLoanform.monthlyEmi.value=0;
			   document.customerLoanform.totalInterest.value=totalInt;
			   document.customerLoanform.totalAmount.value=totalAmt;
			 }
		 
		 
		 }

}

function getMarketValue()
{
	 $("#goldMarketValue").empty();
	 var netWeight = Number($('#netWeight').val());
	 var todayGoldRate = Number($('#todayGoldRate').val());
	 var goldMarketValue=(todayGoldRate/10)*netWeight;
	 document.customerLoanform.goldMarketValue.value=goldMarketValue;
}

function getCheckLoanAmountIsInScheme()
{
	$('#loanAmountSpan').html('');
	 var loanAmount = Number($('#loanAmount').val());
	 var minimumLoanAmount = Number($('#minimumLoanAmount').val());
	 var maximumLoanAmount = Number($('#maximumLoanAmount').val());

	 if(document.customerLoanform.goldloanschemeId.value=="Default")
		{
		}
		else
		{
			if(loanAmount!=0)
				{
				if(loanAmount<minimumLoanAmount || loanAmount>maximumLoanAmount)
				 {
					$('#loanAmountSpan').html('Please, Enter the loan amount between '+minimumLoanAmount+' to '+maximumLoanAmount+'..!');
					document.customerLoanform.loanAmount.value="";
				 }
				}
			
		}
	 CalculateEMI();
}

function getGoldloanschemeDetails()
{

	 $("#interestRate").empty();
	 var goldloanschemeId = $('#goldloanschemeId').val();

		$.ajax({

			url : '${pageContext.request.contextPath}/getGoldloanschemeDetails',
			type : 'Post',
			data : { goldloanschemeId : goldloanschemeId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								for(var i=0;i<result.length;i++)
								{
									document.customerLoanform.interestRate.value=result[i].interestRate;
									document.customerLoanform.minimumLoanAmount.value=result[i].minimumLoanAmount;
									document.customerLoanform.maximumLoanAmount.value=result[i].maximumLoanAmount;
									document.customerLoanform.maximumTenure.value=result[i].maximumTenure;
									//document.customerLoanform.noOFMonth.value=result[i].maximumTenure;
									
									
								 } 
								getCheckLoanAmountIsInScheme();
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
	 
}
 
function clearAll()
{
	$('#packetNumberSpan').html('');
	
	$('#noOFMonthSpan').html('');
	$('#loanAmountSpan').html('');
	$('#goldloanschemeIdSpan').html('');
	$('#interestRateSpan').html('');
	$('#netWeightSpan').html('');
	$('#processingFeeSpan').html('');
	
	$('#totalNumberOfGoldItemsSpan').html('');
	$('#companyBankIdSpan').html('');

	$('#refNumberSpan').html('');
	$('#narrationSpan').html('');
	$('#paymentModeSpan').html('');
	 
	
}

function init()
{
	clearAll();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
}

function validate()
{
	
	clearAll();
	
	 
	 //  validation for itemQty
	  if(document.customerLoanform.loanAmount.value=="")
		{
		  $('#loanAmountSpan').html('Please, Enter loan Amount..!');
			document.customerLoanform.loanAmount.focus();
			return false;
		}
		else if(document.customerLoanform.loanAmount.value.match(/^[\s]+$/))
		{
			$('#loanAmountSpan').html('Please, Enter loan Amount..!');
			document.customerLoanform.loanAmount.value="";
			document.customerLoanform.loanAmount.focus();
			return false; 	
		}
		else if(!document.customerLoanform.loanAmount.value.match(/^[0-9]+$/))
		 {
			$('#loanAmountSpan').html('Please, use only digit value for loan Amount..!');
			document.customerLoanform.loanAmount.value="";
			document.customerLoanform.loanAmount.focus();
		    return false;
		 }

		if(document.customerLoanform.goldloanschemeId.value=="Default")
		{
			$('#goldloanschemeIdSpan').html('Please, select Scheme Name..!');
			document.customerLoanform.goldloanschemeId.focus();
			return false;
		}
		
		 //  validation for itemQty
		  if(document.customerLoanform.noOFMonth.value=="")
			{
			  $('#noOFMonthSpan').html('Please, Enter No. Of Month..!');
				document.customerLoanform.noOFMonth.focus();
				return false;
			}
			else if(document.customerLoanform.noOFMonth.value.match(/^[\s]+$/))
			{
				$('#noOFMonthSpan').html('Please, Enter No. Of Month..!');
				document.customerLoanform.noOFMonth.value="";
				document.customerLoanform.noOFMonth.focus();
				return false; 	
			}
			else if(!document.customerLoanform.noOFMonth.value.match(/^[0-9]+$/))
			 {
				$('#noOFMonthSpan').html('Please, use only digit value for No. Of Month..!');
				document.customerLoanform.noOFMonth.value="";
				document.customerLoanform.noOFMonth.focus();
			    return false;
			 }

			if(document.customerLoanform.loanType.value=="Default")
			{
				$('#loanTypeSpan').html('Please, select Loan Type..!');
				document.customerLoanform.loanType.focus();
				return false;
			}

			if(document.customerLoanform.emiType.value=="Default")
			{
				$('#emiTypeSpan').html('Please, select EMI Type..!');
				document.customerLoanform.emiType.focus();
				return false;
			}
			
		/*   //  validation for netWeight
		  if(document.customerLoanform.netWeight.value=="")
			{
			  $('#netWeightSpan').html('Please, Enter Net Weight..!');
				document.customerLoanform.netWeight.focus();
				return false;
			}
			else if(document.customerLoanform.netWeight.value.match(/^[\s]+$/))
			{
				$('#netWeightSpan').html('Please, Enter  Net Weight..!');
				document.customerLoanform.netWeight.value="";
				document.customerLoanform.netWeight.focus();
				return false; 	
			}
			else if(!document.customerLoanform.netWeight.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
			{
				 if(!document.customerLoanform.netWeight.value.match(/^[0-9]+$/))
					 {
						$('#netWeightSpan').html('Please, use only digit value for Net Weight..!');
						document.customerLoanform.netWeight.value="";
						document.customerLoanform.netWeight.focus();
						return false;
					}
			} */
			
			
			
			 //  validation for processingFee
		  if(document.customerLoanform.processingFee.value=="")
			{
			  $('#processingFeeSpan').html('Please, Enter processing Fee..!');
				document.customerLoanform.processingFee.focus();
				return false;
			}
			else if(document.customerLoanform.processingFee.value.match(/^[\s]+$/))
			{
				$('#processingFeeSpan').html('Please, Enter processing Fee..!');
				document.customerLoanform.processingFee.value="";
				document.customerLoanform.processingFee.focus();
				return false; 	
			}
			else if(!document.customerLoanform.processingFee.value.match(/^[0-9]+$/))
			 {
				$('#processingFeeSpan').html('Please, use only digit value for processing Fee..!');
				document.customerLoanform.processingFee.value="";
				document.customerLoanform.processingFee.focus();
			    return false;
			 }
			 
			 
		  var totalNumberOfGoldItems = Number($('#totalNumberOfGoldItems').val());
		  if(totalNumberOfGoldItems==0)
		  {
			  
			  $('#totalNumberOfGoldItemsSpan').html('Please, add at least one item..!');
			  return false;
		  }
		  
		  var totalItemMarketValue = Number($('#totalItemMarketValue').val());
		  var loanAmount = Number($('#loanAmount').val());
		  
		  if(loanAmount>totalItemMarketValue)
			  {

			  $('#loanAmountSpan').html('This loan Amount is greater then item market value..!');
			  return false;
			  
			  }
/* 
			if(document.customerLoanform.companyBankId.value=="Default")
			{
				$('#companyBankIdSpan').html('Please, select bank A/C No..!');
				document.customerLoanform.companyBankId.focus();
				return false;
			}

			if(document.customerLoanform.paymentMode.value=="Default")
			{
				$('#paymentModeSpan').html('Please, select payment type..!');
				document.customerLoanform.paymentMode.focus();
				return false;
			}
			

			if(document.customerLoanform.refNumber.value == "")
			{
				$('#refNumberSpan').html('Please, enter Ref Number..!');
				document.customerLoanform.refNumber.focus();
				return false;
			}

			if(document.customerLoanform.narration.value == "")
			{
				$('#narrationSpan').html('Please, enter narration..!');
				document.customerLoanform.narration.focus();
				return false;
			}

 	   */
}
</script>
    </body>
</html>