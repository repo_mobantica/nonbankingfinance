<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">

<!-- App Favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/images/favicon.ico">

<!-- App title -->
<title>Add voucher</title>

<!-- Switchery css -->
<link
	href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css"
	rel="stylesheet" />

<!-- Bootstrap CSS -->
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- App CSS -->
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet" type="text/css" />

<!-- Modernizr js -->
<script
	src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


</head>


<body class="fixed-left" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>


	<div id="wrapper">

		<%@ include file="headerpage.jsp"%>

		<%@ include file="menu.jsp"%>

		<div class="content-page">

			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-xl-12">
							<div class="page-title-box">
								<h4 class="page-title float-left">Add voucher</h4>

								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="home">Home</a></li>
									<li class="breadcrumb-item"><a href="voucherMaster">voucher
											Master</a></li>
									<li class="breadcrumb-item active">Add voucher</li>
								</ol>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<form name="voucherform"
						action="${pageContext.request.contextPath}/AddVoucher"
						onSubmit="return validate()" method="post">

						<div class="row">

							<div class="col-12">

								<div class="card-box">


									<div class="row">

										<div class="col-xl-3">
											<div class="form-group">
												<label for="branchName">From Branch Name<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="branchName" name="branchName"
													placeholder="Branch Name"
													style="text-transform: capitalize;" value=${branchName
													} readonly>
											</div>
											<span id="branchNameSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-3">
											<div class="form-group">
												<label for="toBranchId">To Branch Name<span
													class="text-danger">*</span></label> <select class="form-control"
													name="toBranchId" id="toBranchId">
													<option selected="selected" value="Default">-Select
														Branch-</option>
													<c:forEach var="branchList" items="${branchList}">
														<option value="${branchList.branchId}">${branchList.branchName}</option>
													</c:forEach>
												</select>
											</div>
											<span id="toBranchIdSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-3">
											<div class="form-group">
												<label for="amount">Amount<span class="text-danger">*</span></label>
												<input class="form-control" type="text" id="amount"
													name="amount" placeholder="Amount">
											</div>
											<span id="amountSpan" style="color: #FF0000"></span>
										</div>


									</div>

									<br />
									<div class="row">
										<div class="col-xl-2"></div>
										<div class="col-xl-3">
											<a href="VoucherMaster"><button type="button"
													class="btn btn-secondary" value="reset" style="width: 90px">Back</button></a>
										</div>

										<div class="col-xl-3">
											<button type="reset" class="btn btn-default">Reset</button>
										</div>

										<div class="col-xl-3">
											<button class="btn btn-primary" type="submit">Submit</button>
										</div>
									</div>

									<input type="hidden" id="branchId" name="branchId"
										value=${branchId}> 
										<input type="hidden"
										id="userId" name="userId"
										value="<%= session.getAttribute("employeeId") %>">

								</div>

							</div>
							<!-- end col-->

						</div>

					</form>

				</div>
				<!-- container -->
			</div>

			<%@ include file="RightSidebar.jsp"%>

			<%@ include file="footer.jsp"%>

		</div>
	</div>
	<script>
            var resizefunc = [];
        </script>

	<!-- jQuery  -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
	<!-- Tether for Bootstrap -->
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js"
		type="text/javascript"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js"
		type="text/javascript"></script>

	<script>

function init()
{
	clearAll();
	
	
	 document.voucherform.toBranchId.focus();	
}

function clearAll()
{

	$('#toBranchIdSpan').html('');
	$('#amountSpan').html('')

}

function validate()
{
	clearAll();


	if(document.voucherform.toBranchId.value=="Default")
	{
		$('#toBranchIdSpan').html('Please, select Branch Name..!');
		document.voucherform.toBranchId.focus();
		return false;
	}
	  if(document.voucherform.amount.value=="")
		{
		  $('#amountSpan').html('Please, enter Amount..!');
			document.voucherform.amount.focus();
			return false;
		}
		else if(document.voucherform.amount.value.match(/^[\s]+$/))
		{
			$('#amountSpan').html('Please, enter Amount..!');
			document.voucherform.amount.value="";
			document.voucherform.amount.focus();
			return false; 	
		}
	
	

	
}
</script>
</body>
</html>