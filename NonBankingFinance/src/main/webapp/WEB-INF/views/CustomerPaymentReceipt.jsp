<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Customer Payment</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Customer Payment</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="CustomerPaymentMaster">Customer Payment</a></li>
                                        <li class="breadcrumb-item active">Customer Payment Receipt</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="customerPaymentform" action="${pageContext.request.contextPath}/CustomerPaymentReceipt" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
                
	                 <div class="row">
	                 
                      <div class="col-xl-12">
                		 <table class="table mb-0 table-sm">
                              <thead>
                                <tr>
                                  <th><th>
                                  <th>Customer Id</th>
                                  <td>: ${customerDetails[0].customerId}</td>
                                   
                                  <th><th>
                                  <th>Packet Number</th>
                                  <td>: ${customerLoanDetails[0].packetNumber} </td>
                                  
                                  <th><th>
                                  <th>Loan Date</th>
                                  <td>: ${loanDate}</td>
                                  
                                  <th><th>
                                  <th><th>
                                  <th><th>
                                </tr>
                                
                                <tr>
                                
                                  <th><th>
                                  <th>Customer Name</th>
                                  <td>: ${customerDetails[0].cutsomerFirstName} ${customerDetails[0].customerMiddlelName} ${customerDetails[0].customerLastName}</td>
                                   
                                  <th><th>
                                  <th>Mobile No</th>
                                  <td>: ${customerDetails[0].mobileNumber} </td>
                                   
                                  <th><th>
                                  <th>Email Id</th>
                                  <td>: ${customerDetails[0].emailId} </td>
                                   
                                </tr>
                      
                                 <!--          
                            </thead>      
                           </table> 
                                 
                                  <br/>
                                 
                               <table class="table mb-0 table-sm">
                              <thead>  
                               --> 
                                <tr>
                                  <th><th>
                                  <th>Loan Amount</th>
                                  <td>: ${customerLoanDetails[0].loanAmount} </td>
                                 
                                  <th><th>
                                  <th>Scheme Name</th>
                                  <td>: ${goldloanschemeName} </td>
                               
                                  <th><th>
                                  <th><th>
                                  <th><th>
                                
                                </tr>
                     
                                <tr>
                                      
                                  <td><td>
                                  <td></td>
                                  <td></td>
                                     
                                  <td><td>
                                  <td> <td>
                                  <td></td>
                                
                                  <td><td>
                                  <td> <td>
                                  <td></td>
                                
                                </tr>
                   
                          
                              </thead>
                              <tbody>
                              
                              </tbody>
                          </table>
                      </div>
	                 
	                 </div>
	                 
	        <br/>         
	                 
	      <div class="row">
	       <div class="col-xl-6">
	       <h5>Paid History</h5>
               <table class="table table-striped table-bordered table mb-0 table-sm">
                  <thead>
                   <tr>
                    <th>#</th>
                    <th>Principal</th>
                    <th>Interest</th>
                    <th>Due</th>
                    <th>Date</th>
                    <th>Print</th>
                   </tr>
                 </thead>
                       
                    <tbody>
					<c:forEach items="${paymentcustomerDetails}" var="paymentcustomerDetails" varStatus="loopStatus">
					    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
						  <td>${loopStatus.index+1}</td>
						  <td>${paymentcustomerDetails.principalAmount}</td>
						  <td>${paymentcustomerDetails.interestAmount} </td>
						  <td>${paymentcustomerDetails.paymentDue} </td>
						  <td>${paymentcustomerDetails.paidDate} </td>
						   <td>
						    <a onclick="return PrintCustomerPaymentReceipt('${paymentcustomerDetails.customerPaymentId}')" class="btn btn-success btn-sm" data-toggle="tooltip" title="Print EMI Report"><i class="ion-printer"></i></a>
						   </td>
                        </tr>
                        
			 	    </c:forEach>
			 	    
                   <tr>
                    <th>Total</th>
                    <th>${customerPaidPrincipalAmount}</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                   </tr>
                    </tbody>       
               </table>
	       </div>
	      
               </div>  
                <br/>
              <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="CustomerPaymentMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                    </div>      
                    
				</div>
				
                </div>

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>

function PrintCustomerPaymentReceipt(customerPaymentId)
{
	
	$.ajax({

		url : '${pageContext.request.contextPath}/PrintCustomerPaymentReceipt',
		type : 'Post',
		data : { customerPaymentId : customerPaymentId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							
						} 
						else
						{
							alert("failure111");
						}
						
					}
		});
}

function init()
{
}

function validate()
{
	
 
	
}
</script>
</body>
</html>