<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Edit Tax</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Edit Tax</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item"><a href="TaxMaster">Tax Master</a></li>
                                        <li class="breadcrumb-item active">Edit Tax</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="taxform" action="${pageContext.request.contextPath}/EditTax" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="taxId">Tax Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="taxId" name="taxId" value="${taxDetails[0].taxId}" readonly>
                      </div>
					 </div>
				 
					 </div>
					 
					 <div class="row">
	                
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="taxType">Tax Type<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="taxType" name="taxType" placeholder="Tax Type" value="${taxDetails[0].taxType}" style="text-transform: capitalize;">
                      </div>
                       <span id="taxTypeSpan" style="color:#FF0000"></span>
					 </div>
					  
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="taxName">Tax Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="taxName" name="taxName" placeholder="Tax Name" value="${taxDetails[0].taxName}" style="text-transform: capitalize;">
                      </div>
                       <span id="taxNameSpan" style="color:#FF0000"></span>
					 </div>
					  
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="cgstPercentage">CGST Percentage<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="cgstPercentage" name="cgstPercentage" placeholder="CGST Tax Percentage" value="${taxDetails[0].cgstPercentage}" onchange="CalcuateTotalTax(this.value)">
                      </div>
                       <span id="cgstPercentageSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="sgstPercentage">SGST Percentage<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="sgstPercentage" name="sgstPercentage" placeholder="SGST tax Percentage" value="${taxDetails[0].sgstPercentage}" onchange="CalcuateTotalTax(this.value)">
                      </div>
                       <span id="sgstPercentageSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="totaltaxPercentage">Total GST Tax Percentage<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="totaltaxPercentage" name="totaltaxPercentage" placeholder="Total GST Tax Percentage" value="${taxDetails[0].totaltaxPercentage}" readonly>
                      </div>
                       <span id="totaltaxPercentageSpan" style="color:#FF0000"></span>
					 </div>
					 
	                
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="currencyId">Currency Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="currencyId"  id="currencyId" >
					        <option selected="selected" value="${taxDetails[0].currencyId}">${currencyName}</option>
							<c:forEach var="currencyList" items="${currencyList}">
							  	<c:if test="${taxDetails[0].currencyId ne currencyList.currencyId}">
			                    <option value="${currencyList.currencyId}">${currencyList.currencyName}</option>
			                    </c:if>
			                </c:forEach>
			                
		                   </select>
                      </div>
                       <span id="currencyIdSpan" style="color:#FF0000"></span>
					 </div>

                    </div>
                    
                  <br/>
	                 <div class="row">
					     
                      <div class="col-xl-1">
                      </div>
                      <div class="col-xl-3">
                      	<a href="TaxMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="${taxDetails[0].creationDate}" >
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

<script>

function CalcuateTotalTax()
{

	 var sgstPercentage = Number($('#sgstPercentage').val());
	 var cgstPercentage = Number($('#cgstPercentage').val());
	 var totaltaxPercentage=sgstPercentage+cgstPercentage;
	 $('#totaltaxPercentage').val(totaltaxPercentage);

}

function init()
{
	clearAll();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	//document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.taxform.taxType.focus();	
}
function clearAll()
{
	
	$('#taxTypeSpan').html('');
	$('#taxNameSpan').html('');
	$('#cgstPercentageSpan').html('');
	$('#sgstPercentageSpan').html('');
	$('#currencyIdSpan').html('');

}

function validate()
{
	clearAll();
	
	  if(document.taxform.taxType.value=="")
		{
		  $('#taxTypeSpan').html('Please, enter tax type..!');
			document.taxform.taxType.focus();
			return false;
		}
		else if(document.taxform.taxType.value.match(/^[\s]+$/))
		{
			$('#taxTypeSpan').html('Please, enter tax type..!');
			document.taxform.taxType.value="";
			document.taxform.taxType.focus();
			return false; 	
		}
		else if(!document.taxform.taxType.value.match(/^[A-Za-z\s.]+$/))
		{
  		$('#taxTypeSpan').html('tax type should contain alphabets only..!');
			document.taxform.taxType.value="";
			document.taxform.taxType.focus();
			return false;
		}

	  if(document.taxform.taxName.value=="")
		{
		  $('#taxNameSpan').html('Please, enter tax name..!');
			document.taxform.taxName.focus();
			return false;
		}
		else if(document.taxform.taxName.value.match(/^[\s]+$/))
		{
			$('#taxNameSpan').html('Please, enter tax name..!');
			document.taxform.taxName.value="";
			document.taxform.taxName.focus();
			return false; 	
		}

		else if(!document.taxform.taxName.value.match(/^[A-Za-z\s.]+$/))
		{
  		$('#taxNameSpan').html('tax Name should contain alphabets only..!');
			document.taxform.taxName.value="";
			document.taxform.taxName.focus();
			return false;
		}


	  //  validation for cgstPercentage
	  if(document.taxform.cgstPercentage.value=="")
		{
		  $('#cgstPercentageSpan').html('Please, enter CGST..!');
			document.taxform.cgstPercentage.focus();
			return false;
		}
		else if(document.taxform.cgstPercentage.value.match(/^[\s]+$/))
		{
			$('#cgstPercentageSpan').html('Please, enter CGST..!');
			document.taxform.cgstPercentage.value="";
			document.taxform.cgstPercentage.focus();
			return false; 	
		}
		else if(!document.taxform.cgstPercentage.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.taxform.cgstPercentage.value.match(/^[0-9]+$/))
				 {
					$('#cgstPercentageSpan').html('Please, use only digit value for CGST..! eg:21.36 OR 30');
					document.taxform.cgstPercentage.value="";
					document.taxform.cgstPercentage.focus();
					return false;
				}
		}


	  //  validation for sgstPercentage
	  if(document.taxform.sgstPercentage.value=="")
		{
		  $('#sgstPercentageSpan').html('Please, enter SGST..!');
			document.taxform.sgstPercentage.focus();
			return false;
		}
		else if(document.taxform.sgstPercentage.value.match(/^[\s]+$/))
		{
			$('#sgstPercentageSpan').html('Please, enter SGST..!');
			document.taxform.sgstPercentage.value="";
			document.taxform.sgstPercentage.focus();
			return false; 	
		}
		else if(!document.taxform.sgstPercentage.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.taxform.sgstPercentage.value.match(/^[0-9]+$/))
				 {
					$('#sgstPercentageSpan').html('Please, use only digit value for SGST..! eg:21.36 OR 30');
					document.taxform.sgstPercentage.value="";
					document.taxform.sgstPercentage.focus();
					return false;
				}
		}


	if(document.taxform.currencyId.value=="Default")
	{
		$('#currencyIdSpan').html('Please, select Country Name..!');
		document.taxform.currencyId.focus();
		return false;
	}
	
	 	  
}
</script>
    </body>
</html>