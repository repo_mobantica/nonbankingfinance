<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Add Currency</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Currency</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item"><a href="CurrencyMaster">Currency Master</a></li>
                                        <li class="breadcrumb-item active">Add Currency</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="currencyform" action="${pageContext.request.contextPath}/EditCurrency" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="currencyId">Currency Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="currencyId" name="currencyId" value="${currencyDetails[0].currencyId}"  readonly>
                      </div>
					 </div>
				 
					 </div>
					 
					 <div class="row">
	              
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="countryId">Country Name<span class="text-danger">*</span></label>
	                        <select class="form-control" name="countryId"  id="countryId" >
					        <option selected="selected" value="${currencyDetails[0].countryId}">${countryName}</option>
							<c:forEach var="countryList" items="${countryList}">
							  	<c:if test="${currencyDetails[0].countryId ne countryList.countryId}">
			                    <option value="${countryList.countryId}">${countryList.countryName}</option>
			                    </c:if>
			                </c:forEach>
		                   </select>
                      </div>
                       <span id="countryIdSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="currencyName">Currency Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="currencyName" name="currencyName" placeholder="Currency Name" value="${currencyDetails[0].currencyName}" style="text-transform: capitalize;">
                      </div>
                         <span id="currencyNameSpan" style="color:#FF0000"></span>
					 </div>
					 
                    </div>
                    
                  <br/>
	                 <div class="row">
					     
                      <div class="col-xl-3">
                      	<a href="CurrencyMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="${currencyDetails[0].creationDate}">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

<script>


function init()
{
	$('#currencyNameSpan').html('');
	$('#countryIdSpan').html('');
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	//document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.currencyform.currencyName.focus();	
}

function validate()
{
	$('#currencyNameSpan').html('');
	$('#countryIdSpan').html('');
	

	if(document.currencyform.countryId.value=="Default")
	{
		$('#countryIdSpan').html('Please, select Country Name..!');
		document.currencyform.countryId.focus();
		return false;
	}
	  if(document.currencyform.currencyName.value=="")
		{
		  $('#currencyNameSpan').html('Please, enter currency name..!');
			document.currencyform.currencyName.focus();
			return false;
		}
		else if(document.currencyform.currencyName.value.match(/^[\s]+$/))
		{
			$('#currencyNameSpan').html('Please, enter currency name..!');
			document.currencyform.currencyName.value="";
			document.currencyform.currencyName.focus();
			return false; 	
		}
		else if(!document.currencyform.currencyName.value.match(/^[A-Za-z\s.]+$/))
		{
    		$('#currencyNameSpan').html('currency Name should contain alphabets only..!');
			document.currencyform.currencyName.value="";
			document.currencyform.currencyName.focus();
			return false;
		}
}
</script>
    </body>
</html>