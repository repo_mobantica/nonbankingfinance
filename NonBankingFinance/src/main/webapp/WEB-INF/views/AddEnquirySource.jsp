<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Add Enquiry Source</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Enquiry Source</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="EnquirySourceMaster">Enquiry Source Master</a></li>
                                        <li class="breadcrumb-item active">Add Enquiry Source</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="enquirySourceform" action="${pageContext.request.contextPath}/AddEnquirySource" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="enquirySourceId">Enquiry Source Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="enquirySourceId" name="enquirySourceId"  value="${enquirySourceCode}" readonly>
                      </div>
					 </div>
					<!--  
					 </div>
					 
					 <div class="row">
	                  -->
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="enquirySourceName">Enquiry Source Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="enquirySourceName" name="enquirySourceName"  placeholder="EnquirySource Name" style="text-transform: capitalize;">
                      </div>
                       <span id="enquirySourceNameSpan" style="color:#FF0000"></span>
					 </div>
					 
                    </div>
                    
                  <br/>
	                 <div class="row">
					     
                      <div class="col-xl-3">
                      	<a href="EnquirySourceMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

<script>


function init()
{
	$('#enquirySourceNameSpan').html('');
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	document.enquirySourceform.enquirySourceName.focus();	
}

function validate()
{ 
	$('#enquirySourceNameSpan').html('');
	  if(document.enquirySourceform.enquirySourceName.value=="")
		{
		  $('#enquirySourceNameSpan').html('Please, enter EnquirySource name..!');
			document.enquirySourceform.enquirySourceName.focus();
			return false;
		}
		else if(document.enquirySourceform.enquirySourceName.value.match(/^[\s]+$/))
		{
			$('#enquirySourceNameSpan').html('Please, enter EnquirySource name..!');
			document.enquirySourceform.enquirySourceName.value="";
			document.enquirySourceform.enquirySourceName.focus();
			return false; 	
		}
		
}
</script>
    </body>
</html>