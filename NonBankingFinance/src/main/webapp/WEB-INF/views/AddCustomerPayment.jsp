<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Customer Payment</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Customer Payment</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="CustomerPaymentMaster">Customer Payment</a></li>
                                        <li class="breadcrumb-item active">Customer Payment</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="customerPaymentform" action="${pageContext.request.contextPath}/AddCustomerPayment" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
                
	                 <div class="row">
	                 
                      <div class="col-xl-12">
                		 <table class="table mb-0 table-sm">
                              <thead>
                                <tr>
                                  <th><th>
                                  <th>Customer Id</th>
                                  <td>${customerDetails.customerId}</td>
                                         
                                  <th><th>
                                  <th>Packet Number</th>
                                  <td>${customerLoanDetailsList.packetNumber}</td>
                                      
                                  <th><th>
                                  <th>Loan Date</th>
                                  <td>${loanDate}</td>
                                  
                                    <%--       
                                  <th><th>
                                  <th>Total Day's</th>
                                  <td>${totalDays}</td>
                                       --%>
                                </tr>
                                
                                <tr>
                                
                                  <th><th>
                                  <th>Customer Name</th>
                                  <td>${customerDetails.customerName}</td>
                                   
                                  <th><th>
                                  <th>Mobile No</th>
                                  <td>${customerDetails.mobileNumber} </td>
                                       
                                  <th><th>
                                  <th> <th>
                                  <td></td>
                                  
                                         
                                </tr>
                                
                                <tr>     
                                  <th><th>
                                  <th>Email Id</th>
                                  <td>${customerDetails.emailId} </td>
                                         
                                  <th><th>
                                  <th>Pan Card No.</th>
                                  <td>${customerDetails.customerPancardno} </td>
                                         
                                  <th><th>
                                  <th>Aadhar card No.</th>
                                  <td>${customerDetails.customerAadharcardno} </td>
                               
                                          
                                </tr>
                                  <tr>
                                         
                                  <td><td>
                                  <td> <td>
                                  <td></td>
                                      
                                  <td><td>
                                  <td> <td>
                                  <td></td>
                                    
                                  <td><td>
                                  <td></td>
                                  <td> <td>
                                  <td></td>
                                
                                  </tr>
                                          
                            </thead>      
                           </table>       
                                  <br/>
                                 
                              <table class="table mb-0 table-sm">
                              <thead>   
                                <tr>
                                  <th><th>
                                  <th>Loan Amount</th>
                                  <td>${customerLoanDetailsList.loanAmount} </td>
                                 
                                  <th><th>
                                  <th>Scheme Name</th>
                                  <td>${goldloanschemeName} </td>
                               
                                  <th><th>
                                  <th>Interest Rate</th>
                                  <td>${interestPer} </td>
                     
                                </tr>
                      
                                <tr>
                                  <th><th>
                                  <th>Paid Principal Amt</th>
                                  <td>${customerPaidPrincipalAmount} </td>
                                 
                               
                                  <th><th>
                                  <th>Remaining Principal</th>
                                  <td>${remainingPrincipal} </td>
                     
                                  <th><th>
                                  <th>Interest</th>
                                  <td>${interestAmount} </td>
                              
                                  <th><th>
                                  <th>Notice Charge </th>
                                  <td>${noticeCharge} </td>
                               
                                  <th><th>
                                  <th>Total </th>
                                  <td>${totalRemainingAmount} </td>
                               
                                </tr>
                    
                                <tr>
                                      
                                  <td><td>
                                  <td></td>
                                  <td></td>
                                     
                                  <td><td>
                                  <td> <td>
                                  <td></td>
                                     
                                  <td><td>
                                  <td></td>
                                  <td></td>
                                      
                                  <td><td>
                                  <td></td>
                                  <td></td>
                                      
                                  <td><td>
                                  <td></td>
                                  <td></td>
                                
                                </tr>
                   
                          
                              </thead>
                              <tbody>
                              
                              </tbody>
                          </table> 
                      </div>
	                 
	                 </div>
	                 
	        <br/>         
	                 
	      <div class="row">
	      
				 <div class="col-xl-12">
	                 <div class="row">
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="paymentAmount">Add Payment<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="paymentAmount" name="paymentAmount" placeholder="Add Payment" >
                      </div>
                      <span id="paymentAmountSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="paymentMode">Payment Type<span class="text-danger">*</span></label>
	                  	<select class="form-control" id="paymentMode" name="paymentMode">
					  	<option  selected="selected" value="Cash">Cash</option>
					  	<option  value="Cheque">Cheque</option>
					  	<option value="DD">DD</option>
					  	<option value="RTGS">RTGS</option>	
					  	<option value="NEFT">NEFT</option>
					  	<option value="IMPS">IMPS</option>	
	                  	</select>
                      </div>
                       <span id="paymentModeSpan" style="color:#FF0000"></span>
					 </div>
					 
					 <!-- 
                      <div class="col-xl-1">
                      <div class="form-group">
                      		<label for="paymentDue">Due<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="paymentDue" name="paymentDue" value="0" >
                      </div>
                      <span id="paymentDueSpan" style="color:#FF0000"></span>
					 </div>
					 -->
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="bankName">Bank Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="bankName" name="bankName">
                      </div>
                      <span id="bankNameSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="branchName">Branch Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="branchName" name="branchName" >
                      </div>
                      <span id="branchNameSpan" style="color:#FF0000"></span>
					 </div>
					
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="refNumber">REF No.<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="refNumber" name="refNumber" >
                      </div>
                      <span id="refNumberSpan" style="color:#FF0000"></span>
					 </div>
					
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="narration">Narration<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="narration" name="narration" >
                      </div>
                      <span id="narrationSpan" style="color:#FF0000"></span>
					 </div>
					
					 
                     </div>
               
                 </div>
                 
                 
	       <div class="col-xl-10">
	       <h5>Paid History</h5>
                <table class="table table-striped table-bordered table mb-0 table-sm">
                  <thead>
                   <tr>
                    <th>#</th>
                    <th>Principal</th>
                    <th>Interest</th>
                    <th>Date</th>
                    <th>Narration</th>
                    <th>Notice Charge</th>
                   </tr>
                 </thead>
                       
                    <tbody>
					<c:forEach items="${paymentcustomerDetails}" var="paymentcustomerDetails" varStatus="loopStatus">
					    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
						  <td>${loopStatus.index+1}</td>
						  <td>${paymentcustomerDetails.principalAmount}</td>
						  <td>${paymentcustomerDetails.interestAmount} </td>
						  <td>${paymentcustomerDetails.paidDate} </td>
						  <td>${paymentcustomerDetails.narration} </td>
						  <td>${paymentcustomerDetails.noticeCharge} </td>
                        </tr>
			 	    </c:forEach>
                    </tbody>       
               </table> 
	       </div>
	      
	           
	       <div class="col-xl-12">
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="CustomerPaymentMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
                 </div>   
               </div>   
                    
                    
                <input type="hidden" id="packetNumber" name="packetNumber"  value="${customerLoanDetailsList.packetNumber}">	
                <input type="hidden" id="interestAmount" name="interestAmount"  value="${interestAmount}">	
                <input type="hidden" id="noticeCharge" name="noticeCharge"  value="${noticeCharge}">	
                <input type="hidden" id="customerId" name="customerId"  value="${customerDetails.customerId}">	
               	
				 <input type="hidden" id="userId" name="userId"  value="<%= session.getAttribute("employeeId") %>">	
                 <input type="hidden" id="branchId" name="branchId"  value="${customerDetails.branchId}">	    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>

function clearAll()
{
	$('#paymentAmountSpan').html('');
	$('#paymentDueSpan').html('');
	$('#bankNameSpan').html('');
	$('#branchNameSpan').html('');
	$('#refNumberSpan').html('');
	$('#narrationSpan').html('');
	$('#paymentModeSpan').html('');
}

function init()
{
	clearAll();
	
	document.customerPaymentform.paymentAmount.focus();	
	
}

function validate()
{
	
	clearAll();
 
	
	 //  validation for paymentAmount
	  if(document.customerPaymentform.paymentAmount.value=="")
		{
		  $('#paymentAmountSpan').html('Please, Enter payment Amount..!');
			document.customerPaymentform.paymentAmount.focus();
			return false;
		}
		else if(document.customerPaymentform.paymentAmount.value.match(/^[\s]+$/))
		{
			$('#paymentAmountSpan').html('Please, Enter payment Amount..!');
			document.customerPaymentform.loanAmount.value="";
			document.customerPaymentform.loanAmount.focus();
			return false; 	
		}
		else if(!document.customerPaymentform.paymentAmount.value.match(/^[0-9]+$/))
		 {
			$('#paymentAmountSpan').html('Please, use only digit value for payment Amount..!');
			document.customerPaymentform.paymentAmount.value="";
			document.customerPaymentform.paymentAmount.focus();
		    return false;
		 }

		 var interestAmount = Number($('#interestAmount').val());
		 var paymentAmount = Number($('#paymentAmount').val());
		if(interestAmount>paymentAmount)
		 {

			$('#paymentAmountSpan').html('Please, enter at least interest amt..!');
			document.customerPaymentform.paymentAmount.value="";
			document.customerPaymentform.paymentAmount.focus();
		    return false;
		 }
	 

	  if(document.customerPaymentform.paymentDue.value=="")
		{
		  $('#paymentDueSpan').html('Please, Enter payment Amount..!');
			document.customerPaymentform.paymentDue.focus();
			return false;
		}
		else if(document.customerPaymentform.paymentDue.value.match(/^[\s]+$/))
		{
			$('#paymentDueSpan').html('Please, Enter payment Amount..!');
			document.customerPaymentform.paymentDue.value="";
			document.customerPaymentform.paymentDue.focus();
			return false; 	
		}
		else if(!document.customerPaymentform.paymentDue.value.match(/^[0-9]+$/))
		 {
			$('#paymentDueSpan').html('Please, use only digit value for payment Amount..!');
			document.customerPaymentform.paymentDue.value="";
			document.customerPaymentform.paymentDue.focus();
		    return false;
		 }

	  
		if(document.customerPaymentform.paymentMode.value=="Default")
		{
			$('#paymentModeSpan').html('Please, select payment type..!');
			document.customerPaymentform.paymentMode.focus();
			return false;
		}
		

		if(document.customerPaymentform.paymentMode.value == "Cheque" || document.customerPaymentform.paymentMode.value == "DD" || document.customerPaymentform.paymentMode.value == "RTGS"  || document.customerPaymentform.paymentMode.value == "NEFT" || document.customerPaymentform.paymentMode.value == "IMPS" )
		{
			if(document.customerPaymentform.bankName.value == "")
			{
				$('#bankNameSpan').html('Please, enter bank name..!');
				document.customerPaymentform.bankName.focus();
				return false;
			}
			else if(document.customerPaymentform.bankName.value.match(/^[\s]+$/))
			{
				$('#bankNameSpan').html('Bank name should not be empty..!');
				document.customerPaymentform.bankName.focus();
				document.customerPaymentform.bankName.value="";
				return false;
			}
			
			if(document.customerPaymentform.branchName.value == "")
			{
				$('#branchNameSpan').html('Please, enter branch name..!');
				document.customerPaymentform.branchName.focus();
				return false;
			}
			else if(document.customerPaymentform.branchName.value.match(/^[\s]+$/))
			{
				$('#branchNameSpan').html('Branch name should not be empty..!');
				document.customerPaymentform.branchName.focus();
				document.customerPaymentform.branchName.value="";
				return false;
			}
		
			if(document.customerPaymentform.refNumber.value == "")
			{
				$('#refNumberSpan').html('Please, enter Ref Number..!');
				document.customerPaymentform.refNumber.focus();
				return false;
			}

			if(document.customerPaymentform.narration.value == "")
			{
				$('#narrationSpan').html('Please, enter narration..!');
				document.customerPaymentform.narration.focus();
				return false;
			}
		 }
		
	
}
</script>
</body>
</html>