<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Customer Notice</title>

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Multi Item Selection examples -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
        <!-- Begin page -->
        <div id="wrapper">

       
  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Customer Notice</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item active">Customer Notice</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

				<form name="noticeform"
						action="${pageContext.request.contextPath}/NoticeChargeReport"
						onSubmit="return validate()" method="post">
                        <div class="row">
                            <div class="col-12">
                            
                            <div class="card-box">
                
					                 <div class="row">
									     
				                
										<div class="col-xl-2">
											<div class="form-group">
												<label for="noticeDays">Notice Number</label> <select class="form-control"
													name="noticeDays" id="noticeDays">
																						
														 <c:choose>
													  		<c:when test="${noticeDays eq '90'}">
									                    		<option selected="selected" value="90">3 Months</option>
									                    	</c:when>
									                    	<c:otherwise>
									                    	    <option value="90">3 Months</option>
									                    	</c:otherwise>
									                     </c:choose>
									                     						
														 <c:choose>
													  		<c:when test="${noticeDays eq '180'}">
									                    		<option selected="selected" value="180">6 Months</option>
									                    	</c:when>
									                    	<c:otherwise>
									                    	    <option value="180">6 Months</option>
									                    	</c:otherwise>
									                     </c:choose>
									                     						
														 <c:choose>
													  		<c:when test="${noticeDays eq '270'}">
									                    		<option selected="selected" value="270">9 Months</option>
									                    	</c:when>
									                    	<c:otherwise>
									                    	    <option value="270">9 Months</option>
									                    	</c:otherwise>
									                     </c:choose>
									                     					
														 <c:choose>
													  		<c:when test="${noticeDays eq '360'}">
									                    		<option selected="selected" value="365">12 Months</option>
									                    	</c:when>
									                    	<c:otherwise>
									                    	    <option value="360">12 Months</option>
									                    	</c:otherwise>
									                     </c:choose>
												</select>
											</div>
											<span id="noticeDaysSpan" style="color: #FF0000"></span>
										</div>

									 <div class="col-xl-2">
									 
											<div class="form-group">
												<br/>
											<button class="btn btn-primary" type="submit" ><i class="ti-search"></i> Search</button>
											</div>
									</div>
										
										<div class="col-xs-6">
						            	</div>
										<div class="col-xs-2">
			              	
											<div class="form-group">
												<br/>
									        <button type="button" class="btn btn-success"  onclick="return SendCustomerNotice()">Send Notice</button>
						            	    </div>
						            	</div>
				                    </div>
				        	
				                    
                            
                                <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title">Customer Notice List</h4>

                                    <table id="customerNameList" class="table table-striped table-bordered table mb-0 table-sm" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Customer Id</th>
                                            <th>Packet Number</th>
                                            <th>Customer Name</th>
                                            <th>Loan Amount</th>
                                            <th>Paid Principal AMT</th>
                                            <th>Remaining Principal</th>
                                            <th>Interest %</th>
                                            <th>Interest AMT</th>
                                            <th>Notice Charge</th>
                                            <th>Total AMT</th>
                                            <!-- 
                    						<th style="width:50px">Action</th>
                    						 -->
                                        </tr>
                                        </thead>

                                        <tbody>
                                  
						                  <c:forEach items="${noticeChargeReportDTOList}" var="noticeChargeReportDTOList" varStatus="loopStatus">
						                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
						                        <td>${noticeChargeReportDTOList.customerId} </td>
						                        <td>${noticeChargeReportDTOList.packetNumber} </td>
						                        <td>${noticeChargeReportDTOList.customerName} </td>
						                        <td>${noticeChargeReportDTOList.loanAmount} </td>
						                        <td>${noticeChargeReportDTOList.totalPaidPrincipalAmount} </td>
						                        <td>${noticeChargeReportDTOList.remainingPrincipal} </td>
						                        <td>${noticeChargeReportDTOList.interestPer} </td>
						                        <td>${noticeChargeReportDTOList.interestAmount} </td>
						                        <td>${noticeChargeReportDTOList.noticeCharge} </td>
						                        <td>${noticeChargeReportDTOList.totalRemainingAmount} </td>
						                       <%-- 
						                        <td> <a href="${pageContext.request.contextPath}/EditCustomer?customerId=${noticeChargeReportDTOList.customerId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></a></td>
						                        --%> 
						                      </tr>
										   </c:forEach>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                       </form>                       
                    </div> <!-- container -->

                </div> <!-- content -->

            </div>
                  
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

        </div>
        <!-- END wrapper -->


        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/jszip.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#customerNameList').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#customerNameList_wrapper .col-md-6:eq(0)');
            } );

        </script>

 <script>
 
	function SendCustomerNotice()
	{

		var noticeDays = $('#noticeDays').val();
		
		$.ajax({

			url : '${pageContext.request.contextPath}/SendCustomerNotice',
			type : 'Post',
			data : { noticeDays : noticeDays},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
								
								
							} 
							else
							{
								alert("failure111");
							}
							
						}
			});
		
	}

</script>
    </body>
</html>