<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Employee Master</title>

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Multi Item Selection examples -->
        <link href="${pageContext.request.contextPath}/resources/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
        <!-- Begin page -->
        <div id="wrapper">

       
  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Employee Master</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item active">Employee Master</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12">
                            
                            <div class="card-box">
                
				                 <div class="row">
				                 
			                      <div class="col-xl-2">
			                      <div class="form-group">
			                      <label for="departmentId">Department Name<span class="text-danger">*</span></label>
			                          <select class="form-control" name="departmentId"  id="departmentId" onchange="getDesignationList(this.value)">
									  <option selected="selected" value="Default">-Select Department-</option>
										  <c:forEach var="departmentList" items="${departmentList}">
						                    <option value="${departmentList.departmentId}">${departmentList.departmentName}</option>
						                  </c:forEach>
					                   </select>
			                      </div>
								 </div>
								 
			                      <div class="col-xl-2">
			                      <div class="form-group">
			                      <label for="departmentId">Designation Name<span class="text-danger">*</span></label>
			                          <select class="form-control" name="designationId"  id="designationId" onchange="getDesignationWiseEmployeeList(this.value)">
									  <option selected="selected" value="Default">-Select Designation-</option>
					                   </select>
			                      </div>
			                       
			                       </div>
										 
							  <div class="col-xl-1">
		                      <div class="form-group">
		                      
			                      <label></label>
			                      <div class="input-group">
		                      		<a href="EmployeeMaster"><button type="button" class="btn btn-default" value="reset">Reset</button></a>
			                       </div><!-- input-group -->
		                      </div>
							 </div>
							
							  <div class="col-xl-1">
							  
							  </div>
								
			                      <div class="col-xl-2">
			                      <br/>
									  <a href="AddEmployee"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Employee</button></a>
								 </div>
								    
			                      <div class="col-xl-2">
			                      <br/>
			                       <a href="ImportNewEmployee">  <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"> Import Excel File</button></a>
				                </div>
				                
			                    </div>
				                    
                            
                                <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title">Employee Table</h4>

                                    <table id="employeeNameList" class="table table-striped table-bordered table mb-0 table-sm" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Employee Name</th>
                                            <th>Department Name</th>
                                            <th>Designation Name</th>
                                            <th>Mobile No</th>
                                            <th>Email Id</th>
                    						<th style="width:50px">Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                  
						                  <c:forEach items="${employeeList}" var="employeeList" varStatus="loopStatus">
						                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
						                        <td>${employeeList.employeefirstName} ${employeeList.employeemiddleName} ${employeeList.employeelastName}</td>
						                        <td>${employeeList.departmentId}</td>
						                        <td>${employeeList.designationId}</td>
						                        <td>${employeeList.employeeMobileno}</td>
						                        <td>${employeeList.employeeEmailId}</td>
						                        <td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId=${employeeList.employeeId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></a></td>
						                      </tr>
										   </c:forEach>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                                              
                    </div> <!-- container -->

                </div> <!-- content -->



            </div>
                  
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

        </div>
        <!-- END wrapper -->


        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/jszip.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#employeeNameList').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#employeeNameList_wrapper .col-md-6:eq(0)');
            } );

        </script>



 <script>
 
function getDesignationList()
{
	
	 $("#designationId").empty();
	 var departmentId = $('#departmentId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getDesignationList',
		type : 'Post',
		data : { departmentId : departmentId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Designation-");
							$("#designationId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].designationId).text(result[i].designationName);
							    $("#designationId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
   

	 $('#employeeNameList tr').detach();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getDepartmentWiseEmployeeList',
		type : 'Post',
		data : { departmentId : departmentId},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#employeeNameList').append('<tr><th>Employee Name</th><th>Department Name</th><th >Designation Name</th><th>Mobile No</th><th>Email Id</th><th style="width:50px">Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
									var id = result[i].employeeId;
									$('#employeeNameList').append('<tr><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].departmentId+'</td><td>'+result[i].designationId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeEmailId+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></a></td>');
								
							 } 

							 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
}

function getDesignationWiseEmployeeList()
{
	
	 var departmentId = $('#departmentId').val();
	 var designationId = $('#designationId').val();
	 
	 $('#employeeNameList tr').detach();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getDesignationWiseEmployeeList',
		type : 'Post',
		data : { departmentId : departmentId, designationId : designationId},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#employeeNameList').append('<tr><th>Employee Name</th><th>Department Name</th><th >Designation Name</th><th>Mobile No</th><th>Email Id</th><th style="width:50px">Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
									var id = result[i].employeeId;
									$('#employeeNameList').append('<tr><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].departmentId+'</td><td>'+result[i].designationId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeEmailId+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></a></td>');
								
							 } 
							 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
}

  </script>
    </body>
</html>