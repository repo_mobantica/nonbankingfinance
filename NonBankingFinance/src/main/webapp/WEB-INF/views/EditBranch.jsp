<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Edit Branch</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Edit Branch</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="BranchMaster">Branch Master</a></li>
                                        <li class="breadcrumb-item active">Edit Branch</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="branchform" action="${pageContext.request.contextPath}/EditBranch" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="branchId">Branch Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="branchId" name="branchId" value="${branchDetails[0].branchId}" readonly>
                      </div>
					 </div>
				 
					 </div>
					 	 
					 <div class="row">
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="branchName">Branch Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="branchName" name="branchName" placeholder="Branch Name" value="${branchDetails[0].branchName}" style="text-transform: capitalize;">
                      </div>
                      <span id="branchNameSpan" style="color:#FF0000"></span>
					 </div>
					
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="countryId">Select Company Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="companyId" id="companyId">
		                  <option selected="selected" value="${branchDetails[0].companyId}">${companyName}</option>
		                    <c:forEach var="companyList" items="${companyList}">
		                     <c:choose>
		                      <c:when test="${branchDetails[0].companyId ne companyList.companyId}">
			                    <option value="${companyList.companyId}">${companyList.companyName}</option>
			                  </c:when>
			                 </c:choose>
			                 </c:forEach>
		                  </select>
                      </div>
                        <span id="companyIdSpan" style="color:#FF0000"></span>
					 </div>
				
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="branchAddress">Address<span class="text-danger">*</span></label>
                           	<textarea class="form-control" id="branchAddress" name="branchAddress" rows="1">${branchDetails[0].branchAddress}</textarea>
                      </div>
                      <span id="companyAddressSpan" style="color:#FF0000"></span>
					 </div>
					
                      
				
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="pinCode">Pin Code<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="pinCode" name="pinCode" placeholder="Pin Code" value="${branchDetails[0].pinCode}">
                      </div>
                      <span id="pinCodeSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="phoneNumber">Phone<span class="text-danger">*</span></label>
                           	<input type="text" id="phoneNumber" name="phoneNumber" placeholder="" data-mask="(999) 999-99999" class="form-control"value="${branchDetails[0].phoneNumber}">
                      </div>
                      <span id="phoneNumberSpan" style="color:#FF0000"></span>
					 </div>
					 
					
					 </div>
					
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="BranchMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="${branchDetails[0].branchId}">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>
function getpinCode()
{

	 $("#pinCode").empty();
	 var locationareaId = $('#locationareaId').val();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								 $('#pinCode').val(result[i].pinCode);
								
							 } 
						
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}


function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get locationarea List

function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get City List

function init()
{
	clearAll();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.branchform.branchName.focus();	
}

function clearAll()
{

	$('#branchNameSpan').html('');
	$('#companyIdSpan').html('')

	$('#branchAddressSpan').html('');
	$('#pinCodeSpan').html('');
	$('#phoneNumberSpan').html('');
	
}

function validate()
{
	clearAll();

	  if(document.branchform.branchName.value=="")
		{
		  $('#branchNameSpan').html('Please, enter branch name..!');
			document.branchform.branchName.focus();
			return false;
		}
		else if(document.branchform.branchName.value.match(/^[\s]+$/))
		{
			$('#branchNameSpan').html('Please, enter branch name..!');
			document.branchform.branchName.value="";
			document.branchform.branchName.focus();
			return false; 	
		}
	
	
	if(document.branchform.companyId.value=="Default")
	{
		$('#companyIdSpan').html('Please, select company Name..!');
		document.branchform.companyId.focus();
		return false;
	}


	  if(document.branchform.branchAddress.value=="")
		{
		  $('#branchAddressSpan').html('Please, enter branch address..!');
			document.branchform.branchAddress.focus();
			return false;
		}
		else if(document.branchform.branchAddress.value.match(/^[\s]+$/))
		{
			$('#branchAddressSpan').html('Please, enter branch address..!');
			document.branchform.branchAddress.value="";
			document.branchform.branchAddress.focus();
			return false; 	
		}
	
	  
    //validation for branch office phone no
	if(document.branchform.phoneNumber.value=="")
	{
		$('#phoneNumberSpan').html('Please, enter branch phone number..!');
		document.branchform.phoneNumber.focus();
		return false;
	}
	else if(!document.branchform.phoneNumber.value.match(/^\(?([0-9]{3})\)?[\s. ]?([0-9]{3})[-. ]?([0-9]{5})$/))
	{
		$('#phoneNumberSpan').html(' phone number must be 10 digit numbers only..!');
		document.branchform.phoneNumber.value="";
		document.branchform.phoneNumber.focus();
		return false;
	}
	
}
</script>
    </body>
</html>