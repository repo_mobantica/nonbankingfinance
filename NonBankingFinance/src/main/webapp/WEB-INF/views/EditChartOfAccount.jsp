<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Edit Chart Of Account</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Edit Chart Of Account</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="ChartOfAccountMaster">Chart Of Account Master</a></li>
                                        <li class="breadcrumb-item active">Edit Chart Of Account</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>


<form  name="chartOfAccountform" action="${pageContext.request.contextPath}/EditChartOfAccount" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="chartOfAccountId">Chart Of Account Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="chartOfAccountId" name="chartOfAccountId" value="${chartOfAccountDetails[0].chartOfAccountId}" readonly>
                      </div>
					 </div>
					  
					 </div>
					 
					 <div class="row">
	                  
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="chartOfAccountName">Chart Of Account Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="chartOfAccountName" name="chartOfAccountName" value="${chartOfAccountDetails[0].chartOfAccountName}" style="text-transform: capitalize;">
                      </div>
                       <span id="chartOfAccountNameSpan" style="color:#FF0000"></span>
                      
					 </div>
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="chartOfAccountNumber">Chart Of Account Number<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="chartOfAccountNumber" name="chartOfAccountNumber"  placeholder="COA Number"  value="${chartOfAccountDetails[0].chartOfAccountNumber}"style="text-transform: capitalize;">
                      </div>
                       <span id="chartOfAccountNumberSpan" style="color:#FF0000"></span>
					 </div>
					 
                    </div>
                    
                  <br/>
	                 <div class="row">
					     
                      <div class="col-xl-3">
                      	<a href="ChartOfAccountMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="${chartOfAccountDetails[0].creationDate}">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

<script>
function init()
{
	$('#chartOfAccountNameSpan').html('');
	$('#chartOfAccountNumberSpan').html('');
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	//document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.chartOfAccountform.chartOfAccountName.focus();	
}

function validate()
{
	$('#chartOfAccountNameSpan').html('');
	$('#chartOfAccountNumberSpan').html('');
	  if(document.chartOfAccountform.chartOfAccountName.value=="")
		{
		  $('#chartOfAccountNameSpan').html('Please, enter chartOfAccount name..!');
			document.chartOfAccountform.chartOfAccountName.focus();
			return false;
		}
		else if(document.chartOfAccountform.chartOfAccountName.value.match(/^[\s]+$/))
		{
			$('#chartOfAccountNameSpan').html('Please, enter chartOfAccount name..!');
			document.chartOfAccountform.chartOfAccountName.value="";
			document.chartOfAccountform.chartOfAccountName.focus();
			return false; 	
		}

	  if(document.chartOfAccountform.chartOfAccountNumber.value=="")
		{
		  $('#chartOfAccountNumberSpan').html('Please, enter chart Of Account Number..!');
			document.chartOfAccountform.chartOfAccountNumber.focus();
			return false;
		}
		else if(document.chartOfAccountform.chartOfAccountNumber.value.match(/^[\s]+$/))
		{
			$('#chartOfAccountNumberSpan').html('Please, enter chart Of Account Number..!');
			document.chartOfAccountform.chartOfAccountNumber.value="";
			document.chartOfAccountform.chartOfAccountNumber.focus();
			return false; 	
		}
		else if(!document.chartOfAccountform.chartOfAccountNumber.value.match(/^[0-9]+$/))
			 {
				$('#chartOfAccountNumberSpan').html('Please, use only digit value for chart Of Account Number..! eg: 30');
				document.chartOfAccountform.chartOfAccountNumber.value="";
				document.chartOfAccountform.chartOfAccountNumber.focus();
				return false;
			}
}
</script>
    </body>
</html>