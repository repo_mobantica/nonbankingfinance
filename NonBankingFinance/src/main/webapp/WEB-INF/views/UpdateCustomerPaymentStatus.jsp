<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Customer Payment Status</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


        <link href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/mjolnic-bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Customer Payment Status</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="CustomerPaymentMaster">Customer Payment Status</a></li>
                                        <li class="breadcrumb-item active">Customer Payment Status</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="customerPaymentform" action="${pageContext.request.contextPath}/UpdateCustomerPaymentStatus" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
                
	                 <div class="row">
	                 
                      <div class="col-xl-12">
                		 <table class="table mb-0 table-sm">
                              <thead>
                                <tr>
                                
                                  <th><th>
                                  <th>Receipt Id :</th>
                                  <td>${paymentcustomerDetails[0].customerPaymentId}</td>
                                
                                  <th><th>
                                  <th>Packet Number :</th>
                                  <td>${paymentcustomerDetails[0].packetNumber}</td>
                                
                                  <th><th>
                                  <th>Customer Id :</th>
                                  <td>${customerDetails[0].customerId}</td>
                                 
                                  <th><th>
                                  <th>Loan Date :</th>
                                  <td>${customerLoanDate}</td>
                                
                                  <th><th>
                                  <th> <th>
                                  <td></td>
                                  
                                </tr>
                                
                                <tr>
                                
                                  <th><th>
                                  <th>Customer Name :</th>
                                  <td>${customerDetails[0].cutsomerFirstName} ${customerDetails[0].customerMiddlelName} ${customerDetails[0].customerLastName}</td>
                                   
                                  <th><th>
                                  <th>Mobile No :</th>
                                  <td>${customerDetails[0].mobileNumber} </td>
                                   
                                  <th><th>
                                  <th>Email Id :</th>
                                  <td>${customerDetails[0].emailId} </td>
                                     
                                  <td><td>
                                  <td><td>
                                  <td></td>
                                  
                                </tr>
                                
                            </thead>      
                           </table>       
                               
                      </div>
                      
	                
	                 <div class="col-xl-12">
	                  <br/>  <br/>
	                  <h5>Payment Details</h5>
                		 <table class="table mb-0 table-sm">
                              <thead>
                              
                                <tr>
                                
                                  <th><th>
                                  <th>Total Amt :</th>
                                  <th>${totalAmount}</th>
                                 
                                  <th><th>
                                  <th>Principal Amt :</th>
                                  <th>${paymentcustomerDetails[0].principalAmount}</th>
                                
                                  <th><th>
                                  <th>Interest Amt</th>
                                  <th>${paymentcustomerDetails[0].interestAmount}</th>
                                
                                  <th></th>
                                  <th>Due Amt :<th>
                                  <th>${paymentcustomerDetails[0].paymentDue}<th>
                                   
                                  <th><th>
                                  <th> <th>
                                  <th></th>
                                </tr>
                     
                                <tr>
                                
                                  <th><th>
                                  <th>Payment Type :</th>
                                  <th>${paymentcustomerDetails[0].paymentMode}</th>
                                
                                  <th><th>
                                  <th>Bank Name :</th>
                                  <th>${paymentcustomerDetails[0].bankName}</th>
                                 
                                  <th><th>
                                  <th>Branch Name :</th>
                                  <th>${paymentcustomerDetails[0].branchName}</th>
                                
                                  <th></th>
                                  <th>REF No :<th>
                                  <th> ${paymentcustomerDetails[0].refNumber}<th>
                                
                                 
                                  <th></th>
                                  <th>Narration :<th>
                                  <th>${paymentcustomerDetails[0].narration} <th>
                                   
                                </tr>
                     
                                
                            </thead>      
                           </table>       
                               
                      </div>
	                 
	                 </div>
	                 
	        <br/>         
	                 
	      <div class="row">

				 <div class="col-xl-12">
	                 <div class="row">
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="companyBankId">Bank A/C No<span class="text-danger">*</span></label>
                          <select class="form-control" name="companyBankId"  id="companyBankId" onchange="getBankWiseBranchName(this.value)">
						  <option selected="" value="Default">-Select Bank A/C No-</option>
							  <c:forEach var="CompanybankList" items="${CompanybankList}">
			                    <option value="${CompanybankList.companyBankId}">${CompanybankList.bankName}-${CompanybankList.bankAccountNumber}</option>
			                  </c:forEach>
		                   </select>
                      </div>
                       <span id="companyBankIdSpan" style="color:#FF0000"></span>
					 </div>
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="branchName">Branch Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="branchName" name="branchName"  readonly="readonly" >
                      </div>
                      <span id="branchNameSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="paymentStatus">Payment Status<span class="text-danger">*</span></label>
	                  	<select class="form-control" id="paymentStatus" name="paymentStatus">
					  	<option selected="selected" value="Default">-Select Status-</option>
					  	<option value="Cleared">Cleared</option>
					  	<option  value="Canceled">Canceled</option>
	                  	</select>
                      </div>
                       <span id="paymentStatusSpan" style="color:#FF0000"></span>
					 </div>
					 
					  <div class="col-xl-2">
                      <div class="form-group">
	                      <label>Clearance Date</label>
	                      <div class="input-group">
		                      <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="clearanceDate">
		                      <div class="input-group-append">
		                      <span class="input-group-text"><i class="icon-calender"></i></span>
	                      </div>
	                      </div>
                      </div>

                      <span id="clearanceDateSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="remark">Remark<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="remark" name="remark" >
                      </div>
                      <span id="remarkSpan" style="color:#FF0000"></span>
					 </div>
					
					 
                     </div>
                    
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="CustomerPaymentClearanceMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
                 </div>
               </div>   
           
          		  <input type="hidden" id="totalAmount" name="totalAmount"  value="${totalAmount}">
				 <input type="hidden" id="packetNumber" name="packetNumber"  value="${paymentcustomerDetails[0].packetNumber}">
				 <input type="hidden" id="customerPaymentId" name="customerPaymentId"  value="${paymentcustomerDetails[0].customerPaymentId}">	
                 <input type="hidden" id="customerId" name="customerId"  value="${customerDetails[0].customerId}">	
                 
				 <input type="hidden" id="userId" name="userId"  value="<%= session.getAttribute("employeeId") %>">	
                 <input type="hidden" id="branchId" name="branchId"  value="${customerDetails[0].branchId}">	    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/moment/moment.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/mjolnic-bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/clockpicker/bootstrap-clockpicker.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

        <script src="${pageContext.request.contextPath}/resources/pages/jquery.form-pickers.init.js"></script>


<script>

function getBankWiseBranchName()
{
	
	 $("#branchName").empty();
	 var companyBankId = $('#companyBankId').val();

		$.ajax({

			url : '${pageContext.request.contextPath}/getCompanyBankDetailsByBankIdWise',
			type : 'Post',
			data : { companyBankId : companyBankId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								for(var i=0;i<result.length;i++)
								{
									document.customerPaymentform.branchName.value=result[i].bankBranchName;
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
	 
}

function clearAll()
{
	$('#companyBankIdSpan').html('');
	$('#paymentStatusSpan').html('');
	$('#clearanceDateSpan').html('');
	$('#remarkSpan').html('');
}

function init()
{
	clearAll();
	
	document.customerPaymentform.companyBankId.focus();	
	
}

function validate()
{
	
	clearAll();

	if(document.customerPaymentform.companyBankId.value=="Default")
	{
		$('#companyBankIdSpan').html('Please, select bank A/C No..!');
		document.customerPaymentform.companyBankId.focus();
		return false;
	}

	if(document.customerPaymentform.paymentStatus.value=="Default")
	{
		$('#paymentStatusSpan').html('Please, select payment status..!');
		document.customerPaymentform.paymentStatus.focus();
		return false;
	}
	
	
	  if(document.customerPaymentform.clearanceDate.value=="")
		{
		  $('#clearanceDateSpan').html('Please, Enter clearance Date..!');
			document.customerPaymentform.clearanceDate.focus();
			return false;
		}
	 
	  if(document.customerPaymentform.remark.value=="")
		{
		  $('#remarkSpan').html('Please, Enter remark..!');
			document.customerPaymentform.remark.focus();
			return false;
		}
	 
}
</script>
</body>
</html>