<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Add Company</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Company</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="CompanyMaster">Company Master</a></li>
                                        <li class="breadcrumb-item active">Add Company</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="companyform" action="${pageContext.request.contextPath}/AddCompany" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="companyId">Company Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="companyId" name="companyId"  value="${companyCode}" readonly>
                      </div>
					 </div>
				 
					 </div>
					 	 
					 <div class="row">
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="companyName">Company Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="companyName" name="companyName" placeholder="Company Name" style="text-transform: capitalize;">
                      </div>
                      <span id="companyNameSpan" style="color:#FF0000"></span>
					 </div>
					
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="countryId">Select Company Type<span class="text-danger">*</span></label>
                          <select class="form-control" name="companyType" id="companyType">
		                    <option value="Private">Private</option>
		                    <option value="Public">Public</option>
		                    <option value="Proprietary">Proprietary</option>
		                  </select>
                      </div>
                        <span id="companyTypeSpan" style="color:#FF0000"></span>
					 </div>
					
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="companyPanno">PAN Number<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="companyPanno" name="companyPanno" placeholder="PAN Number" style="text-transform:uppercase">
                      </div>
                      <span id="companyPannoSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="companyRegistrationno">Registration No</label>
                           	<input class="form-control" type="text" id="companyRegistrationno" name="companyRegistrationno" placeholder="Company Registration No" style="text-transform: uppercase;">
                      </div>
                      <span id="companyRegistrationnoSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="companyTanno">TAN Number </label>
                           	<input class="form-control" type="text" id="companyTanno" name="companyTanno" placeholder="TAN Number " style="text-transform: uppercase;">
                      </div>
                      <span id="companyTannoSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="companyPfno">Company PF A/c No</label>
                           	<input class="form-control" type="text" id="companyPfno" name="companyPfno" placeholder="Company PF A/c No" style="text-transform: uppercase;">
                      </div>
                      <span id="companyPfnoSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="companyGstno">GST No<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="companyGstno" name="companyGstno" placeholder="GST No" style="text-transform: uppercase;">
                      </div>
                      <span id="companyGstnoSpan" style="color:#FF0000"></span>
					 </div>
					
					<!-- 
                    </div>
                    
					 <div class="row">
	                 -->
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="companyAddress">Address<span class="text-danger">*</span></label>
                           	<textarea class="form-control" id="companyAddress" name="companyAddress" rows="1"></textarea>
                      </div>
                      <span id="companyAddressSpan" style="color:#FF0000"></span>
					 </div>
					
                      
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="pinCode">Pin Code<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="pinCode" name="pinCode" placeholder="Pin Code" >
                      </div>
                      <span id="pinCodeSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="phoneNumber">Phone<span class="text-danger">*</span></label>
                           	<input type="text" id="phoneNumber" name="phoneNumber" placeholder="" data-mask="(999) 999-99999" class="form-control">
                      </div>
                      <span id="phoneNumberSpan" style="color:#FF0000"></span>
					 </div>
					 
					 </div>
					 
				 <div class="row">
				 
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="bankId">Bank Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="bankId"  id="bankId" onchange="getBankDetails(this.value)">
						  <option selected="selected" value="Default">-Select Bank-</option>
							  <c:forEach var="bankList" items="${bankList}">
			                    <option value="${bankList.bankId}">${bankList.bankName}</option>
			                  </c:forEach>
		                   </select>
                      </div>
                       <span id="bankIdSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="branchName">Branch Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="branchName" name="branchName" readonly="readonly" >
                      </div>
                      <span id="branchNameSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="bankIfsc">IFSC<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="bankIfsc" name="bankIfsc" readonly="readonly">
                      </div>
                      <span id="bankIfscSpan" style="color:#FF0000"></span>
					 </div>
					 
					  <div class="col-xl-2">
                      <div class="form-group">
	                     <label for="bankAccountNumber">Bank A/C No<span class="text-danger">*</span></label>
	                      <div class="input-group">
		                     	<input class="form-control" type="text" id="bankAccountNumber" name="bankAccountNumber">
		                       <span class="input-group-btn">
			            	    <button type="button" class="btn btn-success" onclick="return AddCompantAccountDetails()"><i class="fa fa-plus"></i>Add</button>
			              	   </span>
	                      </div>
                      </div>

                      <span id="bankAccountNumberSpan" style="color:#FF0000"></span>
					 </div>
	                   
				 </div>	 
                   
				<div class="row">
				
			  <div class="col-xl-8"> 
               <table class="table table-striped table-bordered table mb-0 table-sm" id="companyBankListTable">
                  <thead>
                   <tr>
                    <th>#</th>
                    <th>Bank</th>
                    <th>Branch</th>
                    <th>IFSC</th>
                    <th>A/C No</th>
                   </tr>
                 </thead>
                       
                    <tbody>
                    
                    </tbody>       
               </table>
               </div>
				 </div> 
                    
                    
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="CompanyMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>

function getBankDetails()
{
	
	 $("#branchName").empty();
	 $("#bankIfsc").empty();
	 var bankId = $('#bankId').val();
	 

		$.ajax({

			url : '${pageContext.request.contextPath}/getBankDetailsByBankIdWise',
			type : 'Post',
			data : { bankId : bankId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								for(var i=0;i<result.length;i++)
								{
									document.companyform.branchName.value=result[i].bankBranchName;
									document.companyform.bankIfsc.value=result[i].bankifscCode;
									
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
	 
}

function AddCompantAccountDetails()
{
	

	$('#bankIdSpan').html('');
	$('#bankAccountNumberSpan').html('');
	
	if(document.companyform.bankId.value=="Default")
	{
		$('#bankIdSpan').html('Please, select BAnk Name..!');
		document.companyform.bankId.focus();
		return false;
	}

	  if(document.companyform.bankAccountNumber.value=="")
		{
		  $('#bankAccountNumberSpan').html('Please, enter A/C NO..!');
			document.companyform.bankAccountNumber.focus();
			return false;
		}
		else if(document.companyform.bankAccountNumber.value.match(/^[\s]+$/))
		{
			$('#bankAccountNumberSpan').html('Please, enter A/C NO..!');
			document.companyform.bankAccountNumber.value="";
			document.companyform.bankAccountNumber.focus();
			return false; 	
		}
	
	
	var companyId = $('#companyId').val();
	var bankId = $('#bankId').val();
	var branchName = $('#branchName').val();
	var bankIfsc = $('#bankIfsc').val();
	var bankAccountNumber = $('#bankAccountNumber').val();
	
   $('#companyBankListTable tr').detach();
   

   $.ajax({

		 url : '${pageContext.request.contextPath}/AddCompantAccountDetails',
		type : 'Post',
		data : { companyId : companyId, bankId : bankId, branchName : branchName, bankIfsc : bankIfsc, bankAccountNumber : bankAccountNumber },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#companyBankListTable').append('<tr><th>#</th><th>Bank</th><th >Branch</th><th>IFSC</th><th>A/C No</th><th style="width:100px">Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].companyBankId;
									$('#companyBankListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].bankId+'</td><td>'+result[i].branchName+'</td><td>'+result[i].bankIfsc+'</td><td>'+result[i].bankAccountNumber+'</td><td><a onclick="DeleteCompanyAccount('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].companyBankId;
									$('#companyBankListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].bankId+'</td><td>'+result[i].branchName+'</td><td>'+result[i].bankIfsc+'</td><td>'+result[i].bankAccountNumber+'</td><td><a onclick="DeleteCompanyAccount('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}

							 } 
							
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
   
}

function DeleteCompanyAccount(companyBankId)
{

	var companyId = $('#companyId').val();

	   $('#companyBankListTable tr').detach();
	   

	   $.ajax({

			 url : '${pageContext.request.contextPath}/DeleteCompanyAccount',
			type : 'Post',
			data : { companyBankId : companyBankId, companyId : companyId },
			dataType : 'json',
			success : function(result)
					  {
						   if (result) 
						   { 
								$('#companyBankListTable').append('<tr><th>#</th><th>Bank</th><th >Branch</th><th>IFSC</th><th>A/C No</th><th style="width:100px">Action</th></tr>');
							
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										var id = result[i].companyBankId;
										$('#companyBankListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].bankId+'</td><td>'+result[i].branchName+'</td><td>'+result[i].bankIfsc+'</td><td>'+result[i].bankAccountNumber+'</td><td><a onclick="DeleteCompanyAccount('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
									}
									else
									{
										var id = result[i].companyBankId;
										$('#companyBankListTable').append('<tr><td>'+(i+1)+'</td><td>'+result[i].bankId+'</td><td>'+result[i].branchName+'</td><td>'+result[i].bankIfsc+'</td><td>'+result[i].bankAccountNumber+'</td><td><a onclick="DeleteCompanyAccount('+id+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
									}

								 } 
								
							} 
							else
							{
								alert("failure111");
							}
					  } 

			});
	   
}

function getpinCode()
{

	 $("#pinCode").empty();
	 var locationareaId = $('#locationareaId').val();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								 $('#pinCode').val(result[i].pinCode);
								
							 } 
						
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}


function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get locationarea List

function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get City List

function getStateList()
{
	
	 $("#stateId").empty();
	 $("#cityId").empty();
	 $("#locationareaId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getCountryWiseStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select District-");
							$("#cityId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List

function clearAll()
{

	$('#companyNameSpan').html('');
	$('#companyTypeSpan').html('');
	$('#companyPannoSpan').html('');
	$('#companyRegistrationnoSpan').html('');
	$('#companyTannoSpan').html('');

	$('#companyPfnoSpan').html('');
	$('#companyGstnoSpan').html('');
	$('#companyAddressSpan').html('');
	$('#pinCodeSpan').html('');
	$('#phoneNumberSpan').html('');
	
}

function init()
{
	clearAll();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.companyform.companyName.focus();	
}

function validate()
{
	
	clearAll();

	  if(document.companyform.companyName.value=="")
		{
		  $('#companyNameSpan').html('Please, enter company name..!');
			document.companyform.companyName.focus();
			return false;
		}
		else if(document.companyform.companyName.value.match(/^[\s]+$/))
		{
			$('#companyNameSpan').html('Please, enter company name..!');
			document.companyform.companyName.value="";
			document.companyform.companyName.focus();
			return false; 	
		}
	
	
	if(document.companyform.companyType.value=="Default")
	{
		$('#companyTypeSpan').html('Please, select Company type..!');
		document.companyform.companyType.focus();
		return false;
	}


	
	
	//validation for company pan no
	if(document.companyform.companyPanno.value=="")
	{
		$('#companyPannoSpan').html('Please, enter pan number..!');
		document.companyform.companyPanno.focus();
		return false;
	}
	else if(!document.companyform.companyPanno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
	{
		$('#companyPannoSpan').html(' PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
		document.companyform.companyPanno.value="";
		document.companyform.companyPanno.focus();
		return false;
	}
	
	if(document.companyform.companyRegistrationno.value.length!=0)
	{			
		if(!document.companyform.companyRegistrationno.value.match(/^[0-9]{15}$/))
		{
			$('#companyRegistrationnoSpan').html('service tax number must be 15 digit numbers only..!');
			document.companyform.companyRegistrationno.value="";
			document.companyform.companyRegistrationno.focus();
			return false;
		}
	}
	
	//validation for company GST no
	if(document.companyform.companyGstno.value=="")
	{
		$('#companyGstnoSpan').html('Please, Enter GST number..!');
		document.companyform.companyGstno.focus();
		return false;
	}
	else if(!document.companyform.companyGstno.value.match(/^[0-9]{2}[A-Za-z]{5}[0-9]{4}[A-z]{1}[0-9]{1}[Zz]{1}[0-9]{1}$/))
	{
		$('#companyGstnoSpan').html('GST number must match format like(22AAAAA0000A1Z5)..!');
		document.companyform.companyGstno.value="";
		document.companyform.companyGstno.focus();
		return false;
	}
	

	  if(document.companyform.companyAddress.value=="")
		{
		  $('#companyAddressSpan').html('Please, enter company address..!');
			document.companyform.companyAddress.focus();
			return false;
		}
		else if(document.companyform.companyAddress.value.match(/^[\s]+$/))
		{
			$('#companyAddressSpan').html('Please, enter company address..!');
			document.companyform.companyAddress.value="";
			document.companyform.companyAddress.focus();
			return false; 	
		}
	

    //validation for company office phone no
	if(document.companyform.phoneNumber.value=="")
	{
		$('#phoneNumberSpan').html('Please, enter company phone number..!');
		document.companyform.phoneNumber.focus();
		return false;
	}
	else if(!document.companyform.phoneNumber.value.match(/^\(?([0-9]{3})\)?[\s. ]?([0-9]{3})[-. ]?([0-9]{5})$/))
	{
		$('#phoneNumberSpan').html(' phone number must be 10 digit numbers only..!');
		document.companyform.phoneNumber.value="";
		document.companyform.phoneNumber.focus();
		return false;
	}

	
}
</script>
    </body>
</html>