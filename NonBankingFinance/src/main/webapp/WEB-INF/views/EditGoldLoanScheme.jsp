<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Edit Gold Loan Scheme</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Edit Gold Loan Scheme</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="SchemeMaster">Gold Loan Scheme Master</a></li>
                                        <li class="breadcrumb-item active">Edit Gold Loan Scheme</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="goldloanschemeform" action="${pageContext.request.contextPath}/EditGoldLoanScheme" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="goldloanschemeId">Scheme Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="goldloanschemeId" name="goldloanschemeId" value="${goldloanschemeDetails[0].goldloanschemeId}" readonly>
                      </div>
					 </div>
				 
					 </div>
					 	 
					 <div class="row">
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="goldloanschemeName">Scheme Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="goldloanschemeName" name="goldloanschemeName" placeholder="Scheme Name" value="${goldloanschemeDetails[0].goldloanschemeName}"  style="text-transform: capitalize;">
                      </div>
                      <span id="goldloanschemeNameSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="interestRate">Interest Rate<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="interestRate" name="interestRate" placeholder="Interest Rate" value="${goldloanschemeDetails[0].interestRate}" >
                      </div>
                      <span id="interestRateSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="minimumLoanAmount">Minimum Loan Amount<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="minimumLoanAmount" name="minimumLoanAmount" placeholder="Minimum Loan Amount" value="${goldloanschemeDetails[0].minimumLoanAmount}" >
                      </div>
                      <span id="minimumLoanAmountSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="maximumLoanAmount">Maximum Loan Amount<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="maximumLoanAmount" name="maximumLoanAmount" placeholder="Maximum Loan Amount" value="${goldloanschemeDetails[0].maximumLoanAmount}" onchange="getCheckMaximumValue(this.value)">
                      </div>
                      <span id="maximumLoanAmountSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="maximumTenure">Maximum Tenure(Days)<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="maximumTenure" name="maximumTenure" placeholder="Maximum Tenure" value="${goldloanschemeDetails[0].maximumTenure}">
                      </div>
                      <span id="maximumTenureSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="lateFine">Due<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="lateFine" name="lateFine" placeholder="Late Fine" value="${goldloanschemeDetails[0].lateFine}">
                      </div>
                      <span id="lateFineSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="interestRate_3_6_month">Interest Rate(3-6 month)<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="interestRate_3_6_month" name="interestRate_3_6_month" placeholder="Interest Rate"value="${goldloanschemeDetails[0].interestRate_3_6_month}">
                      </div>
                      <span id="interestRate_3_6_monthSpan" style="color:#FF0000"></span>
					 </div>
					 
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="interestRate_6_9_month">Interest Rate(6-9 month)<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="interestRate_6_9_month" name="interestRate_6_9_month" placeholder="Interest Rate" value="${goldloanschemeDetails[0].interestRate_6_9_month}">
                      </div>
                      <span id="interestRate_6_9_monthSpan" style="color:#FF0000"></span>
					 </div>
					 
					 </div>
					 
                    
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="GoldLoanSchemeMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="${goldloanschemeDetails[0].creationDate}">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>

function getCheckMaximumValue()
{
	
	$('#maximumLoanAmountSpan').html('');
	 var minimumLoanAmount = Number($('#minimumLoanAmount').val());
	 var maximumLoanAmount = Number($('#maximumLoanAmount').val());
	 
 if(minimumLoanAmount>maximumLoanAmount)
	  {

		$('#maximumLoanAmountSpan').html('The maximum value is less then minmum value, please enter correct value');
		document.goldloanschemeform.maximumLoanAmount.value="";
		document.goldloanschemeform.maximumLoanAmount.focus();
		return false;
	  }
 
}

function init()
{
	clearAll();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.goldloanschemeform.goldloanschemeName.focus();	
}

function clearAll()
{

	$('#goldloanschemeNameSpan').html('');
	$('#interestRateSpan').html('')

	$('#minimumLoanAmountSpan').html('');
	$('#maximumLoanAmountSpan').html('');
	$('#lateFineSpan').html('');
	$('#maximumTenureSpan').html('');

	$('#interestRate_3_6_monthSpan').html('');
	$('#interestRate_6_9_monthSpan').html('');
}

function validate()
{
	clearAll();
	 //  validation for goldloanschemeName
	  if(document.goldloanschemeform.goldloanschemeName.value=="")
		{
		  $('#goldloanschemeNameSpan').html('Please, enter goldloanscheme name..!');
			document.goldloanschemeform.goldloanschemeName.focus();
			return false;
		}
		else if(document.goldloanschemeform.goldloanschemeName.value.match(/^[\s]+$/))
		{
			$('#goldloanschemeNameSpan').html('Please, enter goldloanscheme name..!');
			document.goldloanschemeform.goldloanschemeName.value="";
			document.goldloanschemeform.goldloanschemeName.focus();
			return false; 	
		}
	  //  validation for interestRate
	  if(document.goldloanschemeform.interestRate.value=="")
		{
		  $('#interestRateSpan').html('Please, enter Interest Rate..!');
			document.goldloanschemeform.interestRate.focus();
			return false;
		}
		else if(document.goldloanschemeform.interestRate.value.match(/^[\s]+$/))
		{
			$('#interestRateSpan').html('Please, enter Interest Rate..!');
			document.goldloanschemeform.interestRate.value="";
			document.goldloanschemeform.interestRate.focus();
			return false; 	
		}
		else if(!document.goldloanschemeform.interestRate.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.goldloanschemeform.interestRate.value.match(/^[0-9]+$/))
				 {
					$('#interestRateSpan').html('Please, use only digit value for Interest Rate..! eg:21.36 OR 30');
					document.goldloanschemeform.interestRate.value="";
					document.goldloanschemeform.interestRate.focus();
					return false;
				}
		}

	  
	  //  validation for minimumLoanAmount
	  if(document.goldloanschemeform.minimumLoanAmount.value=="")
		{
		  $('#minimumLoanAmountSpan').html('Please, enter minimum Loan Amount..!');
			document.goldloanschemeform.minimumLoanAmount.focus();
			return false;
		}
		else if(document.goldloanschemeform.minimumLoanAmount.value.match(/^[\s]+$/))
		{
			$('#minimumLoanAmountSpan').html('Please, enter minimum Loan Amount..!');
			document.goldloanschemeform.minimumLoanAmount.value="";
			document.goldloanschemeform.minimumLoanAmount.focus();
			return false; 	
		}
		else if(!document.goldloanschemeform.minimumLoanAmount.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.goldloanschemeform.minimumLoanAmount.value.match(/^[0-9]+$/))
				 {
					$('#minimumLoanAmountSpan').html('Please, use only digit value for minimum Loan Amount..! eg:21.36 OR 30');
					document.goldloanschemeform.minimumLoanAmount.value="";
					document.goldloanschemeform.minimumLoanAmount.focus();
					return false;
				}
		}
	  
	  //  validation for maximumLoanAmount
	  if(document.goldloanschemeform.maximumLoanAmount.value=="")
		{
		  $('#maximumLoanAmountSpan').html('Please, enter maximum Loan Amount..!');
			document.goldloanschemeform.maximumLoanAmount.focus();
			return false;
		}
		else if(document.goldloanschemeform.maximumLoanAmount.value.match(/^[\s]+$/))
		{
			$('#maximumLoanAmountSpan').html('Please, enter maximum Loan Amount..!');
			document.goldloanschemeform.maximumLoanAmount.value="";
			document.goldloanschemeform.maximumLoanAmount.focus();
			return false; 	
		}
		else if(!document.goldloanschemeform.maximumLoanAmount.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.goldloanschemeform.maximumLoanAmount.value.match(/^[0-9]+$/))
				 {
					$('#maximumLoanAmountSpan').html('Please, use only digit value for maximum Loan Amount..! eg:21.36 OR 30');
					document.goldloanschemeform.maximumLoanAmount.value="";
					document.goldloanschemeform.maximumLoanAmount.focus();
					return false;
				}
		}

		 var minimumLoanAmount = Number($('#minimumLoanAmount').val());
		 var maximumLoanAmount = Number($('#maximumLoanAmount').val());
		 
	  if(minimumLoanAmount>maximumLoanAmount)
		  {

			$('#maximumLoanAmountSpan').html('The maximum value is less then minmum value, please enter correct value');
			document.goldloanschemeform.maximumLoanAmount.value="";
			document.goldloanschemeform.maximumLoanAmount.focus();
			return false;
		  }
	  
	  //  validation for maximumTenure
	  if(document.goldloanschemeform.maximumTenure.value=="")
		{
		  $('#maximumTenureSpan').html('Please, enter Tenure..!');
			document.goldloanschemeform.maximumTenure.focus();
			return false;
		}
		else if(document.goldloanschemeform.maximumTenure.value.match(/^[\s]+$/))
		{
			$('#maximumTenureSpan').html('Please, enter maximum Tenure..!');
			document.goldloanschemeform.maximumTenure.value="";
			document.goldloanschemeform.maximumTenure.focus();
			return false; 	
		}
		else if(!document.goldloanschemeform.maximumTenure.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.goldloanschemeform.maximumTenure.value.match(/^[0-9]+$/))
				 {
					$('#maximumTenureSpan').html('Please, use only digit value for maximum Tenure..! eg:21.36 OR 30');
					document.goldloanschemeform.maximumTenure.value="";
					document.goldloanschemeform.maximumTenure.focus();
					return false;
				}
		}
	  
	  //  validation for lateFine
	  if(document.goldloanschemeform.lateFine.value=="")
		{
		  $('#lateFineSpan').html('Please, enter late Fine..!');
			document.goldloanschemeform.lateFine.focus();
			return false;
		}
		else if(document.goldloanschemeform.lateFine.value.match(/^[\s]+$/))
		{
			$('#lateFineSpan').html('Please, enter late Fine..!');
			document.goldloanschemeform.lateFine.value="";
			document.goldloanschemeform.lateFine.focus();
			return false; 	
		}
		else if(!document.goldloanschemeform.lateFine.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.goldloanschemeform.lateFine.value.match(/^[0-9]+$/))
				 {
					$('#lateFineSpan').html('Please, use only digit value for late Fine..! eg:21.36 OR 30');
					document.goldloanschemeform.lateFine.value="";
					document.goldloanschemeform.lateFine.focus();
					return false;
				}
		}

	  if(document.goldloanschemeform.interestRate_3_6_month.value=="")
		{
		  $('#interestRate_3_6_monthSpan').html('Please, enter Interest Rate..!');
			document.goldloanschemeform.interestRate_3_6_month.focus();
			return false;
		}
		else if(document.goldloanschemeform.interestRate_3_6_month.value.match(/^[\s]+$/))
		{
			$('#interestRate_3_6_monthSpan').html('Please, enter Interest Rate..!');
			document.goldloanschemeform.interestRate_3_6_month.value="";
			document.goldloanschemeform.interestRate_3_6_month.focus();
			return false; 	
		}
		else if(!document.goldloanschemeform.interestRate_3_6_month.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.goldloanschemeform.interestRate_3_6_month.value.match(/^[0-9]+$/))
				 {
					$('#interestRate_3_6_monthSpan').html('Please, use only digit value for Interest Rate..! eg:21.36 OR 30');
					document.goldloanschemeform.interestRate_3_6_month.value="";
					document.goldloanschemeform.interestRate_3_6_month.focus();
					return false;
				}
		}

	  if(document.goldloanschemeform.interestRate_6_9_month.value=="")
		{
		  $('#interestRate_6_9_monthSpan').html('Please, enter Interest Rate..!');
			document.goldloanschemeform.interestRate_6_9_month.focus();
			return false;
		}
		else if(document.goldloanschemeform.interestRate_6_9_month.value.match(/^[\s]+$/))
		{
			$('#interestRate_6_9_monthSpan').html('Please, enter Interest Rate..!');
			document.goldloanschemeform.interestRate_6_9_month.value="";
			document.goldloanschemeform.interestRate_6_9_month.focus();
			return false; 	
		}
		else if(!document.goldloanschemeform.interestRate_6_9_month.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.goldloanschemeform.interestRate_6_9_month.value.match(/^[0-9]+$/))
				 {
					$('#interestRate_6_9_monthSpan').html('Please, use only digit value for Interest Rate..! eg:21.36 OR 30');
					document.goldloanschemeform.interestRate_6_9_month.value="";
					document.goldloanschemeform.interestRate_6_9_month.focus();
					return false;
				}
		}
	  
}
</script>
    </body>
</html>