<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>User Profile</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">User Profile</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item active">User Profile</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="userform" action="${pageContext.request.contextPath}/UserProfile" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-5">
         		
                <div class="card-box">
                <div class="row">
	                 
	                 <div class="col-xl-12">
                		 <table class="table mb-0 table-sm">
                              <thead>
                                <tr>
                                  <th><th>
                                  <th>Name</th>
                                  <th>: ${employeeDetails.employeefirstName} ${employeeDetails.employeemiddleName} ${employeeDetails.employeelastName}</th>
                                   
                                </tr>
                                <tr>
                                  <th><th>
                                  <th>Designation </th>
                                  <th>: ${designationName}</th>
                                   
                                </tr>
                                <tr>
                                  <th><th>
                                  <th>Email Id</th>
                                  <th>: ${employeeDetails.employeeEmailId}</th>
                                   
                                </tr>
                            </thead>      
                           </table>       
                                 
                      </div>
	                 
					 </div>
				</div>
				
                </div>


         		<div class="col-7">
         		
         		<div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                
                		  <span id="statusSpan" style="color:#008000"></span>
                  
                                    <h4 class="header-title m-t-0 m-b-30">Update Password</h4>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-3 col-form-label">New Password</label>
                                        <div class="col-6">
                         			  	<input class="form-control" type="password" id="newPassword" name="newPassword" placeholder="Enter New password" onchange ="ValidationForNewpassword(this.value)">
                                        </div>

					                    <span id="newPasswordSpan" style="color:#FF0000" hidden="true"><img src="${pageContext.request.contextPath}/resources/images/ok.gif" border="0"/></span>
					                    <span id="newPasswordMsgSpan" style="color:#FF0000"></span>
					                    
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-3 col-form-label">Confirm Password</label>
                                        <div class="col-6">
                        			   	<input class="form-control" type="password" id="confirmedPassword" name="confirmedPassword" placeholder="Enter New password" onchange ="ValidationForConfirmedpassword(this.value)">
                                        </div>
					                    <span id="confirmedPasswordSpan" style="color:#FF0000" hidden="true"><img src="${pageContext.request.contextPath}/resources/images/ok.gif" border="0"/></span>
					                    <span id="confirmedPasswordMsgSpan" style="color:#FF0000"></span>
					                  
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-3 col-form-label">Old Password</label>
                                        <div class="col-6">
                          			 	<input class="form-control" type="password" id="oldPassword" name="oldPassword" placeholder="Enter Old password" onchange ="ValidationForOldpassword(this.value)">
                                        </div>
					                    <span id="oldPasswordSpan" style="color:#FF0000" hidden="true"><img src="${pageContext.request.contextPath}/resources/images/ok.gif" border="0"/></span>
					                    <span id="oldPasswordMsgSpan" style="color:#FF0000"></span>
					                  
                                    </div>
                                    
               
                 				<input type="hidden" id="oldPassword1" name="oldPassword1" value="${loginDetails[0].passWord}">
                 				<input type="hidden" id="userId" name="userId" value="${loginDetails[0].userId}">
	                              </br>      
				                 <div class="row">

								   <div class="col-xl-3">
								   </div>
								    
			                      <div class="col-xl-3">
			                      	<button type="reset" class="btn btn-default"> Reset </button>
								 </div>
								 
			                      <div class="col-xl-3">
			                          <button type="button" class="btn btn-primary" onclick="return UpdateUserPassword()">Submit</button>
								 </div>
			                    </div>
			                    
		                    </div>
                            </div>
                        </div>
         		
                </div>
         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>
function UpdateUserPassword()
{

	AllClear();
	 var oldPassword = $('#oldPassword').val();
	 var oldPassword1 = $('#oldPassword1').val();
	 
	 var newPassword = $('#newPassword').val();
	 var confirmedPassword = $('#confirmedPassword').val();

	 var userId=$('#userId').val();
		 
	 if(newPassword=="")
		 {
		  $('#newPasswordMsgSpan').html('Please, enter new password..!');
			document.userform.newPassword.focus();
			return false;
		 }

	 if(confirmedPassword=="")
		 {

		  $('#confirmedPasswordMsgSpan').html('Please, enter confirmed password..!');
			document.userform.confirmedPassword.focus();
			return false;
		 }
	 if(oldPassword=="")
		 {
		  $('#oldPasswordMsgSpan').html('Please, enter old password..!');
			document.userform.oldPassword.focus();
			return false;
		 }
	 
	 if(newPassword!=confirmedPassword)
		 {
		  $('#confirmedPasswordMsgSpan').html('Pasword are not match..!');
			document.userform.confirmedPassword.value="";
			document.userform.confirmedPassword.focus();
			return false;
		 }
	 else if(oldPassword!=oldPassword1)
		 {
		  $('#oldPasswordMsgSpan').html('old password incorrect..!');
			document.userform.oldPassword.value="";
			document.userform.oldPassword.focus();
			return false;
		 }
	 else
		 {

			$.ajax({

				url : '${pageContext.request.contextPath}/UpdateUserPassword',
				type : 'Post',
				data : { newPassword : newPassword, userId : userId},
				dataType : 'json',
				success : function(result)
						  {
								if (result) 
								{
								} 
								else
								{
									alert("failure111");
									//$("#ajax_div").hide();
								}

							}
				});
		 
			
			  $('#statusSpan').html('New password update successfully..!');
			
			document.userform.oldPassword.value="";
			document.userform.newPassword.value="";
			document.userform.confirmedPassword.value="";
			
		 }
}

function ValidationForOldpassword()
{

	AllClear();
	 var oldPassword = $('#oldPassword').val();
	 var oldPassword1 = $('#oldPassword1').val();
	 
	 if(oldPassword!=oldPassword1)
		 {
		  $('#oldPasswordMsgSpan').html('old password incorrect..!');
			document.userform.oldPassword.value="";
			document.userform.oldPassword.focus();
			return false;
		 }
	 
}

function ValidationForConfirmedpassword()
{
	AllClear();
	 var newPassword = $('#newPassword').val();
	 var confirmedPassword = $('#confirmedPassword').val();
	 
	 if(newPassword!=confirmedPassword)
		 {
		  $('#confirmedPasswordMsgSpan').html('Pasword are not match..!');
			document.userform.confirmedPassword.value="";
			document.userform.confirmedPassword.focus();
			return false;
		 }
}
function ValidationForNewpassword()
{

	AllClear();
	if(document.userform.newPassword.value == "")
	{
		document.userform.newPassword.value="";
		document.userform.newPassword.focus();
		$('#newPasswordMsgSpan').html('New Password can not be empty..!');
		return false;
	}
	else if(!(document.userform.newPassword.value.length>=8))
	{
		document.userform.newPassword.value="";
		document.userform.newPassword.focus();
		$('#newPasswordMsgSpan').html('Password length must be between 8 to 12 character long ..!');
		return false;
	}
	else if(!document.userform.newPassword.value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/))
	{
		document.userform.newPassword.value="";
		document.userform.newPassword.focus();
		$('#newPasswordMsgSpan').html('Password should contain At least one upper case letter,one lower case letter, one digit & special symbol..!');
		return false;
	}
	
}
function init()
{

	AllClear();
}
function AllClear()
{

	$('#newPasswordMsgSpan').html('');
	$('#confirmedPasswordMsgSpan').html('');
	$('#oldPasswordMsgSpan').html('');
	$('#statusSpan').html('');
	
}
function validate()
{
	AllClear();
	
	
}
</script>
    </body>
</html>