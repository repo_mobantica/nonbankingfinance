<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Add Bank</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Bank</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="BankMaster">Bank Master</a></li>
                                        <li class="breadcrumb-item active">Add Bank</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="bankform" action="${pageContext.request.contextPath}/AddBank" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="bankId">Bank Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="bankId" name="bankId"  value="${bankCode}" readonly>
                      </div>
					 </div>
				 
					 </div>
					 	 
					 <div class="row">
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="bankName">Bank Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="bankName" name="bankName" placeholder="Bank Name" style="text-transform: capitalize;">
                      </div>
                      <span id="bankNameSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="bankBranchName">Bank Branch Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="bankBranchName" name="bankBranchName" placeholder="Bank branch Name" style="text-transform: capitalize;">
                      </div>
                      <span id="bankBranchNameSpan" style="color:#FF0000"></span>
					 </div>
					
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="bankifscCode">IFSC Code<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="bankifscCode" name="bankifscCode" placeholder="Bank IFSC Code" >
                      </div>
                      <span id="bankifscCodeSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="bankPhoneno">Phone<span class="text-danger">*</span></label>
                           	<input type="text" id="phoneNumber" name="bankPhoneno" placeholder="" data-mask="(999) 999-99999" class="form-control">
                      </div>
                      <span id="phoneNumberSpan" style="color:#FF0000"></span>
					 </div>
					
                    </div>
                    
					 <div class="row">
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="bankAddress">Address<span class="text-danger">*</span></label>
                           	<textarea class="form-control" id="bankAddress" name="bankAddress" rows="1"></textarea>
                      </div>
                      <span id="bankAddressSpan" style="color:#FF0000"></span>
					 </div>
					
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="pinCode">Pin Code<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="pinCode" name="pinCode" placeholder="Pin Code" >
                      </div>
                      <span id="pinCodeSpan" style="color:#FF0000"></span>
					 </div>
					 
					 
					 
					 </div>
					 
                    
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="BankMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
               
                 <input type="hidden" id="creationDate" name="creationDate" value="">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId"  value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>
function getVillageWisePinCode()
{

	 $("#pinCode").empty();
	 var countryId = $('#countryId').val();
	 var stateId = $('#stateId').val();
	 var districtId = $('#districtId').val();	
	 var tahsilId = $('#tahsilId').val();
	 var villageId = $('#villageId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getVillageWisePinCode',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId, districtId : districtId, tahsilId : tahsilId, villageId : villageId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								document.bankform.pinCode.value=result[i].pinCode;
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}


function getTahsilWiseVillageList()
{

	 $("#villageId").empty();
	 var countryId = $('#countryId').val();
	 var stateId = $('#stateId').val();
	 var districtId = $('#districtId').val();	
	 var tahsilId = $('#tahsilId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getTahsilWiseVillageList',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId, districtId : districtId, tahsilId : tahsilId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select village-");
							$("#villageId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].villageId).text(result[i].villageName);
							    $("#villageId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}

function getDistrictWiseTahsilList()
{
	
	 $("#tahsilId").empty();
	 var countryId = $('#countryId').val();
	 var stateId = $('#stateId').val();
	 var districtId = $('#districtId').val();	
	$.ajax({

		url : '${pageContext.request.contextPath}/getDistrictWiseTahsilList',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId, districtId : districtId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Tahsil-");
							$("#tahsilId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].tahsilId).text(result[i].tahsilName);
							    $("#tahsilId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	

}

function getStateWiseDistrictList()
{
	 $("#tahsilId").empty();
	 $("#districtId").empty();
	 var countryId = $('#countryId').val();
	 var stateId = $('#stateId').val();
		
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateWiseDistrictList',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Tahsil-");
							$("#tahsilId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select District-");
							$("#districtId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].districtId).text(result[i].districtName);
							    $("#districtId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	

}

function getStateList()
{
	
	 $("#stateId").empty();
	 $("#districtId").empty();
	 $("#tahsilId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getCountryWiseStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select District-");
							$("#districtId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List

function clearAll()
{

	$('#bankNameSpan').html('');
	$('#bankBranchNameSpan').html('');
	$('#bankAddressSpan').html('');
	$('#pinCodeSpan').html('');
	$('#bankPhonenoSpan').html('');
	$('#bankifscCodeSpan').html('');
	
}

function init()
{
	clearAll();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.bankform.bankName.focus();	
}

function validate()
{
	
	clearAll();

	  if(document.bankform.bankName.value=="")
		{
		  $('#bankNameSpan').html('Please, enter bank name..!');
			document.bankform.bankName.focus();
			return false;
		}
		else if(document.bankform.bankName.value.match(/^[\s]+$/))
		{
			$('#bankNameSpan').html('Please, enter bank name..!');
			document.bankform.bankName.value="";
			document.bankform.bankName.focus();
			return false; 	
		}
	

	  if(document.bankform.bankBranchName.value=="")
		{
		  $('#bankBranchNameSpan').html('Please, enter branch name..!');
			document.bankform.bankBranchName.focus();
			return false;
		}
		else if(document.bankform.bankBranchName.value.match(/^[\s]+$/))
		{
			$('#bankBranchNameSpan').html('Please, enter branch name..!');
			document.bankform.bankBranchName.value="";
			document.bankform.bankBranchName.focus();
			return false; 	
		}
	
	//validation for ifsc code
		if(document.bankform.bankifscCode.value=="")
		{
			$('#bankifscCodeSpan').html('Please, enter bank IFSC code..!');
			document.bankform.bankifscCode.focus();
			return false;
		}
		else if(!document.bankform.bankifscCode.value.match(/^\(?([A-Z]{4})\)?[. ]?([0-9]{7})$/))
		{
			$('#bankifscCodeSpan').html(' bank IFSC code must contain 4 capital alphabets and 7 digit numbers only..!');
			document.bankform.bankifscCode.value="";
			document.bankform.bankifscCode.focus();
			return false;
		}
	    //validation for bank office phone no
		if(document.bankform.phoneNumber.value=="")
		{
			$('#phoneNumberSpan').html('Please, enter bank phone number..!');
			document.bankform.phoneNumber.focus();
			return false;
		}
		else if(!document.bankform.phoneNumber.value.match(/^\(?([0-9]{3})\)?[\s. ]?([0-9]{3})[-. ]?([0-9]{5})$/))
		{
			$('#phoneNumberSpan').html(' phone number must be 10 digit numbers only..!');
			document.bankform.phoneNumber.value="";
			document.bankform.phoneNumber.focus();
			return false;
		}

	  if(document.bankform.bankAddress.value=="")
		{
		  $('#bankAddressSpan').html('Please, enter bank address..!');
			document.bankform.bankAddress.focus();
			return false;
		}
		else if(document.bankform.bankAddress.value.match(/^[\s]+$/))
		{
			$('#bankAddressSpan').html('Please, enter bank address..!');
			document.bankform.bankAddress.value="";
			document.bankform.bankAddress.focus();
			return false; 	
		}
	
	
}
</script>
    </body>
</html>