<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

  <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <div id="sidebar-menu">
                        <ul>

                            <li class="has_sub">
                                <a href="home" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i><span> Home </span> </a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Administrator </span> <span class="menu-arrow"></span></a>
                                <ul>
                                <!-- 
                                    <li class="has_sub">
                                        <a href="javascript:void(0);" class="waves-effect"> <span>Area</span>  <span class="menu-arrow"></span> </a>
                                        <ul>
	                                    <li><a href="CountryMaster">Country Master</a></li>
	                                    <li><a href="StateMaster">State Master</a></li>
	                                    <li><a href="CityMaster">City Master</a></li>
	                                    <li><a href="LocationAreaMaster">Location Master</a></li>
                                        </ul>
                                    </li>
                                   -->
		                            <li class="has_sub">
		                                <a href="BankMaster" class="waves-effect"><span> Bank Master </span> </a>
		                            </li>
		                                  
		                            <li class="has_sub">
		                                <a href="javascript:void(0);" class="waves-effect"><span> Company Master </span> <span class="menu-arrow"></span></a>
		                                <ul>
		                                    <li><a href="CompanyMaster">Company Master</a></li>
		                                    <li><a href="BranchMaster">Branch Master</a></li>
		                                </ul>
		                            </li>
 
		                            <li class="has_sub">
		                                <a href="javascript:void(0);" class="waves-effect"><span> Gold Master </span> <span class="menu-arrow"></span></a>
		                                <ul>
		                                    <li><a href="GoldRateMaster">Gold Rate Master</a></li>
		                                    <li><a href="GoldLoanSchemeMaster">Gold Loan Scheme</a></li>
		                                    <li><a href="GoldItemMaster">Gold Item Master</a></li>
		                                </ul>
		                            </li>

		                            <li class="has_sub">
		                                <a href="javascript:void(0);" class="waves-effect"><span> Employee Master </span> <span class="menu-arrow"></span></a>
		                                <ul>
		                                    <li><a href="DepartmentMaster">Department Master</a></li>
		                                    <li><a href="DesignationMaster">Designation Master</a></li>
		                                    <li><a href="EmployeeMaster">Employee Master</a></li>
		                                </ul>
		                            </li>

		                            <li class="has_sub">
		                                <a href="javascript:void(0);" class="waves-effect"><span> Other Master </span> <span class="menu-arrow"></span></a>
		                                <ul>
		                                
		                                    <li><a href="OccupationMaster">Occupation Master</a></li>
		                                    <li><a href="CurrencyMaster">Currency Master</a></li>
		                                    <li><a href="TaxMaster">Tax Master</a></li>
		                                    <li><a href="EnquirySourceMaster">Enquiry Source Master</a></li>
		                                    
		                                    <li><a href="ChartOfAccountMaster">COA Master</a></li>
		                                    <li><a href="SubChartOfAccountMaster">Sub COA Master</a></li>
		                                    
		                                </ul>
		                            </li>

                                    
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="EMICalculator" class="waves-effect"><i class="ion-calculator"></i><span> EMI Calculator </span> </a>
                            </li>
                              
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Customer Details </span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li><a href="CustomerMaster"><span> Customer Master </span> </a></li>
		                            <li><a href="CustomerLoanMaster"><span> Customer Loan Details </span> </a></li>
		                           <li><a href="UploadCustomerDocument"><span> Upload Customer Document </span> </a></li>
		                           <li><a href="NoticeChargeReport?noticeDays=90"><span> Notice Charge Report </span> </a></li>
		                          
                                </ul>
                            </li>
    
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Customer Payment </span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li><a href="CustomerPaymentMaster"><span>Add Customer Payment </span> </a></li>
		                          
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Account Management</span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li><a href="VoucherMaster"><span> Voucher Master </span> </a></li>
		                          
                                </ul>
                            </li>
                            
                            
                            <li class="has_sub">
                             <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Reports </span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li class="has_sub">
		                                <a href="javascript:void(0);" class="waves-effect"><span> Customer Report </span> <span class="menu-arrow"></span></a>
		                                <ul>
				                            <li> <a href="CustomerDetailsMonthlyReport" ><span> Customer Details Report </span> </a> </li>
				                            
				                             <li> <a href="CustomerReport" ><span> CustomerReport </span> </a> </li>
				                            
				                            <li> <a href="PendingCustomerReport" ><span>Pending Customer Report</span> </a> </li>
				                           
				                            <li> <a href="CustomerEMIReport" ><span> EMI Report </span> </a> </li>
				                          
				                            <li><a href="CustomerDailyPaymentReport" ><span> Daily Payment Report </span> </a> </li>
				                            
				                            <li><a href="CustomerDailyLoanReport"><span> Daily Loan Report </span> </a></li>
				                             
				                            <li><a href="TodayCollectionReport" ><span> Today Collection Report </span> </a></li>
				                             
				                            <li><a href="CustomerMonthlyReport" ><span> Monthly Report </span> </a></li>
				                            
				                            <li><a href="CustomerLoanClearanceReport"><span> Loan Clearance Report </span> </a></li>
				                            
		                                </ul>
		                            </li>
                                    
		                            <li class="has_sub">
		                                <a href="javascript:void(0);" class="waves-effect"><span> Bank Report </span> <span class="menu-arrow"></span></a>
		                                <ul>
				                            <li><a href="BankTransactionReport"><span> Bank Report </span> </a></li>
				                          
		                                </ul>
		                            </li>
                                  
		                          
                                </ul>
                            </li>
                           
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>User Management</span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li><a href="UserMaster"><span>User Master</span> </a></li>
		                          
                                </ul>
                            </li>
                             
                        </ul>
                        
                    </div>

                </div>

            </div>
