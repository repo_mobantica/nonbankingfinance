<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Edit Customer</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Edit Customer</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="CustomerMaster">Customer Master</a></li>
                                        <li class="breadcrumb-item active">Edit Customer</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="customerform" action="${pageContext.request.contextPath}/EditCustomer" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="customerId">Customer Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="customerId" name="customerId" value="${customerDetails[0].customerId}" readonly>
                      </div>
                      </div>
                    
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="customerAadharcardno">Aadhar No<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="customerAadharcardno" name="customerAadharcardno" placeholder="Aadhar card No" style="text-transform: uppercase;" value="${customerDetails[0].customerAadharcardno}">
                      </div>
                      <span id="customerAadharcardnoSpan" style="color:#FF0000"></span>
					 </div>
					 
					 </div>
					 	 
					 <div class="row">
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="customerName">Customer Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="customerName" name="customerName" placeholder="Customer Name" value="${customerDetails[0].customerName}" style="text-transform: capitalize;">
                      </div>
                      <span id="customerNameSpan" style="color:#FF0000"></span>
					 </div>
			
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="mobileNumber">Primary Mobile No.<span class="text-danger">*</span></label>
                           	<input type="text" id="mobileNumber" name="mobileNumber" placeholder=""  class="form-control" value="${customerDetails[0].mobileNumber}">
                      </div>
                      <span id="mobileNumberSpan" style="color:#FF0000"></span>
					 </div>
						
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="customerPhoneno">Second Mobile No.</label>
                           	<input type="text" id="customerPhoneno" name="customerPhoneno" placeholder="" class="form-control" value="${customerDetails[0].customerPhoneno}">
                      </div>
                      <span id="customerPhonenoSpan" style="color:#FF0000"></span>
					 </div>
							
					  <div class="col-xl-3">
                      <div class="form-group">
	                      <label>Email ID<span class="text-danger">*</span></label>
	                      <div class="input-group">
		                      <input type="text" class="form-control" placeholder="Email Id" id="emailId" name="emailId" value="${customerDetails[0].emailId}">
		                      <div class="input-group-append">
		                      <span class="input-group-text"><i class="ti-email"></i></span>
	                      	  </div>
	                      </div>
                      </div>

                      <span id="emailIdSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="customerPancardno">PAN Number<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="customerPancardno" name="customerPancardno" placeholder="PAN Number" style="text-transform:uppercase" value="${customerDetails[0].customerPancardno}">
                      </div>
                      <span id="customerPancardnoSpan" style="color:#FF0000"></span>
					 </div>
					
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="countryId">Occupation Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="occupationName"  id="occupationName" >
		                  <option selected="selected" value="${customerDetails[0].occupationName}">${customerDetails[0].occupationName}</option>
		                    <c:forEach var="occupationList" items="${occupationList}">
		                     <c:choose>
		                      <c:when test="${customerDetails[0].occupationName ne occupationList.occupationName}">
			                    <option value="${occupationList.occupationName}">${occupationList.occupationName}</option>
			                  </c:when>
			                 </c:choose>
			                 </c:forEach>
		                   </select>
                      </div>
                       <span id="occupationNameSpan" style="color:#FF0000"></span>
					 </div>
					<%--  
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="enquirySourceName">Enquiry Source Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="enquirySourceName"  id="enquirySourceName" >
		                  <option selected="selected" value="${customerDetails[0].enquirySourceName}">${customerDetails[0].enquirySourceName}</option>
		                    <c:forEach var="enquirySourceList" items="${enquirySourceList}">
		                     <c:choose>
		                      <c:when test="${customerDetails[0].enquirySourceName ne enquirySourceList.enquirySourceName}">
			                    <option value="${enquirySourceList.enquirySourceName}">${enquirySourceList.enquirySourceName}</option>
			                  </c:when>
			                 </c:choose>
			                 </c:forEach>
		                  </select>
                      
                      </div>
                       <span id="enquirySourceNameSpan" style="color:#FF0000"></span>
					 </div>
					  --%>
					  
                      <div class="col-xl-4">
                      <div class="form-group">
                      		<label for="bankAddress">Address<span class="text-danger">*</span></label>
                           	<textarea class="form-control" id="customerAddress" name="customerAddress" rows="1">${customerDetails[0].customerAddress}</textarea>
                      </div>
                      <span id="customerAddressSpan" style="color:#FF0000"></span>
					 </div>
				
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="pinCode">Pin Code<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="pinCode" name="pinCode" placeholder="Pin Code" value="${customerDetails[0].pinCode}">
                      </div>
                      <span id="pinCodeSpan" style="color:#FF0000"></span>
					 </div>
					 
					 
					 
                    </div>
                    
                    
					 <div class="row">
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="customerBankName">Bank Name</label>
                           	<input class="form-control" type="text" id="customerBankName" name="customerBankName"  placeholder="Bank Name" value="${customerDetails[0].customerBankName}">
                      </div>
                       <span id="customerBankNameSpan" style="color:#FF0000"></span>
					 </div>
	                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="bankBranchName">Branch Name</label>
                           	<input class="form-control" type="text" id="bankBranchName" name="bankBranchName"  placeholder="Branch Name" value="${customerDetails[0].bankBranchName}">
                      </div>
                      <span id="bankBranchNameSpan" style="color:#FF0000"></span>
					 </div>
						
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="banckIFSC">IFSC</label>
                           	<input class="form-control" type="text" id="banckIFSC" name="banckIFSC"  placeholder="IFSC" value="${customerDetails[0].banckIFSC}">
                      </div>
                       <span id="banckIFSCSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="bankAccountNumber">Account Number</label>
                           	<input class="form-control" type="text" id="bankAccountNumber" name="bankAccountNumber"  placeholder="Account Number" value="${customerDetails[0].bankAccountNumber}">
                      </div>
                      <span id="bankAccountNumberSpan" style="color:#FF0000"></span>
					 </div>
					 
					 </div>
                    
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="CustomerMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
       
                 <input type="hidden" id="creationDate" name="creationDate" value="${creationDate}">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId"  value="<%= session.getAttribute("employeeId") %>">	
                 <input type="hidden" id="branchId" name="branchId"  value="${customerDetails[0].branchId}">	
                     
				</div>
				
                </div><!-- end col-->

         </div>

</form>

          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script>

function getpinCode()
{

	 $("#pinCode").empty();
	 var locationareaId = $('#locationareaId').val();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								 $('#pinCode').val(result[i].pinCode);
								
							 } 
						
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}


function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get locationarea List

function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get City List

function getStateList()
{
	
	 $("#stateId").empty();
	 $("#cityId").empty();
	 $("#locationareaId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getCountryWiseStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select District-");
							$("#cityId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List

function clearAll()
{
	$('#packetNumberSpan').html('');
	$('#customerNameSpan').html('');
	$('#customerMiddlelNameSpan').html('');
	$('#customerLastNameSpan').html('');
	$('#mobileNumberSpan').html('');
	$('#customerPhonenoSpan').html('');
	$('#emailIdSpan').html('');
	$('#customerPancardnoSpan').html('');
	$('#customerAadharcardnoSpan').html('');
	$('#occupationNameSpan').html('');
	$('#enquirySourceNameSpan').html('');
		
	$('#customerAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#districtIdSpan').html('');
	$('#tahsilIdSpan').html('');
	$('#villageIdSpan').html('');
	$('#pinCodeSpan').html('');
	
}

function init()
{
	clearAll();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	//document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	document.customerform.customerName.focus();	
	
}

function validate()
{
	
	clearAll();
	
	  if(document.customerform.customerName.value=="")
		{
		  $('#customerNameSpan').html('Please, Enter customer First name..!');
			document.customerform.customerName.focus();
			return false;
		}
		else if(document.customerform.customerName.value.match(/^[\s]+$/))
		{
			$('#customerNameSpan').html('Please, Enter customer First name..!');
			document.customerform.customerName.value="";
			document.customerform.customerName.focus();
			return false; 	
		}
	

	  
		//validation for mobileNumber mobile number------------------------------
		if(document.customerform.mobileNumber.value=="")
		{
			$('#mobileNumberSpan').html('Please, Enter Customer mobile number..!');
			document.customerform.mobileNumber.value="";
			document.customerform.mobileNumber.focus();
			return false;
		}
		else if(!document.customerform.mobileNumber.value.match(/^[0-9]{10}$/))
		{
			$('#mobileNumberSpan').html(' Enter valid Customer mobile number..!');
			document.customerform.mobileNumber.value="";
			document.customerform.mobileNumber.focus();
			return false;	
		}
	    
		  //validation for customerform email address
		if(document.customerform.emailId.value=="")
		{
			$('#emailIdSpan').html('Email Id should not be blank..!');
			document.customerform.emailId.focus();
			return false;
		}
		else if(!document.customerform.emailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			$('#emailIdSpan').html(' Enter valid email id..!');
			document.customerform.emailId.value="";
			document.customerform.emailId.focus();
			return false;
		}
		  
		//validation for customerPancardno
		if(document.customerform.customerPancardno.value=="")
		{
			$('#customerPancardnoSpan').html('Please, Enter Pancard number..!');
			document.customerform.customerPancardno.focus();
			return false;
		}
		else if(!document.customerform.customerPancardno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
		{
			$('#customerPancardnoSpan').html('PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
			document.customerform.customerPancardno.value="";
			document.customerform.customerPancardno.focus();
			return false;
		}
		
		//validation for customerAadharcardno
		if(document.customerform.customerAadharcardno.value=="")
		{
			$('#customerAadharcardnoSpan').html('Please, Enter Aadhar card number..!');
			document.customerform.customerAadharcardno.focus();
			return false;
		}
		else if(!document.customerform.customerAadharcardno.value.match(/^\d{4}\d{4}\d{4}$/))
		{
			$('#customerAadharcardnoSpan').html('Aadhar card number should be 12 digit number only..!');
			document.customerform.customerAadharcardno.value="";
			document.customerform.customerAadharcardno.focus();
			return false;
		}
		
		if(document.customerform.occupationName.value=="Default")
		{
			$('#occupationNameSpan').html('Please, select occupation Name..!');
			document.customerform.occupationName.focus();
			return false;
		}
		
		if(document.customerform.enquirySourceName.value=="Default")
		{
			$('#enquirySourceNameSpan').html('Please, select enquiry Source Name..!');
			document.customerform.enquirySourceName.focus();
			return false;
		}
		
	  
	  if(document.customerform.customerAddress.value=="")
		{
		  $('#customerAddressSpan').html('Please, Enter customer address..!');
			document.customerform.customerAddress.focus();
			return false;
		}
		else if(document.customerform.customerAddress.value.match(/^[\s]+$/))
		{
			$('#customerAddressSpan').html('Please, Enter customer address..!');
			document.customerform.customerAddress.value="";
			document.customerform.customerAddress.focus();
			return false; 	
		}
	
	  
}
</script>
</body>
</html>