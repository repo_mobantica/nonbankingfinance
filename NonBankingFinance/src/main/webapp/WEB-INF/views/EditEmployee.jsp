<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Edit Employee</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


        <link href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/mjolnic-bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Edit Employee Details</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="EmployeeMaster">Employee Master</a></li>
                                        <li class="breadcrumb-item active">Edit Employee Details</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="employeeform" action="${pageContext.request.contextPath}/EditEmployee" onSubmit="return validate()" method="post">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
	                 <div class="row">
	                 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeId">Employee Id<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="employeeId" name="employeeId" value="${employeeDetails[0].employeeId}" readonly>
                      </div>
					 </div>
				 
					 </div>
					 	 
					 <div class="row">
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeefirstName">Employee First Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="employeefirstName" name="employeefirstName" placeholder="employee first Name" value="${employeeDetails[0].employeefirstName}" style="text-transform: capitalize;">
                      </div>
                      <span id="employeefirstNameSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeemiddleName">Employee Middle Name</label>
                           	<input class="form-control" type="text" id="employeemiddleName" name="employeemiddleName" placeholder="employee middle Name" value="${employeeDetails[0].employeemiddleName}" style="text-transform: capitalize;">
                      </div>
                      <span id="employeemiddleNameSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeelastName">Employee Last Name<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="employeelastName" name="employeelastName" placeholder="employee last Name " value="${employeeDetails[0].employeelastName}" style="text-transform: capitalize;">
                      </div>
                      <span id="employeelastNameSpan" style="color:#FF0000"></span>
					 </div>
					
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeeGender">Gender<span class="text-danger">*</span></label>
                      		<br/>
			                      		
			                <c:choose>
						     <c:when test="${employeeDetails[0].employeeGender eq 'Male'}">
			                 	&nbsp &nbsp <input type="radio" name="employeeGender" id="employeeGender" value="Male" checked="checked"> &nbsp Male
			                 </c:when>
			                 <c:otherwise>  
			                 	&nbsp &nbsp <input type="radio" name="employeeGender" id="employeeGender" value="Male"> &nbsp Male
			                 </c:otherwise>
						   </c:choose>
						    		&nbsp &nbsp &nbsp 
							  		
						   <c:choose>
							  <c:when test="${employeeDetails[0].employeeGender eq 'Female'}">
			  					&nbsp &nbsp <input type="radio" name="employeeGender" id="employeeGender" value="Female" checked="checked">&nbsp Female
			  				  </c:when>
			  				  <c:otherwise>
			  				  &nbsp &nbsp <input type="radio" name="employeeGender" id="employeeGender" value="Female">&nbsp Female
			  				  </c:otherwise>
			  			   </c:choose>
			  			   
                      </div>
                      <span id="employeeGenderSpan" style="color:#FF0000"></span>
					 </div>
				
					 </div>
					 	 
					 <div class="row">
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeeMarried">Marital Status<span class="text-danger">*</span></label>
                      		<br/>
			                <c:choose>
						     <c:when test="${employeeDetails[0].employeeMarried eq 'Married'}">
			                 	&nbsp &nbsp <input type="radio" name="employeeMarried" id="employeeMarried" value="Married" checked="checked"> &nbsp Married
			                      		
			                 </c:when>
			                 <c:otherwise>  
			                 	&nbsp &nbsp <input type="radio" name="employeeMarried" id="employeeMarried" value="Married"> &nbsp Married
			                 </c:otherwise>
						   </c:choose>
						    		&nbsp &nbsp &nbsp 
							  		
						   <c:choose>
							  <c:when test="${employeeDetails[0].employeeMarried eq 'Unmarried'}">
			  					&nbsp &nbsp <input type="radio" name="employeeMarried" id="employeeMarried" value="Unmarried" checked="checked">&nbsp Unmarried
			  				  </c:when>
			  				  <c:otherwise>
			  				  &nbsp &nbsp <input type="radio" name="employeeMarried" id="employeeMarried" value="Unmarried">&nbsp Unmarried
			  				  </c:otherwise>
			  			   </c:choose>
			  			   
                      </div>
                      <span id="employeeMarriedSpan" style="color:#FF0000"></span>
					 </div>
					
					  <div class="col-xl-3">
                      <div class="form-group">
	                      <label>Date Of Birth<span class="text-danger">*</span></label>
	                      <div class="input-group">
		                      <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" name="employeeDob" value="${employeeDetails[0].employeeDob}" >
		                      <div class="input-group-append">
		                      <span class="input-group-text"><i class="icon-calender"></i></span>
		                      </div>
	                       </div><!-- input-group -->
                      </div>

                      <span id="employeeDobSpan" style="color:#FF0000"></span>
					 </div>
					
					  <div class="col-xl-3">
                      <div class="form-group">
	                      <label>Anniversary Date</label>
	                      <div class="input-group">
		                      <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="employeeAnniversaryDate" value="${employeeDetails[0].employeeAnniversaryDate}" >
		                      <div class="input-group-append">
		                      <span class="input-group-text"><i class="icon-calender"></i></span>
	                      </div>
	                      </div>
                      </div>

                      <span id="employeeAnniversaryDateSpan" style="color:#FF0000"></span>
					 </div>
					
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeeSpouseName">Employee Spouse Name</label>
                           	<input class="form-control" type="text" id="employeeSpouseName" name="employeeSpouseName" placeholder="Employee Spouse Name" value="${employeeDetails[0].employeeSpouseName}" style="text-transform:uppercase">
                      </div>
                      <span id="employeeSpouseNameSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeeEducation">Education Qualification<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="employeeEducation" name="employeeEducation" placeholder="Employee Education" value="${employeeDetails[0].employeeEducation}" style="text-transform:uppercase">
                      </div>
                      <span id="employeeEducationSpan" style="color:#FF0000"></span>
					 </div>
					
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="departmentId">Department Name<span class="text-danger">*</span></label>
			             <select class="form-control" name="departmentId" id="departmentId" onchange="getDesignationList(this.value)">
					  	   <option selected="selected" value="${employeeDetails[0].departmentId}">${departmentName}</option>
	                        <c:forEach var="departmentList" items="${departmentList}">
		                    <c:choose>
		                     <c:when test="${employeeDetails[0].departmentId ne departmentList.departmentId}">
		                 	  	<option value="${departmentList.departmentId}">${departmentList.departmentName}</option>
		                     </c:when>
		                    </c:choose>
					     </c:forEach>
	                  </select>
                      </div>
                       <span id="departmentIdSpan" style="color:#FF0000"></span>
					 </div>
					 
                      <div class="col-xl-3">
                      <div class="form-group">
                      <label for="departmentId">Designation Name<span class="text-danger">*</span></label>
                          <select class="form-control" name="designationId"  id="designationId">
						   <option selected="selected" value="${employeeDetails[0].designationId}">${designationName}</option>
		                   </select>
                      </div>
                       <span id="designationIdSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeePancardno">PAN Number<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="employeePancardno" name="employeePancardno" placeholder="PAN Number" value="${employeeDetails[0].employeePancardno}" style="text-transform:uppercase">
                      </div>
                      <span id="employeePancardnoSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeeAadharcardno">Aadhar No<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="employeeAadharcardno" name="employeeAadharcardno" placeholder="Aadhar Registration No" value="${employeeDetails[0].employeeAadharcardno}" style="text-transform: uppercase;">
                      </div>
                      <span id="employeeAadharcardnoSpan" style="color:#FF0000"></span>
					 </div>
						                 
                      <div class="col-xl-3">
                      <div class="form-group">
                      		<label for="employeeAddress">Address<span class="text-danger">*</span></label>
                           	<textarea class="form-control" id="employeeAddress" name="employeeAddress" rows="1">${employeeDetails[0].employeeAddress}</textarea>
                      </div>
                      <span id="employeeAddressSpan" style="color:#FF0000"></span>
					 </div>
					
				
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="pinCode">Pin Code<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="pinCode" name="pinCode" placeholder="Pin Code" value="${employeeDetails[0].pinCode}">
                      </div>
                      <span id="pinCodeSpan" style="color:#FF0000"></span>
					 </div>
					 
						
					  <div class="col-xl-2">
                      <div class="form-group">
	                      <label> Email ID <span class="text-danger">*</span></label>
	                      <div class="input-group">
		                      <input type="text" class="form-control" placeholder="Email Id" id="employeeEmailId" name="employeeEmailId" value="${employeeDetails[0].employeeEmailId}" onchange="CheckMailId(this.value)">
		                      <div class="input-group-append">
		                      <span class="input-group-text"><i class="ti-email"></i></span>
	                      	  </div>
	                      </div>
                      </div>

                      <span id="employeeEmailIdSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeMobileno"> Mobile No  <span class="text-danger">*</span></label>
                      		<div class="input-group">
	                           	<input type="text" id="employeeMobileno" name="employeeMobileno" placeholder="" data-mask="(99) 9999999999" class="form-control" value="${employeeDetails[0].employeeMobileno}" >
	                           	 <div class="input-group-append">
			                      <span class="input-group-text"><i class="ion-ios7-telephone"></i></span>
		                      	</div>
	                      </div>	
                      </div>
                      <span id="employeeMobilenoSpan" style="color:#FF0000"></span>
					 </div>
					 
						
					  <div class="col-xl-2">
                      <div class="form-group">
	                      <label>Company Email ID</label>
	                      <div class="input-group">
		                      <input type="text" class="form-control" placeholder="Email Id" id="companyEmailId" name="companyEmailId" value="${employeeDetails[0].companyEmailId}" >
		                      <div class="input-group-append">
		                      <span class="input-group-text"><i class="ti-email"></i></span>
	                      	  </div>
	                      </div>
                      </div>

                      <span id="companyEmailIdSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeMobileno">Company Mobile No</label>
                      		<div class="input-group">
	                           	<input type="text" id="companyMobileno" name="companyMobileno" placeholder="" data-mask="(99) 9999999999" class="form-control" value="${employeeDetails[0].companyMobileno}" >
	                           	 <div class="input-group-append">
			                      <span class="input-group-text"><i class="ion-ios7-telephone"></i></span>
		                      	</div>
	                      </div>	
                      </div>
                      <span id="companyMobilenoSpan" style="color:#FF0000"></span>
					 </div>
				
				
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeBasicpay">Basic Pay<span class="text-danger">*</span></label>
                           	<input class="form-control" type="text" id="employeeBasicpay" name="employeeBasicpay" placeholder="employeeBasicpay" style="text-transform:uppercase" value="${employeeDetails[0].employeeBasicpay}" >
                      </div>
                      <span id="employeeBasicpaySpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeHra">HRA</label>
                           	<input class="form-control" type="text" id="employeeHra" name="employeeHra" placeholder="HRA" style="text-transform:uppercase" value="${employeeDetails[0].employeeHra}" >
                      </div>
                      <span id="employeeHraSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeDa">DA</label>
                           	<input class="form-control" type="text" id="employeeDa" name="employeeDa" placeholder="DA" style="text-transform:uppercase" value="${employeeDetails[0].employeeDa}" >
                      </div>
                      <span id="employeeDaSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeCa">CA</label>
                           	<input class="form-control" type="text" id="employeeCa" name="employeeCa" placeholder="CA" style="text-transform:uppercase" value="${employeeDetails[0].employeeCa}" >
                      </div>
                      <span id="employeeCaSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeePfacno">P. F. A/C No</label>
                           	<input class="form-control" type="text" id="employeePfacno" name="employeePfacno" placeholder="PF number" style="text-transform:uppercase" value="${employeeDetails[0].employeePfacno}" >
                      </div>
                      <span id="employeePfacnoSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeEsino">ESI No</label>
                           	<input class="form-control" type="text" id="employeeEsino" name="employeeEsino" placeholder="ESI No" style="text-transform:uppercase" value="${employeeDetails[0].employeeEsino}" >
                      </div>
                      <span id="employeeEsinoSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeePLleaves">PL</label>
                           	<input class="form-control" type="text" id="employeePLleaves" name="employeePLleaves" placeholder="PL" style="text-transform:uppercase" value="${employeeDetails[0].employeePLleaves}" >
                      </div>
                      <span id="employeePLleavesSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeSLleaves">SL</label>
                           	<input class="form-control" type="text" id="employeeSLleaves" name="employeeSLleaves" placeholder="SL" style="text-transform:uppercase" value="${employeeDetails[0].employeeSLleaves}" >
                      </div>
                      <span id="employeeSLleavesSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeCLleaves">CL</label>
                           	<input class="form-control" type="text" id="employeeCLleaves" name="employeeCLleaves" placeholder="CL" style="text-transform:uppercase" value="${employeeDetails[0].employeeCLleaves}" >
                      </div>
                      <span id="employeeCLleavesSpan" style="color:#FF0000"></span>
					 </div>
					
					<!--  
					 </div>
					 	 
					 <div class="row">
					  -->
					 
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeBankName">Bank Name</label>
                           	<input class="form-control" type="text" id="employeeBankName" name="employeeBankName" placeholder="Bank Name" style="text-transform:uppercase" value="${employeeDetails[0].employeeBankName}" >
                      </div>
                      <span id="employeeBankNameSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="branchName">Bank Branch Name</label>
                           	<input class="form-control" type="text" id="branchName" name="branchName" placeholder="Bank Branch Name" style="text-transform:uppercase" value="${employeeDetails[0].branchName}" >
                      </div>
                      <span id="branchNameSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="bankifscCode">IFSC</label>
                           	<input class="form-control" type="text" id="bankifscCode" name="bankifscCode" placeholder="IFSC" style="text-transform:uppercase" value="${employeeDetails[0].bankifscCode}" >
                      </div>
                      <span id="bankifscCodeSpan" style="color:#FF0000"></span>
					 </div>
					
                      <div class="col-xl-2">
                      <div class="form-group">
                      		<label for="employeeBankacno">Bank A/C No</label>
                           	<input class="form-control" type="text" id="employeeBankacno" name="employeeBankacno" placeholder="Bank A/C No" style="text-transform:uppercase" value="${employeeDetails[0].employeeBankacno}" >
                      </div>
                      <span id="employeeBankacnoSpan" style="color:#FF0000"></span>
					 </div>
					
					 	 
                      <div class="col-xl-2">
                      <div class="form-group">
                      <label for="tahsilId">Employee Status<span class="text-danger">*</span></label>
                          <select class="form-control" name="status"  id="status">
							  
		                     <c:choose>
		                    	<c:when test="${employeeDetails[0].status eq 'Active'}">
		                    		<option selected="selected">Active</option>
		                    	</c:when>
		                    	<c:otherwise>
		                    	    <option>Active</option>
		                    	</c:otherwise>
		                     </c:choose>
		                     
		                     <c:choose>	
		                    	<c:when test="${employeeDetails[0].status eq 'Inactive'}">
		                    		<option selected="selected">Inactive</option>
		                    	</c:when>
		                    	<c:otherwise>
		                    	    <option>Inactive</option>
		                    	</c:otherwise>
		                     </c:choose>
		                     
		                  </select>
                      </div>
                       <span id="statusSpan" style="color:#FF0000"></span>
					 </div>
					 
					 
					 
					</div>
					 
                    
                  <br/>
	                 <div class="row">
					   <div class="col-xl-2">
					   </div>
                      <div class="col-xl-3">
                      	<a href="EmployeeMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button type="reset" class="btn btn-default"> Reset </button>
					 </div>
					 
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
                    </div>
                       
                 <input type="hidden" id="mailStatus" name="mailStatus">    
                 <input type="hidden" id="creationDate" name="creationDate" value="">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userId" name="userId" value="<%= session.getAttribute("employeeId") %>">	
                    
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/moment/moment.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/mjolnic-bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/clockpicker/bootstrap-clockpicker.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

        <script src="${pageContext.request.contextPath}/resources/pages/jquery.form-pickers.init.js"></script>


<script>
function CheckMailId()
{
	$('#employeeEmailIdSpan').html('');
	
	 var employeeEmailId = $('#employeeEmailId').val();
		$.ajax({

			url : '${pageContext.request.contextPath}/EmployeeCheckMailId',
			type : 'Post',
			data : { employeeEmailId : employeeEmailId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								if(result=="Present")
									{

									document.employeeform.mailStatus.value="Present";
							    	$('#employeeEmailIdSpan').html('This mail id already exit..!');
									}
								else
									{

									document.employeeform.mailStatus.value="Absent";
									}
								//alert(emialStatus);
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}
						}
			//alert("kdjkfj");
			}); 
}

function getDesignationList()
{
	
	 $("#designationId").empty();
	 var departmentId = $('#departmentId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getDesignationList',
		type : 'Post',
		data : { departmentId : departmentId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Designation-");
							$("#designationId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].designationId).text(result[i].designationName);
							    $("#designationId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}

function getpinCode()
{

	 $("#pinCode").empty();
	 var locationareaId = $('#locationareaId').val();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								 $('#pinCode').val(result[i].pinCode);
								
							 } 
						
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}


function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get locationarea List

function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get City List

function getStateList()
{
	
	 $("#stateId").empty();
	 $("#cityId").empty();
	 $("#locationareaId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getCountryWiseStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select District-");
							$("#cityId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function clearAll()
{ 
	$('#employeefirstNameSpan').html('');
	$('#employeemiddleNameSpan').html('');
	$('#employeelastNameSpan').html('');
	$('#employeeGenderSpan').html('');
	$('#employeeMarriedSpan').html('');
	$('#employeeDobSpan').html('');
	$('#employeeAnniversaryDateSpan').html('');
	$('#employeeSpouseNameSpan').html('');
	$('#employeeEducationSpan').html('');
	$('#departmentIdSpan').html('');
	$('#designationIdSpan').html('');
	
	$('#employeePancardnoSpan').html('');
	$('#employeeAadharcardnoSpan').html('');
	$('#employeeAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#cityIdSpan').html('');
	$('#locationareaIdSpan').html('');
	$('#pinCodeSpan').html('');
	$('#employeeEmailIdSpan').html('');
	$('#employeeMobilenoSpan').html('');
	$('#companyEmailIdSpan').html('');
	$('#companyMobilenoSpan').html('');

	$('#employeeBasicpaySpan').html('');
	$('#employeeHraSpan').html('');
	$('#employeeDaSpan').html('');
	$('#employeeCaSpan').html('');
	
	$('#employeePfacnoSpan').html('');
	$('#employeeEsinoSpan').html('');
	$('#employeePLleavesSpan').html('');
	$('#employeeSLleavesSpan').html('');
	$('#employeeCLleavesSpan').html('');
	
	$('#employeeBankNameSpan').html('');
	$('#branchNameSpan').html('');
	$('#bankifscCodeSpan').html('');
	$('#employeeBankacnoSpan').html('');
	 
}


function init()
{
	clearAll();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 document.employeeform.employeefirstName.focus();	
}

function validate()
{
	clearAll();
		
    //validation for employee first name
	if(document.employeeform.employeefirstName.value=="")
	{
		$('#employeefirstNameSpan').html('First name should not be blank..!');
		document.employeeform.employeefirstName.focus();
		return false;
	}
	else if(document.employeeform.employeefirstName.value.match(/^[\s]+$/))
	{
		$('#employeefirstNameSpan').html('First name should not be blank..!');
		document.employeeform.employeefirstName.value="";
		document.employeeform.employeefirstName.focus();
		return false;
	}

    //validation for employee middle name
    if(document.employeeform.employeemiddleName.value.length!=0)
    {
    	if(document.employeeform.employeemiddleName.value.match(/^[\s]+$/))
    	{
    		document.employeeform.employeemiddleName.value="";
    	}
    	else if(!document.employeeform.employeemiddleName.value.match(/^[A-Za-z\s.]+$/))
		{
    		$('#employeemiddleNameSpan').html('Middle name should contain alphabets only..!');
			document.employeeform.employeemiddleName.value="";
			document.employeeform.employeemiddleName.focus();
			return false;
		}
    }
    
    //validation for employee last name
	if(document.employeeform.employeelastName.value=="")
	{
		$('#employeelastNameSpan').html('Last name should not be blank..!');
		document.employeeform.employeelastName.focus();
		return false;
	}
	else if(document.employeeform.employeelastName.value.match(/^[\s]+$/))
	{
		$('#employeelastNameSpan').html('Last name should not be blank..!');
		document.employeeform.employeelastName.value="";
		document.employeeform.employeelastName.focus();
		return false;
	}

	//validation for gender selection
    if(( document.employeeform.employeeGender[0].checked == false ) && ( document.employeeform.employeeGender[1].checked == false ) )
	{
    	$('#employeeGenderSpan').html('Please, choose emp. Gender: Male or Female..!');
		document.employeeform.employeeGender[0].focus();
		return false;
	}
    
  //validation for gender selection
    if(( document.employeeform.employeeMarried[0].checked == false ) && ( document.employeeform.employeeMarried[1].checked == false ) )
	{
    	$('#employeeMarriedSpan').html('Please, choose emp. marital..!');
		document.employeeform.employeeMarried[0].focus();
		return false;
	} 
    
    
	 //validation employee for date of birth
	if(document.employeeform.employeeDob.value=="")
	{
		$('#employeeDobSpan').html('Please, enter employee date of birth');
		document.employeeform.employeeDob.value="";
		document.employeeform.employeeDob.focus();
		return false;
	}
	else if(document.employeeform.employeeDob.value!="")
	{
		var today = new Date();
		var dd1 = today.getDate();
		var mm1 = today.getMonth()+1;
		var yy1 = today.getFullYear();
		
    	var dob= document.employeeform.employeeDob.value;
   		var dob2=dob.split("/");
   
    	var yydiff=parseInt(yy1)-parseInt(dob2[2]);
	    if(yydiff<=18)
	    {
	    		 $('#employeeDobSpan').html(' date of birth should be 18 or 18+ years..!');
	    		document.employeeform.employeeDob.value="";
	    		document.employeeform.employeeDob.focus();
	    		return false;
	    }
	}
    
	if(document.employeeform.employeeEducation.value=="")
	{
		$('#employeeEducationSpan').html('Please, enter employee Education ..!');
		document.employeeform.employeeEducation.focus();
		return false;
	}
	else if(document.employeeform.employeeEducation.value.match(/^[\s]+$/))
	{
		$('#employeeEducationSpan').html('Please, enter employee Education  ...!');
		document.employeeform.employeeEducation.focus();
		return false;
	}
	 
	if(document.employeeform.departmentId.value=="Default")
	{
		$('#departmentIdSpan').html('Please, select employee Department..!');
		document.employeeform.departmentId.focus();
		return false;
	}
 
    //validation for employee designation
	if(document.employeeform.designationId.value=="Default")
	{
		$('#designationIdSpan').html('Please, select employee designation..!');
		document.employeeform.designationId.focus();
		return false;
	}

	//validation for employee pan card
	if(document.employeeform.employeePancardno.value=="")
	{
		$('#employeePancardnoSpan').html('Please, enter Pancard number..!');
		document.employeeform.employeePancardno.focus();
		return false;
	}
	else if(!document.employeeform.employeePancardno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
	{
		$('#employeePancardnoSpan').html('PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
		document.employeeform.employeePancardno.value="";
		document.employeeform.employeePancardno.focus();
		return false;
	}
	
	//validation for employee aadhar card number
	if(document.employeeform.employeeAadharcardno.value=="")
	{
		$('#employeeAadharcardnoSpan').html('Please, enter Aadhar card number..!');
		document.employeeform.employeeAadharcardno.focus();
		return false;
	}
	else if(!document.employeeform.employeeAadharcardno.value.match(/^\d{4}\d{4}\d{4}$/))
	{
		$('#employeeAadharcardnoSpan').html('Aadhar card number should be 12 digit number only..!');
		document.employeeform.employeeAadharcardno.value="";
		document.employeeform.employeeAadharcardno.focus();
		return false;
	}
   
    //validation for employee address----------------------
	if(document.employeeform.employeeAddress.value=="")
	{
		$('#employeeAddressSpan').html('Please, enter employee address name..!');
		document.employeeform.employeeAddress.focus();
		return false;
	}
	else if(document.employeeform.employeeAddress.value.match(/^[\s]+$/))
	{
		$('#employeeAddressSpan').html('Please, enter employee address ...!');
		document.employeeform.employeeAddress.focus();
		return false;
	}
	
    
    //validation for employee country
	if(document.employeeform.countryId.value=="Default")
	{
		$('#countryIdSpan').html('Please, select employee country name..!');
		document.employeeform.countryId.focus();
		return false;
	}
    
    //validation for employee state
	if(document.employeeform.stateId.value=="Default")
	{
		$('#stateIdSpan').html('Please, select employee state name..!');
		document.employeeform.stateId.focus();
		return false;
	}
    
    //validation for employee city
	if(document.employeeform.cityId.value=="Default")
	{
		$('#cityIdSpan').html('Please, select employee district name..!');
		document.employeeform.cityId.focus();
		return false;
	}
    
    //validation for employee location area
	if(document.employeeform.locationareaId.value=="Default")
	{
		$('#locationareaIdSpan').html('Please, select employee tahsil name..!');
		document.employeeform.locationareaId.focus();
		return false;
	}
    
    //validation for employee email address
	if(document.employeeform.employeeEmailId.value=="")
	{
		$('#employeeEmailIdSpan').html('Email Id should not be blank..!');
		document.employeeform.employeeEmailId.focus();
		return false;
	}
	else if(!document.employeeform.employeeEmailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
	{
		$('#employeeEmailIdSpan').html(' enter valid email id..!');
		document.employeeform.employeeEmailId.value="";
		document.employeeform.employeeEmailId.focus();
		return false;
	}

	if(document.employeeform.mailStatus.value=="Present")
	{
		$('#employeeEmailIdSpan').html('This mail Id Already present..!');
		document.employeeform.employeeEmailId.value="";
		document.employeeform.employeeEmailId.focus();
		return false;
	}
	
	//validation for employee mobile number------------------------------
	if(document.employeeform.employeeMobileno.value=="")
	{
		$('#employeeMobilenoSpan').html('Please, enter employee mobile number..!');
		document.employeeform.employeeMobileno.value="";
		document.employeeform.employeeMobileno.focus();
		return false;
	}
	else if(!document.employeeform.employeeMobileno.value.match(/^[(]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
	{
		$('#employeeMobilenoSpan').html(' enter valid employee mobile number..!');
		document.employeeform.employeeMobileno.value="";
		document.employeeform.employeeMobileno.focus();
		return false;	
	}
	
	//validation for employee email address
	if(document.employeeform.companyEmailId.value.length!=0)
	{
	  if(!document.employeeform.companyEmailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			$('#companyEmailIdSpan').html(' enter valid email id..!');
			document.employeeform.companyEmailId.value="";
			document.employeeform.companyEmailId.focus();
			return false;
		}
	}
	
	//validation for employee mobile number------------------------------
   if(document.employeeform.companyMobileno.value.length!=0)
	{
	 if(!document.employeeform.companyMobileno.value.match(/^[(]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
		{
			$('#companyMobilenoSpan').html(' enter valid employee mobile number..!');
			document.employeeform.companyMobileno.value="";
			document.employeeform.companyMobileno.focus();
			return false;	
		}
		}
	
	
	//validation for employee basic pay
	if(document.employeeform.employeeBasicpay.value=="")
	{
		$('#employeeBasicpaySpan').html('Please, enter basic pay amount..!');
		document.employeeform.employeeBasicpay.focus();
		return false;
	}
	else if(!document.employeeform.employeeBasicpay.value.match(/^[0-9]+$/))
	{
		$('#employeeBasicpaySpan').html('Please, enter valid Basic Pay amount..!');
		document.employeeform.employeeBasicpay.value="";
		document.employeeform.employeeBasicpay.focus();
		return false;
	}
	
	//validation for employee HRA
	if(document.employeeform.employeeHra.value.length!=0)
	{
		if(!document.employeeform.employeeHra.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.employeeform.employeeHra.value.match(/^[0-9]+$/))
				 {
					$('#employeeHraSpan').html('Please, enter valid HRA amount..!');
					document.employeeform.employeeHra.value="";
					document.employeeform.employeeHra.focus();
					return false;
				}
		}
	}
	
	//validation for employee DA
	if(document.employeeform.employeeDa.value.length!=0)
	{
		if(!document.employeeform.employeeDa.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.employeeform.employeeDa.value.match(/^[0-9]+$/))
				 {
					$('#employeeDaSpan').html('Please, enter valid DA amount..!');
					document.employeeform.employeeDa.value="";
					document.employeeform.employeeDa.focus();
					return false;
				}
		}
	}
	
	//validation for employee CA
	if(document.employeeform.employeeCa.value.length!=0)
	{

		if(!document.employeeform.employeeCa.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.employeeform.employeeCa.value.match(/^[0-9]+$/))
				 {
					$('#employeeCaSpan').html('Please, enter valid CA amount..!');
					document.employeeform.employeeCa.value="";
					document.employeeform.employeeCa.focus();
					return false;
				}
		}
	}
	
	//validation for employee PF a/c no
	if(document.employeeform.employeePfacno.value.length!=0)
	{
		if(!document.employeeform.employeePfacno.value.match(/^[A-Za-z0-9/]+$/))
		{
			$('#employeePfacnoSpan').html('Please, enter valid P.F. a/c number..!');
			document.employeeform.employeePfacno.value="";
			document.employeeform.employeePfacno.focus();
			return false;
		}
	}
	
	//validation for employee ESI no
	if(document.employeeform.employeeEsino.value.length!=0)
	{
		if(!document.employeeform.employeeEsino.value.match(/^[0-9]{2}[-]{1}[0-9]{2}[-]{1}[0-9]{6}[-]{1}[0-9]{3}[-]{1}[0-9]{4}$/))
		{
			$('#employeeEsinoSpan').html('Please, enter valid ESI number..!');
			document.employeeform.employeeEsino.value="";
			document.employeeform.employeeEsino.focus();
			return false;
		}
	}
	
	//validation for PL leaves
	if(document.employeeform.employeePLleaves.value.length!=0)
	{
		if(!document.employeeform.employeePLleaves.value.match(/^[0-9]{1,2}$/))
		{
			$('#employeePLleavesSpan').html('Please, enter valid PL leaves..!');
			document.employeeform.employeePLleaves.value="";
			document.employeeform.employeePLleaves.focus();
			return false;
		}
	}
	
	//validation for SL leaves 
	if(document.employeeform.employeeSLleaves.value.length!=0)
	{
		if(!document.employeeform.employeeSLleaves.value.match(/^[0-9]{1,2}$/))
		{
			$('#employeeSLleavesSpan').html('Please, enter valid SL leaves..!');
			document.employeeform.employeeSLleaves.value="";
			document.employeeform.employeeSLleaves.focus();
			return false;
		}
	}
	
	//validation for CL leaves
	if(document.employeeform.employeeCLleaves.value.length!=0)
	{
		if(!document.employeeform.employeeCLleaves.value.match(/^[0-9]{1,2}$/))
		{
			$('#employeeCLleavesSpan').html('Please, enter valid CL leaves..!');
			document.employeeform.employeeCLleaves.value="";
			document.employeeform.employeeCLleaves.focus();
			return false;
		}
	}
	
	 if(document.employeeform.employeeBankacno.value.length!=0)
		{ 
		 if(!document.employeeform.employeeBankacno.value.match(/^[0-9]+$/))
			{
				$('#employeeBankacnoSpan').html('Please, enter valid employee bank Ac/No..!');
				document.employeeform.employeeBankacno.value="";
				document.employeeform.employeeBankacno.focus();
				return false;
			}
		}
	 
	
}
</script>
  
  
    </body>
</html>